public class ATSCustomEmailController{
    public ID OpportunityID {get;set;}    
    private ATSTenderWrapper ATSTender;
    
    public ATSTenderWrapper getATSTender()
    {       
       if(ATSTender==null && OpportunityID!= null)
       {
           ATSTender = new ATSTenderWrapper(OpportunityID);
       }
       return ATSTender;
    
    }
    
 // Wrapper class
 
    public class ATSTenderWrapper 
    {
        public ID parentTenderID {get;set;}
        public List<ATSOpportunityWrapper> Opportunities {get; set;}
        public String Destination {get;set;}
        
        private ATS_Parent_Opportunity__c parentTender;                
        
        public ATSTenderWrapper (ID pTenderID)
        {
           parentTenderID  = pTenderID;
           parentTender = [select Name, 
                           Destination_City_Province__c, 
                           (select id, Name, RecordType.Name from Opportunities__r) 
                           from ATS_Parent_Opportunity__c 
                           where id = :pTenderID limit 1];
                           
            Destination = parentTender.Destination_City_Province__c;
            
            Opportunities = new List<ATSOpportunityWrapper>();
        
            //loop for child opportunities
            if (parentTender.Opportunities__r != null && parentTender.Opportunities__r.size() > 0)
            {
	            for(Opportunity atsOpportunity : parentTender.Opportunities__r){
	                 Opportunities.add( new ATSOpportunityWrapper(atsOpportunity.id,atsOpportunity.RecordType.Name ));
	            }     
            }
        }
    }   
    public class ATSProductWrapper   
    {
        public String Name {get;set;}
        public String Unit{get;set;}        
        public
        Decimal Qty{get;set;}                
        public ATSProductWrapper(String pName, String pUnit, Decimal pQty)
        {
            Name=pName;
            Unit=pUnit;
            Qty=pQty;
        }   
    
    }
       
    public class ATSOpportunityWrapper    
    {
        public List<ATSProductWrapper> Products {get;set;}
        public List<String> Suppliers  {get;set;}
        public String ProductCategory {get;set;}
        public String FreightComments {get; set;}
        public String OpportunityName {get; set;}
        public Date SalesContractDueDate {get; set;}
               
        public ATSOpportunityWrapper(ID pOpportunityID, String pProductCategory)
        {
        
             Products = new List<ATSProductWrapper>();
             Suppliers  = new List<String>();
             
             if(pProductCategory!= null)
             {
                 ProductCategory  =    pProductCategory.replace('ATS: ','');
             }    
             List<OpportunityLineItem> opportunityLinkeItems = [Select 
                                         PricebookEntry.Product2.id, 
                                         PricebookEntry.Product2.Name, 
                                         Unit__c,
                                         Quantity
                                         From OpportunityLineItem WHERE Opportunity.ID=:pOpportunityID ];
             if (opportunityLinkeItems != null && opportunityLinkeItems.size() > 0)
             {
	             for(OpportunityLineItem lineItem : opportunityLinkeItems ){ 
	                 Products.add( 
	                                 new ATSProductWrapper(
	                                     lineItem.PricebookEntry.Product2.Name,
	                                     lineItem.Unit__c,
	                                     lineItem.Quantity
	                             )
	                             ); 
	             }                         
             }            	 
        
             List<ATS_Freight__c> atsFreights = [select id,             
                                                 Husky_Supplier_1__c, 
                                                 Husky_Supplier_2__c,  
                                                 Competitor_Supplier_1__c,
                                                 Competitor_Supplier_2__c,
                                                 Competitor_Supplier_3__c,
                                                 Competitor_Supplier_4__c                                                 												                                             
                                                 from ATS_Freight__c 
                                                 where ATS_Freight__c  =:pOpportunityID ];
             
             List<ATS_Freight__c> atsFreightDetails = [select id,
             								Freight_Comments__c
             								from ATS_Freight__c
             								where ATS_Freight__c = :pOpportunityID];
             if (atsFreightDetails != null && atsFreightDetails.size() > 0)                        
                FreightComments = atsFreightDetails[0].Freight_Comments__c;
             
             Opportunity opp = [select id, Name, Sales_Contract_Due_Date__c
             								from Opportunity
             								where Opportunity.Id = :pOpportunityID];
             				
             if (opp != null)
             {								
			     OpportunityName = opp.Name;             								
	       		 SalesContractDueDate = opp.Sales_Contract_Due_Date__c;
             }
             if (atsFreights != null && atsFreights.size() > 0)
             {
	             for(ATS_Freight__c atsFreight : atsFreights ){           
	                
	                //lets add suppliers
	                if(atsFreight.Husky_Supplier_1__c != null)
	                {
	                    Suppliers.add(atsFreight.Husky_Supplier_1__c);
	                }
	                if(atsFreight.Husky_Supplier_2__c != null)
	                {
	                    Suppliers.add(atsFreight.Husky_Supplier_2__c );
	                }                
	                if(atsFreight.Competitor_Supplier_1__c!= null)
	                {
	                    Suppliers.add(atsFreight.Competitor_Supplier_1__c);
	                }  
	                if(atsFreight.Competitor_Supplier_2__c!= null)
	                {
	                    Suppliers.add(atsFreight.Competitor_Supplier_2__c);
	                }  
	                if(atsFreight.Competitor_Supplier_3__c!= null)
	                {
	                    Suppliers.add(atsFreight.Competitor_Supplier_3__c);
	                }  
	                if(atsFreight.Competitor_Supplier_4__c!= null)
	                {
	                    Suppliers.add(atsFreight.Competitor_Supplier_4__c);
	                }                 
	             }
             }
                                 
        
        }
    
    
    }
}