public class ATSQuoteTemplateControllerOpp {
 public ATSWrapperUtilitiesOpp.ATSQuoteWrapper quote {
  get;
  set;
 }
 public Id tenderId {
  get;
  set;
 }
 public Id accountId {
  get;
  set;
 }
 List < String > categoriesList = new List < String > ();
 Set < String > categoriesSet = new Set < String > ();
 public boolean ShowCommentAsphalt {
  get;
  set;
 } {
  ShowCommentAsphalt = false;
 }
 public boolean ShowCommentEmulsion {
  get;
  set;
 } {
  ShowCommentEmulsion = false;
 }
 public boolean ShowCommentResidual {
  get;
  set;
 } {
  ShowCommentResidual = false;
 }
 //public boolean isEmpty {get; set;}

 public ATS_Freight__c asphaltFreight {
  get;
  set;
 }
 public ATS_Freight__c emulsionFreight {
  get;
  set;
 }
 public ATS_Freight__c residualFreight {
  get;
  set;
 }

 public List < ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper > asphaltLineItems {
  get;
  set;
 }
 public List < ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper > EmulsionLineItems {
  get;
  set;
 }
 public List < ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper > ResidualsLineItems {
  get;
  set;
 }

 public List < ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper > asphaltLineItemsComment {
  get;
  set;
 }
 public List < ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper > EmulsionLineItemsComment {
  get;
  set;
 }
 public List < ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper > ResidualsLineItemsComment {
  get;
  set;
 }

 //    public String opp {get; set;}
 //    List<String> opportunityList;
 List < ATSWrapperUtilitiesOpp.ATSQuoteWrapper > quoteWrapperList {
  get;
  set;
 }
 public Date quoteDate{
  get;
  set;
 } 

 public ATSQuoteTemplateControllerOpp() {
  //System.debug('Sme v ATSQuoteTemplateControllerOpp');        
  tenderId = ApexPages.currentPage().getParameters().get('tenderId');

  asphaltLineItems = new List < ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper > ();
  emulsionLineItems = new List < ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper > ();
  residualsLineItems = new List < ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper > ();

  asphaltLineItemsComment = new List < ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper > ();
  EmulsionLineItemsComment = new List < ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper > ();
  ResidualsLineItemsComment = new List < ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper > ();

  categoriesList = ApexPages.currentPage().getParameters().get('categoriesList').split(',');

  for (string s: categoriesList) {
   categoriesSet.add(s);
  }
  System.debug('categoriesSet###' +categoriesSet);
  
  accountId = ApexPages.currentPage().getParameters().get('accountId');  
  quoteDate = Date.valueOf(ApexPages.currentPage().getParameters().get('quoteDate'));      
  System.debug('quoteDate1###' +quoteDate);
  
  //quoteDate = date.newInstance(tempDate.year(), tempDate.month(), tempDate.day());
  //System.debug('quoteDate1###' +quoteDate);
  //opportunityList =  ApexPages.currentPage().getParameters().get('opportunityList').split(',');

  //opportunityList.add(ApexPages.currentPage().getParameters().get('opportunityList'));
  //        opp += ApexPages.currentPage().getParameters().get('opportunityList');

  //System.debug('opportunityList Id:' + opp);             
  //System.debug(opportunityList.size());        

  initializeQuoteWrapper();

 }

 public void initializeQuoteWrapper() {

  quote = new ATSWrapperUtilitiesOpp.ATSQuoteWrapper();
  system.debug(logginglevel.error, 'quote' + quote);
  /*ATS_Parent_Opportunity__c tender = [SELECT Id, Name,Tender__c, Destination_City_Province__c, Price_Valid_From__c, Price_Valid_To__c, Bid_Due_Date_Time__c, Bid_Description__c, Acceptance_Deadline__c, Marketer__r.Name, Marketer__c, 
                                      Marketer__r.Id
                                      FROM ATS_Parent_Opportunity__c WHERE Id =:tenderId];
  */

  Opportunity tender = [SELECT Id, Name, Destination_City_Province__c, cpm_Freight_Credit__c, cpm_Tender__c, CPM_Autonumber__c, Asphalt_Number__c, PO__c, Sales_Comments__c, cpm_Destination_City_Province__c, cpm_Price_Valid_From__c, cpm_Price_Valid_To__c, cpm_Bid_Due_Date_Time__c, Description, cpm_Acceptance_Deadline__c, Marketer__r.Name, Marketer__c,
   Marketer__r.Id, Opportunity_ATS_Product_Category__c, Opportunity_ATS_Product_Category__r.Name FROM Opportunity WHERE Id =: tenderId
  ];
  //System.debug('Opportunity Id: ' + tender.id);
  //System.debug('Opportunity Name: ' + tender.Name);
  
  Cpm_Customer_Opportunity__c customer = [SELECT Id, Name, cpm_AccountLookup__r.BillingCity, cpm_Comments__c, cpm_AccountLookup__r.BillingPostalCode, cpm_AccountLookup__r.BillingState, cpm_AccountLookup__r.BillingStreet, cpm_AccountLookup__r.Name, cpm_OpportunityLookup__c, cpm_Contact__c, cpm_Contact__r.Name, cpm_Contact__r.Fax, cpm_Contact__r.Email
   FROM Cpm_Customer_Opportunity__c WHERE cpm_AccountLookup__c =: accountId AND cpm_OpportunityLookup__c =: tenderId
  ][0];

  //System.debug('Customer Id: ' + customer.cpm_AccountLookup__r.id);
  //System.debug('Customer Name: ' + customer.cpm_AccountLookup__r.Name);
  /*ATS_Tender_Customers__c customer = [SELECT Id, Account__c, Account__r.Name, ATS_Parent_Opportunity__c, Contact__c, Contact__r.Name, Contact__r.Fax, Comments__c, Contact__r.Email
                                            FROM ATS_Tender_Customers__c WHERE Account__c =:accountId AND ATS_Parent_Opportunity__c =:tenderId][0];
*/
  //System.Debug('**InitializeQuoteWrapper()'); 

  User salesRepresentative = [SELECT Email, Fax, Name FROM User where User.Id =: tender.Marketer__r.Id];

  //System.Debug('Marketer Id: ' + tender.Marketer__r.Id);
  //System.Debug('**Created SalesRep()');
  //-->       quote.tender = tender;
  quote.accountId = this.accountId;
  quote.accountName = customer.cpm_AccountLookup__r.Name;
  quote.accountAddressCity = customer.cpm_AccountLookup__r.BillingCity;
  quote.accountAddressPostal = customer.cpm_AccountLookup__r.BillingPostalCode;
  quote.accountAddressState = customer.cpm_AccountLookup__r.BillingState;
  quote.accountAddressStreet = customer.cpm_AccountLookup__r.BillingStreet;
  quote.opportunityMarketer = tender.Marketer__r.Name;
  quote.customerName = customer.Name;
  //System.Debug('account id + name: ' + quote.accountId + ' + ' + quote.accountName);
  quote.acceptanceDeadline = tender.cpm_Acceptance_Deadline__c;
  quote.offerEndDate = tender.cpm_Bid_Due_Date_Time__c;
  quote.autoNumber = tender.Asphalt_Number__c;
  //need to do this to include proper time zone into the output
  quote.offerEndDateString = tender.cpm_Bid_Due_Date_Time__c.format('h:mm a (z) MM/dd/yyyy ');
  quote.FobShipping = tender.cpm_Destination_City_Province__c;
  quote.contactId = customer.cpm_Contact__c;
  quote.contactName = customer.cpm_Contact__r.Name;
  quote.quoteDate = this.quoteDate;  
  System.debug('quote.quoteDate2###' +quote.quoteDate);
  quote.contactFaxNumber = customer.cpm_Contact__r.Fax;
  quote.contactEmailAddress = customer.cpm_Contact__r.Email;
  quote.oppComments = customer.cpm_Comments__c;
  quote.tenderId = this.tenderId;
  //quote.tenderNumber = tender.Tender__c;
  quote.tenderNumber = tender.Id;
  quote.tenderName = tender.Asphalt_Number__c; //tender.Name;
  quote.description = tender.Description;
  quote.freightCredit = tender.cpm_Freight_Credit__c;
  quote.PO = tender.PO__c;
  //System.debug('TenderNumber: ' + quote.tenderNumber);
  //System.debug('TenderName: ' + quote.tenderName);
  quote.priceValid = tender.cpm_Price_Valid_From__c.format() + ' - ' + tender.cpm_Price_Valid_To__c.format();
  quote.priceValidto = tender.cpm_Price_Valid_To__c.format();
  quote.priceValidFromOpp = tender.cpm_Price_Valid_From__c;
  quote.priceValidToOpp = tender.cpm_Price_Valid_To__c;
  quote.project = tender.Description;
  quote.tenderHeader = tender.cpm_Tender__c;
  quote.destination = tender.Destination_City_Province__c;
  //quote.additionalComments = tender.Bid_Description__c; //need to change this value to customer specific comments (from junction)
  quote.signatureName = tender.Marketer__r.Name;
  quote.marketerName = salesRepresentative.Name;
  quote.marketerFax = salesRepresentative.Fax;
  quote.marketerEmail = salesRepresentative.Email;

  //we suppose to have signatures of marketors as static resource with name ATSQuoteTemplateSignatures
  //in format of 'Firstname_Lastname.gif'

  String baseStaticResourceURL = GetResourceURL('ATSQuoteTemplateSignatures');
  if (baseStaticResourceURL.length() > 0 && quote.signatureName != null) {
   baseStaticResourceURL += '/' + quote.signatureName.replace(' ', '_') + '.gif';
   quote.signatureImageURL = baseStaticResourceURL;
  }

  quote.setOpportunities([SELECT Id,
   cpm_Product_Category__c,
   Product_Category_Comments__c,
   RecordType.Name,
   (SELECT Id,
    PricebookEntry.Name,
    PricebookEntry.Id,
    Price_Type__c,
    UnitPrice,
    Unit__c,
    cpm_Product_Family__c,
    Quantity,
    TotalPrice,
    PricebookEntryId,
    OpportunityId,
    ListPrice,
    Description,
    Anti_Strip__c,
    Additive__c,
    Currency__c,
    Sales_Price__c,
    Axle_5_Price__c,
    Axle_6_Price__c,
    Axle_7_Price__c,
    Axle_8_Price__c,
    Sales_Price_4_decimals__c,
    Sales_Price_Formula__c,
    cpm_Pickup_Price_Formula__c,
    Axle_5_Price_Formula__c,
    Axle_6_Price_Formula__c,
    Axle_7_Price_Formula__c,
    Axle_8_Price_Formula__c,
    Include_Sales_Price__c,
    Include_Axle_5__c,
    Include_Axle_6__c,
    Include_Axle_7__c,
    Include_Axle_8__c,
    Additional_Comments__c,
    cpm_Include_Pickup_Price__c, //<-- new 2/4
    cpm_Pickup_price__c,
    PricebookEntry.Product2.Density__c,
    PricebookEntry.Product2.Product_AKA_SMS_Name__c FROM OpportunityLineItems WHERE PricebookEntry.Product2.Family IN: categoriesSet //new set<String>(categoriesList)  
    AND(Include_Sales_Price__c = true OR Include_Axle_5__c = true OR Include_Axle_6__c = true OR Include_Axle_7__c = true OR Include_Axle_8__c = true OR cpm_Include_Pickup_Price__c = true) Order By PricebookEntry.Name),
   (Select Id,
    cpm_Product_Type__c,
    Husky_Supplier_1_Selected__c,
    Husky_Supplier_2_Selected__c,
    Husky_Supplier_3_Selected__c, //<-- new
    cpm_HuskySupplier1_ats_pricing__c, //<-- Husky_Supplier_1__c,
    cpm_HuskySupplier2_ats_pricing__c, //<-- Husky_Supplier_2__c,                                       
    cpm_HuskySupplier3_ats_pricing__c, //<-- Husky_Supplier_3__c, 
    Supplier_1_Carrier__c,
    Supplier_2_Carrier__c,
    Supplier_3_Carrier__c, // <-- new
    Supplier_1_Min_Load__c,
    Supplier_2_Min_Load__c,
    Supplier_3_Min_Load__c, //<-- new
    Supplier_1_Rate__c,
    Supplier_2_Rate__c,
    Supplier_3_Rate__c, //<-- new
    Supplier_1_Unit__c,
    Supplier_2_Unit__c,
    Supplier_3_Unit__c, //<-- new
    Emulsion_Rate5_Supplier1__c,
    Emulsion_Rate5_Supplier2__c,
    Emulsion_Rate5_Supplier3__c, //<-- new                                      
    Emulsion_Rate6_Supplier_1__c,
    Emulsion_Rate6_Supplier2__c,
    Emulsion_Rate6_Supplier3__c, //<-- new
    Emulsion_Rate7_Supplier1__c,
    Emulsion_Rate7_Supplier2__c,
    Emulsion_Rate7_Supplier3__c, //<-- new
    Emulsion_Rate8_Supplier1__c,
    Emulsion_Rate8_Supplier2__c,
    Emulsion_Rate8_Supplier3__c, //<-- new
    Emulsion_Min_Load5_Supplier1__c,
    Emulsion_Min_Load5_Supplier2__c,
    Emulsion_Min_Load5_Supplier3__c, //<-- new
    Emulsion_Min_Load6_Supplier1__c,
    Emulsion_Min_Load6_Supplier2__c,
    Emulsion_Min_Load6_Supplier3__c, //<-- new
    Emulsion_Min_Load7_Supplier1__c,
    Emulsion_Min_Load7_Supplier2__c,
    Emulsion_Min_Load7_Supplier3__c, //<-- new
    Emulsion_Min_Load8_Supplier1__c,
    Emulsion_Min_Load8_Supplier2__c,
    Emulsion_Min_Load8_Supplier3__c, //<-- new
    Emulsions_Carrier_Supplier1__c,
    Emulsions_Carrier_Supplier2__c,
    Emulsions_Carrier_Supplier3__c, //<-- new
    Prices_F_O_B__c,
    Prices_F_O_B2__c, //<-- new
    Prices_F_O_B3__c, //<-- new                                     
    Fleet_ID__c,
    Fleet_ID2__c, //<-- new
    Fleet_ID3__c, //<-- new
    Rail_Acknowledgment__c,
    Rail_Acknowledgment2__c, //<-- new
    Rail_Acknowledgment3__c, //<-- new
    RR_City__r.Name,
    RR_City2__r.Name, //<-- new
    RR_City3__r.Name, //<-- new
    RR_Address__r.Name,
    RR_Address2__r.Name, //<-- new
    RR_Address3__r.Name, //<-- new
    Invoicing_Bill_To__r.Sequence__c,
    Invoicing_Bill_To2__r.Sequence__c, //<-- new
    Invoicing_Bill_To3__r.Sequence__c, //<-- new
    Freight_to_COLLECT__c,
    Freight_to_COLLECT2__c, //<-- new
    Freight_to_COLLECT3__c, //<-- new                                     
    Rail_Car_Routing__c,
    Rail_Car_Routing2__c, //<-- new
    Rail_Car_Routing3__c, //<-- new
    Freight_to_Pay__c,
    Freight_to_Pay2__c, //<-- new
    Freight_to_Pay3__c, //<-- new
    Freight_Pay_Method__c,
    Freight_Pay_Method2__c, //<-- new
    Freight_Pay_Method3__c, //<-- new
    Emulsion_Rate7_Competitor1__c,
    Emulsion_Rate7_Competitor2__c,
    Emulsion_Rate7_Competitor3__c,
    Emulsion_Rate7_Competitor4__c,
    cpm_Supplier_1_Pickup_Rate__c,
    cpm_Supplier_2_Pickup_Rate__c,
    cpm_Supplier_3_Pickup_Rate__c FROM Freight_Ats__r)    
   //FROM Opportunity WHERE id in :opportunityList], null); 
   FROM Opportunity WHERE id =: tenderId
  ], null);
  system.debug(logginglevel.error, 'quote' + quote);
  system.debug(logginglevel.error, 'quote.getOpportunities()' + quote.getOpportunities());

  for (ATSWrapperUtilitiesOpp.ATSOpportunityWrapper opp: quote.getOpportunities()) {
   for (ATS_Freight__c freight: opp.obj.Freight_ATS__r) {
    if (freight.cpm_Product_Type__c == 'Asphalt') {
     asphaltFreight = freight;
    } else if (freight.cpm_Product_Type__c == 'Residual') {
     residualFreight = freight;
    } else if (freight.cpm_Product_Type__c == 'Emulsion') {
     emulsionFreight = freight;
    }
   }

   for (ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper product: opp.products) {
    if (product.obj.cpm_Product_Family__c == 'Asphalt') {
     asphaltLineItems.add(product);
    } else if (product.obj.cpm_Product_Family__c == 'Residual') {
     residualsLineItems.add(product);
    } else if (product.obj.cpm_Product_Family__c == 'Emulsion') {
     emulsionLineItems.add(product);
    }
   }
   for (ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper product: opp.products) {
    System.debug('Product Familly ' + product.obj.cpm_Product_Family__c);
    System.debug('Additional Comments ' + product.obj.Additional_Comments__c);
    if (product.obj.cpm_Product_Family__c == 'Asphalt' && product.obj.Additional_Comments__c != null) {
     asphaltLineItemsComment.add(product);
     ShowCommentAsphalt = true;
    } else if (product.obj.cpm_Product_Family__c == 'Residual' && product.obj.Additional_Comments__c != null) {
     residualsLineItemsComment.add(product);
     ShowCommentResidual = true;
    } else if (product.obj.cpm_Product_Family__c == 'Emulsion' && product.obj.Additional_Comments__c != null) {
     emulsionLineItemsComment.add(product);
     ShowCommentEmulsion = true;
    }
   }

  }
 }

 public Boolean getIsAsphaltLineItemsEmpty() {
  return asphaltLineItems == null || asphaltLineItems.size() == 0;
 }

 public Boolean getIsResidualsLineItemsEmpty() {
  return residualsLineItems == null || residualsLineItems.size() == 0;
 }

 public Boolean getIsEmulsionLineItemsEmpty() {
  return emulsionLineItems == null || emulsionLineItems.size() == 0;
 }

 @TestVisible private static String GetResourceURL(String resourceName) {

  List < StaticResource > resourceList = [
   SELECT Name, NamespacePrefix, SystemModStamp
   FROM StaticResource
   WHERE Name =: resourceName
  ];

  if (resourceList.size() == 1) {
   String namespace = resourceList[0].NamespacePrefix;
   return '/resource/' +
    resourceList[0].SystemModStamp.getTime() + '/' +
    (namespace != null && namespace != '' ? namespace + '__' : '') +
    resourceName;
  } else return '';

 }

}