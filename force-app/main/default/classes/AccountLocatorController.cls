public with sharing class AccountLocatorController {
    public String Name { get; set;}
    public String City { get; set;}
    public String Zip { get; set;}
    public String State { get; set;}
	public List<SelectOption> Cities{
    	get {
			if (Cities == null) {   
            	Cities  = new List<SelectOption>();
            	Cities.add(new SelectOption('','')); 
            
           		AggregateResult[] results = [
                	SELECT BillingCity,BillingState  FROM account 
                    WHERE BillingCity != null 
                    AND Billinglatitude != null 
                    AND Billinglongitude != null 
                    AND BillingStreet != null 
                    AND BillingCity != null 
                    AND BillingCountry != null
                    GROUP BY BillingCity,BillingState 
                    ORDER BY BillingCity
            	];
                for (AggregateResult ar : results) {
                    String city = (String) ar.get('BillingCity');
                    String state = (String) ar.get('BillingState');
                    if(state != null && state != ''){
                        Cities.add(new SelectOption(city , city + ' (' + state + ')' ));
                    }
                    else{
                        Cities.add(new SelectOption(city, city));
                    }
                }
           
            }
            return Cities;           
         }        
        set;
    }
    public List<SelectOption> States{
    	get {
			if (States == null) {   
            	States = new List<SelectOption>();
            	States.add(new SelectOption('','')); 
            
           		AggregateResult[] results = [
                	SELECT BillingState  FROM account 
                    WHERE BillingCity != null 
                    AND Billinglatitude != null 
                    AND Billinglongitude != null 
                    AND BillingStreet != null 
                    AND BillingCity != null 
                    AND BillingCountry != null
                    GROUP BY BillingState 
                    ORDER BY BillingState
            	];
                for (AggregateResult ar : results) {
                    String state = (String) ar.get('BillingState');
                    if(state != null && state != ''){
                        States.add(new SelectOption(state, state));
                    }
                }
           
            }
            return States;           
         }        
        set;
    }
}