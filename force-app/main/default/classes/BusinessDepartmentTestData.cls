/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the Business_Department__c
-------------------------------------------------------------------------------------------------*/
        
public class BusinessDepartmentTestData
{
    public static Business_Department__c createBusinessDepartment(String name)
    {                
        Business_Department__c results = new Business_Department__c
            (           
                Name = name
            ); 
            
        return results;
    }
}