public with sharing class CarsealAuditReport {
    public String reportRunTime {get; set;}
    public Map<String, Map<String, List<Carseal_Status__c>>> carsealReport {get; set;}
    public String selectedUnits {get;set;} 

    public CarsealAuditReport() {
        setReportRunTime();

        if (ApexPages.currentPage().getParameters().get('unit') != null)   
            selectedUnits = ApexPages.currentPage().getParameters().get('unit'); 

        carsealReport = buildReport(new List<String>());
    }

    public Map<String, Map<String, List<Carseal_Status__c>>> getCarsealReport() {
        return carsealReport;
    }

    public void runReport() {
        List<String> unitList = new List<String>();

        if (selectedUnits != null && selectedUnits != '')
        {
            unitList = selectedUnits.split(',');
        } 
        
        carsealReport = buildReport(unitList);
    } 
    
    public PageReference exportToPdf() {
        String url = '/apex/CarsealAuditReportExport?unit=' + selectedUnits;   
        PageReference detailPage = new PageReference(url);
        
        return detailPage;
    }    
    
    private void setReportRunTime() {
        reportRunTime = System.Now().format('MM/dd/yyyy HH:mm a');
    }  
    
    private Map<String, Map<String, List<Carseal_Status__c>>> buildReport(List<String> unitList) {
        Map<String, Map<String, List<Carseal_Status__c>>> carSelsByUnit = new Map<String, Map<String, List<Carseal_Status__c>>>();

        String soql = 'SELECT Id, Carseal__r.Name, Carseal__r.Status_Updated_On__c, Carseal__r.Current_Status_Text__c, Carseal__r.P_ID_Status__c, ' +
                        'Carseal__r.Reference_Equipment__c, Carseal__r.Equipment_Protected__c, Carseal__r.Line__c, Carseal__r.Size__c,  Date__c,  ' +
                        'Carseal__r.Justification__c, Carseal__r.Description__c, Carseal__r.Special_Consideration__c, Carseal__r.P_ID__c,  ' +
                        'Carseal__r.Unit__r.Name, Carseal__r.Status_Updated_By__c, Status__c ' +
                        'FROM Carseal_Status__c ' +
                        'WHERE Date__c = LAST_90_DAYS '; 

        if(!unitList.isEmpty()) {
            soql = soql + 'AND Carseal__r.Unit__r.Name IN :unitList ';
        }

        soql = soql + 'ORDER BY Carseal__r.Unit__r.Name, Carseal__r.Name ';        

        for(Carseal_Status__c carseal : (List<Carseal_Status__c>)Database.query(soql)) {

            if(carSelsByUnit.containsKey(carseal.Carseal__r.Unit__r.Name)) {
                Map<String, List<Carseal_Status__c>> carSelsById = carSelsByUnit.get(carseal.Carseal__r.Unit__r.Name);

                if(carSelsById.containsKey(carseal.Carseal__r.Name)) {
                    List<Carseal_Status__c> carSealStasus = carSelsById.get(carseal.Carseal__r.Name);
                    carSealStasus.add(carseal);
                    carSelsById.put(carseal.Carseal__r.Name, carSealStasus);

                    carSelsByUnit.put(carseal.Carseal__r.Unit__r.Name, carSelsById);

                } else {
                    List<Carseal_Status__c> carSealStasus = new List<Carseal_Status__c>();
                    carSealStasus.add(carseal);
                    carSelsById.put(carseal.Carseal__r.Name, carSealStasus);
    
                    carSelsByUnit.put(carseal.Carseal__r.Unit__r.Name, carSelsById);
                }

            } else {
                List<Carseal_Status__c> carSealStasus = new List<Carseal_Status__c>();
                carSealStasus.add(carseal);
                Map<String, List<Carseal_Status__c>> carSelsById = new Map<String, List<Carseal_Status__c>>();
                carSelsById.put(carseal.Carseal__r.Name, carSealStasus);

                carSelsByUnit.put(carseal.Carseal__r.Unit__r.Name, carSelsById);
            }
        }

        return carSelsByUnit;
    }

}