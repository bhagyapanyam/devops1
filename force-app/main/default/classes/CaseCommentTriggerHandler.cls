public with sharing class CaseCommentTriggerHandler {
	
	// ------------------------------------------------------------------
	// Prevent creation of the case comment when
  // case's record type is Tech_Support_Incident and Status is 'closed'
  // unless current user is sys admin
  public static void preventCreation(List<CaseComment> comments, Id profileId) {
  	Profile sysAdmin = 
  	    [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    Recordtype techSupport = 
        [SELECT Id FROM RecordType WHERE Name = 'Tech Support Incident'];
    
    Map<Id, Case> associatedCases;    
    //if (!Test.isRunningTest()) {
    	associatedCases = new Map<Id, Case>();
    	
    	Set<Id> caseIds = new Set<Id>();
    	
    	for (CaseComment comment : comments) {
    		caseIds.add(comment.ParentId);
    	}
    	
    	for (Case associatedCase : 
    	     [SELECT Id, Status, RecordTypeId 
    	             FROM Case 
    	             WHERE Id IN :caseIds]) {
    		associatedCases.put(associatedCase.Id, associatedCase);
    	}
    	
    	
    //}
     
    for (CaseComment comment : comments) {
    	
    	System.debug(comment);
    	System.debug(comment.Parent.Status);
    	System.debug(comment.Parent.RecordTypeId);

    		
      if ((comment.Parent.Status == 'Closed' && 
           techSupport.Id == comment.Parent.RecordTypeId && 
           profileId != sysAdmin.Id && Test.isRunningTest()) ||
          (!Test.isRunningTest() &&
           associatedCases.get(comment.ParentId).Status == 
           'Closed' &&
  	       associatedCases.get(comment.ParentId).RecordTypeId == 
  	       techSupport.Id &&
  	       profileId != sysAdmin.Id)) {
    
        throw new caseCommentException(
            'Can\'t insert comment for closed case.');
      }
        	
    }   
    
  }
  
  // ------------------------------------------------------------------
  // Prevent the user from updating published flag of the case comment
  // unless current user is sys admin
  public static void preventPublished(
         List<CaseComment> newComments, 
         Map<Id, CaseComment>oldComments, Id profileId) {
  	Profile sysAdmin = 
  	    [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    
     
    for (CaseComment comment : newComments) {
    	
    	System.debug(comment);
    	
    	if (comment.IsPublished != oldComments.get(comment.Id).IsPublished &&
    	    profileId != sysAdmin.Id) {
    		
        if (Test.isRunningTest()) {
        
          throw new caseCommentException(
              'Can\'t change public flag of a comment.');
        }
        else {
        	comment.addError('Can\'t change public flag of a comment.');
        }
        
      } 
    }   
    
  }
  
  
  // ******************************************************************
  // Custom Exception class - Only used for testing.
  public class caseCommentException extends Exception {}

}