// ====================================================================
// Handler class for all the triggers related to changes in Case object
public with sharing class CaseTriggerHandler {

  // ------------------------------------------------------------------
  // On retail location assignment, do following
  //
  // 1) Populate retail location's account to the case
  public static void onRetailLocationAssignment(
         List<Case>triggeredVal, Map<Id, Case>oldVal, Boolean isInsert) {
  	
  	List<Id>retailLocationIds = new List<Id>();
  	Map<Id, List<Case>>caseByRetailLocation = new Map<Id, List<Case>>();  	
  	
  	// Get the IDs of the retail location
  	for (Case triggered : triggeredVal) {
  		retailLocationIds.add(triggered.Retail_Location__c);
  		
  		if (!caseByRetailLocation.containsKey(triggered.Retail_Location__c)){
  			caseByRetailLocation.put(
  			    triggered.Retail_Location__c, new List<Case>());
  		}
  		
  		caseByRetailLocation.get(triggered.Retail_Location__c).add(triggered);
  	}
  	
  	User activeUser = [SELECT Id, IsPortalEnabled 
  	                          FROM User WHERE ID = :Userinfo.getUserId()];
  	                 
  	
  	System.debug(retailLocationIDs);
  	
  	// Query for retail location and populate triggered with matching value.
  	for (Retail_Location__c location : 
  	     [SELECT Account__c, Id, Primary_Case_Contact__c 
  	             FROM Retail_Location__c 
  	             WHERE Id IN :retailLocationIds]) {
  		for (Case triggered : caseByRetailLocation.get(location.Id)) {
  			triggered.AccountId = location.Account__c;
			  if (!activeUser.IsPortalEnabled && 
			      (isInsert || 
			       oldVal.get(triggered.Id).Retail_Location__c != 
			       triggered.Retail_Location__c)) {
				  triggered.ContactId = location.Primary_Case_Contact__c;
				}

  		}
  	}
  }
  
  // ------------------------------------------------------------------
  // When the user enters closed comments, it should create a case 
  // comment record based on the close comment.
  public static List<CaseComment> onClosedComment(List<Case>triggeredVals) {
  	List<CaseComment> newComment = new List<CaseComment>();
  	
  	List<Id>caseIds = new List<Id>();
  	
  	for (Case triggered : triggeredVals) {
  		caseIds.add(triggered.Id);
  	}
  	/*
  	List<CaseComment> oldComments = 
  	    [SELECT ParentId, CommentBody, isPublished
  	            FROM CaseComment 
  	            WHERE ParentId IN :caseIds];
  	*/
  	for (Case triggeredVal: triggeredVals) {
  		System.debug(triggeredVal.Id);
  		/*
  		Boolean duplicate = false;
  		
  		for (CaseComment oldComment: oldComments) {
  			if (triggeredVal.Id == oldComment.ParentId && 
  			    triggeredVal.Closed_Comments__c == oldComment.CommentBody && 
  			    oldComment.isPublished) {
  				duplicate = true;
  			}
  		}
  		*/
  		//if (!duplicate) {
  		  newComment.add(
  		      new CaseComment(ParentId = triggeredVal.Id, 
  		                      CommentBody = triggeredVal.Closed_Comments__c, 
  		                      isPublished = true)); 
  		//}
  	}
  	
  	//insert newComment;
  	return newComment;
  }
}