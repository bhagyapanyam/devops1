public class DEFT_EOJ_EditExtension {
    
    Id eojId;
    public HOG_EOJ__c record {get; set;}
    public HOG_Service_Rig_Program__c srp {get; private set;}
    
    public Profile Admin {get; private set;}
    public Profile HogAdmin {get; private set;}
    /*{public transient Attachment attachment {get;set;} 
        get{
            if(attachment == null)
            attachment = new Attachment();
            return attachment;
            }
         set;
    }*/
    
    public Boolean newEOJ {get; private set;}

    public DEFT_EOJ_EditExtension(ApexPages.StandardController stdController) {
        
        if(!Test.isRunningTest())
            stdController.addFields(new List<String> {'Rig_Company__c','Rig__c'});
        
        eojId = stdController.getId();
        record = (HOG_EOJ__c)stdController.getRecord();
        
        if(String.isBlank(eojId)){
            newEOJ = true;
            prepopulatePulledFields();
        } else {
            newEOJ = false;
        }
        
        if(record.Rig__c != null) 
            rigId = record.Rig__c;
        if(record.Rig_Company__c != null) 
            companyId = record.Rig_Company__c;
        
    }
    
    public void prepopulatePulledFields(){
        
        List<HOG_Service_Rig_Program__c> srpList = [select Well_Location__r.Id, Budget__c from HOG_Service_Rig_Program__c 
                                                    where Id =: record.Service_Rig_Program__c];
        
        List<HOG_EOJ__c> recentEOJ = new List<HOG_EOJ__c>();
        if( !srpList.isEmpty() ){
            srp = srpList.get(0);
            String wellLocationId = srpList.get(0).Well_Location__r.Id;
            system.debug('wellLocationId:'+wellLocationId);
            
            recentEOJ = [select Pump_Type2__c, Pump_Vendor2__c, Pump_Displacement_Size2__c, Pump_Lift2__c, Pump_Elastomer2__c, 
                                             Test_Efficiency2__c, Rotor_Length2__c, Rotor_Tag_Type2__c, Rotor_Condition2__c, Stator_Condition2__c,
                                             Tubing_JIH2__c, Tubing_Size2__c, Coil_Kit2__c, Scope_Joint2__c, Intake_Depth2__c, NTT_Type2__c,
                                             Drain_Type2__c, Drain_Placement2__c, Comments2__c, Rod_Type2__c, Rod_Size2__c, Pin_Size2__c, Rod_Condition2__c,
                                             Shear_Type2__c, Shear_Placement2__c, Polished_Rod_Length2__c, Length_Off_Tag2__c, Rod_Comments2__c,
                                             Rod_Grade1__c, Rod_Grade2__c
                                        from HOG_EOJ__c 
                                        where Service_Rig_Program__r.Well_Location__r.Id =: wellLocationId 
                                        order by createdDate desc limit 1]; 
        }
        
        system.debug('recentEOJ:'+recentEOJ);
        
        
        if( !recentEOJ.isEmpty() ){
            record.Pump_Type1__c                = recentEOJ.get(0).Pump_Type2__c;
            record.Pump_Vendor1__c              = recentEOJ.get(0).Pump_Vendor2__c;
            record.Pump_Displacement_Size1__c   = recentEOJ.get(0).Pump_Displacement_Size2__c;
            record.Pump_Lift1__c                = recentEOJ.get(0).Pump_Lift2__c;
            record.Pump_Elastomer1__c           = recentEOJ.get(0).Pump_Elastomer2__c;
            record.Test_Efficiency1__c          = recentEOJ.get(0).Test_Efficiency2__c;
            record.Rotor_Length1__c             = recentEOJ.get(0).Rotor_Length2__c;
            record.Rotor_Tag_Type1__c           = recentEOJ.get(0).Rotor_Tag_Type2__c;
            record.Rotor_Condition1__c          = recentEOJ.get(0).Rotor_Condition2__c;
            record.Stator_Condition1__c         = recentEOJ.get(0).Stator_Condition2__c;
            record.Tubing_JIH1__c               = recentEOJ.get(0).Tubing_JIH2__c;
            record.Tubing_Size1__c              = recentEOJ.get(0).Tubing_Size2__c;
            record.Coil_Kit1__c                 = recentEOJ.get(0).Coil_Kit2__c;
            record.Scope_Joint1__c              = recentEOJ.get(0).Scope_Joint2__c;
            record.Intake_Depth1__c             = recentEOJ.get(0).Intake_Depth2__c;
            record.NTT_Type1__c                 = recentEOJ.get(0).NTT_Type2__c;
            record.Drain_Type1__c               = recentEOJ.get(0).Drain_Type2__c;
            record.Drain_Placement1__c          = recentEOJ.get(0).Drain_Placement2__c;
            record.Comments1__c                 = recentEOJ.get(0).Comments2__c;
            record.Rod_Type1__c                 = recentEOJ.get(0).Rod_Type2__c;
            record.Rod_Grade1__c                = recentEOJ.get(0).Rod_Grade2__c;
            record.Rod_Size1__c                 = recentEOJ.get(0).Rod_Size2__c;
            record.Pin_Size1__c                 = recentEOJ.get(0).Pin_Size2__c;
            record.Rod_Condition1__c            = recentEOJ.get(0).Rod_Condition2__c;
            record.Shear_Type1__c               = recentEOJ.get(0).Shear_Type2__c;
            record.Shear_Placement1__c          = recentEOJ.get(0).Shear_Placement2__c;
            record.Polished_Rod_Length1__c      = recentEOJ.get(0).Polished_Rod_Length2__c;
            record.Length_Off_Tag1__c           = recentEOJ.get(0).Length_Off_Tag2__c;
            record.Rod_Comments1__c             = recentEOJ.get(0).Rod_Comments2__c;
        }
        
    }
    
    public PageReference isSubmitted(){
        if(!newEOJ){
            this.Admin = [Select id,name from Profile where name = 'System Administrator' limit 1];
            this.HogAdmin = [Select id,name from Profile where name = 'Standard HOG - Administrator' limit 1];
            Id userProfile = userinfo.getProfileId();
    
            HOG_EOJ__c eojRecord = [select RecordType.Name from HOG_EOJ__c where id =: eojId ];
            String recordTypeName = eojRecord.RecordType.Name; 
            
            PageReference viewPage = new PageReference('/'+ eojId );
            viewPage.setRedirect(true);
            
            if(recordTypeName.contains('Submitted') && !(userProfile == this.Admin.Id || userProfile == this.HogAdmin.Id) ) {
              return viewPage; 
            }
        }
        
        return null;
    }
    
    public String companyId {get;set;}
    public String rigId {get;set;}
    
    public List<SelectOption> getRigCompanies(){
        return DEFT_Utilities.getRigCompanies();
    }
    
    public List<SelectOption> getRigs(){
        return DEFT_Utilities.getRigs(companyId); 
    }
    
    public Boolean getPicklistDisabled(){
        if(!DEFT_Utilities.isValidValue(companyId)) return true;
        return false;
    }
    
    public void populateCompanyId(){
        record.Rig_Company__c = companyId;
    }
    
    public void populateRigId(){
        record.Rig__c = rigId;
    }
    
    public ApexPages.PageReference saveRecord() {
      
        if(!DEFT_Utilities.isValidValue(companyId) || !DEFT_Utilities.isValidValue(rigId)){   
            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Error,'Rig Name is required.');
            apexpages.addmessage(msg);
            
            return null;
        }
        
        //if(attachment.Id == null && attachment.body != null) {
        //  ApexPages.StandardController controller = new ApexPages.StandardController(record);
        //  attachment.ParentId = controller.getId();
        //  insert attachment;
        //  System.debug('insert attachment');
        //  record.Attachment__c = attachment.Id;
            //clear body of uploaded file to remove from view state limit error
            //attachment.body = null;
            //attachment = new Attachment();
        //}
        //
        /*
        if(record.Status__c == 'Submitted' && record.Post_Job_Well_Head_Photo_Included__c != true){
            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Error,
                        'Please check the \'Post Job Well Head Photo Included \' box to indicate you have added an attachment!');
            apexpages.addmessage(msg); 

            return null;
        }*/

      try {
        upsert record;
      }
      catch(Exception e) {
          apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Error,e.getMessage());
        return null;
      }
      
      ApexPages.StandardController controller = new ApexPages.StandardController(record);
      return controller.view();
    }
}