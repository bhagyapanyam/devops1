public with sharing class DOIControllerExtension extends PlantUnitEquipmentController {
    
    public Boolean isNew {get; set;} 
    public DOI__c doi {get; set;}
    public String recordTypeName {get;set;}
    public Operational_Event__c opEvent {get; set;}
    private Map<Id, Process_Area__c> areaMap;
    public Boolean locationRequired {get;set;}
    private Team_Allocation__c ta;
    public Date completedDate { get; set; }
    public Boolean completedCheckbox { get; set; }
    
    public DOIControllerExtension(ApexPages.StandardController stdController) {
        this.doi = (DOI__c) stdController.getRecord();
     //   if (ApexPages.currentPage().getParameters().get('rt') != null)
      //      doi.RecordTypeId = ApexPages.currentPage().getParameters().get('rt');
            
        this.isNew = String.isEmpty(this.doi.id);
        completedCheckbox = false;
        
        if(!this.isNew) {
            this.doi = [SELECT Id, Name, Date__c, Instructions__c, Plant__c, Unit__c, Equipment__c, Completion_Date__c,
                               Completed__c, RecordTypeId, RecordType.Name, Area__c, Owner.Name, DOI_Owner__c
                       FROM DOI__c WHERE Id =: doi.Id];
        }
        else
            setDefaultRecordType();
        
        Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.DOI__c.getRecordTypeInfosById();
        recordTypeName  = rtMap.get(doi.RecordTypeId).getName();
        
        areaMap = new Map<Id, Process_Area__c>([select Id, Name, Location_Required__c from Process_Area__c where RecordType.Name =: recordTypeName]);
            
        system.debug('DOI Record : ' + this.doi);
        
        selectedLevel1 = String.ValueOf(doi.get('Plant__c'));
        selectedLevel2 = String.ValueOf(doi.get('Unit__c'));
        selectedLevel3 = String.ValueOf(doi.get('Equipment__c'));
    }
    private void setDefaultRecordType()
    {
        Map<String, Schema.Recordtypeinfo> recTypesByName = DOI__c.SObjectType.getDescribe().getRecordTypeInfosByName();
        Profile currentProfile = [Select Name from Profile where Id =: UserInfo.getProfileId() ];
        if (doi.RecordTypeId == null)
        {
            List<PermissionSetAssignment> permissionSets = [SELECT PermissionSet.Name 
                             FROM PermissionSetAssignment 
                             WHERE AssigneeId =: UserInfo.getUserId() 
                                 and (PermissionSet.Name like 'Operations%' or PermissionSet.Name like 'Maintenance%') ];
            if (permissionSets != null)
            {
                for(PermissionSetAssignment ps : permissionSets)
                {
                    if (ps.PermissionSet.Name.contains('Operations'))
                      doi.RecordTypeId = recTypesByName.get('Operations').getRecordTypeId();
                      
                    if (ps.PermissionSet.Name.contains('Maintenance'))
                      doi.RecordTypeId = recTypesByName.get('Maintenance').getRecordTypeId();
                }             
            }
        }
        
        setDefaults();     
       
    }
    public PageReference save() {
        
        this.doi.Completed__c = completedCheckbox;
        try{
            List<Shift_Operator__c> ops = [SELECT Team__c FROM Shift_Operator__c WHERE Operator_Contact__c =: doi.DOI_Owner__c and Team__c != null LIMIT 1];
            doi.Team_Allocation__c = ops.get(0).id;        
        } catch(Exception exp) {
            doi.Team_Allocation__c = null;
        }            
        
        if(selectedLevel1 != selectedNone) {
            doi.Plant__c = selectedLevel1;
        }
        else {
            doi.Plant__c = Null;
        }
        
        if(selectedLevel2 != selectedNone) { 
            doi.Unit__c = selectedLevel2;
            
            if(selectedLevel3 != selectedNone) {
                doi.Equipment__c = selectedLevel3;
            }
            else {
                doi.Equipment__c = null;
            }
        }
        else {
            doi.Unit__c = null;
            doi.Equipment__c = null;        
        }
        
        return new ApexPages.StandardController(this.doi).save();
    }
    
    public PageReference saveAndNew() {     
        doi.Plant__c = selectedLevel1;
        this.doi.Completed__c = completedCheckbox;
        
        try{
            List<Shift_Operator__c> ops = [SELECT Team__c FROM Shift_Operator__c WHERE Operator_Contact__c =: doi.DOI_Owner__c LIMIT 1];
            doi.Team_Allocation__c = ops.get(0).id;        
        } catch(Exception exp) {
            doi.Team_Allocation__c = null;
        }              
        
        if(selectedLevel2 != selectedNone) {
            doi.Unit__c = selectedLevel2;
            
            if(selectedLevel3 != selectedNone) {
                doi.Equipment__c = selectedLevel3;
            }
            else {
                doi.Equipment__c = null;
            }
        }
        else {
            doi.Unit__c = null;
            doi.Equipment__c = null;
        }
        
        // Save the doi to the database        
        new ApexPages.StandardController(this.doi).save();

        doi = doi.clone(false);
        return null;
    }
    
    public List<selectOption> processAreaList
    {
        get
        {
            List<selectOption> options = new List<selectOption>();
            if (areaMap != null)
            {
                List<Process_Area__c> areaList = areaMap.values();
                areaList.sort();
                 
                for(Process_Area__c a : areaList)
                {
                    options.add(new SelectOption(a.Id, a.Name));
                }
            }
            return options;
        }  
    }    
    
    public void ProcessAreaIsChange()
    {
        System.debug('select area = ' + opEvent.Area__c);
        locationRequired = areaMap.get(opEvent.Area__c).Location_Required__c;
    }
   
    public String getTeamAllocationName() {
        if (this.ta != null) {
            return ta.Team_Allocation_Name__c;
        }
        
        return '';
    }    
    
    private void setDefaults() {
        List<Contact> operator = [SELECT Id, FirstName, LastName, Email FROM Contact WHERE FirstName =:UserInfo.getFirstName() 
            AND LastName =:UserInfo.getLastName() AND Email =:UserInfo.getUserEmail()];   
        
        if(operator != Null && operator.size() > 0) {    
            this.doi.DOI_Owner__c = operator[0].Id;             
        }
    }
    
    public PageReference setCompletionDate()
    {
        if(completedCheckbox)
            this.doi.Completion_Date__c = system.today();
        else
            this.doi.Completion_Date__c = null;                
    
        return null;
    }  
       
}