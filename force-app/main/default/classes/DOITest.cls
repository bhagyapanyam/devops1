/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class DOITest {

    static testMethod void testEditDOI() {
        Date testDate = Date.today();
        String formattedTestDate = DateUtilities.toDateTime(testDate).format(DateUtilities.REQUEST_DATE_FORMAT);
        User u = TestUtilities.createOperationsUser();
        System.runAs(u)
        {
        	Business_Unit__c businessUnit = TestUtilities.createBusinessUnit('Sunrise Field');
	        insert businessUnit;
	        
	        Business_Department__c businessDepartment = new Business_Department__c(Name = 'Test Business Department');            
	        insert businessDepartment;
	
	        Operating_District__c operatingDistrict = TestUtilities.createOperatingDistrict(businessUnit.Id, businessDepartment.Id, 'Sunrise');
	        insert operatingDistrict;       
	        
	        List<Plant__c> plantList = PlantTestData.createPlants(operatingDistrict.Id, 3); 
	        
	        List<Unit__c> unitList = UnitTestData.createUnits(plantList, 2);
	        
	        List<Equipment__c> equipmentList = EquipmentTestdata.createEquipment(unitList, 3);
	        equipmentList[0].Description_of_Equipment__c = 'This is more than 50 characters in description. So it will be cut off.';
	        equipmentList[1].Description_of_Equipment__c = 'This is less than 50 characters.';
	        update equipmentList;
	        
	        System.debug('Running DOI Controller Extension test: testDOI()...');
	        try {
	            ApexPages.Standardcontroller sc = New ApexPages.StandardController(new DOI__c());
	            PageReference doiEditPage = New PageReference('Page.DOIEdit');
	            Test.SetCurrentPageReference(doiEditPage);
	            DOIControllerExtension doiControllerExt = new DOIControllerExtension(sc);
	            
	            if (doiControllerExt != null) { 
	                System.assertNotEquals(doiControllerExt.level1Items.size(), 0);
	                System.assertEquals(doiControllerExt.level2Items.size(), 0);
	                System.assertEquals(doiControllerExt.level3Items.size(), 0);
	                System.assertNotEquals(doiControllerExt.equipmentSearchList.size(), 0);
	                
	                //System.assertEquals(plantList[0].Id + '', 'Test');
	                doiControllerExt.selectedLevel1 = plantList[0].Id;
	                doiControllerExt.resetUnitAndEquipment();
	                System.assertNotEquals(doiControllerExt.level2Items.size(), 0);
	                System.assertEquals(doiControllerExt.level3Items.size(), 0);
	                
	                //System.assertEquals(unitList[0].Id + '', 'Test');
	                doiControllerExt.selectedLevel2 = unitList[0].Id;
	                System.assertNotEquals(doiControllerExt.level3Items.size(), 0);
	                //System.assertEquals(evControllerExt.level3Items[0] + '', 'Test');
	                
	                doiControllerExt.searchByTagID = 'Equipment';
	                System.assertNotEquals(doiControllerExt.level3Items.size(), 0);
	                 
	                doiControllerExt.selectedLevel1 = plantList[1].Id;
	                doiControllerExt.resetUnitAndEquipment();
	                System.assertNotEquals(doiControllerExt.level2Items.size(), 0);
	                System.assertEquals(doiControllerExt.level3Items.size(), 0);
	                System.assertNotEquals(doiControllerExt.equipmentSearchList.size(), 0);
	                
	                doiControllerExt.selectedEquipmentBySearch = equipmentList[3].Id;
	                doiControllerExt.populatePlantUnitEquipment();
	                doiControllerExt.save();
	                doiControllerExt.saveAndNew();
	                
	               
	            }
	            Map<String, Schema.Recordtypeinfo> doiRTMap = DOI__c.SObjectType.getDescribe().getRecordTypeInfosByName();
	            DOI__c newDOI = TestUtilities.createDOI(plantList[0].Id, doiRTMap.get('Operations').getRecordTypeId());
	            insert newDOI;
	            sc = New ApexPages.StandardController(newDOI);
	            doiEditPage.getParameters().put('id', newDOI.id);
	            Test.SetCurrentPageReference(doiEditPage);
	            DOIControllerExtension editDoiControllerExt = new DOIControllerExtension(sc);
	             editDoiControllerExt.selectedLevel2 = '*';
                editDoiControllerExt.selectedLevel3 = '*';
	            editDoiControllerExt.save();
	            editDoiControllerExt.saveAndNew();
	            
                
	            System.assert((doiControllerExt != null));
	        } catch(Exception ex) {
	            System.assert(false, 'Error creating new DOIControllerExtension instance. ' + ex.getMessage());
	        }   
        }
    }
    
}