global virtual with sharing class DSP_CMS_CardTitleLinkController extends cms.ContentTemplateController{
    global DSP_CMS_CardTitleLinkController(cms.CreateContentController cc){
        super(cc);
    }
    
    global DSP_CMS_CardTitleLinkController(cms.GenerateContent cc){
        super(cc);
    }

    global DSP_CMS_CardTitleLinkController(){}

    global virtual override String getHTML(){return '';}

    public String website {
        get{
            return getProperty('website');
        }
        set;
    }
    
    public String websiteLabel {
        get{
            return String.isNotBlank(getProperty('websiteLabel')) ? getProperty('websiteLabel') : '' ;
        }
        set;
    }
    
    public cms.Link parsedWebsite{
        get{
            return new cms.Link(this.website, this.page_mode, this.site_name);
        }
    }

    public String linkName{
        get{
            cms.Link lk = new cms.Link(this.website, this.page_mode, this.site_name);
            return lk.getLinkName();
        }
    }

    public String linkHTML(){
        String html='';
        
        //If the user needs a URL for the title, the link must be defined
        if(linkName != null && linkName != '' ){
            html+='<a href="';
            if(this.parsedWebsite.targetPage != null)
                html+=parsedWebsite.targetPage;
            html+='"';
            if(this.parsedWebsite.target != null && this.parsedWebsite.target != '')
                html += 'target="' + this.parsedWebsite.target + '"';
            if(this.parsedWebsite.javascript != null && this.parsedWebsite.javascript != '')
                html += ' onlick=\''+this.parsedWebsite.javascript+'\'';
            html+='>';
            
            //If the user needs a URL as title, the label must be blank
            if(websiteLabel != null && websiteLabel != '' ){
                html += websiteLabel + '</a>';
            }else{
                html += linkName + '</a>';
            }
            
        //If only need a title     
        }else{
            html += websiteLabel;
        }
        
        return html;
    }
}