global virtual with sharing class DSP_CMS_FAQController extends cms.ContentTemplateController {
    global DSP_CMS_FAQController(cms.CreateContentController cc) {
        super(cc);
    }
    
    global DSP_CMS_FAQController(cms.GenerateContent cc) {
        super(cc);
    }
    
    global DSP_CMS_FAQController(){}
    
    global virtual override String getHTML(){return '';}
    
    public String question {
        get{
            return String.isNotBlank(getProperty('question')) ?  getProperty('question') : '';
        }
        set;
    }
    
    public String answer {
        get{
            return String.isNotBlank(getProperty('answer')) ?  getProperty('answer') : '';
        }
        set;
    }
    
    public String FAQHTML() {
        String html='';
        
        html += '<li>';
        html += '<div class="collapsible-header">Q: '+question+'</div>';
        html += '<div style="display: none;" class="collapsible-body"><p>A: '+answer+'</p></div>';
        html += '</li>';
        
        return html;
    }
}