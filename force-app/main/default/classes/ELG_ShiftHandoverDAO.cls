/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Database access interface for Shift Handover part of E-Log Application

History:        mbrimus 2019-07-11 - Created.
*************************************************************************************************/
public interface ELG_ShiftHandoverDAO {

    /**
     * Returns settings for currently logged in user
     * @return  List of Settings, should be just one
     */
    List<ELG_User_Setting__c> getUsersSettings();

    /**
     * Returns all available posts for AMU and with each post also retrieves LATEST shift
     *
     * For checking if post has a shift and also what status this shift has
     * @param amuId
     * @return  List of Posts and latest Shift for each
     */
    List<ELG_Post__c> getAvailablePostsForAMU(String amuId);

    /**
     * Returns shift assignment of current user
     * @return  List of Shift Assignements (limited to one) where user is Primary and status of shift is new
     * or handover in progress
     */
    List<ELG_Shift_Assignement__c> getCurrentShiftAssignmentForUser(String amuId);

    /**
     *  Return all logs for in current shift for post and facility
     *
     * @param shiftId
     * @param facilityId
     * @return  List of logs (Generic Record Type) on Facility and Post for Active Shift
     */
    List<ELG_Log_Entry__c> getAllLogsForActiveShiftAndPost(String postId, String facilityId);

    /**
     * Returns handover document for Shift (will only be one)
     *
     * @param shiftId
     *
     * @return
     */
    ELG_Shift_Handover__c getHandoverByShift(String shiftId);


    /**
     * Returns Handover Documents that are in handover in progress status for Take Post functionality
     * is Handover in progress
     *
     * @return
     */
    List<ELG_Shift_Handover__c> getHandoverForTakePost(String amu);


    /**
     * Ordered List of all question categories on handover document
     *
     * @return
     */
    List<ELG_Handover_Category__c> getAllQuestionCategories();

    /**
     * Ordered List of all Active questions
     *
     * @return
     */
    List<ELG_Handover_Category_Question__c> getAllActiveQuestions();


    /**
     * List of Shifts (limited to 1) where Post is Steam and requires Shift Engineer
     *
     * @param amuId
     *
     * @return
     */
    List<ELG_Shift_Assignement__c> getSteamShiftWithMissingShiftEngineer(String amuId);
}