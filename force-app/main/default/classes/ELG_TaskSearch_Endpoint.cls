/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 10/28/2019   
 */

public class ELG_TaskSearch_Endpoint {

	//TODO Retrieve tabs, flexipage, app
	//TODO change task status from In Progress to active
	//TODO Correctable get rid of editor
	//TODO Change app page of search handover form to ltng page that has custom header to match the Tasks

//All AMUs
	@AuraEnabled
	public static ELG_TaskSearch_EndpointService.DataPayload getThermalAMUs(String userId) {
		return ELG_TaskSearch_EndpointService.getAllThermalAMUs(userId);
	}

	//All Posts based on selected AMU
	@AuraEnabled
	public static List<ELG_TaskSearch_EndpointService.TemporaryClassToBuildObject> getAMUsPosts(String currentAMU) {
		return ELG_TaskSearch_EndpointService.getAvailablePosts(currentAMU);
	}

	//
	@AuraEnabled
	public static List<ELG_TaskSearch_EndpointService.DataTableWrapper> getSearchResults(
			Object filterSelection,
			Integer offsetSize,
			String fieldSort,
			String sortDirection,
			Boolean defaultView,
			Boolean completedView,
			Boolean cancelledView) {
		return ELG_TaskSearch_EndpointService.searchResults(
				filterSelection,
				offsetSize,
				fieldSort,
				sortDirection,
				defaultView,
				completedView,
				cancelledView);
	}


}