/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_BlockInformationService
History:        jschn 2019-07-10 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_BlockInformationServiceTest {

    @IsTest
    static void buildEngineBlocksForEPD_withoutParams() {
        List<EPD_FormStructureBlock> result;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        Test.startTest();
        try {
            result = new EPD_BlockInformationService().buildEngineBlocksForEPD(null, null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void buildEngineBlocksForEPD_withBlockInfoParam() {
        List<EPD_FormStructureBlock> result;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        Test.startTest();
        try {
            result = new EPD_BlockInformationService().buildEngineBlocksForEPD(
                    new List<EPD_Block_Information__c>(),
                    null
            );
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void buildEngineBlocksForEPD_withEngineConfigParam() {
        List<EPD_FormStructureBlock> result;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        Test.startTest();
        try {
            result = new EPD_BlockInformationService().buildEngineBlocksForEPD(
                    null,
                    new EPD_Engine__mdt()
            );
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void buildEngineBlocksForEPD_withEmptyParam() {
        List<EPD_FormStructureBlock> result;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        Test.startTest();
        try {
            result = new EPD_BlockInformationService().buildEngineBlocksForEPD(
                    new List<EPD_Block_Information__c>(),
                    new EPD_Engine__mdt()
            );
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void buildEngineBlocksForEPD_withTooMany() {
        List<EPD_FormStructureBlock> result;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        Test.startTest();
        try {
            result = new EPD_BlockInformationService().buildEngineBlocksForEPD(
                    new List<EPD_Block_Information__c>{
                            (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                                    new EPD_Block_Information__c(),
                                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                                    false
                            ),
                            (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                                    new EPD_Block_Information__c(),
                                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                                    false
                            ),
                            (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                                    new EPD_Block_Information__c(),
                                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                                    false
                            )
                    },
                    new EPD_Engine__mdt()
            );
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void buildEngineBlocksForEPD_withProperParam1() {
        List<EPD_FormStructureBlock> result;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Integer expectedNumOfRecords = 1;

        Test.startTest();
        try {
            result = new EPD_BlockInformationService().buildEngineBlocksForEPD(
                    new List<EPD_Block_Information__c>{
                            (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                                    new EPD_Block_Information__c(),
                                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                                    false
                            )
                    },
                    new EPD_Engine__mdt()
            );
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedNumOfRecords, result.size());
    }

    @IsTest
    static void buildEngineBlocksForEPD_withProperParam2() {
        List<EPD_FormStructureBlock> result;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Integer expectedNumOfRecords = 2;

        Test.startTest();
        try {
            result = new EPD_BlockInformationService().buildEngineBlocksForEPD(
                    new List<EPD_Block_Information__c>{
                            (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                                    new EPD_Block_Information__c(),
                                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                                    false
                            ),
                            (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                                    new EPD_Block_Information__c(),
                                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                                    false
                            )
                    },
                    new EPD_Engine__mdt()
            );
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedNumOfRecords, result.size());
    }

    @IsTest
    static void getBlockInformationByEPDId_withoutParam() {
        Map<Id, List<EPD_Block_Information__c>> result;
        Integer expectedMapSize = 0;

        Test.startTest();
        result = new EPD_BlockInformationService().getBlockInformationByEPDId(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedMapSize, result.size());
    }

    @IsTest
    static void getBlockInformationByEPDId_withEmptyParam() {
        Map<Id, List<EPD_Block_Information__c>> result;
        Integer expectedMapSize = 0;

        Test.startTest();
        result = new EPD_BlockInformationService().getBlockInformationByEPDId(new Set<Id>());
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedMapSize, result.size());
    }

    @IsTest
    static void getBlockInformationByEPDId_withWrongParam() {
        Map<Id, List<EPD_Block_Information__c>> result;
        Integer expectedMapSize = 0;

        Test.startTest();
        result = new EPD_BlockInformationService().getBlockInformationByEPDId(new Set<Id>{UserInfo.getUserId()});
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedMapSize, result.size());
    }

    @IsTest//Mocked
    static void getBlockInformationByEPDId_withProperParam() {
        Map<Id, List<EPD_Block_Information__c>> result;
        Integer expectedMapSize = 1;

        Test.startTest();
        EPD_DAOProvider.blockInformationDAO = new BlockInfoTestSelector();
        result = new EPD_BlockInformationService().getBlockInformationByEPDId(new Set<Id>{UserInfo.getUserId()});
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedMapSize, result.size());
    }

    public class BlockInfoTestSelector implements EPD_BlockInformationDAO {
        public List<EPD_Block_Information__c> getBlockInformationByEPDs(Set<Id> epdIds) {
            return new List<EPD_Block_Information__c> {
                    (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(new EPD_Block_Information__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false)
            };
        }
    }

}