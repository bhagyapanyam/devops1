/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_COCTrigger
History:        jschn 2019-08-07 - Created.
*************************************************************************************************/
@IsTest
private class EPD_COCTriggerTest {

    @IsTest
    static void delete_allowed() {
        EPD_Compressor_Operating_Condition_Stage__c COCStage = EPD_TestData.createCOCStage();
        EPD_Constants.SETTINGS.Restrict_Record_Deletion__c = false;
        Boolean expectedFailFlag = EPD_Constants.SETTINGS.Restrict_Record_Deletion__c;
        Boolean failFlag = false;

        Test.startTest();
        try {
            delete COCStage;
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void delete_restricted() {
        EPD_Compressor_Operating_Condition_Stage__c COCStage = EPD_TestData.createCOCStage();
        EPD_Constants.SETTINGS.Restrict_Record_Deletion__c = true;
        Boolean expectedFailFlag = EPD_Constants.SETTINGS.Restrict_Record_Deletion__c;
        Boolean failFlag = false;

        Test.startTest();
        try {
            delete COCStage;
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

}