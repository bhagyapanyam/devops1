/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Class that holds all constant values that EPD logic needs or uses.
Test Class:     all EPD tests
History:        jschn 2019-05-24 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_Constants {

    private static final String CLASS_NAME = String.valueOf(EPD_Constants.class);

    public static final String FILE_EXTENSION = '.pdf';
    public static final String FILE_NAME_BASE = 'EPD - {0} - {1}{2}';

    public static final String ENGINE_CONFIG_STRAIGHT = 'Inline Engine';
    public static final String ENGINE_CONFIG_V_SHAPED = 'V-Engine';

    public static final String BLOCK_TYPE_STRAIGHT = 'Inline Engine';
    public static final String BLOCK_TYPE_V_SHAPED_LEFT = 'V-Engine LB';
    public static final String BLOCK_TYPE_V_SHAPED_RIGHT = 'V-Engine RB';

    public static final String EOC_ADVANCED_RECORD_TYPE = 'Advanced';
    public static final String EOC_SIMPLE_RECORD_TYPE = 'Simple';
    public static final Map<Boolean, String> ADVANCED_EOC_FLAG_TO_RECORD_TYPE = new Map<Boolean, String> {
            true => EOC_ADVANCED_RECORD_TYPE,
            false => EOC_SIMPLE_RECORD_TYPE
    };

    public static final String EOC_BLOCK_NAME_EMPTY = '';
    public static final String EOC_BLOCK_NAME_LEFT = 'Left Bank';
    public static final String EOC_BLOCK_NAME_RIGHT = 'Right Bank';
    public static final Map<String, String> EOC_BLOCK_NAME_TO_INPUT_LABEL_MAP = new Map<String, String> {
            EOC_BLOCK_NAME_EMPTY => '',
            EOC_BLOCK_NAME_LEFT => '(LB)',
            EOC_BLOCK_NAME_RIGHT => '(RB)'
    };

    public static final HOG_Engine_Performance_Data__c SETTINGS = HOG_Engine_Performance_Data__c.getInstance();

    public static final Set<String> SUPPORTED_EQUIPMENT_OBJECT_TYPE = new Set<String> {
            'ENGINES'
    };

//  OTHER MAPPINGS
    public static final Map<EPD_SaveMode, Boolean> SAVE_MODE_TO_BOOLEAN_MAP = new Map<EPD_SaveMode, Boolean> {
            EPD_SaveMode.SAVE => false,
            EPD_SaveMode.SUBMIT => true
    };

    public static final Map<String, String> MEASUREMENT_TYPE_TO_CYLINDER_RECORD_TYPE_NAME = new Map<String, String> {
            'Compression Only' => 'Compression Measurement',
            'Wear' => 'Wear Measurement'
    };

    public static final Set<String> SUPPORTED_WO_ORDER_TYPES = new Set<String> {
            'MPX1'
    };

//    LABEL MAPS
    public static final Map<String, String> BLOCK_TYPE_TO_IMP_INPUT_LABEL_MAP = new Map<String, String> {
            BLOCK_TYPE_STRAIGHT => 'Intake Manifold Pressure ("Hg)',
            BLOCK_TYPE_V_SHAPED_LEFT => 'Intake Manifold Pressure (LB) ("Hg)',
            BLOCK_TYPE_V_SHAPED_RIGHT => 'Intake Manifold Pressure (RB) ("Hg)'
    };

    public static final Map<String, String> BLOCK_TYPE_TO_EO2_INPUT_LABEL_MAP = new Map<String, String> {
            BLOCK_TYPE_STRAIGHT => 'Exhaust O2 (%)',
            BLOCK_TYPE_V_SHAPED_LEFT => 'Exhaust O2 (LB) (%)',
            BLOCK_TYPE_V_SHAPED_RIGHT => 'Exhaust O2 (RB) (%)'
    };

    public static final Map<String, String> BLOCK_TYPE_TO_BLOCK_NAME_MAP = new Map<String, String> {
            BLOCK_TYPE_STRAIGHT => '',
            BLOCK_TYPE_V_SHAPED_LEFT => 'Left Engine Bank',
            BLOCK_TYPE_V_SHAPED_RIGHT => 'Right Engine Bank'
    };

//    ERROR MESSAGE MAPS
    public static final Map<EPD_LoadMode, String> UNEXPECTED_NUM_OF_RECS_BY_LOAD_MODE = new Map<EPD_LoadMode, String> {
            EPD_LoadMode.DIRECT_LOAD => Label.EPD_Unexpected_EPD_Record_Count,
            EPD_LoadMode.INDIRECT_LOAD => Label.EPD_Unexpected_EPD_on_WOA_Record_Count
    };

    public static final Map<EPD_LoadMode, String> MISSING_PARAM_BY_LOAD_MODE = new Map<EPD_LoadMode, String> {
            EPD_LoadMode.DIRECT_LOAD => String.format(
                    Label.EPD_Missing_Param,
                    new List<String> {'Engine Performance Data Id'}
            ),
            EPD_LoadMode.INDIRECT_LOAD => String.format(
                    Label.EPD_Missing_Param,
                    new List<String> {'Work Order Activity Id'}
            )
    };
    //TEMP ERROR MESSAGE (VF COMPONENT)
    public static String THRESHOLD_WARNING_BASE = 'Warning: Value is outside of thresholds: {0}{1}{2}{3}.';
    public static String THRESHOLD_EMAIL_WARNING_BASE_SIMPLE = '{0} - Current Value {1}</br>';
    public static String THRESHOLD_EMAIL_WARNING_BASE = '{0}: {1} - Current Value {2}</br>';
    public static String RETROSPECTIVE_THRESHOLD_WARNING_BASE = '{0}: {1} - Previous value {2}, current value {3}</br>';

    //DELETE PREVENTION ERROR MESSAGE
    public static final String DELETE_RESTRICTION_MESSAGE = 'Deletion of Engine Performance Data records is restricted';

//    EMAIL RELATED STUFF
    public static Id SENDER_ID {
        get {
            if(SENDER_ID == null) {
                OrgWideEmailAddress sender;
                try {
                    sender = [
                            SELECT Id, DisplayName, Address
                            FROM OrgWideEmailAddress
                            WHERE DisplayName = : SETTINGS.PDF_Senders_Name__c
                            LIMIT 1
                    ];
                } catch (Exception ex) {
                    System.debug(CLASS_NAME + ' -> SENDER_ID. Exception: ' + ex.getMessage());
                }
                if(sender != null) {
                    SENDER_ID = sender.Id;
                }
            }
            System.debug(CLASS_NAME + ' -> SENDER_ID: ' + SENDER_ID);
            return SENDER_ID;
        }
        private set;
    }

    public static final String TEMPLATE_NAME_ON_SUBMIT_PDF = 'HOG_EPD_On_Submit_To_Vendor';
    public static final String TEMPLATE_NAME_THRESHOLD_ALERT = 'HOG_EPD_Threshold_Alert';
    public static final String THRESHOLD_ALERT_LIST_PLACEHOLDER = '{exceeded_values_list}';
    public static final List<String> THRESHOLD_ALERT_TARGET_ROLE_TYPE = new List<String> {
            'Tradesman Lead'
    };

}