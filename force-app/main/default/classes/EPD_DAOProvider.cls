/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    DAO Provider. Holds all DAO Implementation providing required functionality for EPD
                service. Later it can be stripped into several standalone classes.
                This is just first version that allows mocking Database outputs.
History:        jschn 2019-07-02 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_DAOProvider {

    public static EPD_BlockInformationDAO blockInformationDAO = new EPD_BlockInformationSelector();
    public static EPD_EngineConfigDAO engineConfigDAO = new EPD_EngineConfigSelector();
    public static EPD_EPDDAO EPDDAO = new EPD_EPDSelector();
    public static EquipmentEngineDAO equipmentEngineDAO = new EquipmentEngineSelector();
    public static VTT_WorkOrderActivityDAO workOrderActivityDAO = new VTT_WorkOrderActivitySelector();
    public static EquipmentDAO equipmentDAO = new EquipmentSelector();
    public static HOG_PlannerGroupNotificationTypeDAO plannerGroupNotificationTypeDAO = new HOG_PlannerGroupNotificationTypeSelector();

}