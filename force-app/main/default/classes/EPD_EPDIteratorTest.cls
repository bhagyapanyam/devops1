/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_EPDIterator
History:        jschn 2019-07-24 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_EPDIteratorTest {

    @IsTest
    static void constructor_withoutParam() {
        Boolean expectedFailFLag = true;
        Boolean failFlag = false;
        EPD_EPDIterator iterator;

        try {
            iterator = new EPD_EPDIterator(null);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFLag, failFlag);
        System.assertEquals(null, iterator);
    }

    @IsTest
    static void constructor_withEmptyParam() {
        Boolean expectedFailFLag = false;
        Boolean failFlag = false;
        EPD_EPDIterator iterator;

        try {
            iterator = new EPD_EPDIterator(new List<EPD_Engine_Performance_Data__c>());
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFLag, failFlag);
        System.assertNotEquals(null, iterator);
    }

    @IsTest
    static void constructor_withParam() {
        Boolean expectedFailFLag = false;
        Boolean failFlag = false;
        EPD_EPDIterator iterator;

        try {
            iterator = new EPD_EPDIterator(
                    new List<EPD_Engine_Performance_Data__c>{
                            (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                                    new EPD_Engine_Performance_Data__c(),
                                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                                    false
                            )
                    }
            );
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFLag, failFlag);
        System.assertNotEquals(null, iterator);
    }

    @IsTest
    static void hasNext_false() {
        Boolean expectedFailFLag = false;
        Boolean failFlag = false;
        EPD_EPDIterator iterator;
        Boolean hasNext;
        Boolean expectedHasNext = false;

        try {
            iterator = new EPD_EPDIterator(
                    new List<EPD_Engine_Performance_Data__c>()
            );
            hasNext = iterator.hasNext();
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFLag, failFlag);
        System.assertNotEquals(null, iterator);
        System.assertEquals(expectedHasNext, hasNext);
    }

    @IsTest
    static void hasNext_true() {
        Boolean expectedFailFLag = false;
        Boolean failFlag = false;
        EPD_EPDIterator iterator;
        Boolean hasNext;
        Boolean expectedHasNext = true;

        try {
            iterator = new EPD_EPDIterator(
                    new List<EPD_Engine_Performance_Data__c>{
                            (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                                    new EPD_Engine_Performance_Data__c(),
                                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                                    false
                            )
                    }
            );
            hasNext = iterator.hasNext();
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFLag, failFlag);
        System.assertNotEquals(null, iterator);
        System.assertEquals(expectedHasNext, hasNext);
    }

    @IsTest
    static void next_null() {
        Boolean expectedFailFLag = false;
        Boolean failFlag = false;
        EPD_EPDIterator iterator;
        EPD_Engine_Performance_Data__c next;
        EPD_Engine_Performance_Data__c expectedNext = null;

        try {
            iterator = new EPD_EPDIterator(
                    new List<EPD_Engine_Performance_Data__c>()
            );
            next = iterator.next();
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFLag, failFlag);
        System.assertNotEquals(null, iterator);
        System.assertEquals(expectedNext, next);
    }

    @IsTest
    static void next_record() {
        Boolean expectedFailFLag = false;
        Boolean failFlag = false;
        EPD_EPDIterator iterator;
        EPD_Engine_Performance_Data__c next;
        EPD_Engine_Performance_Data__c expectedNext = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );

        try {
            iterator = new EPD_EPDIterator(
                    new List<EPD_Engine_Performance_Data__c>{ expectedNext }
            );
            next = iterator.next();
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFLag, failFlag);
        System.assertNotEquals(null, iterator);
        System.assertEquals(expectedNext, next);
    }

}