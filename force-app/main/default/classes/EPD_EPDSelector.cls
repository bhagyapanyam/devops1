/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Selector class containing all queries for EPD_Engine_Performance_Data__c SObject
Test Class:     EPD_EPDSelectorTest
History:        jschn 2019-05-24 - Created. - EPD R1
*************************************************************************************************/
@SuppressWarnings('ApexUnusedDeclaration')
public inherited sharing class EPD_EPDSelector implements EPD_EPDDAO {

    private String EPD_QUERY_BASE =
            'SELECT Name, Submitted__c, Required_Exhaust_Measurements__c, Vendor_Work_Order_Number__c, ' +
                    'Hours__c, Planner_Group__c, LSD__c, EI_Ignition_Timing__c, EI_RPM__c, Previous_EPD__c, ' +
                    'EOC_Crankcase_Pressure__c, EOC_Oil_Pressure__c, EOC_Oil_Temperature__c, ' +
                    'COC_Oil_Pressure__c, COC_Oil_Temperature__c, Operation_Group__c, EJW_Temperature_IN__c, ' +
                    'EJW_Temperature_OUT__c, Notes__c, Data_Collected__c, Data_Collected_By__c, ' +
                    'Data_Collected_By__r.FirstName, Data_Collected_By__r.Name, Data_Collected_By__r.LastName, ' +
                    'Company__c, Company__r.Name, Work_Order_Activity__c, Equipment_Engine__c, ' +
                    'Equipment_Engine__r.Manufacturer__c, Equipment_Engine__r.Model__c, Equipment_Engine__r.Tag__c, ' +
                    'Equipment_Engine__r.Serial_Number__c, Equipment_Engine__r.Equipment__c, ' +
                    'Work_Order_Activity__r.Name, Work_Order_Activity__r.Functional_Location__c, Work_Order__c, ' +
                    'Work_Order__r.Planner_Group__c, Work_Order__r.Work_Order_Number__c, Work_Order__r.Main_Work_Centre__c, ' +
                    'Work_Order__r.Functional_Location__c, Work_Order_Activity__r.Equipment__c, ' +
                    'Work_Order_Activity__r.Assigned_Vendor__c, Work_Order_Activity__r.Assigned_Vendor__r.Name, ' +
            '( ' +
                    'SELECT Name, Replaced__c, Inspected__c, Comments__c, Engine_Performance_Data__c, Form_Order__c ' +
                    'FROM Part_Replacements__r ' +
                    'ORDER BY Form_Order__c' +
            '), ' +
            '( ' +
                    'SELECT Name, Stage_Number__c, Suction_Pressure__c, Discharge_Pressure__c, ' +
                            'Suction_Temperature__c, Discharge_Temperature__c, Engine_Performance_Data__c ' +
                    'FROM Compressor_Operating_Condition_Stages__r ' +
                    'ORDER BY Stage_Number__c' +
            '), ' +
            '( ' +
                    'SELECT Name, RecordTypeId, Engine_Performance_Data__c, Block_Name__c, ' +
                    'Pre_Chamber_Fuel_Pressure__c, Regulated_Fuel_Pressure__c, Supplied_Fuel_Pressure__c ' +
                    'FROM Engine_Operating_Conditions__r ' +
            ')' +
            'FROM EPD_Engine_Performance_Data__c ' +
            'WHERE {0} IN :{1}';

    /**
     * Method for querying list of Engine Performance Data records based on Work Order Activity ID.
     *
     * @param workOrderActivityId
     *
     * @return List<EPD_Engine_Performance__c>
     */
    public List<EPD_Engine_Performance_Data__c> getEnginePerformanceDataByWOAId(Id workOrderActivityId) {
        Set<Id> woaIds = new Set<Id> {workOrderActivityId};
        return Database.query(
                String.format(
                        EPD_QUERY_BASE,
                        new List<String>{
                                'Work_Order_Activity__c',
                                'woaIds'
                        }
                )
        );
    }

    /**
     * Method for querying list of Engine Performance Data records based on set of record IDs.
     *
     * @param epdIds
     *
     * @return List<EPD_Engine_Performance__c>
     */
    public List<EPD_Engine_Performance_Data__c> getEPDRecords(Set<Id> epdIds) {
        return Database.query(
                String.format(
                        EPD_QUERY_BASE,
                        new List<String>{
                                'Id',
                                'epdIds'
                        }
                )
        );
    }

    /**
     * Method for querying list of Engine Performance Data records based on set of Work Order IDs.
     *
     * @param woIds
     *
     * @return List<EPD_Engine_Performance__c>
     */
    public List<EPD_Engine_Performance_Data__c> getEPDRecordsByWorkOrderIds(Set<Id> woIds) {
        return Database.query(
                String.format(
                        EPD_QUERY_BASE,
                        new List<String>{
                                'Work_Order__c',
                                'woIds'
                        }
                )
        );
    }

    /**
     * Method that queries count of submitted Engine Performance Data records based on Work Order Activity ID.
     *
     * @param workOrderActivityId
     *
     * @return Integer
     */
    public Integer getCountOfSubmittedRecords(Id workOrderActivityId) {
        return [
                SELECT COUNT()
                FROM EPD_Engine_Performance_Data__c
                WHERE Submitted__c = TRUE
                AND Work_Order_Activity__c =: workOrderActivityId
        ];
    }

    /**
     * Retrieves latest EPD record on Engine.
     *
     * @param engineId
     *
     * @return List<EPD_Engine_Performance_Data__c>
     */
    public List<EPD_Engine_Performance_Data__c> getPreviousEPDByEngineId(Id engineId) {
        return [
                SELECT Name, Submitted__c, Required_Exhaust_Measurements__c, Vendor_Work_Order_Number__c,
                        Hours__c, Planner_Group__c, LSD__c, EI_Ignition_Timing__c, EI_RPM__c, Previous_EPD__c,
                        EOC_Crankcase_Pressure__c, EOC_Oil_Pressure__c, EOC_Oil_Temperature__c,
                        COC_Oil_Pressure__c, COC_Oil_Temperature__c, Operation_Group__c, EJW_Temperature_IN__c,
                        EJW_Temperature_OUT__c, Notes__c, Data_Collected__c, Data_Collected_By__c,
                        Data_Collected_By__r.FirstName, Data_Collected_By__r.Name, Data_Collected_By__r.LastName,
                        Company__c, Company__r.Name, Work_Order_Activity__c, Equipment_Engine__c,
                        Equipment_Engine__r.Manufacturer__c, Equipment_Engine__r.Model__c, Equipment_Engine__r.Tag__c,
                        Equipment_Engine__r.Serial_Number__c, Equipment_Engine__r.Equipment__c,
                        Work_Order_Activity__r.Name, Work_Order_Activity__r.Functional_Location__c, Work_Order__c,
                        Work_Order__r.Planner_Group__c, Work_Order__r.Work_Order_Number__c,
                        Work_Order__r.Functional_Location__c, Work_Order_Activity__r.Equipment__c,
                        Work_Order_Activity__r.Assigned_Vendor__c, Work_Order_Activity__r.Assigned_Vendor__r.Name,
                (
                        SELECT Name, Replaced__c, Inspected__c, Comments__c, Engine_Performance_Data__c, Form_Order__c
                        FROM Part_Replacements__r
                        ORDER BY Form_Order__c
                ),
                (
                        SELECT Name, Stage_Number__c, Suction_Pressure__c, Discharge_Pressure__c,
                                Suction_Temperature__c, Discharge_Temperature__c, Engine_Performance_Data__c
                        FROM Compressor_Operating_Condition_Stages__r
                        ORDER BY Stage_Number__c
                ),
                (
                        SELECT Name, RecordTypeId, Engine_Performance_Data__c, Block_Name__c,
                                Pre_Chamber_Fuel_Pressure__c, Regulated_Fuel_Pressure__c, Supplied_Fuel_Pressure__c
                        FROM Engine_Operating_Conditions__r
                )
                FROM EPD_Engine_Performance_Data__c
                WHERE Equipment_Engine__c =: engineId
                ORDER BY CreatedDate DESC
                LIMIT 1
        ];
    }

}