/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Handler that handles actions from trigger.
Test Class:     EPD_EPDTriggerHandlerTest
History:        jschn 2019-06-11 - Created. - EPD R1
*************************************************************************************************/
public without sharing class EPD_EPDTriggerHandler {

    private static final String CLASS_NAME = String.valueOf(EPD_EPDTriggerHandler.class);

    public static Boolean skipTrigger = false;

    /**
     * After Insert handler.
     * Restricts recursion for trigger.
     * Then sets Last EPD Check Date for parent Engine records.
     * Send Emails for Submitted records.
     *
     * @param records
     */
    public void handleAfterInsert(List<EPD_Engine_Performance_Data__c> records) {
        if(!skipTrigger) {
            System.debug(CLASS_NAME + ' -> handleAfterInsert. START AFTER INSERT.');

            skipTrigger = true;

            setLastEPDCheckDateOnEngines(records);

            skipTrigger = false;

            System.debug(CLASS_NAME + ' -> handleAfterInsert. FINISH AFTER INSERT.');
        }
    }

    /**
     * Sets Last EPD Check date to current date.
     *
     * @param records
     */
    private void setLastEPDCheckDateOnEngines(List<EPD_Engine_Performance_Data__c> records) {
        List<Equipment_Engine__c> enginesToUpdate = new List<Equipment_Engine__c>();
        Date today = Date.today();

        System.debug(CLASS_NAME + ' -> setLastEPDCheckDateOnEngines. EPD Records: ' + JSON.serialize(records));

        for(EPD_Engine_Performance_Data__c epdRec : records) {
            enginesToUpdate.add(
                    new Equipment_Engine__c(
                            Id = epdRec.Equipment_Engine__c,
                            Last_EPD_Check__c = today
                    )
            );
        }

        System.debug(CLASS_NAME + ' -> setLastEPDCheckDateOnEngines. Engines to update: '
                + JSON.serialize(enginesToUpdate));

        update enginesToUpdate;
    }

}