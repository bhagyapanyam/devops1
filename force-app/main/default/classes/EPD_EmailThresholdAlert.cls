/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Email Alert class for purpose notifying target users that thresholds for EPD records
                were reached.
Test Class:     EPD_EmailThresholdAlertTest
History:        jschn 2019-07-02 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_EmailThresholdAlert extends EPD_EmailServiceBase {

    private Map<String, List<HOG_Planner_Group_Notification_Type__c>> plannerGroupNotificationTypes;
    private Map<Id, EPD_FormStructure> structureMap;
    private Map<Id, EPD_FormStructure> previousStructuresById;

    /**
     * Builds map of Planner Group Notification Types based on Planner Group name.
     * For this purpose we are only using certain Role Types defined in EPD Constants.
     *
     * @return Map<String, List<HOG_Planner_Group_Notification_Type__c>>
     */
    private Map<String, List<HOG_Planner_Group_Notification_Type__c>> generatePlannerGroupNotificationTypesMap() {
        Map<String, List<HOG_Planner_Group_Notification_Type__c>> plannerGroupNotificationTypes = new Map<String, List<HOG_Planner_Group_Notification_Type__c>>();

        for(HOG_Planner_Group_Notification_Type__c pgnt : EPD_DAOProvider.plannerGroupNotificationTypeDAO.getNotificationTypesForRoleTypes(EPD_Constants.THRESHOLD_ALERT_TARGET_ROLE_TYPE)) {

            if(!plannerGroupNotificationTypes.containsKey(pgnt.HOG_Planner_Group__r.Name)) {
                plannerGroupNotificationTypes.put(pgnt.HOG_Planner_Group__r.Name, new List<HOG_Planner_Group_Notification_Type__c>());
            }

            plannerGroupNotificationTypes.get(pgnt.HOG_Planner_Group__r.Name).add(pgnt);
        }

        return plannerGroupNotificationTypes;
    }

    /**
     * Generates map of EPD Form Structures based on EPD Record ID. This reduces number of queries if multiple
     * records are used to call sendEmail.
     *
     * @return Map<Id, EPD_FormStructure>
     */
    private Map<Id, EPD_FormStructure> generateStructureMap(){
        Map<Id, EPD_FormStructure> structureMap = new Map<Id, EPD_FormStructure>();

        for(EPD_FormStructure structure : new EPD_EPDService().getFormStructuresForEPDs(forms)) {
            if(structure.hasReachedThreshold() || hasReachedRetrospectiveThreshold(structure)) {
                structureMap.put(structure.epd.recordId, structure);
            }
        }

        return structureMap;
    }

    /**
     * Checks if there is an previous EPD on current engine, and if so it checks if it has reached retrospective thresholds.
     *
     * @param structure
     *
     * @return Boolean
     */
    private Boolean hasReachedRetrospectiveThreshold(EPD_FormStructure structure) {
        return previousStructuresById.containsKey(structure.epd.previousEPD)
                && structure.hasReachedRetrospectiveThreshold(
                previousStructuresById.get(structure.epd.previousEPD)
        );
    }

    /**
     * Builds recipients map. Recipients are grouped by EPD Record ID. Recipients are extracted from Planner Group
     * Notification Type maps.
     */
    protected override void buildRecipientsMap() {
        plannerGroupNotificationTypes = generatePlannerGroupNotificationTypesMap();
        recipientsMap = new Map<Id, List<Id>>();
        for(EPD_Engine_Performance_Data__c form : forms) {
            if(!recipientsMap.containsKey(form.Id)) {
                recipientsMap.put(form.Id, buildRecipientsListForForm(form));
            }
        }
    }

    /**
     * Builds list of IDs (Contact Ids) from Planner Group Notification Types.
     *
     * @param form
     *
     * @return List<Id>
     */
    private List<Id> buildRecipientsListForForm(EPD_Engine_Performance_Data__c form) {
        List<Id> recipientIds = new List<Id>();

        for(HOG_Planner_Group_Notification_Type__c notificationType : extractRelevantNotificationTypes(form)) {
            recipientIds.add(notificationType.Contact__c);
        }

        return recipientIds;
    }

    /**
     * Extracts relevant Planner Group Notification Types based on Work Center on Work Order and Notification Type.
     * Using VTT Utility class which has method designed for this.
     *
     * @param form
     *
     * @return List<HOG_Planner_Group_Notification_Type__c>
     */
    private List<HOG_Planner_Group_Notification_Type__c> extractRelevantNotificationTypes(EPD_Engine_Performance_Data__c form) {
        List<HOG_Planner_Group_Notification_Type__c> relevantNotificationTypes = new List<HOG_Planner_Group_Notification_Type__c>();

        System.debug(form.Id);
        System.debug(form.Work_Order__r.Planner_Group__c);

        if(plannerGroupNotificationTypes.containsKey(form.Work_Order__r.Planner_Group__c)) {
            for (HOG_Planner_Group_Notification_Type__c notificationType : plannerGroupNotificationTypes.get(form.Work_Order__r.Planner_Group__c)) {

                if ((String.isBlank(notificationType.Work_Center__c)
                        || VTT_Utilities.FieldValueMatchTheRule(form.Work_Order__r.Main_Work_Centre__c, notificationType.Work_Center__c))
                        && VTT_Utilities.isCorrectFLOC(form.Work_Order__r.Functional_Location__c, notificationType.FLOC__c, notificationType.FLOC_Excluded__c)) {

                    relevantNotificationTypes.add(notificationType);
                }
            }
        }

        return relevantNotificationTypes;
    }

    /**
     * Sets email template ID for this type of email.
     */
    protected override void setEmailTemplate() {
        templateId = new HOG_EmailTemplateSelector().getTemplateByDevName(EPD_Constants.TEMPLATE_NAME_THRESHOLD_ALERT).Id;
    }

    /**
     * This type of email doesn't require any attachment, so we are returning empty list.
     *
     * @param form
     *
     * @return List<Messaging.EmailFileAttachment>
     */
    protected override List<Messaging.EmailFileAttachment> setAttachments(EPD_Engine_Performance_Data__c form) {
        if(!attachmentsByEPDId.containsKey(form.Id)) {
            attachmentsByEPDId.put(form.Id, new List<Messaging.EmailFileAttachment>());
        }
        return attachmentsByEPDId.get(form.Id);
    }

    /**
     * Set sender of the email. As this method is not allowed in Unit Tests we have to add condition before it.
     *
     * @param emailMessage
     */
    protected override void setSender(Messaging.SingleEmailMessage emailMessage) {
        if(!Test.isRunningTest()) { emailMessage.setOrgWideEmailAddressId(EPD_Constants.SENDER_ID); }
    }

    /**
     * This method is using actual EmailTemplate where we use values such as Subject and HtmlBody to set it with
     * our additional content.
     *
     * WARNING: Messaging.renderStoredEmailTemplate is considered as SOQL query - do not run in loop or at least don't
     *          rise record count restriction. If you do, I suggest heavy testing.
     *
     * @param emailMessage
     * @param form
     */
    protected override void setCustomContent(Messaging.SingleEmailMessage emailMessage, EPD_Engine_Performance_Data__c form) {
        Messaging.SingleEmailMessage templateMessage = Messaging.renderStoredEmailTemplate(templateId, null, form.Id);
        emailMessage.setSubject(templateMessage.getSubject());
        emailMessage.setHtmlBody(buildBodyForMessage(templateMessage.getHtmlBody(), form));
    }

    /**
     * This replaces placeholder located in EmailTemplate with additional content we need to add.
     * That basically list of all thresholds which were reached.
     *
     * @param templateBody
     * @param form
     *
     * @return String
     */
    private String buildBodyForMessage(String templateBody, EPD_Engine_Performance_Data__c form) {
        return templateBody.replace(
                EPD_Constants.THRESHOLD_ALERT_LIST_PLACEHOLDER,
                buildThresholdWarningListForForm(form)
        );
    }

    /**
     * This method builds string for replacement. It contains list of reached thresholds.
     *
     * @param form
     *
     * @return String
     */
    private String buildThresholdWarningListForForm(EPD_Engine_Performance_Data__c form) {
        EPD_FormStructure formStructure = structureMap.get(form.Id);
        String thresholdWarningList = formStructure.epd.getThresholdWarnings(formStructure.engineThresholds);
        thresholdWarningList += buildThresholdWarningListForBlocks(formStructure);
        return thresholdWarningList;
    }

    /**
     * This method will go trough engine blocks and and gets threshold and retrospective warnings
     *
     * @param formStructure
     *
     * @return String
     */
    private String buildThresholdWarningListForBlocks(EPD_FormStructure formStructure) {
        String thresholdWarningList = '';

        for(EPD_FormStructureBlock block : formStructure.engineBlocks) {
            thresholdWarningList += block.getThresholdWarnings(formStructure.engineThresholds);
            thresholdWarningList += buildRetrospectiveThresholdWarningList(formStructure, block);
        }

        return thresholdWarningList;
    }

    /**
     * Builds Retrospective value warnings for block.
     *
     * @param formStructure
     * @param block
     *
     * @return String
     */
    private String buildRetrospectiveThresholdWarningList(EPD_FormStructure formStructure, EPD_FormStructureBlock block) {
        String thresholdWarningList = '';

        if(previousStructuresById.containsKey(formStructure.epd.previousEPD)) {
            for(EPD_FormStructureBlock previousBlock : previousStructuresById.get(formStructure.epd.previousEPD).engineBlocks) {
                if(block.blockName == previousBlock.blockName) {
                    thresholdWarningList += block.getRetrospectiveThresholdWarnings(previousBlock);
                }
            }
        }

        return thresholdWarningList;
    }

    /**
     * Checks if record is valid for email.
     *
     * @param form
     *
     * @return Boolean
     */
    protected override Boolean validRecordForEmail(EPD_Engine_Performance_Data__c form) {
        return structureMap.containsKey(form.Id);
    }

    /**
     * This method should initialize all implementation specific variables to be able to properly build and send
     * emails.
     * Current implementation builds structure map that is later used for validation and other tasks
     */
    protected override void prepareImplementationSpecificVariables() {
        previousStructuresById = generatePreviousStructures();
        structureMap = generateStructureMap();
    }

    /**
     * Sets CC addresses for email. Using Custom Settings for EPD.
     *
     * @param emailMessage
     */
    protected override void setCCAddresses(Messaging.SingleEmailMessage emailMessage) {
        if(EPD_Constants.SETTINGS != null && String.isNotBlank(EPD_Constants.SETTINGS.Threshold_Alert_Cc_Addresses__c)) {
            emailMessage.setCcAddresses(EPD_Constants.SETTINGS.Threshold_Alert_Cc_Addresses__c.split(';'));
        }
    }

    /**
     * Generates map with previous EPD forms by Engine Id.
     *
     * @return Map<Id, EPD_FormStructure>
     */
    private Map<Id, EPD_FormStructure> generatePreviousStructures() {
        Map<Id, EPD_FormStructure> previousFormsMap = new Map<Id, EPD_FormStructure>();

        for(EPD_FormStructure previousStructure : new EPD_EPDService().getFormStructuresForEPDs(getPreviousRecords())) {
            previousFormsMap.put(previousStructure.epd.recordId, previousStructure);
        }

        return previousFormsMap;
    }

    /**
     * This will retrieve List of EPD records that were previously submitted. (prior current records)
     *
     * @return List<EPD_Engine_Performance_Data__c>
     */
    private List<EPD_Engine_Performance_Data__c> getPreviousRecords() {
        Set<Id> previousIds = new Set<Id>();

        for(EPD_Engine_Performance_Data__c epd : forms) {
            if(String.isNotBlank(epd.Previous_EPD__c)) {
                previousIds.add(epd.Previous_EPD__c);
            }
        }

        return EPD_DAOProvider.EPDDAO.getEPDRecords(previousIds);
    }

}