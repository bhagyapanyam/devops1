/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Endpoint class for Lightning and Classic Engine Performance Data forms.
Test Class:     EPD_EndpointTest
History:        jschn 2019-05-24 - Created. - EPD R1
*************************************************************************************************/
public with sharing class EPD_Endpoint {

    private static final String CLASS_NAME = String.valueOf(EPD_Endpoint.class);

    /**
     * Method that loads EPD form (builds all necessary structures) for Work Order Activity based on provided record ID.
     * To avoid issues in Community, method is using Trampoline to disable sharing for action.
     *
     * @param workOrderActivityId
     *
     * @return EPD_Response
     */
    @AuraEnabled
    public static EPD_Response loadForm(Id workOrderActivityId) {
        System.debug(CLASS_NAME + ' -> loadForm. workOrderActivityId: ' + workOrderActivityId);
        return new EPD_EndpointWithoutSharingTrampoline()
                .loadForm(workOrderActivityId);
    }

    /**
     * Method that saves EPD form (translates all structures into SObjects).
     * To avoid issues in Community, method is using Trampoline to disable sharing for action.
     *
     * @param formStructure
     *
     * @return EPD_Response
     */
    @AuraEnabled
    public static EPD_Response saveForm(String formStructure) {
        System.debug(CLASS_NAME + ' -> saveForm. formStructure: ' + formStructure);
        return new EPD_EndpointWithoutSharingTrampoline()
                .saveForm(formStructure);
    }

    /**
     * Method that submits EPD form (translates all structures into SObjects).
     * To avoid issues in Community, method is using Trampoline to disable sharing for action.
     *
     * @param formStructure
     *
     * @return EPD_Response
     */
    @AuraEnabled
    public static EPD_Response submitForm(String formStructure) {
        System.debug(CLASS_NAME + ' -> submitForm. formStructure: ' + formStructure);
        return new EPD_EndpointWithoutSharingTrampoline()
                .submitForm(formStructure);
    }

    /**
     * Method that load EPD form (builds all necessary structures) for Engine Performance Data record based on provided ID.
     * To avoid issues in Community, method is using Trampoline to disable sharing for action.
     *
     * @param epdId
     *
     * @return EPD_Response
     */
    @AuraEnabled
    //@Deprecated
    public static EPD_Response getEPDRecord(Id epdId) {
        System.debug(CLASS_NAME + ' -> getEPDForm. epdId: ' + epdId);
        return new EPD_EndpointWithoutSharingTrampoline()
                .getEPDRecord(epdId);
    }

    /**
     * Method that saves EPD form (translates all structures into SObjects).
     *
     * @param formStructure
     *
     * @return EPD_Response
     */
    @AuraEnabled
    public static EPD_Response saveFormWithSharing(String formStructure) {
        System.debug(CLASS_NAME + ' -> saveFormForInternal. formStructure: ' + formStructure);
        return new EPD_EndpointHandler()
                .handleSave(
                        formStructure,
                        EPD_SaveMode.SAVE
                );
    }

    /**
     * Method that load EPD form (builds all necessary structures) for Engine Performance Data record based on provided ID.
     *
     * @param epdId
     *
     * @return EPD_Response
     */
    @AuraEnabled
    public static EPD_Response getEPDRecordWithSharing(Id epdId) {
        System.debug(CLASS_NAME + ' -> getEPDRecordForInternal. epdId: ' + epdId);
        return new EPD_EndpointHandler()
                .handleLoad(
                        epdId,
                        EPD_LoadMode.DIRECT_LOAD
                );
    }

}