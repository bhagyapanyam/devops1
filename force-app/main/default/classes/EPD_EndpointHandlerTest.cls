/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for EPD_EndpointHandler
History:        jschn 2019-07-26 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_EndpointHandlerTest {

    @IsTest
    static void handleSave_withoutParams() {
        Boolean expectedSuccessFlag = false;
        EPD_Response response;

        Test.startTest();
        response = new EPD_EndpointHandler().handleSave(null, null);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assert(response.errors.get(0).contains('Form Structure'));
    }

    @IsTest
    static void handleSave_withSaveModeParam() {
        Boolean expectedSuccessFlag = false;
        EPD_Response response;

        Test.startTest();
        response = new EPD_EndpointHandler().handleSave(null, EPD_SaveMode.SAVE);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assert(response.errors.get(0).contains('Form Structure'));
    }

    @IsTest
    static void handleSave_withBlankParam1() {
        Boolean expectedSuccessFlag = false;
        EPD_Response response;

        Test.startTest();
        response = new EPD_EndpointHandler().handleSave('', null);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assert(response.errors.get(0).contains('Form Structure'));
    }

    @IsTest
    static void handleSave_withWrongParam1() {
        Boolean expectedSuccessFlag = false;
        EPD_Response response;

        Test.startTest();
        response = new EPD_EndpointHandler().handleSave('wrongJSONText', null);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.debug(response.errors.get(0));
        System.assert(response.errors.get(0).contains('code 119')); //unexpected character
    }

    @IsTest
    static void handleSave_withProperParamWrongValue() {
        Boolean expectedSuccessFlag = false;
        EPD_Response response;
        EPD_FormStructure formStructure = new EPD_FormStructure();
        formStructure.setEPDStructure(new EPD_FormStructureEPD());

        Test.startTest();
        response = new EPD_EndpointHandler().handleSave(JSON.serialize(formStructure), EPD_SaveMode.SUBMIT);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
    }

    @IsTest
    static void handleLoad_withoutParams() {
        Boolean expectedSuccessFlag = false;
        EPD_Response response;

        Test.startTest();
        response = new EPD_EndpointHandler().handleLoad(null, null);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
    }

    @IsTest
    static void handleLoad_withWrongParam1() {
        Boolean expectedSuccessFlag = false;
        EPD_Response response;

        Test.startTest();
        response = new EPD_EndpointHandler().handleLoad(UserInfo.getUserId(), null);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
    }

    @IsTest
    static void handleLoad_withJustLoadMode1() {
        Boolean expectedSuccessFlag = false;
        EPD_Response response;

        Test.startTest();
        response = new EPD_EndpointHandler().handleLoad(null, EPD_LoadMode.DIRECT_LOAD);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assert(response.errors.get(0).contains(EPD_Constants.MISSING_PARAM_BY_LOAD_MODE.get(EPD_LoadMode.DIRECT_LOAD)));
    }

    @IsTest
    static void handleLoad_withJustLoadMode2() {
        Boolean expectedSuccessFlag = false;
        EPD_Response response;

        Test.startTest();
        response = new EPD_EndpointHandler().handleLoad(null, EPD_LoadMode.INDIRECT_LOAD);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assert(response.errors.get(0).contains(EPD_Constants.MISSING_PARAM_BY_LOAD_MODE.get(EPD_LoadMode.INDIRECT_LOAD)));
    }

}