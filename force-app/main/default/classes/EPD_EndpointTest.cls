/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for EPD_Endpoint & EPD_EndpointWithoutSharingTrampoline
History:        jschn 2019-07-26 - Created.
*************************************************************************************************/
@IsTest
private class EPD_EndpointTest {

    @IsTest
    static void loadForm_success() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();
        epd.Submitted__c = false;
        update epd;

        Test.startTest();
        response = EPD_Endpoint.loadForm(epd.Work_Order_Activity__c);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
    }

    @IsTest
    static void getEPDRecord_success() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();

        Test.startTest();
        response = EPD_Endpoint.getEPDRecord(epd.Id);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
    }

    @IsTest
    static void getEPDRecordWithSharing_success() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();

        Test.startTest();
        response = EPD_Endpoint.getEPDRecordWithSharing(epd.Id);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
    }

    @IsTest
    static void saveForm_success() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        Integer initialCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer initialCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer initialCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer initialCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer initialCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer initialCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        EPD_FormStructure formStructure = EPD_TestData.prepareStructure();

        Test.startTest();
        //TODO after LTNG migration replace with JSON.serialize(formStructure)
        response = EPD_Endpoint.saveForm(formStructure.toString());
        Test.stopTest();

        Integer finalCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer finalCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer finalCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer finalCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer finalCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer finalCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
        System.assertEquals(initialCountEPD+1, finalCountEPD);
        System.assertEquals(initialCountEOC+1, finalCountEOC);
        System.assertEquals(initialCountCOC+1, finalCountCOC);
        System.assertEquals(initialCountBI+1, finalCountBI);
        System.assertEquals(initialCountCI+1, finalCountCI);
        System.assertEquals(initialCountPart+1, finalCountPart);
    }

    @IsTest
    static void saveFormWithSharing_success() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        Integer initialCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer initialCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer initialCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer initialCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer initialCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer initialCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        EPD_FormStructure formStructure = EPD_TestData.prepareStructure();

        Test.startTest();
        //TODO after LTNG migration replace with JSON.serialize(formStructure)
        response = EPD_Endpoint.saveFormWithSharing(formStructure.toString());
        Test.stopTest();

        Integer finalCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer finalCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer finalCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer finalCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer finalCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer finalCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
        System.assertEquals(initialCountEPD+1, finalCountEPD);
        System.assertEquals(initialCountEOC+1, finalCountEOC);
        System.assertEquals(initialCountCOC+1, finalCountCOC);
        System.assertEquals(initialCountBI+1, finalCountBI);
        System.assertEquals(initialCountCI+1, finalCountCI);
        System.assertEquals(initialCountPart+1, finalCountPart);
    }

    @IsTest
    static void submitForm_success() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        Integer initialCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer initialCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer initialCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer initialCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer initialCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer initialCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        EPD_FormStructure formStructure = EPD_TestData.prepareStructure();
        EPD_DAOProvider.plannerGroupNotificationTypeDAO = new HPNTMock();

        Test.startTest();
        //TODO after LTNG migration replace with JSON.serialize(formStructure)
        response = EPD_Endpoint.submitForm(formStructure.toString());
        Test.stopTest();

        Integer finalCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer finalCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer finalCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer finalCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer finalCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer finalCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
        System.assertEquals(initialCountEPD+1, finalCountEPD);
        System.assertEquals(initialCountEOC+1, finalCountEOC);
        System.assertEquals(initialCountCOC+1, finalCountCOC);
        System.assertEquals(initialCountBI+1, finalCountBI);
        System.assertEquals(initialCountCI+1, finalCountCI);
        System.assertEquals(initialCountPart+1, finalCountPart);
    }

    @IsTest
    static void loadForm_withoutAccess() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();
        epd.Submitted__c = false;
        update epd;

        User runningUser = [SELECT Id FROM User WHERE Alias = 'Hulk' LIMIT 1];
        System.runAs(runningUser) {
            Test.startTest();
            response = EPD_Endpoint.loadForm(epd.Work_Order_Activity__c);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
    }

    @IsTest
    static void getEPDRecord_withoutAccess() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();

        User runningUser = [SELECT Id FROM User WHERE Alias = 'Hulk' LIMIT 1];
        System.runAs(runningUser) {
            Test.startTest();
            response = EPD_Endpoint.getEPDRecord(epd.Id);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
    }

    @IsTest
    static void getEPDRecordWithSharing_withoutAccess() {
        Boolean expectedSuccessFlag = false;
        EPD_Response response;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();

        User runningUser = [SELECT Id FROM User WHERE Alias = 'Hulk' LIMIT 1];
        System.runAs(runningUser) {
            Test.startTest();
            response = EPD_Endpoint.getEPDRecordWithSharing(epd.Id);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assertEquals(null, response.formStructure);
    }

    @IsTest
    static void saveForm_withoutAccess() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        Integer initialCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer initialCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer initialCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer initialCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer initialCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer initialCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        EPD_FormStructure formStructure = EPD_TestData.prepareStructure();

        User runningUser = [SELECT Id FROM User WHERE Alias = 'Hulk' LIMIT 1];
        System.runAs(runningUser) {
            Test.startTest();
            //TODO after LTNG migration replace with JSON.serialize(formStructure)
            response = EPD_Endpoint.saveForm(formStructure.toString());
            Test.stopTest();
        }

        Integer finalCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer finalCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer finalCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer finalCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer finalCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer finalCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
        System.assertEquals(initialCountEPD+1, finalCountEPD);
        System.assertEquals(initialCountEOC+1, finalCountEOC);
        System.assertEquals(initialCountCOC+1, finalCountCOC);
        System.assertEquals(initialCountBI+1, finalCountBI);
        System.assertEquals(initialCountCI+1, finalCountCI);
        System.assertEquals(initialCountPart+1, finalCountPart);
    }

    @IsTest
    static void saveFormWithSharing_withoutAccess() {
        Boolean expectedSuccessFlag = false;
        EPD_Response response;
        EPD_FormStructure formStructure = EPD_TestData.prepareStructure();
        Integer initialCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer initialCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer initialCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer initialCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer initialCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer initialCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];

        User runningUser = [SELECT Id FROM User WHERE Alias = 'Hulk' LIMIT 1];
        System.runAs(runningUser) {
            Test.startTest();
            //TODO after LTNG migration replace with JSON.serialize(formStructure)
            response = EPD_Endpoint.saveFormWithSharing(formStructure.toString());
            Test.stopTest();
        }

        Integer finalCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer finalCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer finalCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer finalCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer finalCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer finalCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assertEquals(null, response.formStructure);
        System.assertEquals(initialCountEPD, finalCountEPD);
        System.assertEquals(initialCountEOC, finalCountEOC);
        System.assertEquals(initialCountCOC, finalCountCOC);
        System.assertEquals(initialCountBI, finalCountBI);
        System.assertEquals(initialCountCI, finalCountCI);
        System.assertEquals(initialCountPart, finalCountPart);
    }

    @IsTest
    static void submitForm_withoutAccess() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        Integer initialCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer initialCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer initialCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer initialCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer initialCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer initialCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        EPD_FormStructure formStructure = EPD_TestData.prepareStructure();
        EPD_DAOProvider.plannerGroupNotificationTypeDAO = new HPNTMock();

        User runningUser = [SELECT Id FROM User WHERE Alias = 'Hulk' LIMIT 1];
        System.runAs(runningUser) {
            Test.startTest();
            //TODO after LTNG migration replace with JSON.serialize(formStructure)
            response = EPD_Endpoint.submitForm(formStructure.toString());
            Test.stopTest();
        }

        Integer finalCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer finalCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer finalCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer finalCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer finalCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer finalCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
        System.assertEquals(initialCountEPD+1, finalCountEPD);
        System.assertEquals(initialCountEOC+1, finalCountEOC);
        System.assertEquals(initialCountCOC+1, finalCountCOC);
        System.assertEquals(initialCountBI+1, finalCountBI);
        System.assertEquals(initialCountCI+1, finalCountCI);
        System.assertEquals(initialCountPart+1, finalCountPart);
    }

    @IsTest
    static void loadForm_withAccess() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();
        epd.Submitted__c = false;
        update epd;

        User runningUser = [SELECT Id FROM User WHERE Alias = 'IronMan' LIMIT 1];
        System.runAs(runningUser) {
            Test.startTest();
            response = EPD_Endpoint.loadForm(epd.Work_Order_Activity__c);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
    }

    @IsTest
    static void getEPDRecord_withAccess() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();

        User runningUser = [SELECT Id FROM User WHERE Alias = 'IronMan' LIMIT 1];
        System.runAs(runningUser) {
            Test.startTest();
            response = EPD_Endpoint.getEPDRecord(epd.Id);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
    }

    @IsTest
    static void getEPDRecordWithSharing_withAccess() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();

        User runningUser = [SELECT Id FROM User WHERE Alias = 'IronMan' LIMIT 1];
        System.runAs(runningUser) {
            Test.startTest();
            response = EPD_Endpoint.getEPDRecordWithSharing(epd.Id);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
    }

    @IsTest
    static void saveForm_withAccess() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        Integer initialCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer initialCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer initialCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer initialCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer initialCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer initialCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        EPD_FormStructure formStructure = EPD_TestData.prepareStructure();

        User runningUser = [SELECT Id FROM User WHERE Alias = 'IronMan' LIMIT 1];
        System.runAs(runningUser) {
            Test.startTest();
            //TODO after LTNG migration replace with JSON.serialize(formStructure)
            response = EPD_Endpoint.saveForm(formStructure.toString());
            Test.stopTest();
        }

        Integer finalCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer finalCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer finalCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer finalCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer finalCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer finalCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
        System.assertEquals(initialCountEPD+1, finalCountEPD);
        System.assertEquals(initialCountEOC+1, finalCountEOC);
        System.assertEquals(initialCountCOC+1, finalCountCOC);
        System.assertEquals(initialCountBI+1, finalCountBI);
        System.assertEquals(initialCountCI+1, finalCountCI);
        System.assertEquals(initialCountPart+1, finalCountPart);
    }

    @IsTest
    static void saveFormWithSharing_withAccess() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        Integer initialCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer initialCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer initialCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer initialCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer initialCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer initialCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        EPD_FormStructure formStructure = EPD_TestData.prepareStructure();

        User runningUser = [SELECT Id FROM User WHERE Alias = 'IronMan' LIMIT 1];
        System.runAs(runningUser) {
            Test.startTest();
            //TODO after LTNG migration replace with JSON.serialize(formStructure)
            response = EPD_Endpoint.saveFormWithSharing(formStructure.toString());
            Test.stopTest();
        }

        Integer finalCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer finalCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer finalCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer finalCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer finalCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer finalCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
        System.assertEquals(initialCountEPD+1, finalCountEPD);
        System.assertEquals(initialCountEOC+1, finalCountEOC);
        System.assertEquals(initialCountCOC+1, finalCountCOC);
        System.assertEquals(initialCountBI+1, finalCountBI);
        System.assertEquals(initialCountCI+1, finalCountCI);
        System.assertEquals(initialCountPart+1, finalCountPart);
    }

    @IsTest
    static void submitForm_withAccess() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        Integer initialCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer initialCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer initialCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer initialCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer initialCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer initialCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        EPD_FormStructure formStructure = EPD_TestData.prepareStructure();
        EPD_DAOProvider.plannerGroupNotificationTypeDAO = new HPNTMock();

        User runningUser = [SELECT Id FROM User WHERE Alias = 'IronMan' LIMIT 1];
        System.runAs(runningUser) {
            Test.startTest();
            //TODO after LTNG migration replace with JSON.serialize(formStructure)
            response = EPD_Endpoint.submitForm(formStructure.toString());
            Test.stopTest();
        }

        Integer finalCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer finalCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer finalCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer finalCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer finalCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer finalCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
        System.assertEquals(initialCountEPD+1, finalCountEPD);
        System.assertEquals(initialCountEOC+1, finalCountEOC);
        System.assertEquals(initialCountCOC+1, finalCountCOC);
        System.assertEquals(initialCountBI+1, finalCountBI);
        System.assertEquals(initialCountCI+1, finalCountCI);
        System.assertEquals(initialCountPart+1, finalCountPart);
    }

    @TestSetup
    static void setupData() {
        User user = (User) HOG_SObjectFactory.createSObject(
                new User(),
                EPD_FieldDefaultsGeneral.CLASS_NAME, //IronMan
                true
        );

        EPD_TestData.assignPermission(user);

        HOG_SObjectFactory.createSObject(
                new User(),
                EPD_FieldDefaultsWrong.CLASS_NAME, //This is without permission //Hulk
                true
        );
    }

    public class HPNTMock implements HOG_PlannerGroupNotificationTypeDAO {
        Contact contact = (Contact) HOG_SObjectFactory.createSObject(new Contact(), true);
        public List<HOG_Planner_Group_Notification_Type__c> getNotificationTypesForRoleTypes(List<String> roleTypes) {
            return new List<HOG_Planner_Group_Notification_Type__c> {
                    new HOG_Planner_Group_Notification_Type__c(
                            HOG_Planner_Group__r = new HOG_Planner_Group__c(Name = '100'),
                            Role_Type__c = 'Tradesman Lead',
                            Work_Center__c = 'Test',
                            Contact__c = contact.Id
                    )
            };
        }
    }

}