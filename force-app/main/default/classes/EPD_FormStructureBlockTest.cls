/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_FormStructureBlock
History:        jschn 2019-07-12 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_FormStructureBlockTest {

    @IsTest
    static void setRecord_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Block_Information__c blockInfo;
        EPD_FormStructureBlock result;

        Test.startTest();
        try {
            result = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void setRecord_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Block_Information__c blockInfo = new EPD_Block_Information__c();
        EPD_FormStructureBlock result;

        Test.startTest();
        try {
            result = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(blockInfo.Block_Type__c, result.blockType);
        System.assertEquals(blockInfo.Exhaust_O2__c, result.exhaustO2);
        System.assertEquals(blockInfo.Intake_Manifold_Pressure__c, result.intakeManifoldPressure);
    }

    @IsTest
    static void setRecord_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructureBlock result;

        Test.startTest();
        try {
            result = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(blockInfo.Block_Type__c, result.blockType);
        System.assertEquals(blockInfo.Exhaust_O2__c, result.exhaustO2);
        System.assertEquals(blockInfo.Intake_Manifold_Pressure__c, result.intakeManifoldPressure);
    }

    @IsTest
    static void getRecord_empty() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Block_Information__c blockInfo = new EPD_Block_Information__c();
        EPD_Block_Information__c result;

        Test.startTest();
        try {
            result = (EPD_Block_Information__c) new EPD_FormStructureBlock().setRecord(blockInfo).getRecord();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(blockInfo.Block_Type__c, result.Block_Type__c);
        System.assertEquals(blockInfo.Exhaust_O2__c, result.Exhaust_O2__c);
        System.assertEquals(blockInfo.Intake_Manifold_Pressure__c, result.Intake_Manifold_Pressure__c);
    }

    @IsTest
    static void getRecord_full() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Block_Information__c result;

        Test.startTest();
        try {
            result = (EPD_Block_Information__c) new EPD_FormStructureBlock().setRecord(blockInfo).getRecord();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(blockInfo.Block_Type__c, result.Block_Type__c);
        System.assertEquals(blockInfo.Exhaust_O2__c, result.Exhaust_O2__c);
        System.assertEquals(blockInfo.Intake_Manifold_Pressure__c, result.Intake_Manifold_Pressure__c);
    }

    @IsTest
    static void setConfig_withoutParams() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Block_Information__c blockInfo;
        EPD_FormStructureBlock result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo)).setConfig(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void setConfig_withoutMethodParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructureBlock result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo)).setConfig(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void setConfig_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt();
        EPD_FormStructureBlock result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo)).setConfig(engineConfig);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(engineConfig.Measurement_Type__c, result.measurementType);
        System.assertEquals(EPD_Constants.BLOCK_TYPE_TO_IMP_INPUT_LABEL_MAP.get(blockInfo.Block_Type__c), result.inputLabelIMP);
        System.assertEquals(EPD_Constants.BLOCK_TYPE_TO_EO2_INPUT_LABEL_MAP.get(blockInfo.Block_Type__c), result.inputLabelEO2);
        System.assertEquals(EPD_Constants.BLOCK_TYPE_TO_BLOCK_NAME_MAP.get(blockInfo.Block_Type__c), result.blockName);
    }

    @IsTest
    static void setConfig_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(Measurement_Type__c = 'Wear');
        EPD_FormStructureBlock result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo)).setConfig(engineConfig);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(engineConfig.Measurement_Type__c, result.measurementType);
        System.assertEquals(EPD_Constants.BLOCK_TYPE_TO_IMP_INPUT_LABEL_MAP.get(blockInfo.Block_Type__c), result.inputLabelIMP);
        System.assertEquals(EPD_Constants.BLOCK_TYPE_TO_EO2_INPUT_LABEL_MAP.get(blockInfo.Block_Type__c), result.inputLabelEO2);
        System.assertEquals(EPD_Constants.BLOCK_TYPE_TO_BLOCK_NAME_MAP.get(blockInfo.Block_Type__c), result.blockName);
    }

    @IsTest
    static void getThresholdWarnings_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        String result;

        Test.startTest();
        try {
            result = ((EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo))
                    .getThresholdWarnings(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void getThresholdWarnings_withoutWarning() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBlock block = prepareBlock(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Min_Intake_Manifold_Pressure__c = -20,
                Max_Intake_Manifold_Pressure__c = 25
        );
        String result;

        Test.startTest();
        try {
            result = block.getThresholdWarnings(new EPD_FormStructureThresholds().setRecord(engineConfig));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals('', result);
    }

    @IsTest
    static void getThresholdWarnings_withWarning() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBlock block = prepareBlock(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Min_Intake_Manifold_Pressure__c = -20,
                Max_Intake_Manifold_Pressure__c = 0
        );
        String result;

        Test.startTest();
        try {
            result = block.getThresholdWarnings(new EPD_FormStructureThresholds().setRecord(engineConfig));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assert(result.contains('Warning(s) for bank:'));
    }

    @IsTest
    static void hasReachedThreshold_true() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBlock block = prepareBlock(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Min_Intake_Manifold_Pressure__c = -20,
                Max_Intake_Manifold_Pressure__c = 0
        );
        Boolean expectedResult = true;
        Boolean result;

        Test.startTest();
        try {
            result = block.hasReachedThreshold(new EPD_FormStructureThresholds().setRecord(engineConfig));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedThreshold_false() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Min_Intake_Manifold_Pressure__c = -20,
                Max_Intake_Manifold_Pressure__c = 25
        );
        EPD_FormStructureBlock block = prepareBlock(EPD_FieldDefaultsGeneral.CLASS_NAME);
        Boolean expectedResult = false;
        Boolean result;

        Test.startTest();
        try {
            result = block.hasReachedThreshold(new EPD_FormStructureThresholds().setRecord(engineConfig));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedRetrospectiveThreshold_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_FormStructureBlock block = prepareBlock(EPD_FieldDefaultsGeneral.CLASS_NAME);
        Boolean expectedResult;
        Boolean result;

        Test.startTest();
        try {
            result = block.hasReachedRetrospectiveThreshold(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedRetrospectiveThreshold_false() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBlock block = prepareBlock(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_FormStructureBlock block2 = prepareBlock(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Constants.SETTINGS.Compression_Minor_Threshold__c = 20;
        Boolean expectedResult = false;
        Boolean result;

        Test.startTest();
        try {
            result = block.hasReachedRetrospectiveThreshold(block2);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedRetrospectiveThreshold_true() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBlock block = prepareBlock(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_FormStructureBlock block2 = prepareBlock(EPD_FieldDefaultsWrong.CLASS_NAME);
        EPD_Constants.SETTINGS.Compression_Minor_Threshold__c = 20;
        Boolean expectedResult = true;
        Boolean result;

        Test.startTest();
        try {
            result = block.hasReachedRetrospectiveThreshold(block2);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getRetrospectiveThresholdWarnings_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_FormStructureBlock block = prepareBlock(EPD_FieldDefaultsGeneral.CLASS_NAME);
        String expectedResult;
        String result;

        Test.startTest();
        try {
            result = block.getRetrospectiveThresholdWarnings(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getRetrospectiveThresholdWarnings_withoutError() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBlock block = prepareBlock(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_FormStructureBlock block2 = prepareBlock(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_Constants.SETTINGS.Compression_Minor_Threshold__c = 20;
        String expectedResult = '';
        String result;

        Test.startTest();
        try {
            result = block.getRetrospectiveThresholdWarnings(block2);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getRetrospectiveThresholdWarnings_withError() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBlock block = prepareBlock(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_FormStructureBlock block2 = prepareBlock(EPD_FieldDefaultsWrong.CLASS_NAME);
        EPD_Constants.SETTINGS.Compression_Minor_Threshold__c = 20;
        String result;

        Test.startTest();
        try {
            result = block.getRetrospectiveThresholdWarnings(block2);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assert(String.isNotBlank(result));
    }

    private static EPD_FormStructureBlock prepareBlock(String className) {
        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo);
        block.cylinders = new List<EPD_FormStructureCylinder> {
                (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                        (EPD_Cylinder_Information__c) HOG_SObjectFactory.createSObject(
                                new EPD_Cylinder_Information__c(),
                                className,
                                false
                        )
                )
        };
        return block;
    }

    @IsTest
    static void testClassicGettersAndSetters_emptyValues() {
        EPD_FormStructureBlock structure = new EPD_FormStructureBlock();
        structure.exhaustO2Classic = '';
        structure.intakeManifoldPressureClassic = '';

        System.assertEquals(null, structure.exhaustO2Classic);
        System.assertEquals(null, structure.intakeManifoldPressureClassic);
    }

    @IsTest
    static void testClassicGettersAndSetters_properValues() {
        EPD_FormStructureBlock structure = new EPD_FormStructureBlock();
        structure.exhaustO2Classic = '1';
        structure.intakeManifoldPressureClassic = '1';

        System.assertEquals('1', structure.exhaustO2Classic);
        System.assertEquals('1', structure.intakeManifoldPressureClassic);
    }

    @IsTest
    static void testClassicGettersAndSetters_wrongValues() {
        EPD_FormStructureBlock structure = new EPD_FormStructureBlock();

        Boolean exhaustO2ClassicFail = false;
        try {
            structure.exhaustO2Classic = 'abc';
        } catch(Exception ex) {
            exhaustO2ClassicFail = true;
        }

        Boolean intakeManifoldPressureClassicFail = false;
        try {
            structure.intakeManifoldPressureClassic = 'abc';
        } catch(Exception ex) {
            intakeManifoldPressureClassicFail = true;
        }

        System.assertEquals(true, exhaustO2ClassicFail);
        System.assertEquals(true, intakeManifoldPressureClassicFail);
    }

    //TODO remove after LTNG migration
    @IsTest
    static void toStringTest() {
        EPD_FormStructureBlock structure = new EPD_FormStructureBlock();
        structure.cylinders = new List<EPD_FormStructureCylinder>();
        String structureString = '';

        Test.startTest();
        structureString = structure.toString();
        Test.stopTest();

        System.assert(String.isNotBlank(structureString));
    }

}