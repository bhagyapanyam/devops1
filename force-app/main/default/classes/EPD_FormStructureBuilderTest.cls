/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_FormStructureBuilder
History:        jschn 2019-07-12 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_FormStructureBuilderTest {

    @IsTest
    static void setWorkOrder_withoutParam() {
        HOG_Maintenance_Servicing_Form__c wo;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBuilder builder;

        Test.startTest();
        try {
            builder = new EPD_FormStructureBuilder().setWorkOrder(wo);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, builder);
    }

    @IsTest
    static void setWorkOrder_withParam() {
        HOG_Maintenance_Servicing_Form__c wo = new HOG_Maintenance_Servicing_Form__c();
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBuilder builder;

        Test.startTest();
        try {
            builder = new EPD_FormStructureBuilder().setWorkOrder(wo);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, builder);
    }

    @IsTest
    static void setWorkOrderActivity_withoutParam() {
        Work_Order_Activity__c woa;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBuilder builder;

        Test.startTest();
        try {
            builder = new EPD_FormStructureBuilder().setWorkOrderActivity(woa);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, builder);
    }

    @IsTest
    static void setWorkOrderActivity_withParam() {
        Work_Order_Activity__c woa = new Work_Order_Activity__c();
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBuilder builder;

        Test.startTest();
        try {
            builder = new EPD_FormStructureBuilder().setWorkOrderActivity(woa);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, builder);
    }

    @IsTest
    static void setEngine_withoutParam() {
        Equipment_Engine__c engine;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBuilder builder;

        Test.startTest();
        try {
            builder = new EPD_FormStructureBuilder().setEngine(engine);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, builder);
    }

    @IsTest
    static void setEngine_withParam() {
        Equipment_Engine__c engine = new Equipment_Engine__c();
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBuilder builder;

        Test.startTest();
        try {
            builder = new EPD_FormStructureBuilder().setEngine(engine);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, builder);
    }

    @IsTest
    static void setCompany_withoutParam() {
        Account company;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBuilder builder;

        Test.startTest();
        try {
            builder = new EPD_FormStructureBuilder().setCompany(company);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, builder);
    }

    @IsTest
    static void setCompany_withParam() {
        Account company = new Account();
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBuilder builder;

        Test.startTest();
        try {
            builder = new EPD_FormStructureBuilder().setCompany(company);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, builder);
    }

    @IsTest
    static void setConfig_withoutParam() {
        EPD_Engine__mdt config;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBuilder builder;

        Test.startTest();
        try {
            builder = new EPD_FormStructureBuilder().setConfig(config);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, builder);
    }

    @IsTest
    static void setConfig_withParam() {
        EPD_Engine__mdt config = new EPD_Engine__mdt();
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureBuilder builder;

        Test.startTest();
        try {
            builder = new EPD_FormStructureBuilder().setConfig(config);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, builder);
    }

    @IsTest
    static void build_withoutParams() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_FormStructure result;

        Test.startTest();
        try {
            result = new EPD_FormStructureBuilder().build();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void build_withConfigParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_FormStructure result;
        EPD_Engine__mdt config = new EPD_Engine__mdt(
                Required_Exhaust_Measurements__c = true,
                Number_of_COC_Stages__c = 4,
                Number_of_Cylinders__c = 6,
                Measurement_Type__c = 'Wear'
        );

        Test.startTest();
        try {
            result = new EPD_FormStructureBuilder()
                    .setConfig(config)
                    .build();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void build_withWorkOrderParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_FormStructure result;
        EPD_Engine__mdt config = new EPD_Engine__mdt(
                Required_Exhaust_Measurements__c = true,
                Number_of_COC_Stages__c = 4,
                Number_of_Cylinders__c = 6,
                Measurement_Type__c = 'Wear'
        );
        HOG_Maintenance_Servicing_Form__c wo = new HOG_Maintenance_Servicing_Form__c(
                Planner_Group__c = '100'
        );

        Test.startTest();
        try {
            result = new EPD_FormStructureBuilder()
                    .setConfig(config)
                    .setWorkOrder(wo)
                    .build();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void build_withWorkOrderActivityParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_FormStructure result;
        EPD_Engine__mdt config = new EPD_Engine__mdt(
                Required_Exhaust_Measurements__c = true,
                Number_of_COC_Stages__c = 4,
                Number_of_Cylinders__c = 6,
                Measurement_Type__c = 'Wear'
        );
        HOG_Maintenance_Servicing_Form__c wo = new HOG_Maintenance_Servicing_Form__c(
                Planner_Group__c = '100'
        );
        Work_Order_Activity__c woa = new Work_Order_Activity__c();

        Test.startTest();
        try {
            result = new EPD_FormStructureBuilder()
                    .setConfig(config)
                    .setWorkOrder(wo)
                    .setWorkOrderActivity(woa)
                    .build();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void build_withCompanyParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_FormStructure result;
        EPD_Engine__mdt config = new EPD_Engine__mdt(
                Required_Exhaust_Measurements__c = true,
                Number_of_COC_Stages__c = 4,
                Number_of_Cylinders__c = 6,
                Measurement_Type__c = 'Wear'
        );
        HOG_Maintenance_Servicing_Form__c wo = new HOG_Maintenance_Servicing_Form__c(
                Planner_Group__c = '100'
        );
        Work_Order_Activity__c woa = new Work_Order_Activity__c();
        Account acc = new Account();

        Test.startTest();
        try {
            result = new EPD_FormStructureBuilder()
                    .setConfig(config)
                    .setWorkOrder(wo)
                    .setWorkOrderActivity(woa)
                    .setCompany(acc)
                    .build();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void build_withEngineParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructure result;
        EPD_Engine__mdt config = new EPD_Engine__mdt(
                Required_Exhaust_Measurements__c = true,
                Number_of_COC_Stages__c = 4,
                Number_of_Cylinders__c = 6,
                Measurement_Type__c = 'Wear'
        );
        HOG_Maintenance_Servicing_Form__c wo = new HOG_Maintenance_Servicing_Form__c(
                Planner_Group__c = '100'
        );
        Work_Order_Activity__c woa = new Work_Order_Activity__c();
        Account acc = new Account();
        Equipment_Engine__c engine = new Equipment_Engine__c();

        Test.startTest();
        try {
            result = new EPD_FormStructureBuilder()
                    .setConfig(config)
                    .setWorkOrder(wo)
                    .setWorkOrderActivity(woa)
                    .setCompany(acc)
                    .setEngine(engine)
                    .build();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.epd);
        System.assertEquals('N/A', result.epd.operationGroup);
    }

    @IsTest
    static void build_withProductStrategyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructure result;
        EPD_Engine__mdt config = new EPD_Engine__mdt(
                Required_Exhaust_Measurements__c = true,
                Number_of_COC_Stages__c = 4,
                Number_of_Cylinders__c = 6,
                Measurement_Type__c = 'Wear'
        );
        HOG_Maintenance_Servicing_Form__c wo = new HOG_Maintenance_Servicing_Form__c(
                Planner_Group__c = '100',
                Location__r = new Location__c(
                        Product_Strategy__c = 'CHOPS'
                )
        );
        Work_Order_Activity__c woa = new Work_Order_Activity__c();
        Account acc = new Account();
        Equipment_Engine__c engine = new Equipment_Engine__c();

        Test.startTest();
        try {
            result = new EPD_FormStructureBuilder()
                    .setConfig(config)
                    .setWorkOrder(wo)
                    .setWorkOrderActivity(woa)
                    .setCompany(acc)
                    .setEngine(engine)
                    .build();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.epd);
        System.assertEquals('CHOPS', result.epd.operationGroup);
    }

    @IsTest
    static void build_withInlineEngine() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Integer expectedNumOfBlocks = 1;
        EPD_FormStructure result;
        EPD_Engine__mdt config = new EPD_Engine__mdt(
                Required_Exhaust_Measurements__c = true,
                Number_of_COC_Stages__c = 4,
                Number_of_Cylinders__c = 6,
                Measurement_Type__c = 'Wear',
                Engine_Configuration__c = EPD_Constants.ENGINE_CONFIG_STRAIGHT
        );
        HOG_Maintenance_Servicing_Form__c wo = new HOG_Maintenance_Servicing_Form__c(
                Planner_Group__c = '100',
                Location__r = new Location__c(
                        Product_Strategy__c = 'CHOPS'
                )
        );
        Work_Order_Activity__c woa = new Work_Order_Activity__c();
        Account acc = new Account();
        Equipment_Engine__c engine = new Equipment_Engine__c();

        Test.startTest();
        try {
            result = new EPD_FormStructureBuilder()
                    .setConfig(config)
                    .setWorkOrder(wo)
                    .setWorkOrderActivity(woa)
                    .setCompany(acc)
                    .setEngine(engine)
                    .build();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.engineBlocks);
        System.assertEquals(expectedNumOfBlocks, result.engineBlocks.size());
    }

    @IsTest
    static void build_withVShapedEngine() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Integer expectedNumOfBlocks = 2;
        EPD_FormStructure result;
        EPD_Engine__mdt config = new EPD_Engine__mdt(
                Required_Exhaust_Measurements__c = true,
                Number_of_COC_Stages__c = 4,
                Number_of_Cylinders__c = 6,
                Measurement_Type__c = 'Wear',
                Engine_Configuration__c = EPD_Constants.ENGINE_CONFIG_V_SHAPED
        );
        HOG_Maintenance_Servicing_Form__c wo = new HOG_Maintenance_Servicing_Form__c(
                Planner_Group__c = '100',
                Location__r = new Location__c(
                        Product_Strategy__c = 'CHOPS'
                )
        );
        Work_Order_Activity__c woa = new Work_Order_Activity__c();
        Account acc = new Account();
        Equipment_Engine__c engine = new Equipment_Engine__c();

        Test.startTest();
        try {
            result = new EPD_FormStructureBuilder()
                    .setConfig(config)
                    .setWorkOrder(wo)
                    .setWorkOrderActivity(woa)
                    .setCompany(acc)
                    .setEngine(engine)
                    .build();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.engineBlocks);
        System.assertEquals(expectedNumOfBlocks, result.engineBlocks.size());
    }

    @IsTest
    static void build_withConfigAndParts() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructure result;
        EPD_Engine__mdt config = new EPD_EngineConfigSelector().getEngineConfig('UnitTestData', 'UnitTestData').get(0);
        HOG_Maintenance_Servicing_Form__c wo = new HOG_Maintenance_Servicing_Form__c(
                Planner_Group__c = '100',
                Location__r = new Location__c(
                        Product_Strategy__c = 'CHOPS'
                )
        );
        Work_Order_Activity__c woa = new Work_Order_Activity__c();
        Account acc = new Account();
        Equipment_Engine__c engine = new Equipment_Engine__c();

        Test.startTest();
        try {
            result = new EPD_FormStructureBuilder()
                    .setConfig(config)
                    .setWorkOrder(wo)
                    .setWorkOrderActivity(woa)
                    .setCompany(acc)
                    .setEngine(engine)
                    .build();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.parts);
        System.assertEquals(config.Engine_Parts__r.size(), result.parts.size());
    }

    @IsTest
    static void build_withConfigAndParts_VShapedWith2EOC() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructure result;
        EPD_Engine__mdt config = new EPD_EngineConfigSelector().getEngineConfig('UnitTestData', 'UnitTestData').get(0);
        config.EOC_Fuel_Pressure_Per_Block__c = true;
        config.Engine_Configuration__c = EPD_Constants.ENGINE_CONFIG_V_SHAPED;
        HOG_Maintenance_Servicing_Form__c wo = new HOG_Maintenance_Servicing_Form__c(
                Planner_Group__c = '100',
                Location__r = new Location__c(
                        Product_Strategy__c = 'CHOPS'
                )
        );
        Work_Order_Activity__c woa = new Work_Order_Activity__c();
        Account acc = new Account();
        Equipment_Engine__c engine = new Equipment_Engine__c();

        Test.startTest();
        try {
            result = new EPD_FormStructureBuilder()
                    .setConfig(config)
                    .setWorkOrder(wo)
                    .setWorkOrderActivity(woa)
                    .setCompany(acc)
                    .setEngine(engine)
                    .build();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.parts);
        System.assertEquals(config.Engine_Parts__r.size(), result.parts.size());
        System.assertNotEquals(null, result.EOCs);
        System.assertEquals(2, result.EOCs.size());
    }

}