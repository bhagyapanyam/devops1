/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Form structure for Cylinder Information SObject.
Test Class:     EPD_FormStructureCylinderTest
History:        jschn 2019-06-25 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_FormStructureCylinder extends EPD_FormStructureBase {

    private static final String CLASS_NAME = String.valueOf(EPD_FormStructureCylinder.class);

    @AuraEnabled
    public Id recordId;

    @AuraEnabled
    public Id blockInformationId;

    @AuraEnabled
    public Id recordTypeId;

    @AuraEnabled
    public Decimal cylinderNumber {get;set;}

    @AuraEnabled
    public Boolean headReplacement {get;set;}

    @AuraEnabled
    public Decimal wearExhaust;
    //TODO remove after LTNG migration
    public String wearExhaustClassic {
        get {
            return String.valueOf(wearExhaust);
        }
        set {
            wearExhaustClassic = value;
            if(String.isNotBlank(value)) {
                wearExhaust = Decimal.valueOf(value);
            }
        }
    }
    public Boolean wearExhaustError {get;set;}

    @AuraEnabled
    public Decimal wearIntake;
    //TODO remove after LTNG migration
    public String wearIntakeClassic {
        get {
            return String.valueOf(wearIntake);
        }
        set {
            wearIntakeClassic = value;
            if(String.isNotBlank(value)) {
                wearIntake = Decimal.valueOf(value);
            }
        }
    }
    public Boolean wearIntakeError {get;set;}

    @AuraEnabled
    public Decimal compression;
    //TODO remove after LTNG migration
    public String compressionClassic {
        get {
            return String.valueOf(compression);
        }
        set {
            compressionClassic = value;
            if(String.isNotBlank(value)) {
                compression = Decimal.valueOf(value);
            }
        }
    }
    public Boolean compressionError {get;set;}

    @AuraEnabled
    public Decimal exhaustTemperature;
    //TODO remove after LTNG migration
    public String exhaustTemperatureClassic {
        get {
            return String.valueOf(exhaustTemperature);
        }
        set {
            exhaustTemperatureClassic = value;
            if(String.isNotBlank(value)) {
                exhaustTemperature = Decimal.valueOf(value);
            }
        }
    }

    /**
     * Maps local variables into Cylinder Information record.
     *
     * @return SObject (EPD_Cylinder_Information__c)
     */
    public override SObject getRecord() {
        System.debug(CLASS_NAME + ' -> getRecord. Cylinder Structure: ' + JSON.serialize(this));

        return new EPD_Cylinder_Information__c(
                Id = recordId,
                Block_Information__c = blockInformationId,
                RecordTypeId = recordTypeId,
                Cylinder_Number__c = cylinderNumber,
                Head_Replacement__c = headReplacement,
                Wear_Exhaust__c = wearExhaust,
                Wear_Intake__c = wearIntake,
                Compression__c = compression,
                Exhaust_Temperature__c = exhaustTemperature
        );
    }

    /**
     * Maps Cylinder Information record into local variables.
     *
     * @param obj
     *
     * @return EPD_FormStructureBase
     */
    public override EPD_FormStructureBase setRecord(SObject obj) {
        System.debug(CLASS_NAME + ' -> setRecord. START. Object param: ' + JSON.serialize(obj));

        wearExhaustError = false;
        wearIntakeError = false;
        compressionError = false;

        EPD_Cylinder_Information__c cylinderInformation = (EPD_Cylinder_Information__c) obj;
        recordId = cylinderInformation.Id;
        blockInformationId = cylinderInformation.Block_Information__c;
        recordTypeId = cylinderInformation.RecordTypeId;
        cylinderNumber = cylinderInformation.Cylinder_Number__c;
        headReplacement = cylinderInformation.Head_Replacement__c;
        wearExhaust = cylinderInformation.Wear_Exhaust__c;
        wearIntake = cylinderInformation.Wear_Intake__c;
        compression = cylinderInformation.Compression__c;
        exhaustTemperature = cylinderInformation.Exhaust_Temperature__c;

        System.debug(CLASS_NAME + ' -> setRecord. END. Cylinder Structure: ' + JSON.serialize(this));
        return this;
    }

    /**
     * Checks required values against thresholds and if thresholds are reached it will build string with warning.
     * Currently used only for Threshold Alert email.
     *
     * @param thresholds
     *
     * @return String
     */
    public String getThresholdWarnings(EPD_FormStructureThresholds thresholds){
        String cylinderThresholdWarnings = '';

        if(EPD_Utility.isBigger(wearIntake, thresholds.maxValveSeatRecession)) {
            cylinderThresholdWarnings += getWearIntakeThresholdWarning();
        }

        if(EPD_Utility.isBigger(wearExhaust, thresholds.maxValveSeatRecession)) {
            cylinderThresholdWarnings += getWearExhaustThresholdWarning();
        }

        if(EPD_Utility.isOutsideOfRange(compression, thresholds.minCylinderCompression, thresholds.maxCylinderCompression)) {
            cylinderThresholdWarnings += getCompressionThresholdWarning();
        }

        return cylinderThresholdWarnings;
    }

    /**
     * Builds Threshold warning string for Wear Intake.
     *
     * @return String
     */
    private String getWearIntakeThresholdWarning(

    ) {
        return String.format(
                EPD_Constants.THRESHOLD_EMAIL_WARNING_BASE,
                new List<String> {
                        getCylNumberForEmail(),
                        'Intake valve - recession',
                        String.valueOf(wearIntake) + '"'
                }
        );
    }

    /**
     * Builds Threshold warning string for Wear Exhaust.
     *
     * @return String
     */
    private String getWearExhaustThresholdWarning() {
        return String.format(
                EPD_Constants.THRESHOLD_EMAIL_WARNING_BASE,
                new List<String> {
                        getCylNumberForEmail(),
                        'Exhaust valve - recession',
                        String.valueOf(wearExhaust) + '"'
                }
        );
    }

    /**
     * Builds Threshold warning string for Compression.
     *
     * @return String
     */
    private String getCompressionThresholdWarning() {
        return String.format(
                EPD_Constants.THRESHOLD_EMAIL_WARNING_BASE,
                new List<String> {
                        getCylNumberForEmail(),
                        'Compression Test',
                        String.valueOf(compression) + 'psi'
                }
        );
    }

    /**
     * Builds Cylinder # string for Threshold Email alert.
     *
     * @return String
     */
    private String getCylNumberForEmail() {
        return 'Cylinder #' + String.valueOf(cylinderNumber);
    }

    /**
     * Checks if Cylinder measurements reached engine thresholds.
     *
     * @return Boolean
     */
    public Boolean hasReachedThreshold(EPD_FormStructureThresholds thresholds) {
        return EPD_Utility.isBigger(wearIntake, thresholds.maxValveSeatRecession)
                || EPD_Utility.isBigger(wearExhaust, thresholds.maxValveSeatRecession)
                || EPD_Utility.isOutsideOfRange(compression, thresholds.minCylinderCompression, thresholds.maxCylinderCompression);
    }

    /**
     * Checks if Cylinder has reached retrospective threshold (if it was changed by more % then is set on Custom settings
     *
     * @param previousCylinder
     *
     * @return Boolean
     */
    public Boolean hasReachedRetrospectiveThreshold(EPD_FormStructureCylinder previousCylinder) {
        return EPD_Utility.isLessThenPreviousWithCoefficient(
                previousCylinder.compression,
                compression,
                EPD_Constants.SETTINGS.Compression_Minor_Threshold__c
        );
    }

    /**
     * It will build retrospective threshold warning message if cylinder crossed this threshold.
     *
     * @param previousCylinder
     *
     * @return String
     */
    public String getRetrospectiveThresholdWarnings(EPD_FormStructureCylinder previousCylinder){
        String cylinderThresholdWarnings = '';

        if(hasReachedRetrospectiveThreshold(previousCylinder)) {
            cylinderThresholdWarnings = String.format(
                    EPD_Constants.RETROSPECTIVE_THRESHOLD_WARNING_BASE,
                    new List<String> {
                            getCylNumberForEmail(),
                            'Compression Test',
                            String.valueOf(previousCylinder.compression) + 'psi',
                            String.valueOf(compression) + 'psi'
                    }
            );
        }

        return cylinderThresholdWarnings;
    }

    //TODO remove after LTNG migration
    public override String toString() {
        String contentBuildingString = '';
        contentBuildingString += '"recordId":' + (String.isNotBlank(recordId) ? ('"' + recordId + '"') : 'null') + ',';
        contentBuildingString += '"blockInformationId":' + (String.isNotBlank(blockInformationId) ? ('"' + blockInformationId + '"') : 'null') + ',';
        contentBuildingString += '"recordTypeId":' + (String.isNotBlank(recordTypeId) ? ('"' + recordTypeId + '"') : 'null') + ',';
        contentBuildingString += '"cylinderNumber":' + cylinderNumber + ',';
        contentBuildingString += '"headReplacement":' + headReplacement + ',';
        contentBuildingString += '"wearExhaust":' + wearExhaust + ',';
        contentBuildingString += '"wearIntake":' + wearIntake + ',';
        contentBuildingString += '"compression":' + compression + ',';
        contentBuildingString += '"exhaustTemperature":' + exhaustTemperature;
        return '{' + contentBuildingString + '}';
    }

}