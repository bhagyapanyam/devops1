/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_FormStructureEOC
History:        jschn 2019-07-24 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_FormStructureEOCTest {

    @IsTest
    static void setRecord_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Engine_Operating_Condition__c EOCRecord;
        EPD_FormStructureEOC result;

        Test.startTest();
        try {
            result = (EPD_FormStructureEOC) new EPD_FormStructureEOC().setRecord(EOCRecord);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void setRecord_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Operating_Condition__c EOCRecord = new EPD_Engine_Operating_Condition__c();
        EPD_FormStructureEOC result;

        Test.startTest();
        try {
            result = (EPD_FormStructureEOC) new EPD_FormStructureEOC().setRecord(EOCRecord);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(EOCRecord.Block_Name__c, result.blockName);
        System.assertEquals(EOCRecord.Pre_Chamber_Fuel_Pressure__c, result.preChamberFuelPressure);
        System.assertEquals(EOCRecord.Regulated_Fuel_Pressure__c, result.regulatedFuelPressure);
        System.assertEquals(EOCRecord.Supplied_Fuel_Pressure__c, result.suppliedFuelPressure);
    }

    @IsTest
    static void setRecord_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Operating_Condition__c EOCRecord = (EPD_Engine_Operating_Condition__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Operating_Condition__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructureEOC result;

        Test.startTest();
        try {
            result = (EPD_FormStructureEOC) new EPD_FormStructureEOC().setRecord(EOCRecord);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(EOCRecord.Block_Name__c, result.blockName);
        System.assertEquals(EOCRecord.Pre_Chamber_Fuel_Pressure__c, result.preChamberFuelPressure);
        System.assertEquals(EOCRecord.Regulated_Fuel_Pressure__c, result.regulatedFuelPressure);
        System.assertEquals(EOCRecord.Supplied_Fuel_Pressure__c, result.suppliedFuelPressure);
    }

    @IsTest
    static void setRecord_inputLabelEmpty() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Operating_Condition__c EOCRecord = (EPD_Engine_Operating_Condition__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Operating_Condition__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EOCRecord.Block_Name__c = EPD_Constants.EOC_BLOCK_NAME_EMPTY;
        EPD_FormStructureEOC result;

        Test.startTest();
        try {
            result = (EPD_FormStructureEOC) new EPD_FormStructureEOC().setRecord(EOCRecord);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(EPD_Constants.EOC_BLOCK_NAME_TO_INPUT_LABEL_MAP.get(EOCRecord.Block_Name__c), result.inputLabelPostfix);
    }

    @IsTest
    static void setRecord_inputLabelLeft() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Operating_Condition__c EOCRecord = (EPD_Engine_Operating_Condition__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Operating_Condition__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EOCRecord.Block_Name__c = EPD_Constants.EOC_BLOCK_NAME_LEFT;
        EPD_FormStructureEOC result;

        Test.startTest();
        try {
            result = (EPD_FormStructureEOC) new EPD_FormStructureEOC().setRecord(EOCRecord);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(EPD_Constants.EOC_BLOCK_NAME_TO_INPUT_LABEL_MAP.get(EOCRecord.Block_Name__c), result.inputLabelPostfix);
    }

    @IsTest
    static void setRecord_inputLabelRight() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Operating_Condition__c EOCRecord = (EPD_Engine_Operating_Condition__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Operating_Condition__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EOCRecord.Block_Name__c = EPD_Constants.EOC_BLOCK_NAME_RIGHT;
        EPD_FormStructureEOC result;

        Test.startTest();
        try {
            result = (EPD_FormStructureEOC) new EPD_FormStructureEOC().setRecord(EOCRecord);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(EPD_Constants.EOC_BLOCK_NAME_TO_INPUT_LABEL_MAP.get(EOCRecord.Block_Name__c), result.inputLabelPostfix);
    }

    @IsTest
    static void getRecord_empty() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Operating_Condition__c EOCRecord = new EPD_Engine_Operating_Condition__c();
        EPD_Engine_Operating_Condition__c result;

        Test.startTest();
        try {
            result = (EPD_Engine_Operating_Condition__c) new EPD_FormStructureEOC().setRecord(EOCRecord).getRecord();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(EOCRecord.Block_Name__c, result.Block_Name__c);
        System.assertEquals(EOCRecord.Pre_Chamber_Fuel_Pressure__c, result.Pre_Chamber_Fuel_Pressure__c);
        System.assertEquals(EOCRecord.Regulated_Fuel_Pressure__c, result.Regulated_Fuel_Pressure__c);
        System.assertEquals(EOCRecord.Supplied_Fuel_Pressure__c, result.Supplied_Fuel_Pressure__c);
    }

    @IsTest
    static void getRecord_full() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Operating_Condition__c EOCRecord = (EPD_Engine_Operating_Condition__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Operating_Condition__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Engine_Operating_Condition__c result;

        Test.startTest();
        try {
            result = (EPD_Engine_Operating_Condition__c) new EPD_FormStructureEOC().setRecord(EOCRecord).getRecord();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(EOCRecord.Block_Name__c, result.Block_Name__c);
        System.assertEquals(EOCRecord.Pre_Chamber_Fuel_Pressure__c, result.Pre_Chamber_Fuel_Pressure__c);
        System.assertEquals(EOCRecord.Regulated_Fuel_Pressure__c, result.Regulated_Fuel_Pressure__c);
        System.assertEquals(EOCRecord.Supplied_Fuel_Pressure__c, result.Supplied_Fuel_Pressure__c);
    }

    @IsTest
    static void setFlag_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_FormStructureEOC result;

        Test.startTest();
        try {
            result = new EPD_FormStructureEOC().setFlag(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void setFlag_true() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureEOC result;
        Boolean flag = true;

        Test.startTest();
        try {
            result = new EPD_FormStructureEOC().setFlag(new EPD_Engine__mdt(Advanced_EOC__c = flag));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(flag, result.showPreChamberFuelPressure);
    }

    @IsTest
    static void setFlag_false() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureEOC result;
        Boolean flag = false;

        Test.startTest();
        try {
            result = new EPD_FormStructureEOC().setFlag(new EPD_Engine__mdt(Advanced_EOC__c = flag));
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(flag, result.showPreChamberFuelPressure);
    }

    @IsTest
    static void testClassicGettersAndSetters_emptyValues() {
        EPD_FormStructureEOC structure = new EPD_FormStructureEOC();
        structure.preChamberFuelPressureClassic = '';
        structure.regulatedFuelPressureClassic = '';
        structure.suppliedFuelPressureClassic = '';

        System.assertEquals(null, structure.preChamberFuelPressureClassic);
        System.assertEquals(null, structure.regulatedFuelPressureClassic);
        System.assertEquals(null, structure.suppliedFuelPressureClassic);
    }

    @IsTest
    static void testClassicGettersAndSetters_properValues() {
        EPD_FormStructureEOC structure = new EPD_FormStructureEOC();
        structure.preChamberFuelPressureClassic = '1';
        structure.regulatedFuelPressureClassic = '1';
        structure.suppliedFuelPressureClassic = '1';

        System.assertEquals('1', structure.preChamberFuelPressureClassic);
        System.assertEquals('1', structure.regulatedFuelPressureClassic);
        System.assertEquals('1', structure.suppliedFuelPressureClassic);
    }

    @IsTest
    static void testClassicGettersAndSetters_wrongValues() {
        EPD_FormStructureEOC structure = new EPD_FormStructureEOC();

        Boolean preChamberFuelPressureClassicFail = false;
        try {
            structure.preChamberFuelPressureClassic = 'abc';
        } catch(Exception ex) {
            preChamberFuelPressureClassicFail = true;
        }

        Boolean regulatedFuelPressureClassicFail = false;
        try {
            structure.regulatedFuelPressureClassic = 'abc';
        } catch(Exception ex) {
            regulatedFuelPressureClassicFail = true;
        }

        Boolean suppliedFuelPressureClassicFail = false;
        try {
            structure.suppliedFuelPressureClassic = 'abc';
        } catch(Exception ex) {
            suppliedFuelPressureClassicFail = true;
        }

        System.assertEquals(true, preChamberFuelPressureClassicFail);
        System.assertEquals(true, regulatedFuelPressureClassicFail);
        System.assertEquals(true, suppliedFuelPressureClassicFail);
    }

    //TODO remove after LTNG migration
    @IsTest
    static void toStringTest() {
        EPD_FormStructureEOC structure = new EPD_FormStructureEOC();
        String structureString = '';

        Test.startTest();
        structureString = structure.toString();
        Test.stopTest();

        System.assert(String.isNotBlank(structureString));
    }

}