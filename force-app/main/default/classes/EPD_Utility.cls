/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Utility class for Engine Performance Data logic.
Test Class:     EPD_UtilityTest
History:        jschn 2019-06-26 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_Utility {

    /**
     * Checks if there is just one record in provided list.
     *
     * @param objects
     *
     * @return Boolean
     */
    public static Boolean isSingleRecord(List<Object> objects) {
        return objects != null && objects.size() == 1;
    }

    /**
     * Checks if provided list is empty.
     *
     * @param objects
     *
     * @return Boolean
     */
    public static Boolean isEmptyList(List<Object> objects) {
        return objects == null || objects.size() == 0;
    }

    /**
     * Checks if value is outside of provided range.
     * There has to be value provided.
     *
     * @param value
     * @param bottomRange
     * @param topRange
     *
     * @return Boolean
     */
    public static Boolean isOutsideOfRange(Decimal value, Decimal bottomRange, Decimal topRange) {
        return value != null && (value < bottomRange || value > topRange);
    }

    /**
     * Checks if value is bigger then provided top range.
     * There has to be value provided.
     *
     * @param value
     * @param topRange
     *
     * @return Boolean
     */
    public static Boolean isBigger(Decimal value, Decimal topRange) {
        return value != null && value > topRange;
    }

    /**
     * Checks if previous value is bigger ten current multiplied by coeficient
     *
     * @param previousValue
     * @param currentValue
     * @param coefficient
     *
     * @return Boolean
     */
    public static Boolean isLessThenPreviousWithCoefficient(Decimal previousValue, Decimal currentValue, Decimal percentage) {
        return previousValue > currentValue * getCoefficientFromPercentage(percentage);
    }

    /**
     * Transform percentage value into coefficient.
     * 20% => 20.0 => 1.2
     *
     * @param percentage
     *
     * @return Decimal
     */
    public static Decimal getCoefficientFromPercentage(Decimal percentage) {
        return (0.01 * percentage) + 1;
    }

}