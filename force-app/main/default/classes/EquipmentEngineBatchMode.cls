/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Modes for Batch Job that updates and creates Equipment Engine records based on Equipment records.
                Modes are for determining how many objects should be queried for the job.
History:        jschn 2019-06-13 - Created. - EPD R1
*************************************************************************************************/
global enum EquipmentEngineBatchMode {

    ALL_RECORDS, //Used mostly in initial load or as data fix update job
    NEW_RECORDS

}