/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Schedulable job that run batch that creates and updates Equipment Engine records.
Test Class:     EquipmentEngineCreatorSchedulerTest
History:        jschn 2019-06-13 - Created. - EPD R1
*************************************************************************************************/
global with sharing class EquipmentEngineCreatorSchedulable implements Schedulable {

    public static final String CLASS_NAME = String.valueOf(EquipmentEngineCreatorSchedulable.class);

    public static final String JOB_NAME = 'Equipment Engine Creation' + (Test.isRunningTest() ? ' Test':'');

    private static final Integer BATCH_JOB_CHUNK_SIZE = 2000;

    global void execute(SchedulableContext sc) {
        Database.executeBatch(
                new EquipmentEngineCreatorBatch(EquipmentEngineBatchMode.NEW_RECORDS),
                BATCH_JOB_CHUNK_SIZE
        );
    }

}