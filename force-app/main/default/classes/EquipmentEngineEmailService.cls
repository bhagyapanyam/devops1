/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Email Service that serves for notifying target email that some Engines failed to
                be created or updated.
Test Class:     EquipmentEngineEmailServiceTest
History:        jschn 2019-07-18 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EquipmentEngineEmailService {

    private static final String EMAIL_SUBJECT = 'Equipment Engine create/update notification: error occurred.';
    private static String EMAIL_CONTENT = 'Error occurred during attempt to create/update Equipment Engines.<br/>' +
            '<br/>' +
            'List of records that caused fail:<br/>' +
            '<ul>{0}</ul><br/>' +
            '<br/>' +
            'Best Regards,<br/>' +
            'Husky Energy';

    /**
     * It tries to build email with Failed engines. If succeeded it will send email, if not it won't.
     *
     * @param results
     * @param engines
     */
    public void sendNotificationOnFail(List<Database.SaveResult> results, List<Equipment_Engine__c> engines) {
        Messaging.SingleEmailMessage email = buildMessage(results, engines);
        if(email != null) {
            Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{
                    email
            });
        }
    }

    /**
     * It tries to build content for email. If content is empty it will return null. If content is present, it will
     * set appropriate configuration(sender, recipient, subject, etc) and returns generated email.
     *
     * @param results
     * @param engines
     *
     * @return Messaging.SingleEmailMessage
     */
    private Messaging.SingleEmailMessage buildMessage(List<Database.SaveResult> results, List<Equipment_Engine__c> engines) {
        Messaging.SingleEmailMessage email = null;
        String content = buildContent(results, engines);

        if(String.isNotBlank(content)) {

            email = new Messaging.SingleEmailMessage();
            email.setSaveAsActivity(false);
            email.setToAddresses(getRecipients());
            email.setSubject(EMAIL_SUBJECT);
            email.setHtmlBody(content);
            if(!Test.isRunningTest()) email.setOrgWideEmailAddressId(EPD_Constants.SENDER_ID);

        }

        return email;
    }

    /**
     * It will try to extract Error List from failed results. If succeeded it will build content and return it.
     * If not it will return empty String.
     *
     * @param failedResults
     * @param engines
     *
     * @return String
     */
    private String buildContent(List<Database.SaveResult> failedResults, List<Equipment_Engine__c> engines) {
        String content = '';
        String errorList = buildErrorList(failedResults, engines);

        if(String.isNotBlank(errorList)) {
            content = String.format(
                    EMAIL_CONTENT,
                    new List<String> {
                            errorList
                    }
            );
        }

        return content;
    }

    /**
     * It will go through failed results and extracts unsuccessful results into HTML List
     *
     * @param failedResults
     * @param engines
     *
     * @return String
     */
    private String buildErrorList(List<Database.SaveResult> failedResults, List<Equipment_Engine__c> engines) {
        String errorList = '';
        Integer i = 0;
        for(Database.SaveResult result : failedResults) {
            if(!result.isSuccess()) {
                errorList += '<li>';
                errorList += engines.get(0).Equipment__c + ' - ' + buildErrors(result.getErrors());
                errorList += '</li>';
            }
            i++;
        }
        return errorList;
    }

    /**
     * It will extract errors into String.
     *
     * @param errors
     *
     * @return String
     */
    private String buildErrors(List<Database.Error> errors){
        String errorList = '';

        for(Database.Error error : errors) {
            if(String.isNotBlank(errorList)) {
                errorList += '.';
            }
            errorList += ' ' + error.getMessage();
        }

        return errorList;
    }

    /**
     * It will build recipients list based on EPD Settings.
     *
     * @return List<String>
     */
    private List<String> getRecipients() {
        return EPD_Constants.SETTINGS.Equipment_Engine_Persist_Fail_Addresses__c.split(';');
    }

}