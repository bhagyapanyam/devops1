/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EquipmentEngineEmailService
History:        jschn 2019-07-25 - Created.
*************************************************************************************************/
@IsTest
private class EquipmentEngineEmailServiceTest {

    @IsTest
    static void sendNotificationOnFail_withoutParams() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        Test.startTest();
        try {
            new EquipmentEngineEmailService().sendNotificationOnFail(null, null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void sendNotificationOnFail_withParam1() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;

        Test.startTest();
        try {
            new EquipmentEngineEmailService().sendNotificationOnFail(new List<Database.SaveResult>(), null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag); //but still no message has been sent.
    }

    @IsTest
    static void sendNotificationOnFail_withParam2() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        Test.startTest();
        try {
            new EquipmentEngineEmailService().sendNotificationOnFail(null, new List<Equipment_Engine__c>());
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void sendNotificationOnFail_withOnlySuccessResults() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD();
        List<Equipment_Engine__c> engines = [SELECT Equipment__c FROM Equipment_Engine__c WHERE Id =: epd.Equipment_Engine__c];
        List<Database.SaveResult> results = Database.update(engines, false);

        Test.startTest();
        try {
            new EquipmentEngineEmailService().sendNotificationOnFail(results, engines);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void sendNotificationOnFail_withFailResults_withoutSettings() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD();
        List<Equipment_Engine__c> engines = [SELECT Equipment__c FROM Equipment_Engine__c WHERE Id =: epd.Equipment_Engine__c];
        for(Equipment_Engine__c engine : engines) {
            engine.Id = null;
        }
        List<Database.SaveResult> results = Database.update(engines, false);

        Test.startTest();
        try {
            new EquipmentEngineEmailService().sendNotificationOnFail(results, engines);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void sendNotificationOnFail_withFailResults_2Recs() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        HOG_SObjectFactory.createSObject(
                new HOG_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                true
        );
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD();
        List<Equipment_Engine__c> engines = [SELECT Equipment__c FROM Equipment_Engine__c WHERE Id =: epd.Equipment_Engine__c];
        engines.addAll([SELECT Equipment__c FROM Equipment_Engine__c WHERE Id =: epd.Equipment_Engine__c]);
        for(Equipment_Engine__c engine : engines) {
            engine.Id = null;
        }
        List<Database.SaveResult> results = Database.update(engines, false);

        Test.startTest();
        try {
            new EquipmentEngineEmailService().sendNotificationOnFail(results, engines);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

}