/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Selector class containing all queries for Equipment_Engine__c SObject
Test Class:     EquipmentEngineSelectorTest
History:        jschn 2019-05-26 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EquipmentEngineSelector implements EquipmentEngineDAO {

    /**
     * Method that queries list of Equipment Engine records based on parent Equipment ID.
     *
     * @param equipmentId
     *
     * @return List<Equipment_Engine__c>
     */
    public List<Equipment_Engine__c> getEngineByEquipmentId(Id equipmentId) {
        return [
                SELECT Name, Model__c, Manufacturer__c, Equipment__c, Capacity__c, DLFL__c, Serial_Number__c,
                        Tag__c
                FROM Equipment_Engine__c
                WHERE Equipment__c =: equipmentId
        ];
    }

    /**
     * Method that queries list of Equipment Engine records based on set of Equipment record IDs.
     *
     * @param equipmentIds
     *
     * @return List<Equipment_Engine__c>
     */
    public List<Equipment_Engine__c> getEnginesByEquipmentIds(Set<Id> equipmentIds) {
        return [
                SELECT Name,
                        Equipment__c, Equipment__r.Manufacturer__c, Equipment__r.Model_Number__c,
                        Equipment__r.Tag_Number__c, Equipment__r.Manufacturer_Serial_No__c
                FROM Equipment_Engine__c
                WHERE Equipment__c IN : equipmentIds
        ];
    }

}