/*
 *  Author:         Maros Grajcar
 *  Company:        Husky Energy
 *  Description:
 *  History:        Created on 10/18/2018
 */

global class EquipmentExpirationSchedulable implements Schedulable {

	global void execute(SchedulableContext sc) {
		runNotificationBatchJobs();
	}

	private void runNotificationBatchJobs() {
		Database.executeBatch(new EquipmentExpirationBatchJob('statusRejected'));
		Database.executeBatch(new EquipmentExpirationBatchJob('statusOpen'));
		Database.executeBatch(new EquipmentExpirationBatchJob('sendNotification'));
	}

}