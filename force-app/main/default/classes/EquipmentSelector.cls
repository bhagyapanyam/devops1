/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Selector class for querying Equipment records
Test Class:     EquipmentSelectorTest
History:        jschn 2019-07-01 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EquipmentSelector implements EquipmentDAO {

    /**
     * Queries Equipment record based on set of IDs.
     *
     * @param equipmentIds
     *
     * @return List<Equipment__c>
     */
    public List<Equipment__c> getEquipments(Set<Id> equipmentIds) {
        return [
                SELECT Name, Manufacturer__c, Model_Number__c, Tag_Number__c, Serial_Number__c, Object_Type__c,
                        Manufacturer_Serial_No__c
                FROM Equipment__c
                WHERE Id =: equipmentIds
        ];
    }

}