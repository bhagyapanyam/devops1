/*
 *  Author:         Maros Grajcar
 *  Company:        Husky Energy
 *  Description:
 *  History:        Created on 10/18/2018
 */

global class EquipmentTransferExpirationSchedulable implements Schedulable {

	global void execute(SchedulableContext sc) {
		runNotificationBatchJobs();
	}

	private void runNotificationBatchJobs() {
		Database.executeBatch(new EquipmentTransferExpirationBatchJob('statusRejected'));
		Database.executeBatch(new EquipmentTransferExpirationBatchJob('statusOpen'));
		Database.executeBatch(new EquipmentTransferExpirationBatchJob('sendNotification'));
	}

}