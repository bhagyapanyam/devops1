/*
 *  Author:         Maros Grajcar
 *  Company:        Husky Energy
 *  Description:
 *  History:        Created on 10/18/2018
 */

public class EquipmentTransferExpirationScheduler {

	public static final String DEFAULT_CRON_STRING = '0 0 7 * * ?';
	public static final String JOB_NAME = 'Equipment Transfer Request' + (Test.isRunningTest() ? ' Test':'');

	public static Id schedule() {
		return schedule(DEFAULT_CRON_STRING);
	}

	public static Id schedule(String cronString) {
		EquipmentTransferExpirationSchedulable  job = new EquipmentTransferExpirationSchedulable();
		return System.schedule(JOB_NAME, cronString, job);
	}

}