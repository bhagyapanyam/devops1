/*
 *  Author:         Maros Grajcar
 *  Company:        Husky Energy
 *  Description:
 *  History:        Created on 10/18/2018
 */

global class EquipmentUpdateExpirationSchedulable implements Schedulable {

	global void execute(SchedulableContext sc) {
		runNotificationBatchJobs();
	}

	private void runNotificationBatchJobs() {
		Database.executeBatch(new EquipmentUpdateExpirationBatchJob('statusRejected'));
		Database.executeBatch(new EquipmentUpdateExpirationBatchJob('statusOpen'));
		Database.executeBatch(new EquipmentUpdateExpirationBatchJob('sendNotification'));
	}

}