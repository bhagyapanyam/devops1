@isTest
public with sharing class ExceptionAlertSchedulableTest {

    @isTest
    static void testSchedule() {
        Test.startTest();

        ExceptionAlertSchedulable sch1 = new ExceptionAlertSchedulable();
        String cron = '0 0 16 * * ?';
        System.schedule('Exception Alert Unit Testing', cron, sch1);

        Test.stopTest();
    }
}