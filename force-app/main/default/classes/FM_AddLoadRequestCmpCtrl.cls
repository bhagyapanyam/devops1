/*************************************************************************************************\
Author:         Shreyas Dhond (Derived from Jakub Schon FM_LoadRequestControllerX)
Company:        Husky Energy
Description:    Controller for AddLoadRequestComponent
History: 		jschn 	- 6.7.2018 	- added ability to recreate(link)
**************************************************************************************************/
public with sharing class FM_AddLoadRequestCmpCtrl {

	public static final String CLASS_NAME = 'FM_AddLoadRequestCmpCtrl';
	public static final String PAGE_NAME = 'FM_AddLoadRequest';

	private static final String LOCATION_STRING = 'location';
	private static final String FACILITY_STRING = 'facility';

	public String sourceType {get; set;}
	public String sourceId {get; set;}
	public String routeId {get; set;}

	public List<LoadRequestWrapper> loadRequestWrappers {get; set;}
	public Map<String, Equipment_Tank__c> equipmentTankMap {get; set;}
    public List<SelectOption> tanks {get; set;}
    public List<SelectOption> extraTanks {get; set;}
	public List<SelectOption> createReasonOptions {get; private set;}
	public List<SelectOption> shiftOptions {
		get {
			return FM_LoadRequest_Utilities.getShiftOptions();
		} 
		private set;
	}
	public Boolean showCancelPopup {get; private set;}
	public FM_Load_Request__c cancelLoadRequest {get; set;}

    public Facility__c facility {get; private set;}
    public Location__c location {get; private set;}
	private FM_Load_Request__c prevLoadRequest;

	private String lastRunsheetComment;

	public String cancelLROldStatus;

	public List<String> tankLabels {
		get {
			if (tankLabels == null) {
				tankLabels = new List<String>();
				List<Schema.PicklistEntry> ple = FM_Load_Request__c.Tank__c.getDescribe().getPicklistValues();
				for( Schema.PicklistEntry f : ple) {
				    tankLabels.add(f.getLabel());
				}
			}
			return tankLabels;
		}
		set;
	}

    public FM_AddLoadRequestCmpCtrl() {
		setCreateReasonOptions();
    }

	private void setCreateReasonOptions() {
		createReasonOptions = new List<SelectOption>();
		for (Schema.PicklistEntry ple : FM_Load_Request__c.Create_Reason__c.getDescribe().getPickListValues())
			if (!ple.isDefaultValue()) createReasonOptions.add(new SelectOption(ple.getLabel(), ple.getValue()));
	}

    public Boolean isDispatcher {
        get {
            if(isDispatcher == null) {
                isDispatcher = FM_Utilities.IsDispatcher() ||
                FM_Utilities.VerifyPermissions(UserInfo.getUserId(), FM_Utilities.FLUID_PERMISSION_SET_DISPATCH_STANDARD) ||
                FM_Utilities.VerifyPermissions(UserInfo.getUserId(), FM_Utilities.FLUID_PERMISSION_SET_FLUID_ADMIN) ||
                FM_Utilities.VerifyProfile('Standard HOG - Administrator') ||
                FM_Utilities.VerifyProfile('System Administrator');
            }
            return isDispatcher;
        }
        private set;
    }

	//not needed if getLoadRequests is public...
	public void getData() {
        loadRequestWrappers = null;
		getLoadRequests();
		getLastRunsheetComments();
	}

    private void getSourceData() {
    	if(sourceType == FACILITY_STRING && !sourceId.contains(FM_Utilities.RUNSHEET_FACILITY_WELL_STRING_IDENTIFIER)) {
    		facility = [Select Id, Plant_Section__r.Default_Carrier__c, Plant_Section__r.Default_Carrier_SOUR__c, Functional_Location_Category__c, Hazardous_H2S__c 
    					From Facility__c
    					Where Id =: sourceId];
			location = null;
    	} else if(sourceType == FACILITY_STRING && sourceId.contains(FM_Utilities.RUNSHEET_FACILITY_WELL_STRING_IDENTIFIER)) {
            location = [Select Id, Route__r.Default_Carrier__c, Route__r.Default_Carrier_SOUR__c, Unit_Configuration__c, Functional_Location_Category__c, Hazardous_H2S__c
                        From Location__c
                        Where Id =: sourceId.remove(FM_Utilities.RUNSHEET_FACILITY_WELL_STRING_IDENTIFIER)];
            facility = null;
        } else if (sourceType == LOCATION_STRING) {
    		location = [Select Id, Route__r.Default_Carrier__c, Route__r.Default_Carrier_SOUR__c, Unit_Configuration__c, Functional_Location_Category__c, Hazardous_H2S__c
    					From Location__c
    					Where Id =: sourceId];
            facility = null;
    	} else {
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.FATAL,
                        'Error: Invalid Source Type'));
    	}
    }

    private void getLoadRequests() {
    	if(loadRequestWrappers == null) {
    		try {
	    		List<FM_Load_Request__c> loadRequests = new List<FM_Load_Request__c>();
	    		//Need to do it here since the component attributes are populated after the constructor
	    		getSourceData();
				getTankData();
	    		getTanksForPicklist();
				loadRequests = Database.Query(getLRQueryString());
				prevLoadRequest = loadRequests != null && loadRequests.size() > 0 ? loadRequests.get(0).clone(false, true, false, false) : null;
	            loadRequestWrappers = new List<LoadRequestWrapper>();
	            for (FM_Load_Request__c loadRequest : loadRequests)
	                loadRequestWrappers.add(new LoadRequestWrapper(loadRequest, routeId, sourceType, sourceId));
	        } catch (QueryException ex) {
	    		ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.FATAL,
	                        'Error: ' + ex.getMessage()));
	   		}
        } else {
        	prevLoadRequest = new FM_Load_Request__c();
        }
    }

	private void getTankData() {        
        equipmentTankMap = new Map<String, Equipment_Tank__c>([Select Id, SCADA_Tank_Level__c, Latest_Tank_Reading_Date__c, Low_Level__c, 
														 		      Tank_Size_m3__c, Flow_Rate__c, Tank_Label__c
														   	   From Equipment_Tank__c
														       Where ((Equipment__r.Location__c =: sourceId And Equipment__r.Location__c != null)
														       Or (Equipment__r.Facility__c =: sourceId And Equipment__r.Facility__c != null))
														       And Tank_Settings__c <> 'Hidden - Hide this Tank from the selectable list.']);
	}

	private String getLRQueryString() {
		String srcType;
		if (this.sourceType == FACILITY_STRING && !this.sourceId.contains(FM_Utilities.RUNSHEET_FACILITY_WELL_STRING_IDENTIFIER)) srcType = 'Source_Facility__c';
		else if(this.sourceType == FACILITY_STRING && this.sourceId.contains(FM_Utilities.RUNSHEET_FACILITY_WELL_STRING_IDENTIFIER)) srcType = 'Source_Location__c';
		else if(this.sourceType == LOCATION_STRING) srcType = 'Source_Location__c';
		return 'SELECT id, Carrier_Name__c, Load_Type__c, Name,'
							+ ' Product__c, Shift__c, Shift_Day__c, Status__c, Tank_Label__c,'
							+ ' Tank__c, Standing_Comments__c, Cancel_Reason__c,'
							+ ' Tank_Low_Level__c, Tank_Size__c, Act_Tank_Level__c,'
							+ ' Act_Flow_Rate__c, Flowline_Volume__c, Axle__c, Load_Weight__c,'
							+ ' Create_Reason__c, Ticket_Number__c, Sour__c, Create_Comments__c'
						+ ' FROM FM_Load_Request__c'
						+ ' WHERE ' + getCompletedAndUncompleteLRQueryString(srcType)
						+ ' OR ' + getCancelledLRQUeryString(srcType)
						+ ' OR ' + getCompletedLRQUeryString(srcType)
						+ ' ORDER BY CreatedDate desc LIMIT 30';
	}

	private String getCompletedAndUncompleteLRQueryString(String srcType) {
		return '('
				+ srcType + ' = \'' + sourceId.remove(FM_Utilities.RUNSHEET_FACILITY_WELL_STRING_IDENTIFIER) +'\''
				+ ' AND (Status__c = ' + '\'' + FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED + '\''
					+ ' OR Status__c = ' + '\'' + FM_LoadRequest_Utilities.LOADREQUEST_STATUS_NEW + '\''
					+ ' OR Status__c = ' + '\'' + FM_LoadRequest_Utilities.LOADREQUEST_STATUS_BOOKED + '\''
					+ ' OR Status__c = ' + '\'' + FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED + '\''
					+ ')'
				+ ' AND Status__c != ' + '\'' + FM_LoadRequest_Utilities.LOADREQUEST_STATUS_HAULED + '\''
				+ ' AND Status__c != ' + '\'' + FM_LoadRequest_Utilities.LOADREQUEST_STATUS_CANCELLED + '\''
				+ ')';
	}

	private String getCompletedLRQUeryString(String srcType) {
		Date yesterday = Date.today() - 5;
		return '('
			+ srcType + ' = \'' + sourceId.remove(FM_Utilities.RUNSHEET_FACILITY_WELL_STRING_IDENTIFIER) +'\''
			+ ' AND Status__c In ' + '(\'' + FM_LoadRequest_Utilities.LOADREQUEST_STATUS_HAULED + '\',\'' + FM_LoadRequest_Utilities.LOADREQUEST_STATUS_EXPORTED + '\')'
			+ ' AND CreatedDate >= ' + (DateTime.newInstance(yesterday.year(), yesterday.month(), yesterday.day())).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'')
			+ ')';
	}

	private String getCancelledLRQUeryString(String srcType) {
		Date yesterday = Date.today() - 1;
		return '('
				+ srcType + ' = \'' + sourceId.remove(FM_Utilities.RUNSHEET_FACILITY_WELL_STRING_IDENTIFIER) +'\''
				+ ' AND Status__c = ' + '\'' + FM_LoadRequest_Utilities.LOADREQUEST_STATUS_CANCELLED + '\''
				+ ' AND CreatedDate >= ' + (DateTime.newInstance(yesterday.year(), yesterday.month(), yesterday.day())).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'')
				+ ')';
	}

	private void getLastRunsheetComments() {
		if (location != null || facility != null) {
			FM_Run_Sheet__c runsheet = getLastRunsheet();
			if (runsheet != null) {
				lastRunsheetComment = runsheet.Standing_Comments__c;
				if (prevLoadRequest != null)
					prevLoadRequest.Standing_Comments__c = lastRunsheetComment;
			}
		}
	}

	private FM_Run_Sheet__c getLastRunsheet() {
		List<FM_Run_Sheet__c> runsheets = Database.Query(getRSQueryString());
		if(runsheets != null && runsheets.size() > 0) return runsheets.get(0);
		return null;
	}

	private String getRSQueryString() {
		String queryString = 'SELECT Standing_Comments__c FROM FM_Run_Sheet__c';
		if(facility != null) queryString += ' WHERE Facility__c = \'' + facility.Id + '\'';
		else if(location != null) queryString += ' WHERE Well__c = \'' + location.Id + '\'';
		queryString += ' ORDER BY CreatedDate DESC LIMIT 1';
		return queryString;
	}

	public Boolean getIsHaluedLoadRequest() {
		if (loadRequestWrappers != null)
			for (LoadRequestWrapper lrw : loadRequestWrappers)
				if (lrw.loadRequest.Status__c == FM_LoadRequest_Utilities.LOADREQUEST_STATUS_HAULED)
					return true;
		return false;
	}

    public Boolean getIsLoadRequestsToSave() {
		if (loadRequestWrappers != null)
			for (LoadRequestWrapper lWrapper : loadRequestWrappers) {
	    		if (lWrapper.isNew) return true;
			}
		return false;
    }

    private void getTanksForPicklist() {
    	tanks = (sourceType == LOCATION_STRING) ? loadStandardTanks() : null;
		extraTanks = loadExtraTanks();
    }

    public List<SelectOption> getFacilityLoadTypes() {
		List<SelectOption> nonStandardLoads = new List<SelectOption>();
		List<Schema.PicklistEntry> loadTypes = FM_Load_Request__c.Load_Type__c.getDescribe().getPicklistValues();
		    
		for (Schema.PicklistEntry lt : loadTypes) {
			if (lt.getLabel() != 'Standard Load') {
				nonStandardLoads.add(new SelectOption(lt.getLabel(), lt.getValue()));
			}
		}
		return nonStandardLoads;
    }

    private List<SelectOption> loadStandardTanks() {
        List<SelectOption> stdTanks = new List<SelectOption>();
        String querySourceId = (sourceType == LOCATION_STRING) ? sourceId : null;
                    
        List<Equipment_Tank__c> eqTanks = [SELECT Id, Tank_Label__c
                                           FROM Equipment_Tank__c
                                           Where Equipment__r.Location__c =: querySourceId
                                           And Tank_Settings__c <> 'Hidden - Hide this Tank from the selectable list.'];
        for (Equipment_Tank__c eqt : eqTanks)
            if(eqt.Tank_Label__c != null && eqt.Tank_Label__c != '')
                stdTanks.add(new SelectOption(eqt.Id + '###' + eqt.Tank_Label__c, eqt.Tank_Label__c));
        return stdTanks;
    }

    private List<SelectOption> loadExtraTanks() {
        List<SelectOption> xtrTanks = FM_Utilities.GetTankLabels(sourceType);
        if (sourceType == LOCATION_STRING)
        	xtrTanks = FM_LoadRequest_Utilities.removeUsedLabels(xtrTanks, tanks);

        return xtrTanks;
    }

    public static String getLoadRequestCancelledStatus() {
        return FM_LoadRequest_Utilities.LOADREQUEST_STATUS_CANCELLED;
    }

    public static String getLoadRequestSubmittedStatus() {
        return FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
    }

    public static String getLoadRequestHauledStatus() {
        return FM_LoadRequest_Utilities.LOADREQUEST_STATUS_HAULED;
    }

    public static String getLoadRequestBookedStatus() {
        return FM_LoadRequest_Utilities.LOADREQUEST_STATUS_BOOKED;
    }

    public static String getLoadRequestNewStatus() {
        return FM_LoadRequest_Utilities.LOADREQUEST_STATUS_NEW;
    }

	public Boolean getIsAction() {
		if (loadRequestWrappers != null)
			for(LoadRequestWrapper lrw : loadRequestWrappers)
				if (lrw.isNew
					 || lrw.loadRequest.Status__c == getLoadRequestSubmittedStatus()
					 || lrw.loadRequest.Status__c == getLoadRequestNewStatus()
					) return true;
		return false;
	}

	public void removeLoadRequestByIndex() {
		Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
		if (loadRequestWrappers.get(index).isNew) loadRequestWrappers.remove(index);
		if (loadRequestWrappers != null & loadRequestWrappers.size() > 0) {
			prevLoadRequest = loadRequestWrappers.get(0).loadRequest.clone(false, true, false, false);
			if (!loadRequestWrappers.get(0).isNew && String.isNotBlank(lastRunsheetComment)) 
				prevLoadRequest.Standing_Comments__c = lastRunsheetComment;
		}
	}

    public void addLoadRequest() {
		if(loadRequestWrappers == null) loadRequestWrappers = new List<LoadRequestWrapper>();
		LoadRequestWrapper newLrw = new LoadRequestWrapper(facility, location, routeId, sourceType, sourceId);

		//Set Default values
		prepopulateTank(newLrw);
		prepopulateInputFields(newLrw);

		if(loadRequestWrappers.isEmpty()) loadRequestWrappers.add(newLrw); 
		else loadRequestWrappers.add(0, newLrw);
		prevLoadRequest = newLrw.loadRequest;
    }

    private void prepopulateTank(LoadRequestWrapper newLrw) {
        if (tanks != null && !tanks.isEmpty())
            newLrw.tank = tanks[0].getValue();
        else if (extraTanks != null && !extraTanks.isEmpty())
            newLrw.tank = extraTanks[0].getValue();
    }

    private void prepopulateInputFields(LoadRequestWrapper newLrw) {
		//newLrw.tank = (tanks != null && !tanks.isEmpty()) ? tanks[0].getValue() : extraTanks[0].getValue();
    	String tankId = (newLrw.tank.contains('###')) ? newLrw.tank.split('###')[0] : newLrw.tank;
		newLrw.loadRequest.Act_Flow_Rate__c = (prevLoadRequest != null) ? prevLoadRequest.Act_Flow_Rate__c : null;
		newLrw.loadRequest.Flowline_Volume__c = (prevLoadRequest != null) ? prevLoadRequest.Flowline_Volume__c : null;
		newLrw.loadRequest.Act_Tank_Level__c = equipmentTankMap.containsKey(tankId) ? (equipmentTankMap.get(tankId).SCADA_Tank_Level__c != null) ? Math.round(equipmentTankMap.get(tankId).SCADA_Tank_Level__c) : null : null;
		newLrw.loadRequest.Tank_Low_Level__c = equipmentTankMap.containsKey(tankId) ? equipmentTankMap.get(tankId).Low_Level__c : null;
		newLrw.loadRequest.Tank_Size__c = equipmentTankMap.containsKey(tankId) ? equipmentTankMap.get(tankId).Tank_Size_m3__c : null;
		newLrw.loadRequest.Load_Type__c = (prevLoadRequest != null) ? prevLoadRequest.Load_Type__c : FM_LoadRequest_Utilities.LOADREQUEST_LOADTYPE_STANDARD;
		newLrw.loadRequest.Product__c = (prevLoadRequest != null) ? prevLoadRequest.Product__c : FM_LoadRequest_Utilities.LOADREQUEST_PRODUCT_OIL;
		newLrw.loadRequest.Shift__c = (prevLoadRequest != null) ? prevLoadRequest.Shift__c : FM_LoadRequest_Utilities.LOADREQUEST_SHIFT_NIGHT;
		newLrw.loadRequest.Shift_Day__c = (prevLoadRequest != null) ? prevLoadRequest.Shift_Day__c : FM_LoadRequest_Utilities.LOADREQUEST_DAYSHIFT_TODAY;
		newLrw.loadRequest.Load_Weight__c = (prevLoadRequest != null) ? prevLoadRequest.Load_Weight__c : FM_LoadRequest_Utilities.LOADREQUEST_LOADWEIGHT_PRIMARY;
		newLrw.loadRequest.Create_Reason__c = (prevLoadRequest != null) ? prevLoadRequest.Create_Reason__c : '';
		newLrw.loadRequest.Axle__c = location != null ? location.Unit_Configuration__c : FM_LoadRequest_Utilities.defaultAxles.get(FM_Utilities.FACILITY_DEFAULT_AXLE_VALUE);
		newLrw.loadRequest.Unit_Configuration__c = location != null ? location.Unit_Configuration__c : FM_Utilities.FACILITY_DEFAULT_AXLE_VALUE;
		newLrw.loadRequest.Standing_Comments__c = (prevLoadRequest != null) ? prevLoadRequest.Standing_Comments__c : null;
		newLrw.loadRequest.Create_Comments__c = (prevLoadRequest != null) ? prevLoadRequest.Create_Comments__c : '';
    }

    public PageReference updateLoadRequestTankInfo() {
    	Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
    	LoadRequestWrapper lrwToUpdate = loadRequestWrappers.get(index);
    	String tankId = (lrwToUpdate.tank.contains('###')) ? lrwToUpdate.tank.split('###')[0] : lrwToUpdate.tank;
    	lrwToUpdate.loadRequest.Act_Tank_Level__c = equipmentTankMap.containsKey(tankId) ?
			(equipmentTankMap.get(tankId).SCADA_Tank_Level__c != null) ? Math.round(equipmentTankMap.get(tankId).SCADA_Tank_Level__c) : null : null;
		lrwToUpdate.loadRequest.Tank_Low_Level__c = equipmentTankMap.containsKey(tankId) ?
			equipmentTankMap.get(tankId).Low_Level__c : null;
		lrwToUpdate.loadRequest.Tank_Size__c = equipmentTankMap.containsKey(tankId) ?
			equipmentTankMap.get(tankId).Tank_Size_m3__c : null;
    	return null;
    }

    public PageReference cancelLoadRequestPopupOpen() {
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
		cancelLoadRequest = loadRequestWrappers.get(index).loadRequest;
		cancelLoadRequest.Cancel_Reason__c = getDefaultCancelReason();
        showCancelPopup = true;
        return null;
    }

	private String getDefaultCancelReason() {
		List<Schema.PicklistEntry> plv = FM_Load_Request__c.Cancel_Reason__c.getDescribe().getPicklistValues();
		return plv.get(0).getValue();
	}

    public PageReference cancelLoadRequestPopupConfirm() {
        SavePoint sp = Database.SetSavePoint();
        try {
        	cancelLROldStatus = cancelLoadRequest.Status__c;
            cancelLoadRequest.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_CANCELLED;
            update cancelLoadRequest;
        } catch (DMLException ex) {
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, ex.getMessage()));

            //Restore record
            cancelLoadRequest.Status__c = cancelLROldStatus;
            cancelLoadRequest.Cancel_Reason__c = null;

            return cancelLoadRequestPopupCancel();
        }

        return cancelLoadRequestPopupCancel();
    }

    public PageReference cancelLoadRequestPopupCancel() {
        //Re-initialize popup variables
        cancelLoadRequest = null;
        cancelLROldStatus = null;
        showCancelPopup = false;
        System.debug('cancelLoadRequestPopupCancel->showRebookPopup: ' + showCancelPopup);
        return null;
    }

    public void saveNewLoadRequests() {
    	try {
	            List<FM_Load_Request__c> newLoadRequests = new List<FM_Load_Request__c>();
	            for (LoadRequestWrapper lrw : loadRequestWrappers) {
	                if (lrw.isNew) {
	                	//Required Fields
	                	if(lrw.loadRequest.Act_Flow_Rate__c == null) 
	                		throw new AddLoadRequestException('Please enter the well flow rate.');
	                	if(isTankLabelInPicklist(lrw.tank)) {
	                		if(lrw.loadRequest.Act_Tank_Level__c == null)
	                			throw new AddLoadRequestException('Please enter the tank level.');
	                		if(lrw.loadRequest.Tank_Low_Level__c == null)
	                			throw new AddLoadRequestException('Please enter the tank low level.');
	                		if(lrw.loadRequest.Tank_Size__c == null)
	                			throw new AddLoadRequestException('Please enter the tank size.');
	                	}
	                	if(String.isBlank(lrw.loadRequest.Create_Reason__c))
	                		throw new AddLoadRequestException('Please enter the create reason.');
	                	if(String.isBlank(lrw.loadRequest.Load_Type__c))
	                		throw new AddLoadRequestException('Please enter the load type.');
	                	if(String.isBlank(lrw.loadRequest.Product__c))
	                		throw new AddLoadRequestException('Please enter the product.');
	                	if(String.isBlank(lrw.loadRequest.Shift__c))
	                		throw new AddLoadRequestException('Please enter the shift.');
	                	if(sourceType == FACILITY_STRING && lrw.loadRequest.Load_Type__c != 'Standard Load' && String.isBlank(lrw.loadRequest.Standing_Comments__c))
	                		throw new AddLoadRequestException('Please enter comment for non standard load.');

	                	lrw.loadRequest.Tank__c = null;
	                	lrw.loadRequest.Equipment_Tank__c = null;
	                	if (isTankLabelInPicklist(lrw.tank)) {
	                		String tankKey = '';
	                		if (lrw.tank.contains('###')) {
	                			lrw.loadRequest.Tank__c = lrw.tank.split('###')[1];
	                			tankKey = lrw.tank.split('###')[0];
	                		} else {
	                			lrw.loadRequest.Tank__c = lrw.tank;
	                			tankKey = lrw.tank;
	                		}
	                		if(equipmentTankMap.containsKey(tankKey))
                				lrw.loadRequest.Equipment_Tank__c = equipmentTankMap.get(tankKey).Id;
	                	}
	                	newLoadRequests.add(lrw.loadRequest);
	                }
                }

	            insert newLoadRequests;
	            loadRequestWrappers = null;
				getLoadRequests();
	        	getLastRunsheetComments();
	        
	    } catch (AddLoadRequestException ex) {
	    	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage()));
	    } catch (DmlException ex) {
	    	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
	                'Error: ' + ex.getMessage()));
	    }
    }

    // replaces old logical check for loadRequest.Tank__c.contains('Tank') (before other tanks labels had been added - Sales, Test, Group)
    public Boolean isTankLabelInPicklist(String checkString) {
    	if (checkString.contains('###')) checkString = checkString.split('###')[1];
		for (String tankLabel : tankLabels)
			if (tankLabel.contains(checkString))
				return true;
		return false;
    }

    // checkes if string 'Tank' is in existing tank labels
    public Boolean getIsTankInTankLabels() {
    	return isTankLabelInPicklist('Tank');
    }

    /////////////////////////////////////////
    // * WRAPPER CLASS FOR LOAD REQUESTS * //
    /////////////////////////////////////////
    public class LoadRequestWrapper {
        public FM_Load_Request__c   loadRequest         {get; set;}
        public String               tank                {get; set;}

        private String routeId;
        private String sourceType;
        private String sourceId;

        public String 				shift {
        	get {
        		return FM_LoadRequest_Utilities.getShiftString(loadRequest); 
        	} set {
        		loadRequest = FM_LoadRequest_Utilities.setShift(loadRequest, value);
        	}
        }

        private String recreateUrl {
        	get {
        		if(recreateAllowed 
    			&& String.isBlank(recreateUrl))
	        		recreateUrl = '/apex/' + FM_LoadRequestRecreateCtrl.PAGE_NAME 
	        					+ '?' + FM_LoadRequestRecreateCtrl.URL_PARAM_ID + '=' + loadRequest.Id
								+ '&' + FM_LoadRequestRecreateCtrl.URL_PARAM_CANCEL + '=/apex/' + PAGE_NAME
								+ '&' + FM_LoadRequestRecreateCtrl.URL_PARAM_SAVE + '=/apex/' + PAGE_NAME
								+ '&' + FM_LoadRequestRecreateCtrl.URL_PARAM_ROUTE_ID + '=' + routeId
								+ '&' + FM_LoadRequestRecreateCtrl.URL_PARAM_SOURCE_TYPE + '=' + sourceType
								+ '&' + FM_LoadRequestRecreateCtrl.URL_PARAM_SOURCE_ID + '=' + sourceId;
				return recreateUrl;
        	}
        	private set;
    	} 

    	public PageReference recreate() {
    		return new PageReference(recreateUrl);
    	}

		public Boolean recreateAllowed {
			get {
				return !isNew
					&& (loadRequest.Status__c == FM_LoadRequest_Utilities.LOADREQUEST_STATUS_BOOKED
						|| loadRequest.Status__c == FM_LoadRequest_Utilities.LOADREQUEST_STATUS_NEW
						|| loadRequest.Status__c == FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED);
			}
			private set;
		} 

        public LoadRequestWrapper(FM_Load_Request__c loadRequest, String routeId, String sourceType, String sourceId) {
            this.loadRequest = loadRequest;
        	setRedirectParams(routeId, sourceType, sourceId);
        }

        public LoadRequestWrapper(Facility__c facility, Location__c location, String routeId, String sourceType, String sourceId) {
            setUpWrapper(facility, location);
        	setRedirectParams(routeId, sourceType, sourceId);
        }

		private void setUpWrapper(Facility__c facility, Location__c location) {
			System.debug('Location: ' + location);
			System.debug('Facility: ' + facility);
			this.loadRequest = new FM_Load_Request__c(
                Carrier__c = getCarrier(facility, location),
                Source_Location__c = (location != null) ? location.Id : null,
                Source_Facility__c = (facility != null) ? facility.Id : null,
                Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED,
                Shift__c = FM_LoadRequest_Utilities.LOADREQUEST_SHIFT_DAY,
                Product__c = FM_LoadRequest_Utilities.LOADREQUEST_PRODUCT_OIL,
                Load_Type__c = FM_LoadRequest_Utilities.LOADREQUEST_LOADTYPE_STANDARD,
				Load_Weight__c = FM_LoadRequest_Utilities.LOADREQUEST_LOADWEIGHT_PRIMARY);
		}

		private void setRedirectParams(String routeId, String sourceType, String sourceId) {
			this.routeId = routeId;
			this.sourceType = sourceType;
			this.sourceId = sourceId;
		}

		private String getCarrier(Facility__c facility, Location__c location) {
			if (facility != null) {
				if (facility.Hazardous_H2S__c == 'Y') {
					return (facility.Plant_Section__r.Default_Carrier_SOUR__c != null)
						? facility.Plant_Section__r.Default_Carrier_SOUR__c 
						: facility.Plant_Section__r.Default_Carrier__c;
				} else {
					return facility.Plant_Section__r.Default_Carrier__c; 
				}
			}
			else if (location != null) {
				if (location.Hazardous_H2S__c == 'Y') {
					return (location.Route__r.Default_Carrier_SOUR__c != null)
						? location.Route__r.Default_Carrier_SOUR__c
						: location.Route__r.Default_Carrier__c;
				} else {
					return location.Route__r.Default_Carrier__c;
				}
			}
			return '';
		}

        public Boolean isNew {
        	get {
        		return this.loadRequest.Id == null;
        	}
        	private set;
        }

        public void handleReasonChange() {
        	if (String.isNotBlank(loadRequest.Create_Reason__c)
    		&& !loadRequest.Create_Reason__c.equals('Other'))
    			loadRequest.Create_Comments__c = '';
        }
    }

    ///////////////////////////////////////////////
    // * EXCEPTION CLASS FOR NEW LOAD REQUESTS * //
    ///////////////////////////////////////////////
    public class AddLoadRequestException extends Exception {}
}