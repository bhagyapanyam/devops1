/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest

private class FM_OperatorOnCallControllerTest
{
	private static final Integer TEST_ROUTE_COUNT = 10;

	static testMethod void TestOperatorOnCall()
	{
		CreateTestData();

		/////////////////////////////////
		//* RETRIEVE TEST DATA VALUES *//
		/////////////////////////////////

		User oUser = GetTestUser();

		/////////////////////////////////////
		//* MISC VARIABLES FOR ASSIGNMENT *//
		/////////////////////////////////////

		List<FM_OperatorOnCallController.OperatorOnCallWrapper> lWrappers;
		FM_OperatorOnCallController.OperatorOnCallWrapper oOneWrapper;
		List<SelectOption> lOptions;
		PageReference oPage;
		Date dToday = Date.today();
		String sTest = '';
		Integer iTest = 0;

		//////////////////
		//* START TEST *//
		//////////////////

		Test.startTest();

		System.runAs(oUser)
		{
			// Create Controller
			FM_OperatorOnCallController oController = new FM_OperatorOnCallController();

			// Sort Columns --> Default SHOULD be Route Number
			System.assert(oController.sSearchField == 'Name');
			oController.ColumnSortToggle();

			// Change Search Column AND Re-Sort
			oController.sSearchField = 'Operator_On_Call__r.Operator__r.Name';
			oController.ColumnSortToggle();

			// Enter Edit Mode AND Cancel --> SHOULD revert
			System.assert(oController.EditMode() == null);
			System.assert(oController.getInEditMode() == true);
			System.assert(oController.EditMode() == null);
			System.assert(oController.getInEditMode() == false);

			// Attempt Change Operator --> SHOULD fail (no Route selected)
			System.assert(oController.ChangeOperator() == null);

			// Get ALL Wrappers --> SHOULD match the TEST # Constant
			lWrappers = oController.getRouteList();
			System.assert(lWrappers.size() == TEST_ROUTE_COUNT);

			// Get Misc Assignments
			sTest = FM_OperatorOnCallController.getSplitterString();
			sTest = oController.getRouteNum();
			oController.setRouteNum(sTest);

			// Check Operator Status --> SHOULD return TRUE (TEST USER has Field Operator permission set)
			System.assert(oController.getIsOperator() == true);

			// Check Control Room Operator Status --> SHOULD return FALSE (TEST USER does NOT have this permission set)
			System.assert(oController.getIsControlRoomOperator() == false);

			// Get First Route Wrapper
			oOneWrapper = lWrappers[0];

			// Operator assignment list SHOULD have Operators assigned
			System.assert(oOneWrapper.getOperatorAssignmentList().size() > 0);

			// Get Other Assignments from Wrapper
			sTest = oOneWrapper.getSelectedOperator();
			sTest = oOneWrapper.getWrapperSplitterString();

			// Switch to Edit Mode, Attempt Save
			oPage = oController.EditMode();
			oController.ChangeOperator();

			// Get Operator Assignment List --> SHOULD only contain 1 (TEST USER)
//			lOptions = oController.getOperatorAssignmentList();
//			System.assert(lOptions.size() == 1);
		}

		/////////////////
		//* STOP TEST *//
		/////////////////

		Test.stopTest();
	}

	//////////////////////////////////////////////
	//* DATA RETRIEVAL FUNCTIONS --> TEST DATA *//
	//////////////////////////////////////////////

	private static User GetTestUser()
	{
		List<User> lTempUsers = [SELECT Id, Name FROM User WHERE Name = 'Guy Incognito' AND Alias = 'guyincog' AND Email = 'guy.incognito@testinghuskyfluidmanagement.com' LIMIT 1];
		return (lTempUsers.size() == 1) ? lTempUsers[0] : new User();
	}

	// ******************************************* //
	// ** CREATE ALL TEST DATA FOR TEST METHODS ** //
	// ******************************************* //

	private static void CreateTestData()
	{
		// Get HOG Profile
		List<Profile> lProfiles = [SELECT Id FROM Profile WHERE Name = 'Standard HOG - General User' LIMIT 1];

		// Test USER
		User oUser = new User();
		oUser.FirstName = 'Guy';
		oUser.LastName = 'Incognito';
		oUser.CommunityNickname = 'Homer';
		oUser.Email = oUser.FirstName.toLowerCase() + '.' + oUser.LastName.toLowerCase() + '@testinghuskyfluidmanagement.com';
		oUser.UserName = oUser.Email;
		oUser.Alias = 'guyincog';
		oUser.TimeZoneSidKey = 'America/New_York';
		oUser.LocaleSidKey = 'en_US';
		oUser.EmailEncodingKey = 'ISO-8859-1'; 
		oUser.LanguageLocaleKey='en_US';
		oUser.ProfileId = lProfiles[0].Id;
		oUser.IsActive = true;
		insert oUser;

		// Test USER (Field Senior)
		User oFsUser = new User();
		oFsUser.FirstName = 'Senior';
		oFsUser.LastName = 'Fields';
		oFsUser.CommunityNickname = 'Junior';
		oFsUser.Email = oFsUser.FirstName.toLowerCase() + '.' + oFsUser.LastName.toLowerCase() + '@testinghuskyfluidmanagement.com';
		oFsUser.UserName = oFsUser.Email;
		oFsUser.Alias = 'senflds';
		oFsUser.TimeZoneSidKey = 'America/New_York';
		oFsUser.LocaleSidKey = 'en_US';
		oFsUser.EmailEncodingKey = 'ISO-8859-1'; 
		oFsUser.LanguageLocaleKey='en_US';
		oFsUser.ProfileId = lProfiles[0].Id;
		oFsUser.IsActive = true;
		insert oFsUser;

		PermissionSet oPermission;
		PermissionSetAssignment oPermissionAssign;

		System.runAs(oUser)
		{
			// Permission Set Assignment - Field Operator
			oPermission = [SELECT Id, Name FROM PermissionSet WHERE Name = :FM_Utilities.FLUID_PERMISSION_SET_OPERATOR LIMIT 1];
			oPermissionAssign = new PermissionSetAssignment(AssigneeId = oUser.Id, PermissionSetId = oPermission.id);
			insert oPermissionAssign;
		}

		//////////////////////////////////////////////
		// ** CREATE MULTIPLE ROUTES FOR TESTING ** //
		//////////////////////////////////////////////

		List<Route__c> lAllRoutes = new List<Route__c>();
		Route__c oRoute;

		Integer iRouteCount = TEST_ROUTE_COUNT;
		Integer iCount;

		for(iCount = 0; iCount < iRouteCount; iCount++)
		{
			oRoute = new Route__c();
			oRoute.Name = String.valueOf(100 + iCount);
			oRoute.Route_Number__c = String.valueOf(100 + iCount);
			oRoute.Fluid_Management__c = true;
			oRoute.Field_Senior__c = oFsUser.Id;

			lAllRoutes.add(oRoute);
		}

		if(lAllRoutes.size() > 0)
		{
			insert lAllRoutes;
		}
	}
}