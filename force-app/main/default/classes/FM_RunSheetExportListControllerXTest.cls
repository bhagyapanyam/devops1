/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class FM_RunSheetExportListControllerXTest{
    static string BusinessUnitID;
    static string BusinessDepartmentID;
    static string OperatingDistrictID;
    static string FieldID;
    static string Routeid;
    static string Locationid;

    public static final String STATUS_CANCELLED = 'Cancelled';
    public static final String STATUS_EXPORTED = 'Exported';

    static testMethod void myUnitTest() {

        User runningUser = UserTestData.createTestsysAdminUser();



        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);

        PermissionSet ps = [select id, name from PermissionSet where name = 'HOG_FM_Dispatcher' limit 1];
        PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = runningUser.id,PermissionSetId = ps.id);
        insert psa;

		// GM 2015-07-15 ADDING THIS TO ALLOW STATUS CHANGE TO "EXPORTED"
        ps = [select id, name from PermissionSet where name = 'HOG_FM_Dispatcher_Vendor' limit 1];
        psa = new PermissionSetAssignment(AssigneeId = runningUser.id,PermissionSetId = ps.id);
        insert psa;

        PageReference pageRef = Page.FM_AddRunSheet;
        Test.setCurrentPage(pageRef);


        System.runAs(runningUser) {

            setup();

            Test.startTest();

            //first create runsheet for our well for today
            Date dt = Date.Today();
            List<FM_Run_Sheet__c> newrunsheetList = new List<FM_Run_Sheet__c>();
            newrunsheetList.add(FM_RunSheetTestData.createRunSheet(dt, Locationid, 'Tank 1',
                    120,  10, 100, 15,
                    1, 1,
                    2, 2,
                    '5',
                    'standing comments',
                    true,
                    true));

            newrunsheetList.add(FM_RunSheetTestData.createRunSheet(dt, Locationid, 'Tank 2',
                    120,  10, 100, 15,
                    1, 1,
                    2, 2,
                    '5',
                    'standing comments',
                    true,
                    true));

            newrunsheetList.add(FM_RunSheetTestData.createRunSheet(dt, Locationid, 'Tank 3',
                    120,  10, 100, 15,
                    1, 1,
                    2, 2,
                    '5',
                    'standing comments',
                    true,
                    true));

            system.assertEquals(3, newrunsheetList.Size());

            //lets change status for the last tank to be cancelled
            newrunsheetList[2].Status__c = STATUS_CANCELLED;

            try {
                update newrunsheetList[2]; // it should fail
            } catch (DmlException e) {
                //Assert Error Message
                System.assert( e.getMessage().contains('Status can not be Cancelled, all loads must be 0'), e.getMessage() );

                //Assert field
                System.assertEquals(FM_Run_Sheet__c.Status__c, e.getDmlFields(0)[0]);

                //Assert Status Code
                System.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION' ,
                                     e.getDmlStatusCode(0) );
            } //catch
            newrunsheetList[2].Tonight_Oil__c = 0;
            newrunsheetList[2].Tonight_Water__c = 0;
            newrunsheetList[2].Tomorrow_Oil__c = 0;
            newrunsheetList[2].Tomorrow_Water__c = 0;
            newrunsheetList[2].Outstanding_Oil__c = 0;
            newrunsheetList[2].Outstanding_Water__c = 0;
            update newrunsheetList[2]; //now we can save

            Database.QueryLocator dbq = Database.getQueryLocator(
                    [SELECT id, Name, Date__c, Status__c FROM FM_Run_Sheet__c]);

            ApexPages.StandardSetController stdcon = new ApexPages.StandardSetController(dbq);

            FM_RunSheetExportListControllerX runsheetExtListController  = new FM_RunSheetExportListControllerX(stdcon);

            //Export List method should redirect us to FM_RyderExportExcel page
            system.assertNotEquals(null, runsheetExtListController.ExportList());

            //and ExportList property should have non Canceled run sheets with status updated to 'Exported'
            system.assertEquals(2, runsheetExtListController.ExportList.Size());   //should be 2 run sheets

			PageReference oTest;
			Integer iTest;

			oTest = runsheetExtListController.EditList();
			oTest = runsheetExtListController.SaveList();
			oTest = runsheetExtListController.CancelEditList();
			iTest = runsheetExtListController.getMySelectedSize();
			iTest = runsheetExtListController.getMyRecordsSize();

            Test.stopTest();
        }
    }
    public static void setup() {

            //create test BU
            Business_Unit__c  testbu = FM_RunSheetTestData.createBusinessUnit('Test BU',  true);
            BusinessUnitID = testbu.id;

            //create test BD
            Business_Department__c testbd = FM_RunSheetTestData.createBusinessDepartment('Test BD',  true);
            BusinessDepartmentID = testbd.id;
            //create test OD
            Operating_District__c testOD = FM_RunSheetTestData.createOperatingDistrict('Test OD', testbd.Id, testbu.Id,  true) ;
            OperatingDistrictID =testOD.id;
            //create test field
            Field__c testField = FM_RunSheetTestData.createField('Test Field', testOD.Id, true) ;
            FieldID = testField.id;
            //create test route
            Route__c testRoute = FM_RunSheetTestData.createRoute('120', true) ;

            //create test well
            Location__c location = FM_RunSheetTestData.createLocation('Test Well 1', testField.id, testRoute.id , true, true);
            //we need location with category = 4
            location.Functional_Location_Category__c =4;
            //and setup unit configuration
            location.Unit_Configuration__c = 'T5X';
            upsert location;


            //and lets create well without category 4
            Location__c location1 = FM_RunSheetTestData.createLocation('Test Well 2', testField.id, testRoute.id , false, true);

            //save route and well id in static variables
            Routeid = testRoute.id;
            Locationid = location.id;
    }
}