@isTest
public class FM_TestData {
	public static FM_Load_Request__c createLoadRequest(Id runSheetId, String loadType, String product,
		String shift, String carrier, String axle, String loadWeight, String comments, String createReason,
		String sourceFacility, String sourceLocation, String status, String tankId, String tankLabel,
		Decimal tankLevel, Decimal tankLevelLow, Decimal tankSize, Decimal flowRate, Decimal flowlineVolume){
		FM_Load_Request__c loadRequest = new FM_Load_Request__c(	Run_Sheet_Lookup__c = runSheetId,
			Load_Type__c = loadType,
		 	Product__c = product,
		  	Shift__c = shift,
		  	Carrier__c = carrier,
		  	Axle__c = axle,
		  	Load_Weight__c = loadWeight,
		  	Unit_Configuration__c = FM_LoadRequest_Utilities.defaultAxles.get(axle),
		  	Standing_Comments__c = comments,
		  	Create_Reason__c = createReason,
			Source_Facility__c = sourceFacility,
			Source_Location__c =sourceLocation,
			Status__c = status,
			Equipment_Tank__c = tankId,
			Tank__c = tankLabel,
			Act_Tank_Level__c = tankLevel,
			Tank_Low_Level__c = tankLevelLow,
			Tank_Size__c = tankSize,
			Act_Flow_Rate__c = flowRate,
			Flowline_Volume__c = flowlineVolume
		);
		insert loadRequest;
		return loadRequest;
	}

	public static List<FM_Load_Request__c> createLoadRequests(Id runSheetId, String loadType, String product,
		String shift, String carrier, String axle, String loadWeight, String comments, String createReason,
		String sourceFacility, String sourceLocation, String status, String tankId, String tankLabel,
		Decimal tankLevel, Decimal tankLevelLow, Decimal tankSize, Decimal flowRate, Decimal flowlineVolume,
		Integer numLoadRequests) {
		List<FM_Load_Request__c> loadRequests = new List<FM_Load_Request__c>();
		
		for(Integer i=0; i < numLoadRequests; i++) {
			loadRequests.add(new FM_Load_Request__c(	Run_Sheet_Lookup__c = runSheetId,
					Load_Type__c = loadType,
				 	Product__c = product,
				  	Shift__c = shift,
				  	Carrier__c = carrier,
				  	Axle__c = axle,
				  	Load_Weight__c = loadWeight,
				  	Unit_Configuration__c = FM_LoadRequest_Utilities.defaultAxles.get(axle),
				  	Standing_Comments__c = comments,
				  	Create_Reason__c = createReason,
					Source_Facility__c = sourceFacility,
					Source_Location__c =sourceLocation,
					Status__c = status,
					Equipment_Tank__c = tankId,
					Tank__c = tankLabel,
					Act_Tank_Level__c = tankLevel,
					Tank_Low_Level__c = tankLevelLow,
					Tank_Size__c = tankSize,
					Act_Flow_Rate__c = flowRate,
					Flowline_Volume__c = flowlineVolume
				)
			);
		}

		insert loadRequests;
		return loadRequests;
	}

	public static FM_Truck_Trip__c createTruckTrip(FM_Load_Request__c loadRequest, Date runsheetDate) {
		FM_Truck_Trip__c truckTrip = new FM_Truck_Trip__c(Load_Request__c	= loadRequest.Id,
			Load_Type__c = loadRequest.Load_Type__c == FM_LoadRequest_Utilities.LOADREQUEST_LOADTYPE_STANDARD?
				null : loadRequest.Load_Type__c,
			Product__c = loadRequest.Product__c,
			Shift__c = loadRequest.Shift__c,
			Carrier__c = loadRequest.Carrier__c,
			Axle__c = FM_Utilities.RUNSHEET_UNITCONFIG_MAP.get(loadRequest.Axle__c),
	  		Standing_Comments__c = loadRequest.Standing_Comments__c,
	  		Run_Sheet_Date__c = runsheetDate
		);
		insert truckTrip;
		return truckTrip;
	}

	public static FM_Truck_Trip__c createTruckTrip(FM_Load_Request__c loadRequest, Date runsheetDate, Carrier_Unit__c unit) {
		FM_Truck_Trip__c truckTrip = new FM_Truck_Trip__c(Load_Request__c	= loadRequest.Id,
			Load_Type__c = loadRequest.Load_Type__c == FM_LoadRequest_Utilities.LOADREQUEST_LOADTYPE_STANDARD?
				null : loadRequest.Load_Type__c,
			Product__c = loadRequest.Product__c,
			Shift__c = loadRequest.Shift__c,
			Carrier__c = loadRequest.Carrier__c,
			Unit__c = unit.Id,
			Axle__c = FM_Utilities.RUNSHEET_UNITCONFIG_MAP.get(loadRequest.Axle__c),
	  		Standing_Comments__c = loadRequest.Standing_Comments__c,
	  		Run_Sheet_Date__c = runsheetDate
		);
		insert truckTrip;
		return truckTrip;
	}

	public static FM_Stuck_Truck__c createStuckTruck(FM_Truck_Trip__c truckTrip, String status, Towing_Company__c company){
		FM_Stuck_Truck__c stuckTruck = new FM_Stuck_Truck__c(
			Truck_Trip__c = truckTrip.Id,
			Stuck_From__c = System.now().addDays(-7),
			Location__c = truckTrip.Location_Lookup__c,
			Carrier__c = truckTrip.Carrier__c,
			Carrier_Unit__c = truckTrip.Unit__c,
			Towing_Company_Lookup__c = company.Id,
			Status__c = status
		);

		insert stuckTruck;
		return stuckTruck;
	}

	public static User createUser() {
		User user = new User();
        Profile p = [Select Id from Profile where Name='Standard HOG - General User'];

        Double random = Math.Random();

        user.Email = 'TestUser' +  random + '@hog.com';
        user.Alias = 'TestUser' ;
        user.EmailEncodingKey = 'UTF-8';
        user.LastName = 'User';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        user.ProfileId = p.Id;
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.UserName = 'TestUser' + random + '@hog.com.unittest';

        insert user;
        return user;
	}

	public static User assignPermissionSet(User user, String userPermissionSetName){
         PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Name =: userPermissionSetName];
         PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment();

         permissionSetAssignment.PermissionSetId = permissionSet.Id;
         permissionSetAssignment.AssigneeId = user.Id;

         insert permissionSetAssignment;

         return user;
     }

    public static User assignProfile (User user, String profileName){
        Profile profile = [SELECT Id FROM Profile WHERE Name =: profileName];
        user.ProfileId = profile.Id;
        update user;
        return user;
    }
}