/*************************************************************************************************\
Author:         Unknown
Company:        Husky Energy
Class:          FM_TruckListsCustomCtrl
History:        jschn 05.31.2018 - Removed getAVG... asserts methods. US - W-001046
**************************************************************************************************/
@isTest
public class FM_TruckListCustomCtrlTest {
    @isTest
    static void controllerWithoutURLParams(){

        // test CarrierUnits


        List<FM_Truck_Trip__c> lTrips = [SELECT Id, Facility_Type__c, Location_Lookup__c, Run_Sheet_Lookup__r.Facility__c,
                Run_Sheet_Lookup__r.Well__c, Run_Sheet_Lookup__r.Well__r.Well_Type__c, Run_Sheet_Lookup__r.Well__r.Fluid_Location_Ind__c,
                Product__c,  Facility__c, Shift__c, Load_Weight__c, Truck_Trip_Status__c, Run_Sheet_Date__c
        FROM FM_Truck_Trip__c];
        Account oAccount = [SELECT Id FROM Account LIMIT 1];
        Carrier__c oCarrier = [SELECT Id, Carrier_Name_For_Fluid__c, Enabled__c FROM Carrier__c LIMIT 1];
        Carrier_Unit__c oCarrierUnit = [SELECT Id FROM Carrier_Unit__c LIMIT 1];
        Facility__c oFacility = [SELECT Id, Facility_Name_For_Fluid__c FROM Facility__c LIMIT 1];
        Location__c oLocation = [SELECT Id,  Location_Name_For_Fluid__c, Fluid_Location_Ind__c  FROM Location__c LIMIT 1];

        for(FM_Truck_Trip__c tt:lTrips){
            tt.Carrier__c = oCarrier.Id;
            tt.Unit__c = oCarrierUnit.Id;
            tt.Location_Lookup__c = oLocation.Id;
            tt.Facility_Type__c = 'Location';
            tt.Facility_String_Hidden__c = '###_WELL_###'+oLocation.Id;
        }

        update lTrips;

        Test.startTest();

        //set current page - FM_TruckListsCustom
        PageReference pageRef = Page.FM_TruckListsCustom;
        Test.setCurrentPage(pageRef);

        FM_TruckListsCustomCtrl controller = new FM_TruckListsCustomCtrl();

        //filter properties
        List<SelectOption> unitSelectOptionList = controller.unitSelectOptionList;
        List<SelectOption> dispatchDeskList = controller.dispatchDeskSelectOptionList;
        //List<SelectOption> lDispatchUsersList = controller.lDispatchUsersList;
        String DispatchedOnString = controller.DispatchedOnString;
        List<SelectOption> shiftSelectOptionList = controller.shiftSelectOptionList;
        List<SelectOption> unitSelectOptions = controller.unitSelectOptions;
        List<SelectOption> statusSelectOptionListForEntry = controller.statusSelectOptionListForEntry;

        //search test
        controller.searchTodayTruckLists();
        system.assertEquals(Date.today(), controller.truckTrip.Run_Sheet_Date__c );
        system.assertEquals('Day', controller.truckTrip.Shift__c );
        controller.searchTonightTruckLists();
        system.assertEquals('Night', controller.truckTrip.Shift__c );

        controller.combinationSelected = controller.getWrapperValues().get(0).combination;
        controller.showDetail();

        system.assertNotEquals(null, controller.truckTripGeneral );
        controller.searchAllTruckLists();

        SelectOption carrierUnit = controller.carrierUnitSelectOptions.get(0);
        System.assertEquals(0, carrierUnit.getLabel().indexOf('Test Carrier'));
        System.assertEquals(false, controller.isSaveDisabled);

        // coverage
        System.assertEquals(false, controller.TruckTrips.get(0).isCleanLoad);

        controller.getDispatchedStatus();
        controller.getRebookedStatus();
        controller.getCancelledStatus();
        controller.getAddOnStatus();
        controller.getCompletedStatus();
        controller.getIsTrucktripCompatible();
        System.debug('TEST Trips: ' + controller.truckTrips);
        Test.stopTest();
    }

    @isTest
    static void controllerWithURLParamsDetailPage(){


        List<FM_Truck_Trip__c> lTrips = [SELECT Id, Facility_Type__c, Location_Lookup__c, Run_Sheet_Lookup__r.Facility__c,
                Run_Sheet_Lookup__r.Well__c, Run_Sheet_Lookup__r.Well__r.Well_Type__c, Run_Sheet_Lookup__r.Well__r.Fluid_Location_Ind__c,
                Product__c,  Facility__c, Shift__c, Load_Weight__c, Truck_Trip_Status__c, Unit__c, Run_Sheet_Date__c
        FROM FM_Truck_Trip__c];
        Account oAccount = [SELECT Id FROM Account LIMIT 1];
        Carrier__c oCarrier = [SELECT Id, Carrier_Name_For_Fluid__c, Enabled__c FROM Carrier__c LIMIT 1];
        Carrier_Unit__c oCarrierUnit = [SELECT Id FROM Carrier_Unit__c LIMIT 1];
        Facility__c oFacility = [SELECT Id, Facility_Name_For_Fluid__c FROM Facility__c LIMIT 1];
        Location__c oLocation = [SELECT Id,  Location_Name_For_Fluid__c, Fluid_Location_Ind__c  FROM Location__c LIMIT 1];

        for(FM_Truck_Trip__c tt:lTrips){
            tt.Carrier__c = oCarrier.Id;
            tt.Unit__c = oCarrierUnit.Id;
            tt.Facility_Lookup__c = oFacility.Id;
            tt.Facility_Type__c = 'Facility';
            tt.Facility_String_Hidden__c = tt.Facility_Lookup__c;
        }
        update lTrips;

        Test.startTest();

        //set current page - FM_TruckListsCustom
        PageReference pageRef = Page.fm_trucklistscustomdetail;
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('dv_date', String.valueOf(lTrips.get(0).Run_Sheet_Date__c));
        ApexPages.currentPage().getParameters().put('dv_status', 'Booked');
        ApexPages.currentPage().getParameters().put('dv_shift', lTrips.get(0).Shift__c);
        ApexPages.currentPage().getParameters().put('dv_unit', lTrips.get(0).Unit__c);

        FM_TruckListsCustomCtrl controller = new FM_TruckListsCustomCtrl();

        //filter properties
        List<SelectOption> unitSelectOptionList = controller.unitSelectOptionList;
        List<SelectOption> dispatchDeskList = controller.dispatchDeskSelectOptionList;
        String DispatchedOnString = controller.DispatchedOnString;
        List<SelectOption> shiftSelectOptionList = controller.shiftSelectOptionList;
        List<SelectOption> unitSelectOptions = controller.unitSelectOptions;
        List<SelectOption> statusSelectOptionListForEntry = controller.statusSelectOptionListForEntry;
        FM_Truck_List_Comments__c oComments = controller.oComments;
        List<SelectOption> lFacilityList = controller.lFacilityList;
        FM_TruckListsCustomCtrl.ParamCombinationWrapper currentWrap = controller.getCurrentWrapper();

        //Test Navigation Urls
        system.assertEquals(controller.setParams(Page.FM_TruckListsCustomEdit).getURL(), controller.showEdit().getURL());
        system.assertEquals(Page.FM_TruckListsCustom.getURL(), controller.back().getURL());

        //Test cancel dialog
        pageRef = Page.FM_TruckListsCustomEdit;
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('truckTripId', lTrips.get(0).Id);
        ApexPages.currentPage().getParameters().put('dispatchedTrips', 'true');
        ApexPages.currentPage().getParameters().put('oldTruckTripStatus', 'Dispatched');
        ApexPages.currentPage().getParameters().put('dv_date', String.valueOf(lTrips.get(0).Run_Sheet_Date__c));
        ApexPages.currentPage().getParameters().put('dv_status', 'Booked');
        ApexPages.currentPage().getParameters().put('dv_shift', lTrips.get(0).Shift__c);
        ApexPages.currentPage().getParameters().put('dv_unit', lTrips.get(0).Unit__c);
        ApexPages.currentPage().getParameters().put('filter_date', String.valueOf(lTrips.get(0).Run_Sheet_Date__c));
        ApexPages.currentPage().getParameters().put('filter_status', 'Booked');
        ApexPages.currentPage().getParameters().put('filter_shift', lTrips.get(0).Shift__c);
        ApexPages.currentPage().getParameters().put('filter_unit', lTrips.get(0).Unit__c);
        ApexPages.currentPage().getParameters().put('filter_desks', String.ValueOf([SELECT Id FROM Dispatch_Desk__c].get(0).Id));

        controller = new FM_TruckListsCustomCtrl();

        FM_Truck_Trip__c tt = controller.truckTrips.get(0).truckTrip;
        tt.Facility_String_Hidden__c = null;
        tt.Facility_Lookup__c = oFacility.Id;
        tt.Location_Lookup__c = oLocation.Id;
        tt.Well__c = oLocation.Id;
        ApexPages.currentPage().getParameters().put('truckTripId', tt.Id);
        controller.clearFacilityAndLocation();
        System.assertEquals(null, tt.Facility_Lookup__c);
        System.assertEquals(null, tt.Location_Lookup__c);
        System.assertEquals(null, tt.Well__c);

        controller.truckTrips.get(0).tripProductChanged = false;
        controller.displayProductChange = false;
        tt.Truck_Trip_Status__c = FM_Utilities.TRUCKLIST_STATUS_DISPATCHED;
        controller.productChangeReasonVisibility();
        System.assert(controller.truckTrips.get(0).tripProductChanged);
        System.assert(controller.displayProductChange);

        controller.openCancelDialog();
        System.assertEquals(true, controller.showCancelPopup);
        controller.closeCancelDialog();
        System.assertEquals(false, controller.showCancelPopup);
        controller.openCancelDialog();
        controller.confirmCancelDialog();

        Test.stopTest();
    }

    @isTest static void standaloneMethodsTest() {


        List<FM_Truck_Trip__c> lTrips = [SELECT Id, Facility_Type__c, Location_Lookup__c, Run_Sheet_Lookup__r.Facility__c,
                Run_Sheet_Lookup__r.Well__c, Run_Sheet_Lookup__r.Well__r.Well_Type__c, Run_Sheet_Lookup__r.Well__r.Fluid_Location_Ind__c,
                Product__c,  Facility__c, Shift__c, Load_Weight__c, Truck_Trip_Status__c, Unit__c, Run_Sheet_Date__c
        FROM FM_Truck_Trip__c];
        Account oAccount = [SELECT Id FROM Account LIMIT 1];
        Carrier__c oCarrier = [SELECT Id, Carrier_Name_For_Fluid__c, Enabled__c FROM Carrier__c LIMIT 1];
        Carrier_Unit__c oCarrierUnit = [SELECT Id FROM Carrier_Unit__c LIMIT 1];
        Facility__c oFacility = [SELECT Id, Facility_Name_For_Fluid__c FROM Facility__c LIMIT 1];
        Location__c oLocation = [SELECT Id,  Location_Name_For_Fluid__c, Fluid_Location_Ind__c  FROM Location__c LIMIT 1];

        for(FM_Truck_Trip__c tt:lTrips){
            tt.Carrier__c = oCarrier.Id;
            tt.Unit__c = oCarrierUnit.Id;
            tt.Facility_Lookup__c = oFacility.Id;
            tt.Facility_Type__c = 'Facility';
            tt.Facility_String_Hidden__c = tt.Facility_Lookup__c;
        }
        update lTrips;

        Test.startTest();

        //set current page - FM_TruckListsCustom
        PageReference pageRef = Page.FM_TruckListsCustom;
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('dv_date', String.valueOf(lTrips.get(0).Run_Sheet_Date__c));
        ApexPages.currentPage().getParameters().put('dv_status', 'Booked');
        ApexPages.currentPage().getParameters().put('dv_shift', lTrips.get(0).Shift__c);
        ApexPages.currentPage().getParameters().put('dv_unit', lTrips.get(0).Unit__c);

        FM_TruckListsCustomCtrl controller = new FM_TruckListsCustomCtrl();

        //filter properties
        List<SelectOption> unitSelectOptionList = controller.unitSelectOptionList;
        List<SelectOption> dispatchDeskList = controller.dispatchDeskSelectOptionList;
        String DispatchedOnString = controller.DispatchedOnString;
        List<SelectOption> shiftSelectOptionList = controller.shiftSelectOptionList;
        List<SelectOption> unitSelectOptions = controller.unitSelectOptions;
        List<SelectOption> statusSelectOptionListForEntry = controller.statusSelectOptionListForEntry;
        FM_Truck_List_Comments__c oComments = controller.oComments;
        List<SelectOption> lFacilityList = controller.lFacilityList;
        FM_TruckListsCustomCtrl.ParamCombinationWrapper currentWrap = controller.getCurrentWrapper();

        //Test Navigation Urls
        system.assertEquals(controller.setParams(Page.FM_TruckListsCustomEdit).getURL(), controller.showEdit().getURL());
        system.assertEquals(Page.FM_TruckListsCustom.getURL(), controller.back().getURL());

        //Test cancel dialog
        pageRef = Page.FM_TruckListsCustom;
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('truckTripId', lTrips.get(0).Id);
        ApexPages.currentPage().getParameters().put('dispatchedTrips', 'true');
        ApexPages.currentPage().getParameters().put('oldTruckTripStatus', 'Dispatched');
        ApexPages.currentPage().getParameters().put('dv_date', String.valueOf(lTrips.get(0).Run_Sheet_Date__c));
        ApexPages.currentPage().getParameters().put('dv_status', 'Booked');
        ApexPages.currentPage().getParameters().put('dv_shift', lTrips.get(0).Shift__c);
        ApexPages.currentPage().getParameters().put('dv_unit', lTrips.get(0).Unit__c);
        ApexPages.currentPage().getParameters().put('filter_date', String.valueOf(lTrips.get(0).Run_Sheet_Date__c));
        ApexPages.currentPage().getParameters().put('filter_status', 'Booked');
        ApexPages.currentPage().getParameters().put('filter_shift', lTrips.get(0).Shift__c);
        ApexPages.currentPage().getParameters().put('filter_unit', lTrips.get(0).Unit__c);
        ApexPages.currentPage().getParameters().put('filter_desks', String.ValueOf([SELECT Id FROM Dispatch_Desk__c].get(0).Id));

        controller = new FM_TruckListsCustomCtrl();

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Info message'));
        System.assert(!controller.isSaveDisabled);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Comments'));
        System.assert(controller.isSaveDisabled);

        Test.stopTest();
    }

    @isTest
    static void controllerWithURLParamsEditPage(){


        List<FM_Truck_Trip__c> lTrips = [SELECT Id, Facility_Type__c, Location_Lookup__c, Run_Sheet_Lookup__r.Facility__c,
                Run_Sheet_Lookup__r.Well__c, Run_Sheet_Lookup__r.Well__r.Well_Type__c, Run_Sheet_Lookup__r.Well__r.Fluid_Location_Ind__c,
                Product__c,  Facility__c, Shift__c, Load_Weight__c, Truck_Trip_Status__c, Unit__c, Run_Sheet_Date__c,
                TL_First_Booked_By__c, TT_Key_String__c
        FROM FM_Truck_Trip__c];
        Account oAccount = [SELECT Id FROM Account LIMIT 1];
        Carrier__c oCarrier = [SELECT Id, Carrier_Name_For_Fluid__c, Enabled__c FROM Carrier__c LIMIT 1];
        Carrier_Unit__c oCarrierUnit = [SELECT Id FROM Carrier_Unit__c LIMIT 1];
        Facility__c oFacility = [SELECT Id, Facility_Name_For_Fluid__c FROM Facility__c LIMIT 1];
        Location__c oLocation = [SELECT Id,  Location_Name_For_Fluid__c, Fluid_Location_Ind__c  FROM Location__c LIMIT 1];
        Location__c oLocation_oil = [SELECT Id,  Location_Name_For_Fluid__c, Name  FROM Location__c WHERE Name = 'Husky Well Oil' LIMIT 1];

        for(FM_Truck_Trip__c tt:lTrips){
            tt.Carrier__c = oCarrier.Id;
            tt.Unit__c = oCarrierUnit.Id;
            tt.Location_Lookup__c = oLocation.Id;
            tt.Facility_Type__c = 'Location';
            tt.Facility_String_Hidden__c = oLocation.Id+'###_WELL_###';
            tt.Well__c = oLocation_oil.Id;
            tt.Facility_Lookup__c = oFacility.Id;
        }


        //to test clearFacilityAndLocation() and destination facility/location field visibility
        lTrips[0].Load_Type__c = null;

        lTrips[0].Well__c = oLocation_oil.Id;


        //to test clearFacility()
        lTrips[1].Well__c = oLocation_oil.Id;
        lTrips[1].Facility_Lookup__c = oFacility.Id;
        lTrips[1].Facility_Type__c = 'Facility';

        //to test clearLocation()
        lTrips[2].Facility_Lookup__c = oFacility.Id;
        lTrips[2].Well__c = oLocation_oil.Id;

        update lTrips;

        Test.startTest();

        PageReference pageRef = Page.fm_trucklistscustomedit;
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('dv_date', String.valueOf(lTrips.get(0).Run_Sheet_Date__c));
        ApexPages.currentPage().getParameters().put('dv_status', 'Booked');
        ApexPages.currentPage().getParameters().put('dv_shift', lTrips.get(0).Shift__c);
        ApexPages.currentPage().getParameters().put('dv_unit', lTrips.get(0).Unit__c);

        ApexPages.currentPage().getParameters().put('filter_date', String.valueOf(lTrips.get(0).Run_Sheet_Date__c));
        ApexPages.currentPage().getParameters().put('filter_status', 'Booked');
        ApexPages.currentPage().getParameters().put('filter_shift', lTrips.get(0).Shift__c);
        ApexPages.currentPage().getParameters().put('filter_unit', lTrips.get(0).Unit__c);
        ApexPages.currentPage().getParameters().put('filter_dispatchers', String.ValueOf(UserInfo.getUserId()));

        FM_TruckListsCustomCtrl controller = new FM_TruckListsCustomCtrl();

        //filter properties
        List<SelectOption> unitSelectOptionList = controller.unitSelectOptionList;
        String DispatchedOnString = controller.DispatchedOnString;
        List<SelectOption> shiftSelectOptionList = controller.shiftSelectOptionList;
        List<SelectOption> unitSelectOptions = controller.unitSelectOptions;
        List<SelectOption> statusSelectOptionListForEntry = controller.statusSelectOptionListForEntry;
        Boolean canDispatch = controller.canDispatch;
        FM_Truck_List_Comments__c oComments = controller.oComments;
        List<SelectOption> lFacilityList = controller.lFacilityList;
        FM_TruckListsCustomCtrl.ParamCombinationWrapper currentWrap = controller.getCurrentWrapper();

        controller.getDispatchers();

        system.assertEquals(false,controller.readOnlyMode);

        system.assertEquals(oLocation.Id, controller.truckTrips[0].truckTrip.Location_Lookup__c);
        system.assertEquals(oLocation_oil.Id, controller.truckTrips[0].truckTrip.Well__c);
        System.currentPageReference().getParameters().put('trucktripId', controller.truckTrips[0].truckTrip.Id);
        controller.clearFacilityAndLocation();
        system.assertEquals(null, controller.truckTrips[0].truckTrip.Location_Lookup__c);
        system.assertEquals(null, controller.truckTrips[0].truckTrip.Well__c);
        String tmp_FacStringHidden = controller.truckTrips[0].truckTrip.Facility_String_Hidden__c;

        controller.truckTrips[0].truckTrip.Facility_String_Hidden__c = null;

        //System.assert(controller.truckTrips[0].getDestinationFacilityFieldVisibility());
        System.assert(!controller.truckTrips[0].getDestinationLocationFieldVisibility());
        controller.truckTrips[0].truckTrip.Load_Type__c = 'FloodUp';
        //System.assert(controller.truckTrips[0].getDestinationFacilityFieldVisibility());
        System.assert(controller.truckTrips[0].getDestinationLocationFieldVisibility());
        controller.truckTrips[0].truckTrip.Facility_String_Hidden__c = tmp_FacStringHidden;
        controller.truckTrips[0].truckTrip.Load_Type__c = null;

        system.assertEquals(oFacility.Id, controller.truckTrips[1].truckTrip.Facility_Lookup__c);
        System.currentPageReference().getParameters().put('trucktripId', controller.truckTrips[1].truckTrip.Id);
        controller.clearFacility();
        system.assertEquals(null, controller.truckTrips[1].truckTrip.Facility_Lookup__c);


        system.assertEquals('Location', controller.truckTrips[2].truckTrip.Facility_Type__c);
        System.currentPageReference().getParameters().put('trucktripId', controller.truckTrips[2].truckTrip.Id);
        controller.clearLocation();
        system.assertEquals('Facility', controller.truckTrips[2].truckTrip.Facility_Type__c);



        //controller.truckTrips[0].truckTrip.Load_Type__c = 'ServiceLoad';
        //controller.clearFacilityAndLocation();
        System.debug('TT0 LT:' + controller.truckTrips[0].truckTrip.Load_Type__c);
        System.debug('TT0 FSH:' + controller.truckTrips[0].truckTrip.Facility_String_Hidden__c);
        System.debug('TT0 Well:' + controller.truckTrips[0].truckTrip.Well__c);

        System.debug('TT1 LT:' + controller.truckTrips[1].truckTrip.Load_Type__c);
        System.debug('TT1 FSH:' + controller.truckTrips[1].truckTrip.Facility_String_Hidden__c);
        System.debug('TT1 Well:' + controller.truckTrips[1].truckTrip.Well__c);

        System.debug('TT2 LT:' + controller.truckTrips[2].truckTrip.Load_Type__c);
        System.debug('TT2 FSH:' + controller.truckTrips[2].truckTrip.Facility_String_Hidden__c);
        System.debug('TT2 Well:' + controller.truckTrips[2].truckTrip.Well__c);

        System.assertEquals(null, controller.redirectToConfirmationCreationPage());
        for(Integer i=0; i < ApexPages.getMessages().size(); i++) {
            System.assertEquals('You can only confirm dispatched trucktrips.', ApexPages.getMessages().get(i).getSummary());
            System.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages().get(i).getSeverity());
        }

        //submit without order
        controller.SaveTruckList();
        controller.SubmitTruckList();

        Integer i = 1;
        for(FM_TruckListsCustomCtrl.TruckTripWrapper tt: controller.truckTrips){
            tt.truckTrip.Order__c = i;
            i++;
        }

        //submit without dispatch desk phone
        controller.SaveTruckList();
        controller.SubmitTruckList();

        RecordType ddRecordType = [select Id from RecordType where DeveloperName = 'Fluid_Dispatch'];
        Dispatch_Desk__c dd = new Dispatch_Desk__c(Name = 'ddtest1',
                Phone__c = '123456789',
                RecordTypeId = ddRecordType.Id);
        insert dd;

        List<SelectOption> dispatchDeskList = controller.dispatchDeskSelectOptionList;
        controller.sDeskSelection = dd.Id;

        //submit without email on unit
        controller.SaveTruckList();
        controller.SubmitTruckList();

        oCarrierUnit.Unit_Email__c = 'testing@testing.com';
        update oCarrierUnit;

        //submit final
        controller.SaveTruckList();
        controller.SubmitTruckList();

        System.currentPageReference().getParameters().put('trucktripId', controller.truckTrips[0].truckTrip.Id);
        System.assertNotEquals(null, controller.redirectToConfirmationCreationPage());
        System.assertNotEquals(null, controller.redirectToDispatchView());

        //Test rebook open
        Test.setCurrentPage(controller.showDetail());
        controller.refreshTruckTrips();
        System.currentPageReference().getParameters().put('trucktripId', controller.truckTrips[0].truckTrip.Id);
        System.assertEquals(null, controller.RebookTruckTripPopupOpen());
        controller.truckTripRebooked.truckTrip.Shift__c = 'Night';
        System.assertEquals(controller.truckTrips[0].truckTrip.Id, controller.truckTripGivenBack.Id);
        System.assertNotEquals(null, controller.truckTripRebooked);
        System.assertEquals(FM_Utilities.TRUCKTRIP_STATUS_REDISPATCH, controller.truckTripRebooked.truckTrip.Truck_Trip_Status__c);
        System.assertEquals(Date.today(), controller.truckTripRebooked.truckTrip.Run_Sheet_Date__c);
        //Test required information validation
        System.assertEquals(null, controller.RebookTruckTripPopupConfirm());
        System.assert(controller.isRebookError);
        System.assertEquals('Rebook reason is required!', controller.rebookErrorMsg);

        //Test pop up confirm redispatch
        Carrier_Unit__c unit = [SELECT Id, Name FROM Carrier_Unit__c WHERE Id !=: controller.truckTripGivenBack.Unit__c][0];

        controller.truckTripGivenBack.Rebook_Reason__c = 'Broke down';
        controller.truckTripRebooked.truckTrip.Shift__c = 'Night';
        controller.truckTripRebooked.truckTrip.Unit__c = unit.Id;
        controller.truckTripRebooked.truckTrip.Unit__r = unit;

        String truckListName = FM_Utilities.TRUCKTRIP_STATUS_BOOKED + ' ' +
                controller.truckTripRebooked.truckTrip.Unit__r.Name + ' ' +
                controller.truckTripRebooked.truckTrip.Shift__c + ' ' +
                controller.truckTripRebooked.truckTrip.Planned_Dispatch_Date__c.format();
        String successMessage = 'The trucktrip was booked' +
                ' to the following truck list ' + truckListName;

        System.assertEquals(null, controller.RebookTruckTripPopupConfirm());


        List<Apexpages.Message> msgs = ApexPages.getMessages();

        boolean b = false;
        for (Apexpages.Message msg:msgs) {
            if (msg.getDetail().contains(successMessage)) b = true;
        }

        system.assert(b);

        //Test Navigation Urls
        System.assertEquals(0, controller.back().getURL().indexOf(Page.FM_TruckListsCustom.getURL()));

        Test.stopTest();
    }

    @isTest
    static void controllerWithURLParamsEditPage2() {
        System.debug('********* UNIT TEST: controllerWithURLParamsEditPage2 ***************');

        List<FM_Truck_Trip__c> lTrips = [SELECT Id, Facility_Type__c, Location_Lookup__c, Run_Sheet_Lookup__r.Facility__c,
                Run_Sheet_Lookup__r.Well__c, Run_Sheet_Lookup__r.Well__r.Well_Type__c, Run_Sheet_Lookup__r.Well__r.Fluid_Location_Ind__c,
                Product__c,  Facility__c, Shift__c, Load_Weight__c, Truck_Trip_Status__c, Unit__c, Run_Sheet_Date__c,
                TL_First_Booked_By__c, TT_Key_String__c
        FROM FM_Truck_Trip__c];
        Account oAccount = [SELECT Id FROM Account LIMIT 1];
        Carrier__c oCarrier = [SELECT Id, Carrier_Name_For_Fluid__c, Enabled__c FROM Carrier__c LIMIT 1];
        Carrier_Unit__c oCarrierUnit = [SELECT Id FROM Carrier_Unit__c LIMIT 1];
        Facility__c oFacility = [SELECT Id, Facility_Name_For_Fluid__c FROM Facility__c LIMIT 1];
        Facility__c nFacility = [SELECT Id, Facility_Name_For_Fluid__c FROM Facility__c WHERE Facility_Name_For_Fluid__c='Facility C' LIMIT 1];
        Location__c oLocation = [SELECT Id,  Location_Name_For_Fluid__c, Fluid_Location_Ind__c  FROM Location__c LIMIT 1];
        Location__c oLocation_oil = [SELECT Id,  Location_Name_For_Fluid__c, Name  FROM Location__c WHERE Name = 'Husky Well Oil' LIMIT 1];

        for(FM_Truck_Trip__c tt:lTrips){
            tt.Carrier__c = oCarrier.Id;
            tt.Unit__c = oCarrierUnit.Id;
            tt.Location_Lookup__c = oLocation.Id;
            tt.Facility_Type__c = 'Location';
            tt.Facility_String_Hidden__c = oLocation.Id+'###_WELL_###';
        }


        //to test clearFacilityAndLocation() and destination facility/location field visibility
        lTrips[0].Load_Type__c = null;

        lTrips[0].Well__c = oLocation_oil.Id;


        //to test clearFacility()
        lTrips[1].Well__c = oLocation_oil.Id;
        lTrips[1].Facility_Lookup__c = oFacility.Id;
        lTrips[1].Facility_Lookup_Original__c = nFacility.Id;
        lTrips[1].Facility_Type__c = 'Facility';

        //to test clearLocation()
        lTrips[2].Facility_Lookup__c = oFacility.Id;
        lTrips[2].Well__c = oLocation_oil.Id;

        update lTrips;

        Test.startTest();

        PageReference pageRef = Page.fm_trucklistscustomedit;
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('dv_date', String.valueOf(lTrips.get(0).Run_Sheet_Date__c));
        ApexPages.currentPage().getParameters().put('dv_status', 'Booked');
        ApexPages.currentPage().getParameters().put('dv_shift', lTrips.get(0).Shift__c);
        ApexPages.currentPage().getParameters().put('dv_unit', lTrips.get(0).Unit__c);

        ApexPages.currentPage().getParameters().put('filter_date', String.valueOf(lTrips.get(0).Run_Sheet_Date__c));
        ApexPages.currentPage().getParameters().put('filter_status', 'Booked');
        ApexPages.currentPage().getParameters().put('filter_shift', lTrips.get(0).Shift__c);
        ApexPages.currentPage().getParameters().put('filter_unit', lTrips.get(0).Unit__c);
        ApexPages.currentPage().getParameters().put('filter_dispatchers', String.ValueOf(UserInfo.getUserId()));

        FM_TruckListsCustomCtrl controller = new FM_TruckListsCustomCtrl();

        System.debug(controller.truckTrips.get(1).getOriginalDestination());

        Test.stopTest();
    }



    @testSetup
    private static void CreateTestData() {
        Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), true);
        Field__c field = HOG_TestDataFactory.createAMU(true);

        Dispatch_Desk__c dispatchDesk = HOG_TestDataFactory.createDispatchDesk(true);
        Route__c route = HOG_TestDataFactory.createRoute('999', false);
        route.Dispatch_Desk__c = dispatchDesk.Id;
        insert route;

        Carrier__c carrier = FM_TestDataFactory.createCarrier(acc.Id, true);

        Carrier_Unit__c carrierUnit1 = FM_TestDataFactory.createCarrierUnit('Unit1', carrier.Id, true);
        Carrier_Unit__c carrierUnit2 = FM_TestDataFactory.createCarrierUnit('Unit2', carrier.Id, true);

        Location__c location1 = HOG_TestDataFactory.createLocation(route.Id, field.Id, false);
        location1.Name = 'Husky Well';
        location1.Location_Name_for_Fluid__c = 'Husky Well Fluid';
        Location__c location2 = HOG_TestDataFactory.createLocation(route.Id, field.Id, false);
        location2.Well_Type__c = 'OIL';
        location2.Fluid_Location_Ind__c = false;
        location2.Name = 'Husky Well Oil';
        insert new List<Location__c>{location1, location2};

        Facility__c facility = HOG_TestDataFactory.createFacility('Facility B', route.Id, field.Id, true);
        Facility__c facility2 = HOG_TestDataFactory.createFacility('Facility C', route.Id, field.Id, true);

        FM_Run_Sheet__c runSheet = FM_TestDataFactory.createRunSheetOnLocation(location1.Id, false);
        runSheet.Act_Tank_Level__c = 40;
        insert runSheet;

        List<FM_Load_Request__c> loadRequests = new List<FM_Load_Request__c>();
        loadRequests.addAll(FM_TestDataFactory.createLoadRequests(runSheet.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_LOADTYPE_STANDARD,
                'O', 'night', carrier.Id,
                'T5X',
                FM_LoadRequest_Utilities.LOADREQUEST_LOADWEIGHT_PRIMARY, null,
                'Load Request created from Runsheet', null,
                location1.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED, null,
                'Tank 1',
                100, 60, 120, 4, 5, 3,
                false));
        loadRequests.addAll(FM_TestDataFactory.createLoadRequests(runSheet.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_LOADTYPE_STANDARD,
                'W', 'night', carrier.Id,
                'T5X',
                FM_LoadRequest_Utilities.LOADREQUEST_LOADWEIGHT_PRIMARY, null,
                'Load Request created from Runsheet', null,
                location1.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED, null,
                'Tank 1',
                100, 60, 120, 4, 5, 4,
                false));
        loadRequests.addAll(FM_TestDataFactory.createLoadRequests(runSheet.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_LOADTYPE_STANDARD,
                'O', 'night', carrier.Id,
                'T5X',
                FM_LoadRequest_Utilities.LOADREQUEST_LOADWEIGHT_PRIMARY, null,
                'Load Request created from Runsheet', null,
                location1.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED, null,
                'Tank 1',
                100, 60, 120, 4, 5, 1,
                false));
        loadRequests.addAll(FM_TestDataFactory.createLoadRequests(runSheet.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_LOADTYPE_STANDARD,
                'W', 'night', carrier.Id,
                'T5X',
                FM_LoadRequest_Utilities.LOADREQUEST_LOADWEIGHT_PRIMARY, null,
                'Load Request created from Runsheet', null,
                location1.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED, null,
                'Tank 1',
                100, 60, 120, 4, 5, 2,
                false));

        insert loadRequests;
        loadRequests.clear();
        loadRequests.addAll(FM_TestDataFactory.createLoadRequests(runSheet.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_LOADTYPE_STANDARD,
                'O', 'night', carrier.Id,
                'T6X',
                FM_LoadRequest_Utilities.LOADREQUEST_LOADWEIGHT_PRIMARY, null,
                'Load Request created from Runsheet', null,
                location1.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED, null,
                'Tank 1',
                100, 60, 120, 4, 5, 3,
                true));
        List<FM_Truck_Trip__c> truckTrips = FM_TestDataFactory.createTruckTrips(loadRequests, null, null, null,
                'Location',
                location2.Id,
                'Today',
                carrierUnit1.Id,
                location2.Id,
                location2.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED,
                true);

        HOG_Settings__c hogSettings = HOG_TestDataFactory.createHOGSettings(true);
    }

}