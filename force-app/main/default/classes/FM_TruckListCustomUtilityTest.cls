/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Test for Truck List Utility classt.
History:        
**************************************************************************************************/
@isTest private class FM_TruckListCustomUtilityTest {
	
	@isTest static void helperMethodsTest() {
		String whereString = ' where';
		String orString = ' or';
		String andString = ' and';

		FM_TruckListCustomUtility.isFirst = true;
		System.assertEquals(whereString ,FM_TruckListCustomUtility.whereOrAnd());
		System.assertEquals(andString ,FM_TruckListCustomUtility.whereOrAnd());
		System.assert(!FM_TruckListCustomUtility.isFirst);
		FM_TruckListCustomUtility.isFirst = true;
		System.assertEquals(whereString ,FM_TruckListCustomUtility.whereOrOr());
		System.assertEquals(orString ,FM_TruckListCustomUtility.whereOrOr());
		System.assert(!FM_TruckListCustomUtility.isFirst);
	}

}