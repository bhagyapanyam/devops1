@isTest
private class FeedbackClass_Test
{
    static TestMethod void testFeedback()
    {
        Case caseRecord = new Case(Status='New',RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Service').getRecordTypeId());
        insert caseRecord;
        ApexPages.currentPage().getParameters().put('id', caseRecord.Id);
        ApexPages.currentPage().getParameters().put('rating', '1');        
        FeedbackClass  f = new FeedbackClass ();
        f.MYVAR = '1';
        f.submitSatisfactionComment();
        f.comments='My Comment';
        f.selectedFeedback = 'Very Unsatisfied';     
        f.submit();
        ApexPages.currentPage().getParameters().put('id', caseRecord.Id);
        ApexPages.currentPage().getParameters().put('rating', '2');                   
        f.MYVAR = '2'; 
        f.submitSatisfactionComment();
        f.submit();
        ApexPages.currentPage().getParameters().put('rating', '3');        
        //f.selectedFeedback = 'Satisfied'; 
        f.MYVAR = '3';    
        f.submitSatisfactionComment();
        f.submit();
        ApexPages.currentPage().getParameters().put('rating', '4');        
        //f.selectedFeedback = 'Satisfied'; 
        f.MYVAR = '4';    
        f.submitSatisfactionComment();
        f.submit(); 
        
        ApexPages.currentPage().getParameters().put('rating', '123456789101112');        
        f.MYVAR = '4';  
        f.submitSatisfactionComment();
        f.submit(); 
    }    
}