public with sharing class GRDFilterController
{
    // For now, only display these wells.
//    public static Set<String> DisplayGRDWells = new Set<String>{'HUSKY HZ ANSELL 102/14-23-50-19','HUSKY HZ ANSELL 2-13-50-19','HUSKY HZ MINEHEAD 16-34-49-20','HUSKY HZ MINEHEAD 1-27-49-20','HUSKY HZ ANSELL 14-34-50-19','HUSKY HZ ANSELL 15-34-50-19','HUSKY HZ ANSELL 4-26-50-19','HUSKY HZ ANSELL 3-26-50-19','HUSKY HZ MINEHEAD 4-14-49-19','HUSKY HZ ANSELL 16-24-51-19','HUSKY HZ ANSELL 1-13-51-19','HUSKY HZ ANSELL 1-12-51-19','HUSKY HZ ANSELL 3-3-51-19','HUSKY HZ ANSELL 1-3-51-19','HUSKY HZ ANSELL 14-9-51-19','HUSKY HZ ANSELL 16-9-51-19','HUSKY HZ ANSELL 16-14-51-19','HUSKY HZ ANSELL 14-14-51-19','HUSKY HZ ANSELL 2-12-51-19','HUSKY HZ ANSELL 10-15-50-20','HUSKY HZ ANSELL 1-11-50-20','HUSKY HZ ANSELL 12-11-50-20','HUSKY HZ MINEHEAD 13-34-49-20','HUSKY HZ MINEHEAD 4-27-49-20','HUSKY 103 HZ ANSELL 4-13-50-19','HUSKY HZ ANSELL 15-23-50-19','HUSKY HZ MINEHEAD 13-1-49-19','HUSKY MINEHEAD 2-25-48-18','HUSKY HZ ANSELL 2-22-50-20','HUSKY HZ ANSELL 9-12-50-19','HUSKY HZ ANSELL 13-14-50-19','HUSKY HZ ANSELL 2-12-50-19','HUSKY HZ ANSELL 3-23-50-19','HUSKY HZ ANSELL 2-23-50-19','HUSKY HZ MINEHEAD 9-36-49-19','HUSKY HZ SUNDANCE 14-6-54-21 ','HUSKY HZ ANSELL 16-28-50-19','HUSKY HZ ANSELL 5-23-50-19','HUSKY HZ ANSELL 11-23-50-19','HUSKY HZ ANSELL 13-2-50-19','HUSKY HZ ANSELL 2-4-50-18','HUSKY HZ ANSELL 16-34-50-20','HUSKY HZ ANSELL 16-5-50-18','HUSKY HZ ANSELL 5-18-50-18','HUSKY HZ FOX CREEK 4-6-61-17W5','HUSKY HZ FOX CREEK 2-1-61-18W5','HUSKY HZ FOX CREEK 1-1-61-18W5','HUSKY HZ KAYBOBS 16-6-61-17W5','HUSKY HZ KAYBOBS 13-5-61-17W5','HUSKY HZ KAYBOBS 14-5-61-17W5','HUSKY HZ KAYBOBS 16-16-60-18W5','HUSKY 102 KAYBOBS 13-15-60-18W5','HUSKY 102 HZ KAYBOBS 5-22-60-18'};
    
    // List view defaults
    public Integer wellsPerPage {get;set;}
    
    // The field the user wants to sort by, and the previous field user sorted by
    public String sortField         {get;set;}
    public String previousSortField {get;set;}
    public String sortOrder         {get;set;}
    
    // Returns the wells shown in the list view
    public List<GRD_Borehole__c> getWells()
    {
        return (List<GRD_Borehole__c>) mySetController.getRecords();
    }
    
    // The standard controller
    public ApexPages.StandardSetController mySetController {get;set;}
    
    public String fieldValue {get;set;}
    public String fieldLabel {get;set;}
    public String fieldAPIName {get;set;}
    
    public GRDFilterController()
    {
        if(ApexPages.CurrentPage().getParameters().containsKey('field') && ApexPages.CurrentPage().getParameters().containsKey('value'))
        {
            fieldAPIName = ApexPages.CurrentPage().getParameters().get('field');
            
            if(fieldAPIName == 'Budget_Year')
            {
                fieldLabel = 'Budget Year';
            }
            else if(fieldAPIName == 'Contact')
            {
                fieldLabel = 'Contact';
            }
            else
            {
                fieldLabel = Schema.getGlobalDescribe().get('GRD_Borehole__c').getDescribe().fields.getMap().get(fieldAPIName).getDescribe().getLabel();
            }
            
            fieldValue = System.EncodingUtil.urlDecode(ApexPages.CurrentPage().getParameters().get('value'), 'UTF-8');
            
            // By default, we sort wells in page block table by Name.
            sortField = 'Name';
            
            wellsPerPage = 20;
            
            // Fetch the wells
            viewWells();
        }
    }
    
    // Resets the previous sort field.
    public PageReference viewWells()
    {
        previousSortField = null;
        return sortWells();
    }
    
    public PageReference sortWells()
    {
        //========================================================
        // Now run the query to fetch the wells for the list view
        //========================================================
        
        
        
        Boolean hasWHERE = false;
        
        String order = 'asc NULLS FIRST';
        
        sortOrder = 'ASC';
        
        // If we`re sorting the same field as last time, then we want to switch the order from ASC to DESC
        if(previousSortField == sortField)
        {
            order = 'desc NULLS LAST';
            previousSortField = null;
            sortOrder = 'DESC';
        }
        else
        {
            previousSortField = sortField;
        }
        
        String query = 'SELECT Id,Name,Well_Class__c,Well__r.Surface_Location__c,Well__r.Surface_Location__r.Name,Well_Orientation__c,UWI_Location__r.Coordinate__Latitude__s,UWI_Location__r.Coordinate__Longitude__s,Well_License_No__c,Budget_Year_Drilling__c,Budget_Year_Completion__c,Budget_Year_Tie_In__c,Budget_Year_Facility_Equipment__c,Target_Formation__c FROM GRD_Borehole__c ';
        
        if(fieldAPIName == 'Budget_Year')
        {
////            query += ' WHERE Name IN: DisplayGRDWells AND (Budget_Year_Drilling__c =: fieldValue OR Budget_Year_Completion__c =: fieldValue OR Budget_Year_Tie_In__c =: fieldValue) ';
            query += ' WHERE (Budget_Year_Drilling__c =: fieldValue OR Budget_Year_Completion__c =: fieldValue OR Budget_Year_Tie_In__c =: fieldValue OR Budget_Year_Facility_Equipment__c =: fieldValue) ';
        }
        else if(fieldAPIName == 'Contact')
        {
/////            query += ' WHERE Name IN: DisplayGRDWells AND (Geologist__c =: fieldValue OR Reservoir_Engineer__c =: fieldValue OR Construction_Superintendent__c =: fieldValue OR Drilling_Superintendent__c =: fieldValue OR Completion_Superintendent__c =: fieldValue OR Facilities_Engineer__c =: fieldValue) ';
            query += ' WHERE (Geologist__c =: fieldValue OR Reservoir_Engineer__c =: fieldValue OR Construction_Superintendent__c =: fieldValue OR Drilling_Superintendent__c =: fieldValue OR Completion_Superintendent__c =: fieldValue OR Facilities_Engineer__c =: fieldValue) ';
        }
        else
        {
/////            query += ' WHERE Name IN: DisplayGRDWells AND ' + fieldAPIName + ' =: fieldValue ';
            query += ' WHERE ' + fieldAPIName + ' =: fieldValue ';
        }
        
        if(sortField == 'Surface Location')
        {
            query += 'ORDER BY Well__r.Surface_Location__r.Name ' + order;
        }
        else if(sortField == 'Target Formation')
        {
            query += 'ORDER BY Target_Formation__c ' + order;
        }
        else if(sortField == 'Well Class')
        {
            query += 'ORDER BY Well_Class__c ' + order;
        }
        else if(sortField == 'Well Orientation')
        {
            query += 'ORDER BY Well_Orientation__c ' + order;
        }
        else if(sortField == 'License Number')
        {
            query += 'ORDER BY Well_License_No__c ' + order;
        }
        else if(sortField == 'Budget Year')
        {
                query += 'ORDER BY Budget_Year_Drilling__c,Budget_Year_Completion__c,Budget_Year_Tie_In__c,Budget_Year_Facility_Equipment__c ' + order;
        }
        else // sortField == 'Name'
        {
            query += 'ORDER BY Name ' + order;
        }
        
        mySetController = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        mySetController.setPageSize(wellsPerPage);
        mySetController.first();
        return null;
    }
}