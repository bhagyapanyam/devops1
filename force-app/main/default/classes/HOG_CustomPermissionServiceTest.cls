/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for HOG_CustomPermissionService
History:        jschn 2019-02-14 - Created.
*************************************************************************************************/
@IsTest
private class HOG_CustomPermissionServiceTest {

    private static final String CUSTOM_PERMISSION_NAME = 'HOG_Vent_Gas_create_Subtask';

    @IsTest
    static void isUserInCustomPermission_withoutParam() {

        Test.startTest();
        Boolean result = HOG_CustomPermissionService.isUserInCustomPermission(null);
        Test.stopTest();

        System.assert(!result);
    }

    @IsTest
    static void isUserInCustomPermission_withBadParam() {
        Boolean result;
        User runningUser = [SELECT Id FROM User WHERE Alias = 'goodBoy'];

        Test.startTest();
        System.runAs(runningUser) {
            result = HOG_CustomPermissionService.isUserInCustomPermission(new Set<String>{
                    CUSTOM_PERMISSION_NAME + 'Test'
            });
        }
        Test.stopTest();

        System.assert(!result);
    }

    @IsTest
    static void isUserInCustomPermission_goodParam_badUser() {
        Boolean result;
        User runningUser = [SELECT Id FROM User WHERE Alias = 'badBoy'];

        Test.startTest();
        System.runAs(runningUser) {
            result = HOG_CustomPermissionService.isUserInCustomPermission(new Set<String>{
                    CUSTOM_PERMISSION_NAME
            });
        }
        Test.stopTest();

        System.assert(!result);
    }

    @IsTest
    static void isUserInCustomPermission_goodParam_goodUser() {
        Boolean result;
        User runningUser = [SELECT Id FROM User WHERE Alias = 'goodBoy'];

        Test.startTest();
        System.runAs(runningUser) {
            result = HOG_CustomPermissionService.isUserInCustomPermission(new Set<String>{
                    CUSTOM_PERMISSION_NAME
            });
        }
        Test.stopTest();

        System.assert(result);
    }

    @TestSetup
    private static void prepareUsers() {
        HOG_TestDataFactory.createUser('Bad','TestUser','badBoy');
        User usr2 = HOG_TestDataFactory.createUser('Good','TestUser','goodBoy');
        usr2 = HOG_TestDataFactory.assignPermissionSet(usr2, 'HOG_Production_Engineer');
        CustomPermission permission = [SELECT Id FROM CustomPermission WHERE DeveloperName =: CUSTOM_PERMISSION_NAME];
        PermissionSetAssignment assignment = [
                SELECT PermissionSetId
                FROM PermissionSetAssignment
                WHERE AssigneeId =: usr2.Id
                        AND PermissionSet.Name = 'HOG_Production_Engineer'
        ];
        SetupEntityAccess access = new SetupEntityAccess(
                SetupEntityId = permission.Id,
                ParentId = assignment.PermissionSetId
        );
        insert access;
    }

}