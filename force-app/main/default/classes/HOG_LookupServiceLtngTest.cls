/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    HOG Service class for lightning containing endpoints for Custom Lookup component.
History:        jschn 07.20.2018 - Created.
**************************************************************************************************/
@IsTest private class HOG_LookupServiceLtngTest {

	@IsTest static void doSearchWithouSharing_matchRecord() {
		List<Account> results = (List<Account>) HOG_LookupServiceLtng.prefetchLookupData('Account', 'Test', false);
		System.assertEquals(1, results.size());
		System.assert(results.get(0).Name.contains('Test'));
	}

	@IsTest static void doSearch_matchRecord() {
		List<Account> results = (List<Account>) HOG_LookupServiceLtng.prefetchLookupData('Account', 'Test', true);
		System.assertEquals(1, results.size());
		System.assert(results.get(0).Name.contains('Test'));
	}
	
	@IsTest static void doSearch_doesntMatchRecord() {
		List<SObject> results = HOG_LookupServiceLtng.prefetchLookupData('Account', '123', true);
		System.assertEquals(0, results.size());
		results = HOG_LookupServiceLtng.prefetchLookupData('', '', true);
		System.assertEquals(null, results);
	}

	@IsTest static void fetchData_matchRecord() {
		List<Account> results = (List<Account>) HOG_LookupServiceLtng.fetchData('Account', 'Name', 'Test', '', true);
		System.assertEquals(1, results.size());
		System.assert(results.get(0).Name.contains('Test'));
	}

	@IsTest static void fetchData_doesntMatchRecord() {
		List<SObject> results = HOG_LookupServiceLtng.fetchData('Account', 'Name', '123', '', true);
		System.assertEquals(0, results.size());
		results = HOG_LookupServiceLtng.fetchData('', '', '', '', true);
		System.assertEquals(null, results);
	}

	@IsTest static void fetchData_matchRecord_NulLSharing() {
		List<Account> results = (List<Account>) HOG_LookupServiceLtng.fetchData('Account', 'Name', 'Test', '', null);
		System.assertEquals(1, results.size());
		System.assert(results.get(0).Name.contains('Test'));
	}

	@IsTest static void fetchData_doesntMatchRecord_NulLSharing() {
		List<SObject> results = HOG_LookupServiceLtng.fetchData('Account', 'Name', '123', '', null);
		System.assertEquals(0, results.size());
		results = HOG_LookupServiceLtng.fetchData('', '', '', '', null);
		System.assertEquals(null, results);
	}

	@IsTest static void fetchData_matchRecordWithoutSharing() {
		List<Account> results = (List<Account>) HOG_LookupServiceLtng.fetchData('Account', 'Name', 'Test', '', false);
		System.assertEquals(1, results.size());
		System.assert(results.get(0).Name.contains('Test'));
	}

	@IsTest static void fetchData_doesntMatchRecordWithoutSharing() {
		List<SObject> results = HOG_LookupServiceLtng.fetchData('Account', 'Name', '123', '', false);
		System.assertEquals(0, results.size());
		results = HOG_LookupServiceLtng.fetchData('', '', '', '', false);
		System.assertEquals(null, results);
	}

	@TestSetup private static void prepareData() {
		HOG_SObjectFactory.createSObject(new Account(), true);
	}
	
}