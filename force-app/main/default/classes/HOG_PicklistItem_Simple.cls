/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Wrapper class for rendering picklist values for Lightning experience.
Test Class:     HOG_PicklistItem_SimpleTest
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
public inherited sharing class HOG_PicklistItem_Simple extends HOG_PicklistItem_Base {

    public HOG_PicklistItem_Simple(String value, String label) {
        super(value, label);
    }

}