/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Utility class for common actions related to Picklists.
Test Class:     HOG_PicklistUtilsTest
History:        jschn 11/10/2019 - Created.
*************************************************************************************************/
public inherited sharing class HOG_PicklistUtils {

    private static final String CLASS_NAME = String.valueOf(HOG_PicklistUtils.class);

    public List<HOG_PicklistItem_Simple> simpleWrap(List<SObject> items, String valueField, String labelField) {
        List<HOG_PicklistItem_Simple> wrappedOptions = new List<HOG_PicklistItem_Simple>();

        if(items != null) {
            for(SObject item : items) {
                try {
                    wrappedOptions.add(
                            new HOG_PicklistItem_Simple(
                                    String.valueOf(item.get(valueField)),
                                    String.valueOf(item.get(labelField))
                            )
                    );
                } catch(Exception ex) {
                    System.debug(CLASS_NAME + ' -> simpleWrap. Exception occurred. Value Field: ' + valueField
                            + '. Label Field: ' + labelField
                            + '. Item: ' + JSON.serialize(item));
                    System.debug(CLASS_NAME + ' -> simpleWrap. Exception: ' + ex.getMessage());
                }
            }
        }

        return wrappedOptions;
    }

}