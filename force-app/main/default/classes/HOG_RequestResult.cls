public abstract class HOG_RequestResult {

	@AuraEnabled public Boolean success {get;set;}
	@AuraEnabled public List<String> errors {get;set;}

	abstract List<String> addError(Exception ex);
	abstract List<String> addError(String errorMsg);

}