/*************************************************************************************************\
Author:         Shreyas Dhond
Company:        Husky Energy
Description:    Batch class for sending notifications on Vent Gas Alert.
Test class:     HOG_VGEmailNotificationsTest
History:        sdhond 04.25.2018 - Created.
                mz: 23-May-2018 - sending notifications to Production Engineers on Re-assignment on Alert (generateEmailNotification method)
                jschn 25/10/2018 - Major refactoring. Part of most of logic moved to HOG_VG_Alert_EmailService.
                                - US W-001290, W-001293, W-001304
**************************************************************************************************/
global class HOG_VGEmailNotifications implements Database.Batchable<SObject> {

    private String query;

    private HOG_VGEmailNotificationsType notificationType;
    private HOG_VG_Alert_EmailService emailService;

    public HOG_VGEmailNotifications(HOG_VGEmailNotificationsType notificationType) {
        this.notificationType = notificationType;
        emailService = new HOG_VG_Alert_EmailService();
        buildQuery();
    }

    private void buildQuery() {
        if (notificationType == HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_ALERTS
                || notificationType == HOG_VGEmailNotificationsType.NON_URGENT_INCOMPLETE_ALERTS) {
            query = emailService.getNotificationQuery(notificationType,
                    emailService.getDayOfWeek(System.now()));
        }
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug(query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope) {
        List<HOG_Vent_Gas_Alert__c> ventGasAlertList = (List<HOG_Vent_Gas_Alert__c>) scope;
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();

        if (notificationType != HOG_VGEmailNotificationsType.PRODUCTION_ENGINEER_REASSIGNMENT) {
            emailList = emailService.generateInCompleteVentGasAlertEmails(ventGasAlertList, notificationType);
        }

        if (Limits.getEmailInvocations() < Limits.getLimitEmailInvocations()) {
            Messaging.reserveSingleEmailCapacity(emailList.size());
            Messaging.sendEmail(emailList);
        }
    }

    global void finish(Database.BatchableContext BC) {

    }

    //mz: 23-May-2018 - Method called from "Vent Gas Alert Flow - Send Notification on Production Engineer Re-assignment" Flow
    @InvocableMethod
    public static void generateEmailNotification(List<List<String>> AlertIdAndProdEngId) {
        new HOG_VG_Alert_EmailService().generateEmailNotification(AlertIdAndProdEngId,
                HOG_VGEmailNotificationsType.PRODUCTION_ENGINEER_REASSIGNMENT);
    }

}