/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Scheduling class for Vent Gas notification job.
Test Class: 	HOG_VGEmailNotificationsSchedulerTest
History:        jschn 17/10/2018 - Created.
*************************************************************************************************/
public class HOG_VGEmailNotificationsScheduler {

	public static final String DEFAULT_CRON_STRING = '0 0 9 ? * MON-FRI';
	public static final String JOB_NAME = 'HOG Vent Gas Notification' + (Test.isRunningTest() ? ' Test':'');

	public static Id schedule() {
		return schedule(DEFAULT_CRON_STRING);
	}

	public static Id schedule(String cronString) {
		HOG_VGEmailNotificationsSchedulable  job = new HOG_VGEmailNotificationsSchedulable ();
		return System.schedule(JOB_NAME, cronString, job);
	}

}