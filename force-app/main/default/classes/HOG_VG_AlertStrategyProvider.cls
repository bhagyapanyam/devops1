/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Base class for providing decision whether Alert should be created after Location
                is changed.
Test Class:     XXX
History:        jschn 19/10/2018 - Created. - W-001275 - W-001277
*************************************************************************************************/
public abstract class HOG_VG_AlertStrategyProvider {

    protected List<Location__c> locations;
    protected Map<Id, Location__c> oldLocationsMap;
    protected Map<String, HOG_Vent_Gas_Alert_Exemption_Request__c> ventGasExemptionsByLocationId;
    protected Map<String, HOG_Vent_Gas_Alert__c> activeAlertsByLocationAndType;

    protected final HOG_Vent_Gas_Alert_Configuration__c settings = HOG_Vent_Gas_Alert_Configuration__c.getInstance();

    public HOG_VG_AlertStrategyProvider(List<Location__c> locations,
            Map<Id, Location__c> oldLocationsMap,
            Map<String, HOG_Vent_Gas_Alert_Exemption_Request__c> ventGasExemptionsByLocationId,
            Map<String, HOG_Vent_Gas_Alert__c> activeAlertsByLocationAndType) {
        this.locations = locations;
        this.oldLocationsMap = oldLocationsMap;
        this.ventGasExemptionsByLocationId = ventGasExemptionsByLocationId;
        this.activeAlertsByLocationAndType = activeAlertsByLocationAndType;
    }

    /**
     * This method implementation should run decision logic, whether Alert should be created.
     *
     * @param location
     *
     * @return
     */
    public abstract Boolean shouldCreate(Location__c location);

    /**
     * Provides information whether location supports Vent Gas Alerts.
     * Only Location with Functional Location Category 4, OIL Well Type and non-thermal locations are supported.
     *
     * @param location
     *
     * @return
     */
    protected Boolean isCorrectLocation(Location__c location) {
        return location.Functional_Location_Category__c == 4
            && location.Well_Type__c != null
            && location.Well_Type__c.contains('OIL')
            && !location.Operating_Field_AMU__r.Is_Thermal__c;
    }

    /**
     * Alerts are time dependent. This method provides information whether today is correct date to create requested
     * alert.
     *
     * @return
     */
    protected abstract Boolean correctDateToCreate();

}