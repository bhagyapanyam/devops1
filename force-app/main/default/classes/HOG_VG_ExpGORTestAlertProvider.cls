/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Implementation of HOG_VG_AlertProvider for purpose of creating Expired GOR Test
                Alerts.
Test Class:     HOG_VG_ExpGORTestAlertProviderTest
History:        jschn 19/10/2018 - Created. - W-001275 - W-001277, W-001305
                jschn 23/10/2018 - Removed Description generate methods - W-001303
*************************************************************************************************/
public with sharing class HOG_VG_ExpGORTestAlertProvider extends HOG_VG_AlertProvider {

    /**
     * Implementation that creates instance of Expired GOR Test alert for insertion.
     *
     * @param location
     *
     * @return
     */
    public override HOG_Vent_Gas_Alert__c createAlert(Location__c location) {
        return super.prepopulateGeneralData(new HOG_Vent_Gas_Alert__c(
                Name = location.Name + ' - '
                        + HOG_VentGas_Utilities.ALERT_TYPE_ABV_EXPIRED_TEST + ' - '
                        + String.valueOf(Date.today()),
                Priority__c = HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM,
                Type__c = HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST
        ), location);
    }

}