/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Test class for HOG_VG_ExpGORTestAlertStrategyProvider
History:        jschn 24/10/2018 - Created.
*************************************************************************************************/
@IsTest
private class HOG_VG_ExpGORTestAlertStrategyPrvdrTest {

    static List<Location__c> locations;
    static Map<Id, Location__c> oldLocationsMap;
    static Map<String, HOG_Vent_Gas_Alert_Exemption_Request__c> ventGasExemptionsByLocationId;
    static Map<String, HOG_Vent_Gas_Alert__c> activeAlertsByLocationAndType;

    @IsTest
    static void testStrategy_falseBcsActiveExemption() {
        prepareData();
        HOG_VG_AlertStrategyProvider provider = new HOG_VG_ExpGORTestAlertStrategyProvider(locations,
                oldLocationsMap,
                ventGasExemptionsByLocationId,
                activeAlertsByLocationAndType);

        Test.startTest();
        Boolean strategyResult = provider.shouldCreate(locations.get(0));
        Test.stopTest();

        System.assert(!strategyResult, 'Should be false, bcs there is active Vent Gas Exemption');

    }

    @IsTest
    static void testStrategy_falseBcsActiveAlert() {
        prepareData();
        ventGasExemptionsByLocationId.clear();
        HOG_VG_AlertStrategyProvider provider = new HOG_VG_ExpGORTestAlertStrategyProvider(locations,
                oldLocationsMap,
                ventGasExemptionsByLocationId,
                activeAlertsByLocationAndType);

        Test.startTest();
        Boolean strategyResult = provider.shouldCreate(locations.get(0));
        Test.stopTest();

        System.assert(!strategyResult, 'Should be false, bcs there is active Vent Gas Alert');
    }

    @IsTest
    static void testStrategy_falseBcsNoFlagChange() {
        prepareData();
        ventGasExemptionsByLocationId.clear();
        activeAlertsByLocationAndType.clear();
        HOG_VG_AlertStrategyProvider provider = new HOG_VG_ExpGORTestAlertStrategyProvider(locations,
                oldLocationsMap,
                ventGasExemptionsByLocationId,
                activeAlertsByLocationAndType);

        Test.startTest();
        Boolean strategyResult = provider.shouldCreate(locations.get(0));
        Test.stopTest();

        System.assert(!strategyResult, 'Should be false, bcs Expired Test flag on location hasn\'t changed');
    }

    @IsTest
    static void testStrategy_falseBcsFlagNotYes() {
        prepareData();
        ventGasExemptionsByLocationId.clear();
        activeAlertsByLocationAndType.clear();
        HOG_VG_AlertStrategyProvider provider = new HOG_VG_ExpGORTestAlertStrategyProvider(locations,
                oldLocationsMap,
                ventGasExemptionsByLocationId,
                activeAlertsByLocationAndType);

        Test.startTest();
        Boolean strategyResult = provider.shouldCreate(locations.get(0));
        Test.stopTest();

        System.assert(!strategyResult, 'Should be false, bcs Expired Test flag on location was not YES');
    }

    @IsTest
    static void testStrategy_falseBcsNotCorrectLocation() {
        prepareData();
        ventGasExemptionsByLocationId.clear();
        activeAlertsByLocationAndType.clear();
        Location__c location = locations.get(0);
        location.GOR_Effective_Date__c = Date.today().addYears(-3);
        location.recalculateFormulas();
        Field__c amu = [SELECT Name, Is_Thermal__c, Planner_Group__c FROM Field__c WHERE Id = : location.Operating_Field_AMU__c];
        amu.Planner_Group__c = '110';
        amu.recalculateFormulas();
        location.Operating_Field_AMU__r = amu;
        location.Operating_Field_AMU__r.Name = 'THERMAL';
        HOG_VG_AlertStrategyProvider provider = new HOG_VG_ExpGORTestAlertStrategyProvider(locations,
                oldLocationsMap,
                ventGasExemptionsByLocationId,
                activeAlertsByLocationAndType);

        Test.startTest();
        Boolean strategyResult = provider.shouldCreate(locations.get(0));
        Test.stopTest();

        System.assert(!strategyResult, 'Should be false, bcs Operating Field name was THERMAL');
    }

    @IsTest
    static void testStrategy_true() {
        prepareData();
        ventGasExemptionsByLocationId.clear();
        activeAlertsByLocationAndType.clear();
        Location__c location = locations.get(0);
        location.GOR_Effective_Date__c = Date.today().addYears(-3);
        location.recalculateFormulas();
        Field__c amu = [SELECT Name, Is_Thermal__c, Planner_Group__c FROM Field__c WHERE Id = : location.Operating_Field_AMU__c];
        amu.Planner_Group__c = '100';
        amu.recalculateFormulas();
        location.Operating_Field_AMU__r = amu;
        HOG_VG_AlertStrategyProvider provider = new HOG_VG_ExpGORTestAlertStrategyProvider(locations,
                oldLocationsMap,
                ventGasExemptionsByLocationId,
                activeAlertsByLocationAndType);

        Test.startTest();
        Boolean strategyResult = provider.shouldCreate(locations.get(0));
        Test.stopTest();

        System.assert(strategyResult, 'Should be true. Strategy for Expired GOR Test was fulfilled');
    }

    private static void prepareData() {
        HOG_Vent_Gas_Alert_Configuration__c config = new HOG_Vent_Gas_Alert_Configuration__c();
        insert config;

        locations = new List<Location__c>();
        oldLocationsMap = new Map<Id, Location__c>();
        ventGasExemptionsByLocationId = new Map<String, HOG_Vent_Gas_Alert_Exemption_Request__c>();
        activeAlertsByLocationAndType = new Map<String, HOG_Vent_Gas_Alert__c>();

        Location__c location = HOG_VentGas_TestData.createLocationForAlert(false);
        location.GOR_Effective_Date__c = Date.today();
        location.Measured_Vent_Rate__c = 100;
        location.PVR_Hours_On_Prod__c = 24;
        location.PVR_Fuel_Consumption__c = 1;
        insert location;

        locations.add(location);
        oldLocationsMap.put(location.Id, location.clone());
        ventGasExemptionsByLocationId.put(location.Id, new HOG_Vent_Gas_Alert_Exemption_Request__c());
        activeAlertsByLocationAndType.put(location.Id + HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST, new HOG_Vent_Gas_Alert__c());
    }

}