@isTest
public class HuskyErrorTriggerTest {

    @isTest
    static void testValidSingleErrorMessage() {
        Husky_Error__e errorLog = new Husky_Error__e();
        errorLog.Interface__c = 'TestClass';
        errorLog.Error_Date__c = System.now();
        errorLog.Severity__c = 'ERROR';
        errorLog.Stack_Trace__c = 'This is an example stack trace';

        Test.startTest();
        Database.SaveResult sr = EventBus.publish(errorLog);
        Test.stopTest();

        System.assertEquals(true, sr.isSuccess());

        List<Error_Log__c> errors = [SELECT Id, Apex_Class_Trigger__c, Severity__c, Description__c FROM Error_Log__c];
        System.assertEquals(1, errors.size());
        System.assertEquals(errorLog.Interface__c, errors[0].Apex_Class_Trigger__c);
        System.assertEquals(errorLog.Severity__c, errors[0].Severity__c);
        System.assertEquals(errorLog.Stack_Trace__c, errors[0].Description__c);
    }

    @isTest
    static void testValidMultipleErrorMessage() {
        List<Husky_Error__e> errors = new List<Husky_Error__e>();
        final Integer recordCount = 100;

        for(Integer i = 0; i < recordCount; i++) {
            Husky_Error__e errorLog = new Husky_Error__e();
            errorLog.Interface__c = 'TestClass' + i;
            errorLog.Error_Date__c = System.now();
            errorLog.Severity__c = 'ERROR';
            errorLog.Stack_Trace__c = 'This is an example stack trace ' + i;

            errors.add(errorLog);
        }

        Test.startTest();
        EventBus.publish(errors);
        Test.stopTest();

        List<Error_Log__c> errorRecords = [SELECT Id FROM Error_Log__c];
        System.assertEquals(recordCount, errorRecords.size());

    }    
}