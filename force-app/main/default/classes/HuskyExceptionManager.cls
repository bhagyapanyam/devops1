public with sharing class HuskyExceptionManager {
    
    public static void log(String interfaceName, String exceptionMessage, String severity) {
        List<Husky_Error__e> publishEvents = new List<Husky_Error__e>();

        Husky_Error__e	errorLog = new Husky_Error__e();
        errorLog.Interface__c = interfaceName;
        errorLog.Severity__c = severity;
        errorLog.Stack_Trace__c = exceptionMessage;
        errorLog.Error_Date__c = System.now();

        publishEvents.add(errorLog);

        publish(publishEvents);
    }

    public static void log(String interfaceName, String exceptionMessage) {
        List<Husky_Error__e> publishEvents = new List<Husky_Error__e>();

        Husky_Error__e	errorLog = new Husky_Error__e();
        errorLog.Interface__c = interfaceName;
        errorLog.Severity__c = 'ERROR';
        errorLog.Stack_Trace__c = exceptionMessage;
        errorLog.Error_Date__c = System.now();

        publishEvents.add(errorLog);

        publish(publishEvents);
    }

    public static void log(String interfaceName, List<String> exceptionMessages) {
        List<Husky_Error__e> publishEvents = new List<Husky_Error__e>();

        for(String exceptionMessage : exceptionMessages) {
            Husky_Error__e	errorLog = new Husky_Error__e();
            errorLog.Interface__c = interfaceName;
            errorLog.Severity__c = 'ERROR';
            errorLog.Stack_Trace__c = exceptionMessage;
            errorLog.Error_Date__c = System.now();
            
            publishEvents.add(errorLog); 
        }

        publish(publishEvents);
    }

    public static void log(String interfaceName, Exception exceptionMessage) {
        List<Husky_Error__e> publishEvents = new List<Husky_Error__e>();

        Husky_Error__e	errorLog = new Husky_Error__e();
        errorLog.Interface__c = interfaceName;
        errorLog.Severity__c = 'ERROR';
        errorLog.Stack_Trace__c = exceptionMessage.getMessage();
        errorLog.Error_Date__c = System.now();

        publishEvents.add(errorLog);        

        publish(publishEvents);
    }    

    public static void log(String interfaceName, List<Exception> exceptionMessages) {
        List<Husky_Error__e> publishEvents = new List<Husky_Error__e>();

        for(Exception exceptionMessage : exceptionMessages) {
            Husky_Error__e	errorLog = new Husky_Error__e();
            errorLog.Interface__c = interfaceName;
            errorLog.Severity__c = 'ERROR';
            errorLog.Stack_Trace__c = exceptionMessage.getMessage();
            errorLog.Error_Date__c = System.now();
            
            publishEvents.add(errorLog); 
        }        

        publish(publishEvents);
    }  

    private static void publish(List<Husky_Error__e> publishEvents) {
        if(!publishEvents.isEmpty()) {
            EventBus.publish(publishEvents); 
        }
    }    

}