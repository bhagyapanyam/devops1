/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = true)
private class LOM_Invoice_Report_ControllerTest {

    static testMethod void myUnitTest() {
    
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
    
    
       
            PageReference pageRef = Page.LOM_Invoice_Report;
            Test.setCurrentPageReference(pageRef);

            //Create Dashboard Controller, set dates            
            LOM_Invoice_Report_Controller controller = new LOM_Invoice_Report_Controller();
            
            System.AssertNotEquals(controller.baseURL , Null);            
            
            
            Report report = [SELECT Id, name, description FROM Report where description !=null  Limit 1];
            controller.ReportID =  report.id;
            controller.RefreshData();
            
            System.AssertNotEquals(controller.ReportName, Null);
            System.AssertNotEquals(controller.ReportDescription, Null);            
            
            
            Date dt = Date.newInstance(2013, 01, 30);
            controller.dateFromStr  = '30/01/2013';
            controller.dateToStr  = '30/01/2013';            
            System.AssertEquals(dt, controller.dateFrom);
            System.AssertEquals(dt, controller.dateTo);            
            
        }    
           
    }

}