public class LocalDateTimeController {
	public DateTime dateTimeValue {get; set;}
	public String dateFormat {get; set;}
	
	public String getTimeZoneValue() {
		String localeFormatDT;
		if(dateTimeValue != null) {
			if(dateFormat != Null && dateFormat != '')
				localeFormatDT = dateTimeValue.format(dateFormat);
			else
				localeFormatDT = dateTimeValue.format();
				
			return localeFormatDT;
		}
		return null;
	}
}