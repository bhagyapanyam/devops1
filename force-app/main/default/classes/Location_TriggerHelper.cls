/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Helper for Location_Trigger providing Vent Gas Alert creation processes.
Test Class:     Location_TriggerHelperTest
History:        jschn 19. 9. 2018 - Created.
                jschn 19.10. 2018 - W-001275 - W-001277 -> Refactored createVentGasAlerts method
                                                        to create all types of alerts required
                                                        within USs.
**************************************************************************************************/
public with sharing class Location_TriggerHelper {

    private List<Location__c> newLocations;
    private Map<Id, Location__c> oldLocationsMap;

    public Location_TriggerHelper(List<Location__c> newLocations, Map<Id, Location__c> oldLocationsMap) {
        this.newLocations = newLocations;
        this.oldLocationsMap = oldLocationsMap;
    }

    /**
     * Handler for Location trigger.
     * Ran After Update.
     * Creates Vent Gas Alerts.
     */
    public void handleAfterUpdate() {
        createVentGasAlerts();
    }

    /**
     * Creates Vent Gas Alerts.
     * Get set of Location ids entering to this helper.
     * Get active Alerts grouped by Location ID and Alert Type.
     * Retrieves location based on generated set of IDs. (Required for missing fields from related records)
     * Get active Vent Gas Exemption records.
     * Generate list of Alerts to insert
     * Insert Alerts.
     */
    private void createVentGasAlerts() {
        Set<Id> locationIds = getSetOfLocationIds();

        Map<String, HOG_Vent_Gas_Alert__c> activeAlertsByLocationAndType = getActiveAlertsByLocationAndType(locationIds);

        List<Location__c> locations = getLocations(locationIds);

        Map<String, HOG_Vent_Gas_Alert_Exemption_Request__c> ventGasExemptionsByLocationId =
                getVentGasExemptionsByLocationId(locationIds);

        // jschn 19.10.2018 ->
        List<HOG_Vent_Gas_Alert__c> alertsToInsert = createAlerts(
                locations,
                ventGasExemptionsByLocationId,
                activeAlertsByLocationAndType);
        // jschn 19.10.2018 <-

        insert alertsToInsert;
    }

    /**
     * Generate List of Alert of all types.
     *
     * @param locations
     * @param ventGasExemptionsByLocationId
     * @param activeAlertsByLocationAndType
     *
     * @return
     */
    //jschn 19.10.2018
    private List<HOG_Vent_Gas_Alert__c> createAlerts(
            List<Location__c> locations,
            Map<String, HOG_Vent_Gas_Alert_Exemption_Request__c> ventGasExemptionsByLocationId,
            Map<String, HOG_Vent_Gas_Alert__c> activeAlertsByLocationAndType) {

        List<HOG_Vent_Gas_Alert__c> alertsToInsert = new List<HOG_Vent_Gas_Alert__c>();

        alertsToInsert.addAll(HOG_VG_Alert_Service.generateAlerts(locations,
                new HOG_VG_ExpGORTestAlertStrategyProvider(newLocations,
                        oldLocationsMap,
                        ventGasExemptionsByLocationId,
                        activeAlertsByLocationAndType),
                new HOG_VG_ExpGORTestAlertProvider()));

        alertsToInsert.addAll(HOG_VG_Alert_Service.generateAlerts(locations,
                new HOG_VG_LowVarianceAlertStrategyProvider(newLocations,
                        oldLocationsMap,
                        ventGasExemptionsByLocationId,
                        activeAlertsByLocationAndType),
                new HOG_VG_LowVarianceAlertProvider()));

        alertsToInsert.addAll(HOG_VG_Alert_Service.generateAlerts(locations,
                new HOG_VG_HighVarianceAlertStrategyProvider(newLocations,
                        oldLocationsMap,
                        ventGasExemptionsByLocationId,
                        activeAlertsByLocationAndType),
                new HOG_VG_HighVarianceAlertProvider()));

        alertsToInsert.addAll(HOG_VG_Alert_Service.generateAlerts(locations,
                new HOG_VG_HighVentAlertStrategyProvider(newLocations,
                        oldLocationsMap,
                        ventGasExemptionsByLocationId,
                        activeAlertsByLocationAndType),
                new HOG_VG_HighVentAlertProvider()));

        return alertsToInsert;
    }

    /**
     * Generates set of IDs from Location entering helper.
     *
     * @return
     */
    private Set<Id> getSetOfLocationIds() {
        Set<Id> locationIds = new Set<Id>();
        for (Location__c location : newLocations) {
            locationIds.add(location.Id);
        }
        return locationIds;
    }

    /**
     * Retrieves Active Alerts.
     * Group Active Alerts by combination of Location ID and Alert Type.
     *
     * @param locationIds
     *
     * @return
     */
    private static Map<String, HOG_Vent_Gas_Alert__c> getActiveAlertsByLocationAndType(Set<Id> locationIds) {
        Map<String, HOG_Vent_Gas_Alert__c> activeAlertsByLocationAndType = new Map<String, HOG_Vent_Gas_Alert__c>();

        List<HOG_Vent_Gas_Alert__c> activeAlerts = HOG_VG_Alert_Service.getActiveAlerts(locationIds);

        for (HOG_Vent_Gas_Alert__c alert : activeAlerts) {
            activeAlertsByLocationAndType.put(alert.Well_Location__c + alert.Type__c, alert);
        }

        return activeAlertsByLocationAndType;
    }

    /**
     * Retrieves List of Locations by set of Location IDs.
     * Adding fields from related SObjects.
     *
     * @param locationIds
     *
     * @return
     */
    public static List<Location__c> getLocations(Set<Id> locationIds) {
        return [
                SELECT Name, Expired_GOR_Test__c, Functional_Location_Category__c, Well_Type__c,
                        Operating_Field_AMU__r.Name, Operating_Field_AMU__r.Is_Thermal__c, Operating_Field_AMU__r.Production_Engineer_User__c,
                        Total_Gas_Production__c, GOR_Effective_Date__c, Low_Variance__c, High_Vent__c, High_Variance__c
                        , PVR_GOR_Factor__c, GOR_Test_Date__c, PVR_Fuel_Consumption__c
                        , PVR_Fuel_Consumption_Rate__c , Measured_Vent_Rate__c , Monthly_Trucked_Oil__c
                        , Predicted_Reported_Vent_Rate__c, PVR_Hours_On_Prod__c
                FROM Location__c
                WHERE Id IN :locationIds
        ];
    }

    /**
     * Retrieves List of active Vent Gas Exemptions based on set of Location IDs.
     * Group Exemption records by combination of Location ID and Alert Type.
     *
     * @param locationIds
     *
     * @return
     */
    private Map<String, HOG_Vent_Gas_Alert_Exemption_Request__c> getVentGasExemptionsByLocationId(Set<Id> locationIds) {
        Map<String, HOG_Vent_Gas_Alert_Exemption_Request__c> ventGasExemptionsByLocationId =
                new Map<String, HOG_Vent_Gas_Alert_Exemption_Request__c>();

        for (HOG_Vent_Gas_Alert_Exemption_Request__c exemption :
                HOG_VG_Alert_Exemption_Request_Service.getActiveExemptionsByLocationId(locationIds)) {
            ventGasExemptionsByLocationId.put(exemption.Well_Location__c, exemption);
        }

        return ventGasExemptionsByLocationId;
    }

}