public class NCR_ViewPageControllerX{
    String recordId;
    ID contactId;
    ID AccID;
    ID userID;
  
    private final ApexPages.StandardController stdcontroller;
    private final NCR_Case__c ncr_record;
    
    public Boolean ViewMode {get; private set;}
    public List<String> viewSelection {get; private set;}
    public Map<String,String> viewSelectionMap {get; private set;}
    
    
    public String ConfirmMessage {get; private set;}
    public String SaveButtonLabel {get; private set;}
    
    /* Button flags*/
    public Boolean btnEditEnabled {get; private set;}
    public Boolean btnChangeStateClicked   {get; private set;}   

    public String FieldSetName {
            get; 
            private set
            {
                FieldSetName = value;
                try
                {
                    EditSectionName = SObjectType.NCR_Case__c.FieldSets.getMap().get(value).getLabel();                  
                }
                catch(Exception ex)
                {}
            }
    }
            
    public String EditSectionName {get; private set;} 
    public String EditSectionDescription {get; private set;}     
    

    private Map<String, NCR_State__c> NCRStateMap {get; set;}
    public NCR_State__c CurrentNCRState {get; private set;}
    public integer ActionCount {get; private set;}

    private String NewRecordState {get; set;}
    private String NewRecordOwner {get; set;}    
    
    
    private Boolean redirectToList {get; set;}
    
    
    public String ViewScreenDescription {get; private set;}
    

    public Boolean UserHasReadAccess {get; private set;}
    public Boolean UserHasEditAccess {get; private set;}    
    public Boolean UserHasDeleteAccess   {get; private set;}        
     
    //public Map<String, Schema.FieldSet> fieldSetMap {get; private set;}
    public Map<String, string> fieldSetLabelsMap {get; private set;}

    private void LoadStateMap()
    {
    
        Map<String, NCR_State__c> m = new Map<String, NCR_State__c>([
        select id, name, ActionCount__c, ViewScreen_Description__c,
            (select id, name, Edit_Screen_Required__c, Edit_Section_Title__c,Edit_Section_Description__c,Confirmation_Message__c, Save_Button_Label__c, redirect__c, Change_Owner__c,
                FieldSet__c, NCR_New_State__r.Name, NCR_Action__r.Name 
                from NCR_StateActions__r order by Order__c ) 
        from NCR_State__c
        ]);
        NCRStateMap = new Map<String, NCR_State__c>();
        for(String strid: m.keySet())
        {
            NCR_State__c state = (NCR_State__c)m.get(strid);
            NCRStateMap.put((String)state.Name, (NCR_State__c)state);
        }    
    }
    private void LoadFieldSetFields()
    {
       List<string> fields = new List<String>{'Name','NCR_Case_State__c' };
        if(!Test.isRunningTest()) {
            stdController.addFields(fields);
        }    

        //add fields from fieldset
        List<String> fieldNames = new List<String>();
        
        Map<String, Schema.FieldSet> fieldSetMap = SObjectType.NCR_Case__c.FieldSets.getMap();
        for (String fieldSetName : fieldSetMap.keySet()){
            
            //System.debug('field name is ' + fieldName);

            for(Schema.FieldSetMember f : SObjectType.NCR_Case__c.FieldSets.getMap().get(fieldSetName).getFields()){
                        fieldNames.add(f.getFieldPath());
                      }
                      //stdController.addFields(fieldNames);            
        }
         

        if(!Test.isRunningTest()) {
            stdController.addFields(fieldNames);   
        }    
    }
       
    public NCR_ViewPageControllerX(ApexPages.StandardController stdController) {
        recordId = stdController.getId();
        userID =   UserInfo.getUserid();

        //defaults
        this.SaveButtonLabel = 'Save';
        this.ViewScreenDescription  = '';
        
        
       List<string> fields = new List<String>{'Name','NCR_Case_State__c' };
        if(!Test.isRunningTest()) {
            stdController.addFields(fields);
        }    

        //add fields from fieldset
        List<String> fieldNames = new List<String>();

        fieldSetLabelsMap = new Map<string, String>();
        
        fieldSetLabelsMap.put('NCR_Case_Detail', 'NCR Case Details');
        
        Map<String, Schema.FieldSet> fieldSetMap = SObjectType.NCR_Case__c.FieldSets.getMap();
        for (String fieldSetName : fieldSetMap.keySet()){
            
            //System.debug('field name is ' + fieldSetName );
            
            fieldSetLabelsMap.put(fieldSetName, SObjectType.NCR_Case__c.FieldSets.getMap().get(fieldSetName).getLabel()) ;
            
            for(Schema.FieldSetMember f : SObjectType.NCR_Case__c.FieldSets.getMap().get(fieldSetName).getFields()){
                        fieldNames.add(f.getFieldPath());
                      }
                      //stdController.addFields(fieldNames);            
        }
         

        if(!Test.isRunningTest()) {
            stdController.addFields(fieldNames);   
        }



        

        this.stdcontroller = stdController;
        //LoadFieldSetFields();
        
        this.ncr_record = (NCR_Case__c)stdController.getRecord();  
        
        ViewMode  = true;
        
        NewRecordState = null;
        NewRecordOwner = null;
        
        LoadStateMap();

        
        LoadCurrentStateActions();

        SetButtonFlags();       
        
        CheckRecordAccess();
        
        if(this.UserHasEditAccess && CurrentNCRState.ViewScreen_Description__c <> null) 
        {
            this.ViewScreenDescription = CurrentNCRState.ViewScreen_Description__c;
        }
    }
    
    private void CheckRecordAccess()
    {
       UserRecordAccess  recAccess = [SELECT 
                RecordId
                , HasReadAccess
                , HasEditAccess
                , HasDeleteAccess 
            FROM 
                UserRecordAccess 
            WHERE 
                UserId=:UserInfo.getUserId() 
                AND 
                RecordId =:recordId  LIMIT 1];    
                
       this.userHasReadAccess = recAccess.HasReadAccess;         
       this.userHasEditAccess = recAccess.HasEditAccess;       
       this.userHasDeleteAccess = recAccess.HasDeleteAccess;          
    }



    
    private void LoadCurrentStateActions()
    {
        this.ActionCount = 0;
        this.CurrentNCRState = NCRStateMap.get(ncr_record.NCR_Case_State__c);
        if(currentNCRState == null)
        {
        //throw error
            return;
        }
        this.ActionCount  = (Integer)currentNCRState.ActionCount__c;
        system.debug('Current State: ' + ncr_record.NCR_Case_State__c);
    }
    public PageReference NCR_GenericAction(Integer actionNumber)
    {
    
        NewRecordOwner = null;
        NewRecordState = null;    
    
        NCR_StateAction__c stateAction = this.CurrentNCRState.NCR_StateActions__r[actionNumber];
    
        System.debug(stateAction.NCR_New_State__r.Name);
        if(stateAction.NCR_New_State__r.Name != null)
        {
          NewRecordState  = stateAction.NCR_New_State__r.Name;
        }
        
        if(stateAction.Change_Owner__c != null)
        {
          NewRecordOwner = stateAction.Change_Owner__c;
        }        
        
        
        
        //assign fieldset if needed
        if(stateAction.FieldSet__c != null) 
        {
          this.FieldSetName = stateAction.FieldSet__c;
        }   
        //assign section name if needed     
        if(stateAction.Edit_Section_Title__c!= null)
        {
          this.EditSectionName  =  stateAction.Edit_Section_Title__c;         
        }
        //assign section name if needed    
        this.EditSectionDescription = null; 
        if(stateAction.Edit_Section_Description__c!= null)
        {
          this.EditSectionDescription  =  stateAction.Edit_Section_Description__c;         
        }   
        
        //change Save button label if needed     
        if(stateAction.Save_Button_Label__c!= null)
        {
          this.SaveButtonLabel  =  stateAction.Save_Button_Label__c;         
        }             
        else
        {
            this.SaveButtonLabel = 'Save';
        }
        
        redirectToList = (stateAction.Redirect__c == 'List Screen'); 
       
        
        if(stateAction.Edit_Screen_Required__c)
        {
             ViewMode = false;
             btnChangeStateClicked = true;
        }
        else
        {
            NCR_Save(); //save record and change state
            
            if(redirectToList)
            {
                return Exit();
            }            
        }
         

                
         return null;    
    }
    
    public PageReference exit(){
        //change the Any_ObjectName__c with your Custom or Standard Object name.
        Schema.DescribeSObjectResult anySObjectSchema = NCR_Case__c.SObjectType.getDescribe();
        String objectIdPrefix = anySObjectSchema.getKeyPrefix();
        PageReference pageReference = new PageReference('/'+objectIdPrefix+'/o');
        pageReference.setRedirect(true);
        return pageReference;
    }    
    
    
    public PageReference NCR_Action1()
    {
        return NCR_GenericAction(0);
    }    
    public PageReference NCR_Action2()
    {
        return NCR_GenericAction(1);
    }
    public PageReference NCR_Action3()
    {
        return NCR_GenericAction(2);
    }

    public PageReference NCR_Edit()
    {
        ViewMode = false;
        return null;
    }
    public PageReference NCR_Save()
    {
    
        System.debug('Save');
        System.debug('NewRecordState: ' + NewRecordState);        
        System.debug('NewRecordOwner: ' + NewRecordOwner);            
        ViewMode = true;
        if(NewRecordState!= null) 
        {
        
            ncr_record.NCR_Case_State__c = NewRecordState;
            System.debug('New State:' + NewRecordState);
        }

        if(NewRecordOwner != null)
        {
        
            UpdateRecordOwner(NewRecordOwner);
        }


        upsert this.ncr_record;
        this.stdcontroller.save();

        LoadCurrentStateActions();
        
        SetButtonFlags();
        
        NewRecordOwner = null;
        NewRecordState = null;

        if(redirectToList)
        {
           return Exit();
        }  
        return null;
    }
    public PageReference NCR_CancelEdit()
    {
        ViewMode = true;
        SetButtonFlags();
        this.stdcontroller.cancel();
        return null;
    }

    private void UpdateRecordOwner(String recordOwnerType)
    {
        if(recordOwnerType =='Current User')
        {
            ncr_record.OwnerID = UserInfo.getUserId();
            return;
        }
        if(recordOwnerType =='Originator' && ncr_record.Originator__c != null)
        {
            ncr_record.OwnerID = ncr_record.Originator__c;
            return;
        }  
        if(recordOwnerType =='QA Lead' && ncr_record.QA_Lead__c!= null)
        {
            ncr_record.OwnerID = ncr_record.QA_Lead__c;
            return;
        }  
        if(recordOwnerType =='Assignee' && ncr_record.Assignee__c != null)
        {
            ncr_record.OwnerID = ncr_record.Assignee__c ;
            return;
        }                    
    }


    private void SetButtonFlags()
    {
        btnEditEnabled = false;
        btnChangeStateClicked = false;
    }
    
    public List<String> getViewScreenStateList(){
        viewSelection = new List<String>();
        
        try{
        
            NCR_State__c viewScreenOptions  = [select Screen_Sections__c from NCR_State__c 
                    where Name = : ncr_record.NCR_Case_State__c];

            if(viewSelection == null){
                viewSelection.add('NCR_Case_Detail');
            } 
            else{
                viewSelection = viewScreenOptions.Screen_Sections__c.split(',');
            } 
         }
         catch(Exception exp){
             viewSelection.add('NCR_Case_Detail');
         }
         
        return viewSelection;
    } 
    
    public Map<String,String> getViewScreenStateMap(){
        viewSelectionMap = new Map<String,String>();
         
        try{
            NCR_State__c viewScreenOptions  = [SELECT     Screen_Sections__c 
                                               FROM       NCR_State__c 
                                               WHERE      Name = : ncr_record.NCR_Case_State__c]; 
            
            for(String f : viewScreenOptions.Screen_Sections__c.split(',')) {
                viewSelectionMap.put(f,Schema.SObjectType.NCR_Case__c.fieldSets.getMap().get(f).getLabel());
            }              
        }
        catch(Exception exp){
            viewSelectionMap.put('NCR_Case_Detail',Schema.SObjectType.NCR_Case__c.fieldSets.getMap().get('NCR_Case_Detail').getLabel());
        }        
        
        return viewSelectionMap;
    }       

    /*
    public PageReference redirect() {

            Profile p = [select name from Profile where id = :UserInfo.getProfileId()];

                      if ('US Portal User Profile'.equals(p.name)) 
                         {
                           PageReference customPage =  Page.USCP_Customer;
                           customPage.setRedirect(true);
                           
                           contactId = [Select contactid from User where id =:userID].contactId;
                           AccID  = [Select AccountID from Contact where id =:contactId].AccountId;


                           customPage.getParameters().put('id', AccID);
                           return customPage;
                          
                          } else {
                              return null; //otherwise stay on the same page  
                          }
   }
   */
}