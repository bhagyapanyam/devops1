/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the HOG_Notification_Type__c
-------------------------------------------------------------------------------------------------*/
@isTest        
public class NotificationTypeTestData
{
    /*---------------------------------------------------------------------------------
    Description: Function to create an item of HOG_Notification_Type__c
    Inputs     : serviceCategoryId, autoGenerateWorkOrderNumber
    Returns    : HOG_Notification_Type__c
    -----------------------------------------------------------------------------------*/        
    public static HOG_Notification_Type__c createNotificationType
    (
    	Id serviceCategoryId,
    	Boolean autoGenerateNotification_Number,
    	Boolean autoGenerateWorkOrderNumber,
    	Boolean generateNumbersFromSAP,
    	Boolean allowManualUpdateOfWorkOrderNumber
   	)
    {                
        HOG_Notification_Type__c results = new HOG_Notification_Type__c
            (
                HOG_Service_Category__c = serviceCategoryId,
                Auto_Generate_Notification_Number__c = autoGenerateNotification_Number,
                Auto_Generate_Work_Order_Number__c = autoGenerateWorkOrderNumber,
                Generate_Numbers_From_SAP__c = generateNumbersFromSAP,
                Allow_Manual_Update_Of_Work_Order_Number__c = allowManualUpdateOfWorkOrderNumber  
            );
        return results;
    }
}