public class OliAsphaltTableEditController {
    
    private ApexPages.StandardController stdController;
    private OpportunityLineItem oli;
    public ATS_Freight__c atsFreight {get; set;}
    
public double ats_RChuskysupp1 {get; set;}
public double ats_A5huskysupp1 {get; set;}
public double ats_A6huskysupp1 {get; set;}
public double ats_A7huskysupp1 {get; set;}
public double ats_A8huskysupp1 {get; set;}

public double ats_RChuskysupp2 {get; set;}
public double ats_A5huskysupp2 {get; set;}
public double ats_A6huskysupp2 {get; set;}
public double ats_A7huskysupp2 {get; set;}
public double ats_A8huskysupp2 {get; set;}

public double ats_RChuskysupp3 {get; set;}
public double ats_A5huskysupp3 {get; set;}
public double ats_A6huskysupp3 {get; set;}
public double ats_A7huskysupp3 {get; set;}
public double ats_A8huskysupp3 {get; set;}

public double ats_RCcomp1 {get; set;}
public double ats_A5comp1 {get; set;}
public double ats_A6comp1 {get; set;}
public double ats_A7comp1 {get; set;}
public double ats_A8comp1 {get; set;}

public double ats_RCcomp2 {get; set;}
public double ats_A5comp2 {get; set;}
public double ats_A6comp2 {get; set;}
public double ats_A7comp2 {get; set;}
public double ats_A8comp2 {get; set;}

public double ats_RCcomp3 {get; set;}
public double ats_A5comp3 {get; set;}
public double ats_A6comp3 {get; set;}
public double ats_A7comp3 {get; set;}
public double ats_A8comp3 {get; set;}

public double ats_RCcomp4 {get; set;}
public double ats_A5comp4 {get; set;}
public double ats_A6comp4 {get; set;}
public double ats_A7comp4 {get; set;}
public double ats_A8comp4 {get; set;}

public string ats_freight_method {get; set;} 
public string ProductNameVar {get; set;} 
public string ProductFamilyVar {get; set;} 
    
//new
public string ats_Huskysupp1Name {get; set;}
public string ats_Huskysupp2Name {get; set;}
public string ats_Huskysupp3Name {get; set;}
public string ats_Comp1Name {get; set;}
public string ats_Comp2Name {get; set;}
public string ats_Comp3Name {get; set;}
public string ats_Comp4Name {get; set;}    

public string ats_freight_huskysupp1_unit {get; set;}   
public string ats_freight_huskysupp2_unit {get; set;} 
public string ats_freight_huskysupp3_unit {get; set;} 
    public string ats_freight_comp1_unit {get; set;}
    public string ats_freight_comp2_unit {get; set;} 
    public string ats_freight_comp3_unit {get; set;} 
    public string ats_freight_comp4_unit {get; set;} 
    
public string OliProductUnit {get; set;}
public string AtsFreightId {get; set;}
public string AtsFreightName {get; set;}
public double OliProductDensity {get; set;}
    
//new end

    public OliAsphaltTableEditController(ApexPages.StandardController controller) {
    	stdController = controller;
    	oli = (OpportunityLineItem)controller.getRecord();
        
        oli = [select OpportunityId, Name, Unit__c, cpm_product_density__c, cpm_Product_Family__c from OpportunityLineItem where Id =: oli.id];
        this.getAtsFreightRecords();
    }
    
    public void getAtsFreightRecords() {
        if (oli != null && oli.OpportunityId != null && oli.cpm_Product_Family__c != null) {
            atsFreight = [
                select	
                ATS_Freight__c, Id, Name, Local_Freight_Via__c,
                Supplier_1_Unit__c, Supplier_2_Unit__c, Supplier_3_Unit__c,																													//Huskysupplier's unit
              	Competitor_1_Unit__c, Competitor_2_Unit__c, Competitor_3_Unit__c, Competitor_4_Unit__c,																						//Competitor's unit
                cpm_HuskySupplier1_ats_pricing__r.Name, cpm_HuskySupplier2_ats_pricing__r.Name, cpm_HuskySupplier3_ats_pricing__r.Name,														//Huskysupp names
                cpm_Competitor1_ats_pricing__r.Name, cpm_Competitor2_ats_pricing__r.Name, cpm_Competitor3_ats_pricing__r.Name, cpm_Competitor4_ats_pricing__r.Name,							//Competitor names
                Supplier_1_Rate__c, Emulsion_Rate5_Supplier1__c, Emulsion_Rate6_Supplier_1__c, Emulsion_Rate7_Supplier1__c, Emulsion_Rate8_Supplier1__c, 									//huskysupplier 1 from ATS freight
                Supplier_2_Rate__c, Emulsion_Rate5_Supplier2__c, Emulsion_Rate6_Supplier2__c, Emulsion_Rate7_Supplier2__c, Emulsion_Rate8_Supplier2__c, 									//huskysupplier 2 from ATS freight
                Supplier_3_Rate__c, Emulsion_Rate5_Supplier3__c, Emulsion_Rate6_Supplier3__c, Emulsion_Rate7_Supplier3__c, Emulsion_Rate8_Supplier3__c, 									//huskysupplier 3 from ATS freight
                Competitor_Supplier_1_Rate__c, Emulsion_Rate5_Competitor1__c, Emulsion_Rate6_Competitor1__c, Emulsion_Rate7_Competitor1__c, Emulsion_Rate8_Competitor1__c,					//competitor 1 from ATS freight
                Competitor_Supplier_2_Rate__c, Emulsion_Rate5_Competitor2__c, Emulsion_Rate6_Competitor2__c, Emulsion_Rate7_Competitor2__c, Emulsion_Rate8_Competitor2__c,					//competitor 2 from ATS freight
                Competitor_Supplier_3_Rate__c, Emulsion_Axle5_Rate_Competitor3__c, Emulsion_Axle6_Rate_Competitor3__c, Emulsion_Rate7_Competitor3__c, Emulsion_Axle8_Rate_Competitor3__c,	//competitor 3 from ATS freight
                Competitor_Supplier_4_Rate__c, Emulsion_Axle5_Rate_Competitor4__c, Emulsion_Axle6_Rate_Competitor4__c, Emulsion_Rate7_Competitor4__c, Emulsion_Axle8_Rate_Competitor4__c 	//competitor 4 from ATS freight
				from ATS_Freight__c
                where ATS_Freight__c =: oli.OpportunityId AND cpm_Product_Type__c =: oli.cpm_Product_Family__c
                LIMIT 1
            ];
        }
        
        //cpm_product_density
        
        ProductNameVar =  oli.Name;
        ProductFamilyVar = oli.cpm_Product_Family__c;
        OliProductUnit = oli.Unit__c;
        OliProductDensity = oli.cpm_product_density__c;
        
        AtsFreightId = atsFreight.Id;
        AtsFreightName = atsFreight.Name;
        ats_freight_method = atsFreight.Local_Freight_Via__c;
        
        ats_freight_huskysupp1_unit = atsFreight.Supplier_1_Unit__c;
        ats_freight_huskysupp2_unit = atsFreight.Supplier_2_Unit__c;
        ats_freight_huskysupp3_unit = atsFreight.Supplier_3_Unit__c;
        ats_freight_comp1_unit = atsFreight.Competitor_1_Unit__c;
        ats_freight_comp2_unit = atsFreight.Competitor_2_Unit__c;
        ats_freight_comp3_unit = atsFreight.Competitor_3_Unit__c;
        ats_freight_comp4_unit = atsFreight.Competitor_4_Unit__c;
        
        ats_Huskysupp1Name = atsFreight.cpm_HuskySupplier1_ats_pricing__r.Name;
        ats_Huskysupp2Name = atsFreight.cpm_HuskySupplier2_ats_pricing__r.Name;
        ats_Huskysupp3Name = atsFreight.cpm_HuskySupplier3_ats_pricing__r.Name;
        ats_Comp1Name = atsFreight.cpm_Competitor1_ats_pricing__r.Name;
        ats_Comp2Name = atsFreight.cpm_Competitor2_ats_pricing__r.Name;
        ats_Comp3Name = atsFreight.cpm_Competitor3_ats_pricing__r.Name;
        ats_Comp4Name = atsFreight.cpm_Competitor4_ats_pricing__r.Name;        
               
        ats_RChuskysupp1 = (atsFreight.Supplier_1_Rate__c != null ? Double.valueOf(atsFreight.Supplier_1_Rate__c): 0);
        ats_A5huskysupp1 = (atsFreight.Emulsion_Rate5_Supplier1__c != null ? Double.valueOf(atsFreight.Emulsion_Rate5_Supplier1__c): 0);
        ats_A6huskysupp1 = (atsFreight.Emulsion_Rate6_Supplier_1__c != null ? Double.valueOf(atsFreight.Emulsion_Rate6_Supplier_1__c): 0);
        ats_A7huskysupp1 = (atsFreight.Emulsion_Rate7_Supplier1__c != null ? Double.valueOf(atsFreight.Emulsion_Rate7_Supplier1__c): 0);
        ats_A8huskysupp1 = (atsFreight.Emulsion_Rate8_Supplier1__c != null ? Double.valueOf(atsFreight.Emulsion_Rate8_Supplier1__c): 0);

        ats_RChuskysupp2 = (atsFreight.Supplier_2_Rate__c != null ? Double.valueOf(atsFreight.Supplier_2_Rate__c): 0);
        ats_A5huskysupp2 = (atsFreight.Emulsion_Rate5_Supplier2__c != null ? Double.valueOf(atsFreight.Emulsion_Rate5_Supplier2__c): 0);
        ats_A6huskysupp2 = (atsFreight.Emulsion_Rate6_Supplier2__c != null ? Double.valueOf(atsFreight.Emulsion_Rate6_Supplier2__c): 0);
        ats_A7huskysupp2 = (atsFreight.Emulsion_Rate7_Supplier2__c != null ? Double.valueOf(atsFreight.Emulsion_Rate7_Supplier2__c): 0);
        ats_A8huskysupp2 = (atsFreight.Emulsion_Rate8_Supplier2__c != null ? Double.valueOf(atsFreight.Emulsion_Rate8_Supplier2__c): 0);
                
        ats_RChuskysupp3 = (atsFreight.Supplier_3_Rate__c != null ? Double.valueOf(atsFreight.Supplier_3_Rate__c): 0);
        ats_A5huskysupp3 = (atsFreight.Emulsion_Rate5_Supplier3__c != null ? Double.valueOf(atsFreight.Emulsion_Rate5_Supplier3__c): 0);
        ats_A6huskysupp3 = (atsFreight.Emulsion_Rate6_Supplier3__c != null ? Double.valueOf(atsFreight.Emulsion_Rate6_Supplier3__c): 0);
        ats_A7huskysupp3 = (atsFreight.Emulsion_Rate7_Supplier3__c != null ? Double.valueOf(atsFreight.Emulsion_Rate7_Supplier3__c): 0);
        ats_A8huskysupp3 = (atsFreight.Emulsion_Rate8_Supplier3__c != null ? Double.valueOf(atsFreight.Emulsion_Rate8_Supplier3__c): 0);
        
		ats_RCcomp1 = (atsFreight.Competitor_Supplier_1_Rate__c != null ? Double.valueOf(atsFreight.Competitor_Supplier_1_Rate__c): 0);
        ats_A5comp1 = (atsFreight.Emulsion_Rate5_Competitor1__c != null ? Double.valueOf(atsFreight.Emulsion_Rate5_Competitor1__c): 0);
        ats_A6comp1 = (atsFreight.Emulsion_Rate6_Competitor1__c != null ? Double.valueOf(atsFreight.Emulsion_Rate6_Competitor1__c): 0);
        ats_A7comp1 = (atsFreight.Emulsion_Rate7_Competitor1__c != null ? Double.valueOf(atsFreight.Emulsion_Rate7_Competitor1__c): 0);
        ats_A8comp1 = (atsFreight.Emulsion_Rate8_Competitor1__c != null ? Double.valueOf(atsFreight.Emulsion_Rate8_Competitor1__c): 0);
        
		ats_RCcomp2 = (atsFreight.Competitor_Supplier_2_Rate__c != null ? Double.valueOf(atsFreight.Competitor_Supplier_2_Rate__c): 0);
        ats_A5comp2 = (atsFreight.Emulsion_Rate5_Competitor2__c != null ? Double.valueOf(atsFreight.Emulsion_Rate5_Competitor2__c): 0);
        ats_A6comp2 = (atsFreight.Emulsion_Rate6_Competitor2__c != null ? Double.valueOf(atsFreight.Emulsion_Rate6_Competitor2__c): 0);
        ats_A7comp2 = (atsFreight.Emulsion_Rate7_Competitor2__c != null ? Double.valueOf(atsFreight.Emulsion_Rate7_Competitor2__c): 0);
        ats_A8comp2 = (atsFreight.Emulsion_Rate8_Competitor2__c != null ? Double.valueOf(atsFreight.Emulsion_Rate8_Competitor2__c): 0);
        
        ats_RCcomp3 = (atsFreight.Competitor_Supplier_3_Rate__c != null ? Double.valueOf(atsFreight.Competitor_Supplier_3_Rate__c): 0);
        ats_A5comp3 = (atsFreight.Emulsion_Axle5_Rate_Competitor3__c != null ? Double.valueOf(atsFreight.Emulsion_Axle5_Rate_Competitor3__c): 0);
        ats_A6comp3 = (atsFreight.Emulsion_Axle6_Rate_Competitor3__c != null ? Double.valueOf(atsFreight.Emulsion_Axle6_Rate_Competitor3__c): 0);
        ats_A7comp3 = (atsFreight.Emulsion_Rate7_Competitor3__c != null ? Double.valueOf(atsFreight.Emulsion_Rate7_Competitor3__c): 0);
        ats_A8comp3 = (atsFreight.Emulsion_Axle8_Rate_Competitor3__c != null ? Double.valueOf(atsFreight.Emulsion_Axle8_Rate_Competitor3__c): 0);
        
		ats_RCcomp4 = (atsFreight.Competitor_Supplier_4_Rate__c != null ? Double.valueOf(atsFreight.Competitor_Supplier_4_Rate__c): 0);
        ats_A5comp4 = (atsFreight.Emulsion_Axle5_Rate_Competitor4__c != null ? Double.valueOf(atsFreight.Emulsion_Axle5_Rate_Competitor4__c): 0);
        ats_A6comp4 = (atsFreight.Emulsion_Axle6_Rate_Competitor4__c != null ? Double.valueOf(atsFreight.Emulsion_Axle6_Rate_Competitor4__c): 0);
        ats_A7comp4 = (atsFreight.Emulsion_Rate7_Competitor4__c != null ? Double.valueOf(atsFreight.Emulsion_Rate7_Competitor4__c): 0);
        ats_A8comp4 = (atsFreight.Emulsion_Axle8_Rate_Competitor4__c != null ? Double.valueOf(atsFreight.Emulsion_Axle8_Rate_Competitor4__c): 0);
        
    }
    
}