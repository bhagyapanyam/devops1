public with sharing class OperatingFieldRouteListController
{ 
    public OperatingFieldRouteListController(ApexPages.StandardController controller) {

    }


/*             
    @TestVisible private List<RouteList> route;
   
    @TestVisible private Field__c field;
        
    public OperatingFieldRouteListController (ApexPages.StandardController controller)
    {
        this.field = (Field__c)controller.getRecord();   
    }
    
    @TestVisible private class RouteList
    {
        public String Route_Link { get; set; }
        public String Route_Number { get; set; }
        public String Operator_1 { get; set; }
        public String Operator_2 { get; set; }
        public String Operator_3 { get; set; }
        public String Operator_4 { get; set; }
        public String Operator_1_MobilePhone { get; set; }
        public String Operator_2_MobilePhone { get; set; }
        public String Operator_3_MobilePhone { get; set; }
        public String Operator_4_MobilePhone { get; set; }
        public String Operator_1_Email { get; set; }
        public String Operator_2_Email { get; set; }
        public String Operator_3_Email { get; set; }
        public String Operator_4_Email { get; set; }
        public String Field_Senior { get; set; }
        
        public RouteList 
        (
            String sRoute_Link,             
            String sRoute, 
            String sOperator_1, 
            String sOperator_1_MobilePhone,
            String sOperator_1_Email,
            String sOperator_2, 
            String sOperator_2_MobilePhone,
            String sOperator_2_Email,
            String sOperator_3, 
            String sOperator_3_MobilePhone,
            String sOperator_3_Email,
            String sOperator_4,
            String sOperator_4_MobilePhone,
            String sOperator_4_Email,
            String sField_Senior
        )
        {
            Route_Link = sRoute_Link;
            Route_Number = sRoute;
            Operator_1 = sOperator_1;
            Operator_2 = sOperator_2;
            Operator_3 = sOperator_3;
            Operator_4 = sOperator_4;
            Operator_1_MobilePhone = sOperator_1_MobilePhone;
            Operator_2_MobilePhone = sOperator_2_MobilePhone;
            Operator_3_MobilePhone = sOperator_3_MobilePhone;
            Operator_4_MobilePhone = sOperator_4_MobilePhone;                        
            Operator_1_Email = sOperator_1_Email;
            Operator_2_Email = sOperator_2_Email;
            Operator_3_Email = sOperator_3_Email;
            Operator_4_Email = sOperator_4_Email;
            Field_Senior = sField_Senior;
        }
    }
    
    public List<RouteList> getRoute()
    {
        //Create new list to hold results based on new class
        route = new List<RouteList>();
        
        AggregateResult[] groupedResults
               = [SELECT 
                        Route__c,
                        Route__r.Route_Number__c Route_Number,
                        Route__r.Operator_1__r.FirstName Operator_1_FirstName, 
                        Route__r.Operator_1__r.LastName Operator_1_LastName,
                        Route__r.Operator_1__r.MobilePhone Operator_1_MobilePhone,
                        Route__r.Operator_1__r.Email Operator_1_Email,
                        Route__r.Operator_2__r.FirstName Operator_2_FirstName, 
                        Route__r.Operator_2__r.LastName Operator_2_LastName, 
                        Route__r.Operator_2__r.MobilePhone Operator_2_MobilePhone,
                        Route__r.Operator_2__r.Email Operator_2_Email,
                        Route__r.Operator_3__r.FirstName Operator_3_FirstName, 
                        Route__r.Operator_3__r.LastName Operator_3_LastName, 
                        Route__r.Operator_3__r.MobilePhone Operator_3_MobilePhone,
                        Route__r.Operator_3__r.Email Operator_3_Email,
                        Route__r.Operator_4__r.FirstName Operator_4_FirstName, 
                        Route__r.Operator_4__r.LastName Operator_4_LastName, 
                        Route__r.Operator_4__r.MobilePhone Operator_4_MobilePhone,
                        Route__r.Operator_4__r.Email Operator_4_Email,
                        Route__r.Field_Senior__r.FirstName Field_Senior_FirstName,
                        Route__r.Field_Senior__r.LastName Field_Senior_LastName,
                        Operating_Field_AMU__r.Id
                    FROM Location__c
                    GROUP BY
                        Route__c,
                        Route__r.Route_Number__c,
                        Route__r.Operator_1__r.FirstName,
                        Route__r.Operator_1__r.LastName, 
                        Route__r.Operator_1__r.MobilePhone,
                        Route__r.Operator_1__r.Email,
                        Route__r.Operator_2__r.FirstName,
                        Route__r.Operator_2__r.LastName, 
                        Route__r.Operator_2__r.MobilePhone,
                        Route__r.Operator_2__r.Email,                        
                        Route__r.Operator_3__r.FirstName,
                        Route__r.Operator_3__r.LastName, 
                        Route__r.Operator_3__r.MobilePhone,
                        Route__r.Operator_3__r.Email,
                        Route__r.Operator_4__r.FirstName,
                        Route__r.Operator_4__r.LastName, 
                        Route__r.Operator_4__r.MobilePhone,
                        Route__r.Operator_4__r.Email,
                        Route__r.Field_Senior__r.FirstName,
                        Route__r.Field_Senior__r.LastName,
                        Operating_Field_AMU__r.Id
                    HAVING Operating_Field_AMU__r.Id = :field.Id
                    ORDER BY Route__r.Route_Number__c];
                                                        
        String 
            Operator_1_FirstName, 
            Operator_1_LastName,
            Operator_2_FirstName, 
            Operator_2_LastName,
            Operator_3_FirstName, 
            Operator_3_LastName,
            Operator_4_FirstName, 
            Operator_4_LastName,
            Field_Senior_FirstName,
            Field_Senior_LastName;
        
        for (AggregateResult ar : groupedResults)  
        {
            Operator_1_FirstName = (String)ar.get('Operator_1_FirstName') == null ? '' : (String)ar.get('Operator_1_FirstName');
            Operator_1_LastName = (String)ar.get('Operator_1_LastName') == null ? '' : (String)ar.get('Operator_1_LastName');
            Operator_2_FirstName = (String)ar.get('Operator_2_FirstName') == null ? '' : (String)ar.get('Operator_2_FirstName');
            Operator_2_LastName = (String)ar.get('Operator_2_LastName') == null ? '' : (String)ar.get('Operator_2_LastName');
            Operator_3_FirstName = (String)ar.get('Operator_3_FirstName') == null ? '' : (String)ar.get('Operator_3_FirstName');
            Operator_3_LastName = (String)ar.get('Operator_3_LastName') == null ? '' : (String)ar.get('Operator_3_LastName');
            Operator_4_FirstName = (String)ar.get('Operator_4_FirstName') == null ? '' : (String)ar.get('Operator_4_FirstName');
            Operator_4_LastName = (String)ar.get('Operator_4_LastName') == null ? '' : (String)ar.get('Operator_4_LastName');
            Field_Senior_FirstName = (String)ar.get('Field_Senior_FirstName') == null ? '' : (String)ar.get('Field_Senior_FirstName');
            Field_Senior_LastName = (String)ar.get('Field_Senior_LastName') == null ? '' : (String)ar.get('Field_Senior_LastName');
            
            route.add
            (
                new RouteList 
                (
                    (String)ar.get('Route__c'), 
                    (String)ar.get('Route_Number'), 
                    Operator_1_FirstName + ' ' + Operator_1_LastName, 
                    (String)ar.get('Operator_1_MobilePhone'),
                    (String)ar.get('Operator_1_Email'),
                    Operator_2_FirstName + ' ' + Operator_2_LastName, 
                    (String)ar.get('Operator_2_MobilePhone'),
                    (String)ar.get('Operator_2_Email'),
                    Operator_3_FirstName + ' ' + Operator_3_LastName, 
                    (String)ar.get('Operator_3_MobilePhone'),
                    (String)ar.get('Operator_3_Email'),
                    Operator_4_FirstName + ' ' + Operator_4_LastName, 
                    (String)ar.get('Operator_4_MobilePhone'),
                    (String)ar.get('Operator_4_Email'),
                    Field_Senior_FirstName + ' ' + Field_Senior_LastName
                )
            ); 
        }
        return route;        
    }
*/    
}