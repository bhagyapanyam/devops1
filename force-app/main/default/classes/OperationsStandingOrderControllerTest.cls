@isTest
private class OperationsStandingOrderControllerTest {

    @testSetup
    static void setUp() {
        Standing_Order__c stdOrder1 = new Standing_Order__c();
        stdOrder1.Details__c = 'Standing Order 1 details here';
        stdOrder1.Plant__c = 'Oil Processing';
        stdOrder1.Start_Date__c = System.Today() - 365;
        stdOrder1.Completion_Date__c = System.Today() + 365;

        Standing_Order__c stdOrder2 = new Standing_Order__c();
        stdOrder2.Details__c = 'Standing Order 2 details here';
        stdOrder2.Plant__c = 'Field Facilities';
        stdOrder2.Start_Date__c = System.Today() - 365;
        stdOrder2.Completion_Date__c = System.Today();        
        
        Standing_Order__c stdOrder3 = new Standing_Order__c();
        stdOrder3.Details__c = 'Standing Order 3 details here';
        stdOrder3.Plant__c = 'Water De-Oiling';
        stdOrder3.Start_Date__c = System.Today() - 365;
        stdOrder3.Completion_Date__c = null;        
        
        Standing_Order__c stdOrder4 = new Standing_Order__c();
        stdOrder4.Details__c = 'Standing Order 4 details here';
        stdOrder4.Plant__c = 'Steam Generation';
        stdOrder4.Start_Date__c = System.Today() - 365;
        stdOrder4.Completion_Date__c = System.Today() - 20;        

        insert stdOrder1;
        insert stdOrder2;
        insert stdOrder3;
        insert stdOrder4;

    }

    @isTest
    static void testBuildStandingOrderWrapper_OnPageLoad() {
        OperationsStandingOrderController stdOrderController = new OperationsStandingOrderController();
        Map<String, List<OperationsStandingOrderController.OperationsStandingOrderWrapper>> pendingStandingOrderWrapperMap = stdOrderController.pendingStandingOrderWrapperMap;
        Map<String, List<OperationsStandingOrderController.OperationsStandingOrderWrapper>> completedStandingOrderWrapperMap = stdOrderController.completedStandingOrderWrapperMap;

        System.assertEquals(3, pendingStandingOrderWrapperMap.size());
        System.assertEquals(true, pendingStandingOrderWrapperMap.containsKey('Water De-Oiling'));
        System.assertEquals(0, completedStandingOrderWrapperMap.size());
    }

    @isTest
    static void testConfirmReadAcknowledgement() {
        OperationsStandingOrderController stdOrderController = new OperationsStandingOrderController();

        List<Standing_Order__c> standingOrders = [SELECT Id, Details__c, Plant__c, Start_Date__c, Completion_Date__c FROM Standing_Order__c];

        User u = TestUtilities.createOperationsUser();

        System.runAs(u) {
            stdOrderController.selectedStandingOrderId = standingOrders[0].Id;
            stdOrderController.confirmReadAcknowledgement();

            List<Standing_Order_Acknowledgement__c> acknowledgements = [SELECT Id, Standing_Order__c, Acknowledged_User__c, Read_Acknowledged__c, Read_Acknowledged_Date__c 
                                                                            FROM Standing_Order_Acknowledgement__c]; 

            System.assertEquals(1, acknowledgements.size());
            System.assertEquals(u.Id, acknowledgements[0].Acknowledged_User__c);
            System.assertEquals(true, acknowledgements[0].Read_Acknowledged__c);
            System.assertNotEquals(null, acknowledgements[0].Read_Acknowledged_Date__c);

        }       
    }

    @isTest
    static void testBuildStandingOrderWrapper_WithConfirmed_OnPageLoad() {
        List<Standing_Order__c> standingOrders = [SELECT Id, Details__c, Plant__c, Start_Date__c, Completion_Date__c 
                                                    FROM Standing_Order__c
                                                    WHERE Completion_Date__c = null OR Completion_Date__c >= TODAY]; 

        Standing_Order_Acknowledgement__c acknowledgement = new Standing_Order_Acknowledgement__c();
        acknowledgement.Standing_Order__c = standingOrders[0].Id;
        acknowledgement.Read_Acknowledged__c = true;
        acknowledgement.Read_Acknowledged_Date__c = System.Now();
        acknowledgement.Acknowledged_User__c = UserInfo.getUserId();
        insert acknowledgement;

        PageReference pageRef = Page.OperationsStandingOrder;
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController stcController = new ApexPages.StandardController(acknowledgement);

        OperationsStandingOrderController stdOrderController = new OperationsStandingOrderController(stcController);
        Map<String, List<OperationsStandingOrderController.OperationsStandingOrderWrapper>> pendingStandingOrderWrapperMap = stdOrderController.pendingStandingOrderWrapperMap;
        Map<String, List<OperationsStandingOrderController.OperationsStandingOrderWrapper>> completedStandingOrderWrapperMap = stdOrderController.completedStandingOrderWrapperMap;

        System.assertEquals(1, completedStandingOrderWrapperMap.size());        

    }

}