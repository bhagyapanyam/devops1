public with sharing class PREPChangeCategoryController {
    public PREP_Initiative__c preInitiative { get; set; }
    public PREP_Material_Service_Group__c taxonomy { get; set; }
    public PREP_Initiative__c preInitiativeNew { get; set; }
    public PREPWrapper prepWrapperIns { get; set; }
    public String requestTitle { get; set; }
    
    Map<Id, PREP_Initiative__c> initiativeMap;
    Map<Id, PREP_Material_Service_Group__c> taxonomyMap;
    
    public PREPChangeCategoryController() {
        preInitiative = new PREP_Initiative__c();
        preInitiativeNew = new PREP_Initiative__c();
        taxonomy = new PREP_Material_Service_Group__c();
    }
    
    public void searchRecords() {
        Boolean whereCondition = false;
        String categoryInitiative = System.Label.Category_Initiative_RecId;
        String projectInitiative = System.Label.Projects_Initiative_RecId;

        String inititiveQuery = 'SELECT Id, RecordTypeId, Name, Owner.Name, Initiative_Name__c, Discipline__r.Name, Tech_Services__r.Name, PMO__r.Name, '
                                + 'Safety__r.Name, Category_Manager__r.Name, Status__c, Project_Lead__R.Name, Technician__r.Name, Sub_Category__c, '
                                + 'Commercial_Representative__r.Name, Procurement_Representative__r.Name, Category_Specialist__r.Name, Discipline__c, '
                                + 'SCM_Manager__r.Name, GSO__r.Name, Business_Analyst__r.Name, Category__c, Category__r.Name, Sub_Category__r.Name '
                                + 'FROM PREP_Initiative__c';
                                
        String taxonomyQuery = 'SELECT Id, Name, Owner.Name, Material_Service_Group_Name__c, Category__c, Discipline__c, SAP_Short_Text_Name__c, '
                                + 'Type__c, Includes__c, Category_Manager__r.Name, Sub_Category__r.Category__r.Name, Category_Specialist__r.Name, '
                                + 'Sub_Category__r.Name, Category_Link__c, Category_Link__r.Name, Discipline_Link__c, Discipline_Link__r.Name, '
                                + 'Sub_Category__c FROM PREP_Material_Service_Group__c';                       
        
        if( preInitiative.Category__c != null ){
            if( !whereCondition ){
                inititiveQuery += ' Where Category__c = \'' + preInitiative.Category__c + '\'';
                taxonomyQuery += ' Where Category_Link__c = \'' + preInitiative.Category__c + '\'';
            } else {
                inititiveQuery += ' AND Category__c = \'' + preInitiative.Category__c + '\'';
                taxonomyQuery += ' AND Category_Link__c = \'' + preInitiative.Category__c + '\'';
            }
            whereCondition = true;
        }
        
        if( taxonomy.Sub_Category__c != null ){
            if( !whereCondition ){
                inititiveQuery += ' Where Sub_Category__c = \'' + taxonomy.Sub_Category__c + '\'';
                taxonomyQuery += ' Where Sub_Category__c = \'' + taxonomy.Sub_Category__c + '\'';
            } else {
                inititiveQuery += ' AND Sub_Category__c = \'' + taxonomy.Sub_Category__c + '\'';
                taxonomyQuery += ' AND Sub_Category__c = \'' + taxonomy.Sub_Category__c + '\''; 
            }
            whereCondition = true;
        }
        
        if( preInitiative.Discipline__c != null ){
            if( !whereCondition ){
                inititiveQuery += ' Where Discipline__c = \'' + preInitiative.Discipline__c + '\'';
                taxonomyQuery += ' Where Discipline_Link__c = \'' + preInitiative.Discipline__c + '\'';
            } else {
                inititiveQuery += ' AND Discipline__c = \'' + preInitiative.Discipline__c + '\'';
                taxonomyQuery += ' AND Discipline_Link__c = \'' + preInitiative.Discipline__c + '\''; 
            }
            whereCondition = true;
        }
        
        if( preInitiative.Category_Manager__c != null ){
            if( !whereCondition ){
                inititiveQuery += ' Where Category_Manager__c = \'' + preInitiative.Category_Manager__c + '\'';
                taxonomyQuery += ' Where Category_Manager__c = \'' + preInitiative.Category_Manager__c + '\'';
            } else {
                inititiveQuery += ' AND Category_Manager__c = \'' + preInitiative.Category_Manager__c + '\'';
                taxonomyQuery += ' AND Category_Manager__c = \'' + preInitiative.Category_Manager__c + '\''; 
            }
            whereCondition = true;
        }
        
        if( preInitiative.Category_Specialist__c != null ){
            if( !whereCondition ){
                inititiveQuery += ' Where Category_Specialist__c = \'' + preInitiative.Category_Specialist__c + '\'';
                taxonomyQuery += ' Where Category_Specialist__c = \'' + preInitiative.Category_Specialist__c + '\'';
            } else {
                inititiveQuery += ' AND Category_Specialist__c = \'' + preInitiative.Category_Specialist__c + '\''; 
                taxonomyQuery += ' AND Category_Specialist__c = \'' + preInitiative.Category_Specialist__c + '\'';
            }
            whereCondition = true;
        }
        
        if( taxonomy.Material_Service_Group_Name__c != null && taxonomy.Material_Service_Group_Name__c != '' ){
            if( !whereCondition )
                taxonomyQuery += ' Where Material_Service_Group_Name__c LIKE \'%' + String.escapeSingleQuotes( taxonomy.Material_Service_Group_Name__c ) + '%\'';
            else
                taxonomyQuery += ' AND Material_Service_Group_Name__c LIKE \'%' + String.escapeSingleQuotes( taxonomy.Material_Service_Group_Name__c ) + '%\'';
            whereCondition = true;
        }
        
        List<PREP_Initiative__c> initiativesList = Database.query( inititiveQuery );
        List<PREP_Material_Service_Group__c> taxonomyList = Database.query( taxonomyQuery );
        
        List<PREPInitiativeWrapper> prepInitiativeWrapperList = new List<PREPInitiativeWrapper>(); 
        List<PREPInitiativeWrapper> prepPrjInitiativeWrapperList = new List<PREPInitiativeWrapper>();
        List<PREPTaxonomyWrapper> prepTaxonomyWrapperList = new List<PREPTaxonomyWrapper>();
        initiativeMap = new Map<Id, PREP_Initiative__c>();
        taxonomyMap = new Map<Id, PREP_Material_Service_Group__c>();
    
        for( PREP_Initiative__c ini : initiativesList ){            
            if( ini.RecordTypeId == categoryInitiative )
                prepInitiativeWrapperList.add( new PREPInitiativeWrapper( ini.clone( true, true ), ini.Name ));
            else if( ini.RecordTypeId == projectInitiative )
                prepPrjInitiativeWrapperList.add( new PREPInitiativeWrapper( ini.clone( true, true ), ini.Name ));
        
            initiativeMap.put( ini.Id, ini );
        }
        
        for( PREP_Material_Service_Group__c t : taxonomyList ){
            prepTaxonomyWrapperList.add( new PREPTaxonomyWrapper( t.clone( true, true ) ));
            taxonomyMap.put( t.Id, t );
        }
    
        prepWrapperIns = new PREPWrapper( prepInitiativeWrapperList, prepTaxonomyWrapperList, prepPrjInitiativeWrapperList );
    }
    
    public PageReference saveChanges() {
        Set<Id> categoryIds = new Set<Id>();
        Set<Id> subCategoryIds = new Set<Id>();
        Set<Id> disciplineIds = new Set<Id>();
        
        List<PREPInitiativeWrapper> iniWrapper = new List<PREPInitiativeWrapper>();
        List<PREPInitiativeWrapper> prjiniWrapper = new List<PREPInitiativeWrapper>();
        List<PREPTaxonomyWrapper> taxWrapper = new List<PREPTaxonomyWrapper>();
        
        //system.assert( false, prepWrapperIns );
        for( PREPInitiativeWrapper w : prepWrapperIns.initWrapperList ){
            if( w.selected ){ 
                iniWrapper.add( w );
                categoryIds.add( w.initiative.Category__c );
                //subCategoryIds.add( w.initiative.Sub_Category__c );
                disciplineIds.add( w.initiative.Discipline__c );
            }
        }
        
        for( PREPInitiativeWrapper w : prepWrapperIns.prjinitWrapperList ){
            if( w.selected ){ 
                prjiniWrapper.add( w );
                categoryIds.add( w.initiative.Category__c );
                //subCategoryIds.add( w.initiative.Sub_Category__c );
                disciplineIds.add( w.initiative.Discipline__c );
            }
        }
        
        for( PREPTaxonomyWrapper w : prepWrapperIns.taxonomyWrapperList ){
            if( w.selected ){ 
                taxWrapper.add( w );
                categoryIds.add( w.taxonomy.Category_Link__c );
                subCategoryIds.add( w.taxonomy.Sub_Category__c );
                disciplineIds.add( w.taxonomy.Discipline_Link__c );
            }        
        }
        
        Map<Id, PREP_Category__c> categoryMap = new Map<Id, PREP_Category__c>([ Select Id, Name FROM PREP_Category__c WHERE Id IN :categoryIds ]);
        Map<Id, PREP_Sub_Category__c> subCategoryMap = new Map<Id, PREP_Sub_Category__c>([ Select Id, Name FROM PREP_Sub_Category__c WHERE Id IN :subCategoryIds ]);
        Map<Id, PREP_Discipline__c> disciplineMap = new Map<Id, PREP_Discipline__c>([ Select Id, Name FROM PREP_Discipline__c WHERE Id IN :disciplineIds ]);
        
        if( iniWrapper.size() > 0 ){
            saveInitiatives( iniWrapper, categoryMap, subCategoryMap, disciplineMap, 'PREP_Initiative__c', 'Initiatives Change' );
        }
        
        if( prjiniWrapper.size() > 0 ){
            saveInitiatives( prjiniWrapper, categoryMap, subCategoryMap, disciplineMap, 'PREP_prjinitiative__c', 'Project Initiatives Change' );
        }
        
        if( taxWrapper.size() > 0 ){
            saveTaxonomy( taxWrapper, categoryMap, subCategoryMap, disciplineMap, 'PREP_Material_Service_Group__c', 'Taxonomy Change' );
        }
    
        PageReference rageRef = Page.Taxonomy_Requests;
        rageRef.setRedirect( true );
        return rageRef;
    }
    
    public PageReference saveAndSubmitChanges(){
        Set<Id> categoryIds = new Set<Id>();
        Set<Id> subCategoryIds = new Set<Id>();
        Set<Id> disciplineIds = new Set<Id>();
        
        List<PREPInitiativeWrapper> iniWrapper = new List<PREPInitiativeWrapper>();
        List<PREPInitiativeWrapper> prjiniWrapper = new List<PREPInitiativeWrapper>();
        List<PREPTaxonomyWrapper> taxWrapper = new List<PREPTaxonomyWrapper>();
        
        for( PREPInitiativeWrapper w : prepWrapperIns.initWrapperList ){
            if( w.selected ){ 
                iniWrapper.add( w );
                categoryIds.add( w.initiative.Category__c );
                //subCategoryIds.add( w.initiative.Sub_Category__c );
                disciplineIds.add( w.initiative.Discipline__c );
            }
        }
        
        for( PREPInitiativeWrapper w : prepWrapperIns.prjinitWrapperList ){
            if( w.selected ){ 
                prjiniWrapper.add( w );
                categoryIds.add( w.initiative.Category__c );
                //subCategoryIds.add( w.initiative.Sub_Category__c );
                disciplineIds.add( w.initiative.Discipline__c );
            }
        }
        
        for( PREPTaxonomyWrapper w : prepWrapperIns.taxonomyWrapperList ){
            if( w.selected ){ 
                taxWrapper.add( w );
                categoryIds.add( w.taxonomy.Category_Link__c );
                subCategoryIds.add( w.taxonomy.Sub_Category__c );
                disciplineIds.add( w.taxonomy.Discipline_Link__c );
            }        
        }
        
        Map<Id, PREP_Category__c> categoryMap = new Map<Id, PREP_Category__c>([ Select Id, Name FROM PREP_Category__c WHERE Id IN :categoryIds ]);
        Map<Id, PREP_Sub_Category__c> subCategoryMap = new Map<Id, PREP_Sub_Category__c>([ Select Id, Name FROM PREP_Sub_Category__c WHERE Id IN :subCategoryIds ]);
        Map<Id, PREP_Discipline__c> disciplineMap = new Map<Id, PREP_Discipline__c>([ Select Id, Name FROM PREP_Discipline__c WHERE Id IN :disciplineIds ]);

        if( iniWrapper.size() > 0 ){
            List<PREP_Change_Header__c> headerList = saveInitiatives( iniWrapper, categoryMap, subCategoryMap, disciplineMap, 'PREP_Initiative__c',
                                                                        'Initiatives Change' );
            
            for( PREP_Change_Header__c changeHeader : headerList ){
                Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
                request.setObjectId( changeHeader.Id );
                request.setProcessDefinitionNameOrId( 'PREP_Taxonomy_Change_Approval' );
                
                if( !Test.isRunningTest() )
                    Approval.ProcessResult result = Approval.process( request );
            }
        }
        
        if( prjiniWrapper.size() > 0 ){
            List<PREP_Change_Header__c> headerList = saveInitiatives( prjiniWrapper, categoryMap, subCategoryMap, disciplineMap, 'PREP_prjinitiative__c',
                                                                        'Project Initiatives Change' );
            
            for( PREP_Change_Header__c changeHeader : headerList ){
                Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
                request.setObjectId( changeHeader.Id );
                request.setProcessDefinitionNameOrId( 'PREP_Taxonomy_Change_Approval' );
                
                if( !Test.isRunningTest() )
                    Approval.ProcessResult result = Approval.process( request );
            }
        }
            
        if( taxWrapper.size() > 0 ){
            List<PREP_Change_Header__c> headerList = saveTaxonomy( taxWrapper, categoryMap, subCategoryMap, disciplineMap, 'PREP_Material_Service_Group__c',
                                                                    'Taxonomy Change' );
            
            for( PREP_Change_Header__c changeHeader : headerList ){
                Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
                request.setObjectId( changeHeader.Id );
                request.setProcessDefinitionNameOrId( 'PREP_Taxonomy_Change_Approval' );
                
                if( !Test.isRunningTest() )
                    Approval.ProcessResult result = Approval.process(request);
            }
        }
    
        PageReference rageRef = Page.Taxonomy_Requests;
        rageRef.setRedirect( true );
        return rageRef;
    }
    
    private List<PREP_Change_Header__c> saveInitiatives( List<PREPInitiativeWrapper> iniWrapper, Map<Id, PREP_Category__c> categoryMap, Map<Id, PREP_Sub_Category__c> subCategoryMap,
                                                            Map<Id, PREP_Discipline__c> disciplineMap, String objectName, String reasonVal ){
        Boolean catFlag = false;
        //Boolean subCatFlag = false;
        Boolean disciFlag = false;
        
        for( PREPInitiativeWrapper w : iniWrapper ){
            //system.assert( false, w.initiative.Category__c + ':::::::' + initiativeMap.get( w.initiative.Id ).Category__c );
            if( w.initiative.Category__c != initiativeMap.get( w.initiative.Id ).Category__c ){
                catFlag = true;
            }
            
            /*if( w.initiative.Sub_Category__c != initiativeMap.get( w.initiative.Id ).Sub_Category__c ){
                subCatFlag = true;
            }*/
            
            if( w.initiative.Discipline__c != initiativeMap.get( w.initiative.Id ).Discipline__c ){
                disciFlag = true;
            }
        }
        //system.assert( false, catFlag );
        
        List<PREP_Change_Header__c> headerList = new List<PREP_Change_Header__c>();
        PREP_Change_Header__c headerCat;
        //PREP_Change_Header__c headerSubCat;
        PREP_Change_Header__c headerDiscipline;
        if( catFlag ){
            headerCat = new PREP_Change_Header__c( Reason__c = reasonVal, Change_Type__c = 'Change Category',
                                                    Request_Created_Date__c = Date.today(), Request_Last_Updated_Date__c = Date.today(),
                                                    Request_Title__c= requestTitle, Request_Status__c = 'New', Approval_Status__c = 'New', Change_Status__c = 'New',
                                                    Object_Name__c = objectName );
        
            headerList.add( headerCat );
        }
        
        /*if( subCatFlag ){
            headerSubCat = new PREP_Change_Header__c( Reason__c = reasonVal, Change_Type__c = 'Change Sub Category',
                                                        Request_Created_Date__c = Date.today(), Request_Last_Updated_Date__c = Date.today(),
                                                        Request_Title__c= requestTitle, Request_Status__c = 'New', Approval_Status__c = 'New',
                                                        Change_Status__c = 'New', Object_Name__c = objectName );
            headerList.add( headerSubCat );
        }*/
        
        if( disciFlag ){
            headerDiscipline = new PREP_Change_Header__c( Reason__c = reasonVal, Change_Type__c = 'Change Discipline',
                                                            Request_Created_Date__c = Date.today(), Request_Last_Updated_Date__c = Date.today(),
                                                            Request_Title__c= requestTitle, Request_Status__c = 'New', Approval_Status__c = 'New',
                                                            Change_Status__c = 'New', Object_Name__c = objectName );
            headerList.add( headerDiscipline );
        }                                                                    
        
        if( headerList.size() > 0 ){
            insert headerList;
            
            List<PREP_Change_Detail__c> detailList = new List<PREP_Change_Detail__c>();
            for( PREPInitiativeWrapper w : iniWrapper ){
                if( w.initiative.Category__c != initiativeMap.get( w.initiative.Id ).Category__c ){
                    PREP_Change_Detail__c detailRec = new PREP_Change_Detail__c( PREP_Change_Header__c = headerCat.id, Field_Name__c = 'Category',
                                                                                    From_Field_value__c = initiativeMap.get( w.initiative.Id ).Category__r.Name,
                                                                                    Record_Id__c = w.initiative.Category__c,
                                                                                    To_Field_value__c = categoryMap.get( w.initiative.Category__c ).Name,
                                                                                    To_Field_Id__c = w.initiative.Category__c,
                                                                                    From_Field_Id__c = initiativeMap.get( w.initiative.Id ).Category__c,
                                                                                    Update_Record_Id__c = w.initiative.Id, Update_Record_Name__c = initiativeMap.get( w.initiative.Id ).Name,
                                                                                    Related_Field_Id__c = w.initiative.Sub_Category__c );
                    detailList.add( detailRec );
                }
                
                /*if( w.initiative.Sub_Category__c != initiativeMap.get( w.initiative.Id ).Sub_Category__c ){
                    PREP_Change_Detail__c detailRec = new PREP_Change_Detail__c( PREP_Change_Header__c = headerSubCat.id, Field_Name__c = 'Sub Category',
                                                                                    From_Field_value__c = initiativeMap.get( w.initiative.Id ).Sub_Category__r.Name,
                                                                                    Record_Id__c = w.initiative.Sub_Category__c,
                                                                                    To_Field_value__c = subCategoryMap.get( w.initiative.Sub_Category__c ).Name,
                                                                                    To_Field_Id__c = w.initiative.Sub_Category__c,
                                                                                    From_Field_Id__c = initiativeMap.get( w.initiative.Id ).Sub_Category__c,
                                                                                    Update_Record_Id__c = w.initiative.Id, Update_Record_Name__c = initiativeMap.get( w.initiative.Id ).Name,
                                                                                    Related_Field_Id__c = w.initiative.Category__c );
                    detailList.add( detailRec );
                }*/
                
                if( w.initiative.Discipline__c != initiativeMap.get( w.initiative.Id ).Discipline__c ){
                    PREP_Change_Detail__c detailRec = new PREP_Change_Detail__c( PREP_Change_Header__c = headerDiscipline.id, Field_Name__c = 'Discipline',
                                                                                    From_Field_value__c = initiativeMap.get( w.initiative.Id ).Discipline__r.Name,
                                                                                    Record_Id__c = w.initiative.Discipline__c,
                                                                                    To_Field_value__c = disciplineMap.get( w.initiative.Discipline__c ).Name,
                                                                                    To_Field_Id__c = w.initiative.Discipline__c,
                                                                                    From_Field_Id__c = initiativeMap.get( w.initiative.Id ).Discipline__c,
                                                                                    Update_Record_Id__c = w.initiative.Id, Update_Record_Name__c = initiativeMap.get( w.initiative.Id ).Name );
                    detailList.add( detailRec ); 
                }
            }
            
            if( detailList.size() > 0 )    
                insert detailList; 
        }    
        return headerList; 
    }
    
    private List<PREP_Change_Header__c> saveTaxonomy( List<PREPTaxonomyWrapper> taxWrapper, Map<Id, PREP_Category__c> categoryMap, Map<Id, PREP_Sub_Category__c> subCategoryMap,
                                                        Map<Id, PREP_Discipline__c> disciplineMap, String objectName, String reasonVal ){
        Boolean catFlag = false;
        Boolean subCatFlag = false;
        Boolean disciFlag = false;
        
        for( PREPTaxonomyWrapper w : taxWrapper ){
            if( w.taxonomy.Category_Link__c != taxonomyMap.get( w.taxonomy.Id ).Category_Link__c ){
                catFlag = true;
            }
            
            if( w.taxonomy.Sub_Category__c != taxonomyMap.get( w.taxonomy.Id ).Sub_Category__c ){
                subCatFlag = true;
            }
            
            if( w.taxonomy.Discipline_Link__c != taxonomyMap.get( w.taxonomy.Id ).Discipline_Link__c ){
                disciFlag = true;
            }
        }
        
        List<PREP_Change_Header__c> headerList = new List<PREP_Change_Header__c>();
        PREP_Change_Header__c headerCat;
        PREP_Change_Header__c headerSubCat;
        PREP_Change_Header__c headerDiscipline;
        
        if( catFlag ){
            headerCat = new PREP_Change_Header__c( Reason__c = reasonVal, Change_Type__c = 'Change Category',
                                                    Request_Created_Date__c = Date.today(), Request_Last_Updated_Date__c = Date.today(),
                                                    Request_Title__c= requestTitle, Request_Status__c = 'New', Approval_Status__c = 'New', Change_Status__c = 'New',
                                                    Object_Name__c = objectName );
        
            headerList.add( headerCat );
        }
        
        if( subCatFlag ){
            headerSubCat = new PREP_Change_Header__c( Reason__c = reasonVal, Change_Type__c = 'Change Sub Category',
                                                        Request_Created_Date__c = Date.today(), Request_Last_Updated_Date__c = Date.today(),
                                                        Request_Title__c= requestTitle, Request_Status__c = 'New', Approval_Status__c = 'New',
                                                        Change_Status__c = 'New', Object_Name__c = objectName );
            headerList.add( headerSubCat );
        }
        
        if( disciFlag ){
            headerDiscipline = new PREP_Change_Header__c( Reason__c = reasonVal, Change_Type__c = 'Change Discipline',
                                                            Request_Created_Date__c = Date.today(), Request_Last_Updated_Date__c = Date.today(),
                                                            Request_Title__c= requestTitle, Request_Status__c = 'New', Approval_Status__c = 'New',
                                                            Change_Status__c = 'New', Object_Name__c = objectName );
            headerList.add( headerDiscipline );
        }
        
        if( headerList.size() > 0 ){
            insert headerList;
        
            List<PREP_Change_Detail__c> detailList = new List<PREP_Change_Detail__c>();
            for( PREPTaxonomyWrapper w : taxWrapper ){
                if( w.taxonomy.Category_Link__c != taxonomyMap.get( w.taxonomy.Id ).Category_Link__c ){                                                                                
                    PREP_Change_Detail__c detailRec = new PREP_Change_Detail__c( PREP_Change_Header__c = headerCat.id, Field_Name__c = 'Category',
                                                                                    From_Field_value__c = taxonomyMap.get( w.taxonomy.Id ).Category_Link__r.Name,
                                                                                    Record_Id__c = w.taxonomy.Category_Link__c,
                                                                                    To_Field_value__c = categoryMap.get( w.taxonomy.Category_Link__c ).Name,
                                                                                    To_Field_Id__c = w.taxonomy.Category_Link__c,
                                                                                    From_Field_Id__c = taxonomyMap.get( w.taxonomy.Id ).Category_Link__c,
                                                                                    Update_Record_Id__c = w.taxonomy.Id, Update_Record_Name__c = taxonomyMap.get( w.taxonomy.Id ).Name );
                    detailList.add( detailRec );
                }
                
                if( w.taxonomy.Sub_Category__c != taxonomyMap.get( w.taxonomy.Id ).Sub_Category__c ){                                                                                
                    PREP_Change_Detail__c detailRec = new PREP_Change_Detail__c( PREP_Change_Header__c = headerSubCat.id, Field_Name__c = 'Sub Category',
                                                                                    From_Field_value__c = taxonomyMap.get( w.taxonomy.Id ).Sub_Category__r.Name,
                                                                                    Record_Id__c = w.taxonomy.Sub_Category__c,
                                                                                    To_Field_value__c = subCategoryMap.get( w.taxonomy.Sub_Category__c ).Name,
                                                                                    To_Field_Id__c = w.taxonomy.Sub_Category__c,
                                                                                    From_Field_Id__c = taxonomyMap.get( w.taxonomy.Id ).Sub_Category__c,
                                                                                    Update_Record_Id__c = w.taxonomy.Id, Update_Record_Name__c = taxonomyMap.get( w.taxonomy.Id ).Name );
                    detailList.add( detailRec );
                }
                
                if( w.taxonomy.Discipline_Link__c != taxonomyMap.get( w.taxonomy.Id ).Discipline_Link__c ){                                                                                
                    PREP_Change_Detail__c detailRec = new PREP_Change_Detail__c( PREP_Change_Header__c = headerDiscipline.id, Field_Name__c = 'Discipline',
                                                                                    From_Field_value__c = taxonomyMap.get( w.taxonomy.Id ).Discipline_Link__r.Name,
                                                                                    Record_Id__c = w.taxonomy.Discipline_Link__c,
                                                                                    To_Field_value__c = disciplineMap.get( w.taxonomy.Discipline_Link__c ).Name,
                                                                                    To_Field_Id__c = w.taxonomy.Discipline_Link__c,
                                                                                    From_Field_Id__c = taxonomyMap.get( w.taxonomy.Id ).Discipline_Link__c,
                                                                                    Update_Record_Id__c = w.taxonomy.Id, Update_Record_Name__c = taxonomyMap.get( w.taxonomy.Id ).Name );
                    detailList.add( detailRec );
                }
            }
            
            if( detailList.size() > 0 )    
                insert detailList;  
        }
        return headerList;
    }
    
    public class PREPWrapper {
        public List<PREPInitiativeWrapper> initWrapperList { get; set; }
        public List<PREPInitiativeWrapper> prjInitWrapperList { get; set; }
        public List<PREPTaxonomyWrapper> taxonomyWrapperList { get; set; }
        
        public PREPWrapper( List<PREPInitiativeWrapper> initWrapperList, List<PREPTaxonomyWrapper> taxonomyWrapperList, List<PREPInitiativeWrapper> prjInitWrapperList ){
            this.initWrapperList = initWrapperList;
            this.prjInitWrapperList = prjInitWrapperList;
            this.taxonomyWrapperList = taxonomyWrapperList;
        }        
    }
    
    public class PREPInitiativeWrapper {
        public Boolean selected { get; set; }
        public PREP_Initiative__c initiative { get; set; }
        public String iniName { get; set; }
        
        public PREPInitiativeWrapper( PREP_Initiative__c initiative, String iniName ){
            this.initiative = initiative;
            this.selected = false;
            this.iniName = iniName;
        }  
    }
    
    public class PREPTaxonomyWrapper {
        public Boolean selected { get; set; }
        public PREP_Material_Service_Group__c taxonomy { get; set; }
        
        public PREPTaxonomyWrapper( PREP_Material_Service_Group__c taxonomy ){
            this.taxonomy = taxonomy;
            this.selected = false;
        }
    }
}