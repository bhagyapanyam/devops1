public with sharing class PREPChangeGLAccountCodeController {
    public PREP_Initiative__c preInitiative { get; set; }
    public PREP_Material_Service_Group__c taxonomy { get; set; }
    public List<PREPTaxonomyWrapper> taxonomyWrapperList { get; set; }
    public String requestTitle { get; set; }
    public String valuationClassDesc { get; set; }
    private String sortDirection = 'ASC';
    private String sortExp = 'Name';   
    Map<Id, PREP_Material_Service_Group__c> taxonomyMap;
    
    public PREPChangeGLAccountCodeController() {
        preInitiative = new PREP_Initiative__c();
        taxonomy = new PREP_Material_Service_Group__c();
        taxonomyWrapperList = new List<PREPTaxonomyWrapper>();
        valuationClassDesc = '';
        //sortOrder = 'Name';
        //ascendingOrDescending = 'ASC';
    }
    
    public String sortExpression
    {
        get
        {
            return sortExp;
        }
        set
        {
            //if the column is clicked on then switch between Ascending and Descending modes
            if( value == sortExp )
                sortDirection = ( sortDirection == 'ASC' ) ? 'DESC' : 'ASC';
            else
                sortDirection = 'ASC';
            sortExp = value;
        }
    }

    public String getSortDirection()
    {
        //if not column is selected 
        if( sortExpression == null || sortExpression == '' )
            return 'ASC';
        else
            return sortDirection;
    }

    public void setSortDirection(String value)
    {  
        sortDirection = value;
    }
    
    public void searchRecords() {
        Boolean whereCondition = false;
        taxonomyWrapperList = new List<PREPTaxonomyWrapper>();

        String taxonomyQuery = 'SELECT Id, Name, Owner.Name, Material_Service_Group_Name__c, SAP_Short_Text_Name__c, '
                                + 'Type__c, Category_Manager__r.Name, Category_Specialist__r.Name, '
                                + 'Category_Link__c, Discipline_Link__c, '
                                + 'Sub_Category__c, GL_Account__c, GL_Account__r.Name, GL_Account_Number__c, Valuation_Class__c, '
                                + 'Valuation_Class_Number__c FROM PREP_Material_Service_Group__c';                       
        
        if( preInitiative.Category__c != null ){
            if( !whereCondition ){
                taxonomyQuery += ' Where Category_Link__c = \'' + preInitiative.Category__c + '\'';
            } else {
                taxonomyQuery += ' AND Category_Link__c = \'' + preInitiative.Category__c + '\'';
            }
            whereCondition = true;
        }
        
        if( taxonomy.Sub_Category__c != null ){
            if( !whereCondition ){
                taxonomyQuery += ' Where Sub_Category__c = \'' + taxonomy.Sub_Category__c + '\'';
            } else {
                taxonomyQuery += ' AND Sub_Category__c = \'' + taxonomy.Sub_Category__c + '\''; 
            }
            whereCondition = true;
        }
        
        if( preInitiative.Discipline__c != null ){
            if( !whereCondition ){
                taxonomyQuery += ' Where Discipline_Link__c = \'' + preInitiative.Discipline__c + '\'';
            } else {
                taxonomyQuery += ' AND Discipline_Link__c = \'' + preInitiative.Discipline__c + '\''; 
            }
            whereCondition = true;
        }
        
        if( preInitiative.Category_Manager__c != null ){
            if( !whereCondition ){
                taxonomyQuery += ' Where Category_Manager__c = \'' + preInitiative.Category_Manager__c + '\'';
            } else {
                taxonomyQuery += ' AND Category_Manager__c = \'' + preInitiative.Category_Manager__c + '\''; 
            }
            whereCondition = true;
        }
        
        if( preInitiative.Category_Specialist__c != null ){
            if( !whereCondition ){
                taxonomyQuery += ' Where Category_Specialist__c = \'' + preInitiative.Category_Specialist__c + '\'';
            } else {
                taxonomyQuery += ' AND Category_Specialist__c = \'' + preInitiative.Category_Specialist__c + '\'';
            }
            whereCondition = true;
        }
        
        if( taxonomy.GL_Account__c != null ){
            if( !whereCondition )
                taxonomyQuery += ' Where GL_Account__c = \'' + taxonomy.GL_Account__c + '\'';
            else
                taxonomyQuery += ' AND GL_Account__c = \'' + taxonomy.GL_Account__c + '\'';
            whereCondition = true;
        }
        
        if( taxonomy.Name != null && taxonomy.Name != '' ){
            if( !whereCondition )
                taxonomyQuery += ' Where Name LIKE \'%' + String.escapeSingleQuotes( taxonomy.Name ) + '%\'';
            else
                taxonomyQuery += ' AND Name LIKE \'%' + String.escapeSingleQuotes( taxonomy.Name ) + '%\'';
            whereCondition = true;
        }
        
        if( valuationClassDesc != null && valuationClassDesc != '' ){
            if( !whereCondition )
                taxonomyQuery += ' Where Valuation_Class__c LIKE \'%' + String.escapeSingleQuotes( valuationClassDesc ) + '%\'';
            else
                taxonomyQuery += ' AND Valuation_Class__c LIKE \'%' + String.escapeSingleQuotes( valuationClassDesc ) + '%\'';
            whereCondition = true;
        }
        
        taxonomyQuery += ' ORDER BY ' + sortExp + ' ' + sortDirection + ' LIMIT 1000';
        
        List<PREP_Material_Service_Group__c> taxonomyList = Database.query( taxonomyQuery );
                
        taxonomyMap = new Map<Id, PREP_Material_Service_Group__c>();
        
        for( PREP_Material_Service_Group__c t : taxonomyList ){
            taxonomyWrapperList.add( new PREPTaxonomyWrapper( t.clone( true, true ) ));
            taxonomyMap.put( t.Id, t );
        }
        
        taxonomyList = new List<PREP_Material_Service_Group__c>();
    }
    
    public PageReference saveChanges() {
        Set<Id> glAccountIds = new Set<Id>();
        Set<Id> valClassIds = new Set<Id>();
        
        List<PREPTaxonomyWrapper> taxWrapper = new List<PREPTaxonomyWrapper>();
        
        for( PREPTaxonomyWrapper w : taxonomyWrapperList ){
            if( w.selected ){ 
                taxWrapper.add( w );
                glAccountIds.add( w.taxonomy.GL_Account__c );
            }        
        }
        
        Map<Id, GL_Account__c> glAccountMap = new Map<Id, GL_Account__c>([ Select Id, Name, GL_Account_Number__c FROM GL_Account__c WHERE Id IN :glAccountIds ]);
        
        if( taxWrapper.size() > 0 ){
            saveTaxonomy( taxWrapper, glAccountMap, 'PREP_Material_Service_Group__c', 'Taxonomy Change' );
        }
    
        PageReference rageRef = Page.Taxonomy_Requests;
        rageRef.setRedirect( true );
        return rageRef;
    }
    
    public PageReference sortRecords(){
        //system.assert( false, sortDirection + ':::' + sortExp );
        searchRecords();
        
        /*if( ascendingOrDescending == 'ASC' ){
            ascendingOrDescending = 'DESC';
        } else {
            ascendingOrDescending = 'ASC';
        }*/
        return null;
    }
    
    public PageReference saveAndSubmitChanges(){
        Set<Id> glAccountIds = new Set<Id>();
        Set<Id> valClassIds = new Set<Id>();
        
        List<PREPTaxonomyWrapper> taxWrapper = new List<PREPTaxonomyWrapper>();
        
        for( PREPTaxonomyWrapper w : taxonomyWrapperList ){
            if( w.selected ){ 
                taxWrapper.add( w );
                glAccountIds.add( w.taxonomy.GL_Account__c );
            }        
        }
        
        Map<Id, GL_Account__c> glAccountMap = new Map<Id, GL_Account__c>([ Select Id, Name, GL_Account_Number__c FROM GL_Account__c WHERE Id IN :glAccountIds ]);
        
        if( taxWrapper.size() > 0 ){
            List<PREP_Change_Header__c> headerList = saveTaxonomy( taxWrapper, glAccountMap, 'PREP_Material_Service_Group__c', 'Taxonomy Change' );
            
            for( PREP_Change_Header__c changeHeader : headerList ){
                Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
                request.setObjectId( changeHeader.Id );
                request.setProcessDefinitionNameOrId( 'PREP_Taxonomy_Change_Approval' );
                
                if( !Test.isRunningTest() )
                    Approval.ProcessResult result = Approval.process(request);
            }
        }

        PageReference rageRef = Page.Taxonomy_Requests;
        rageRef.setRedirect( true );
        return rageRef;
    }
    
    private List<PREP_Change_Header__c> saveTaxonomy( List<PREPTaxonomyWrapper> taxWrapper, Map<Id, GL_Account__c> glAccountMap,
                                                        String objectName, String reasonVal ){
        Boolean glAccountFlag = false;
        Set<Id> taxonomyIds = new Set<Id>();
        
        for( PREPTaxonomyWrapper w : taxWrapper ){
            if( w.taxonomy.GL_Account__c != taxonomyMap.get( w.taxonomy.Id ).GL_Account__c ){
                glAccountFlag = true;
                taxonomyIds.add( w.taxonomy.Id );
            }
        }
        
        List<PREP_Change_Header__c> headerList = new List<PREP_Change_Header__c>();
        PREP_Change_Header__c headerGL;
        Set<Id> taxonomyNotIds = new Set<Id>();
        
        if( taxonomyIds.size() > 0 )
            taxonomyNotIds = cancelPreviousRequests( taxonomyIds );
        
        if( glAccountFlag ){
            headerGL = new PREP_Change_Header__c( Reason__c = reasonVal, Change_Type__c = 'Change GL Account',
                                                    Request_Created_Date__c = Date.today(), Request_Last_Updated_Date__c = Date.today(),
                                                    Request_Title__c= requestTitle, Request_Status__c = 'New', Approval_Status__c = 'New', Change_Status__c = 'New',
                                                    Object_Name__c = objectName );
        
            headerList.add( headerGL );
        }
        
        if( headerList.size() > 0 ){
            insert headerList;
        
            List<PREP_Change_Detail__c> detailList = new List<PREP_Change_Detail__c>();
            for( PREPTaxonomyWrapper w : taxWrapper ){
                if( w.taxonomy.GL_Account__c != taxonomyMap.get( w.taxonomy.Id ).GL_Account__c
                    && !taxonomyNotIds.contains( w.taxonomy.Id )){
                    PREP_Change_Detail__c detailRec = new PREP_Change_Detail__c( PREP_Change_Header__c = headerGL.id, Field_Name__c = 'GL Account',
                                                                                    From_Field_value__c = taxonomyMap.get( w.taxonomy.Id ).GL_Account__r.Name,
                                                                                    Record_Id__c = w.taxonomy.GL_Account__c,
                                                                                    To_Field_value__c = glAccountMap.get( w.taxonomy.GL_Account__c ).Name,
                                                                                    To_Field_Id__c = w.taxonomy.GL_Account__c,
                                                                                    From_Field_Id__c = taxonomyMap.get( w.taxonomy.Id ).GL_Account__c,
                                                                                    Update_Record_Id__c = w.taxonomy.Id, Update_Record_Name__c = w.taxonomy.Name,
                                                                                    Update_Record_Description__c = w.taxonomy.Material_Service_Group_Name__c,
                                                                                    From_Number__c = taxonomyMap.get( w.taxonomy.Id ).GL_Account_Number__c,
                                                                                    To_Number__c = glAccountMap.get( w.taxonomy.GL_Account__c ).GL_Account_Number__c );
                    detailList.add( detailRec );
                }
            }
            
            if( detailList.size() > 0 )    
                insert detailList;
            
            if( detailList.size() == 0 )    
                delete headerList;
        }
        
        return headerList;
    }
    
    private Set<Id> cancelPreviousRequests( Set<Id> taxonomyIds ){
        List<PREP_Change_Header__c> requestToBeCancelled = new List<PREP_Change_Header__c>();
        Set<Id> changeHeaderIds = new Set<Id>();
        Map<Id, Set<Id>> changeHeaderToTaxonomyId = new Map<Id, Set<Id>>();
        Set<Id> taxonomyNotToUpdateIds = new Set<Id>();
        
        for( PREP_Change_Detail__c chng : [ Select Id, PREP_Change_Header__c, Update_Record_Id__c from
                                            PREP_Change_Detail__c where Update_Record_Id__c IN: taxonomyIds
                                            AND ( PREP_Change_Header__r.Change_Status__c = 'New' OR
                                            PREP_Change_Header__r.Change_Status__c = 'Submitted' )
                                            AND PREP_Change_Header__r.Change_Type__c = 'Change GL Account' ]){
            changeHeaderIds.add( chng.PREP_Change_Header__c ); 
            
            Set<Id> taxonomys = new Set<Id>();
            if( changeHeaderToTaxonomyId.containsKey( chng.PREP_Change_Header__c )){
                taxonomys = changeHeaderToTaxonomyId.get( chng.PREP_Change_Header__c );
            }
            
            taxonomys.add( chng.Update_Record_Id__c );
            changeHeaderToTaxonomyId.put( chng.PREP_Change_Header__c, taxonomys );                          
        }
        
        for( Id changeId : changeHeaderIds ){
            requestToBeCancelled.add( new PREP_Change_Header__c( Id = changeId, Change_Status__c = 'Cancelled' ));
        }
        
        if( requestToBeCancelled.size() > 0 ){
            Database.SaveResult[] updateResults = Database.update( requestToBeCancelled, false );
            
            for( Integer i = 0; i < updateResults.size(); i++ ){
                if( !updateResults.get(i).isSuccess() ){
                    if( changeHeaderToTaxonomyId.containsKey( requestToBeCancelled.get( i ).Id )){
                        for( Id taxId : changeHeaderToTaxonomyId.get( requestToBeCancelled.get( i ).Id )){
                            taxonomyNotToUpdateIds.add( taxId );
                        }
                    }
                }            
            }
        }
        
        return taxonomyNotToUpdateIds;
    }
    
    public class PREPTaxonomyWrapper {
        public Boolean selected { get; set; }
        public PREP_Material_Service_Group__c taxonomy { get; set; }
        
        public PREPTaxonomyWrapper( PREP_Material_Service_Group__c taxonomy ){
            this.taxonomy = taxonomy;
            this.selected = false;
        }
    }
}