public with sharing class PREPChangeMaterialServiceNameController {
    public PREP_Initiative__c preInitiative { get; set; }
    public PREP_Material_Service_Group__c taxonomy { get; set; }
    public List<PREPTaxonomyWrapper> taxonomyWrapperList { get; set; }
    public String requestTitle { get; set; }
    
    Map<Id, PREP_Material_Service_Group__c> taxonomyMap;
    
    public PREPChangeMaterialServiceNameController() {
        preInitiative = new PREP_Initiative__c();
        taxonomy = new PREP_Material_Service_Group__c();
        taxonomyWrapperList = new List<PREPTaxonomyWrapper>();
    }
    
    public void searchRecords() {
        Boolean whereCondition = false;
        taxonomyWrapperList = new List<PREPTaxonomyWrapper>();

        String taxonomyQuery = 'SELECT Id, Name, Owner.Name, Material_Service_Group_Name__c, Category__c, Discipline__c, SAP_Short_Text_Name__c, '
                                + 'Type__c, Includes__c, Category_Manager__r.Name, Sub_Category__r.Category__r.Name, Category_Specialist__r.Name, '
                                + 'Sub_Category__r.Name, Category_Link__c, Category_Link__r.Name, Discipline_Link__c, Discipline_Link__r.Name, '
                                + 'Sub_Category__c FROM PREP_Material_Service_Group__c';                       
        
        if( preInitiative.Category__c != null ){
            if( !whereCondition ){
                taxonomyQuery += ' Where Category_Link__c = \'' + preInitiative.Category__c + '\'';
            } else {
                taxonomyQuery += ' AND Category_Link__c = \'' + preInitiative.Category__c + '\'';
            }
            whereCondition = true;
        }
        
        if( taxonomy.Sub_Category__c != null ){
            if( !whereCondition ){
                taxonomyQuery += ' Where Sub_Category__c = \'' + taxonomy.Sub_Category__c + '\'';
            } else {
                taxonomyQuery += ' AND Sub_Category__c = \'' + taxonomy.Sub_Category__c + '\''; 
            }
            whereCondition = true;
        }
        
        if( preInitiative.Discipline__c != null ){
            if( !whereCondition ){
                taxonomyQuery += ' Where Discipline_Link__c = \'' + preInitiative.Discipline__c + '\'';
            } else {
                taxonomyQuery += ' AND Discipline_Link__c = \'' + preInitiative.Discipline__c + '\''; 
            }
            whereCondition = true;
        }
        
        if( preInitiative.Category_Manager__c != null ){
            if( !whereCondition ){
                taxonomyQuery += ' Where Category_Manager__c = \'' + preInitiative.Category_Manager__c + '\'';
            } else {
                taxonomyQuery += ' AND Category_Manager__c = \'' + preInitiative.Category_Manager__c + '\''; 
            }
            whereCondition = true;
        }
        
        if( preInitiative.Category_Specialist__c != null ){
            if( !whereCondition ){
                taxonomyQuery += ' Where Category_Specialist__c = \'' + preInitiative.Category_Specialist__c + '\'';
            } else {
                taxonomyQuery += ' AND Category_Specialist__c = \'' + preInitiative.Category_Specialist__c + '\'';
            }
            whereCondition = true;
        }
        
        if( taxonomy.Material_Service_Group_Name__c != null && taxonomy.Material_Service_Group_Name__c != '' ){
            if( !whereCondition )
                taxonomyQuery += ' Where Material_Service_Group_Name__c LIKE \'%' + String.escapeSingleQuotes( taxonomy.Material_Service_Group_Name__c ) + '%\'';
            else
                taxonomyQuery += ' AND Material_Service_Group_Name__c LIKE \'%' + String.escapeSingleQuotes( taxonomy.Material_Service_Group_Name__c ) + '%\'';
            whereCondition = true;
        }
        
        if( taxonomy.Name != null && taxonomy.Name != '' ){
            if( !whereCondition )
                taxonomyQuery += ' Where Name LIKE \'%' + String.escapeSingleQuotes( taxonomy.Name ) + '%\'';
            else
                taxonomyQuery += ' AND Name LIKE \'%' + String.escapeSingleQuotes( taxonomy.Name ) + '%\'';
            whereCondition = true;
        }
        
        if( taxonomy.SAP_Short_Text_Name__c != null && taxonomy.SAP_Short_Text_Name__c != '' ){
            if( !whereCondition )
                taxonomyQuery += ' Where SAP_Short_Text_Name__c LIKE \'%' + String.escapeSingleQuotes( taxonomy.SAP_Short_Text_Name__c ) + '%\'';
            else
                taxonomyQuery += ' AND SAP_Short_Text_Name__c LIKE \'%' + String.escapeSingleQuotes( taxonomy.SAP_Short_Text_Name__c ) + '%\'';
            whereCondition = true;
        }
        
        List<PREP_Material_Service_Group__c> taxonomyList = Database.query( taxonomyQuery );
        
        taxonomyMap = new Map<Id, PREP_Material_Service_Group__c>();
        
        for( PREP_Material_Service_Group__c t : taxonomyList ){
            taxonomyWrapperList.add( new PREPTaxonomyWrapper( t.clone( true, true ) ));
            taxonomyMap.put( t.Id, t );
        }
    }
    
    public PageReference saveChanges() {
        List<PREPTaxonomyWrapper> taxWrapper = new List<PREPTaxonomyWrapper>();
        
        for( PREPTaxonomyWrapper w : taxonomyWrapperList ){
            if( w.selected ){ 
                taxWrapper.add( w );
            }        
        }
        
        if( taxWrapper.size() > 0 ){
            saveTaxonomy( taxWrapper, 'PREP_Material_Service_Group__c', 'Taxonomy Change' );
        }
    
        PageReference rageRef = Page.Taxonomy_Requests;
        rageRef.setRedirect( true );
        return rageRef;
    }
    
    public PageReference saveAndSubmitChanges(){
        List<PREPTaxonomyWrapper> taxWrapper = new List<PREPTaxonomyWrapper>();
        
        for( PREPTaxonomyWrapper w : taxonomyWrapperList ){
            if( w.selected ){ 
                taxWrapper.add( w );
            }        
        }
        
        if( taxWrapper.size() > 0 ){
            List<PREP_Change_Header__c> headerList = saveTaxonomy( taxWrapper, 'PREP_Material_Service_Group__c', 'Taxonomy Change' );
            
            for( PREP_Change_Header__c changeHeader : headerList ){
                Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
                request.setObjectId( changeHeader.Id );
                request.setProcessDefinitionNameOrId( 'PREP_Taxonomy_Change_Approval' );
                
                if( !Test.isRunningTest() )
                    Approval.ProcessResult result = Approval.process(request);
            }
        }

        PageReference rageRef = Page.Taxonomy_Requests;
        rageRef.setRedirect( true );
        return rageRef;
    }
    
    private List<PREP_Change_Header__c> saveTaxonomy( List<PREPTaxonomyWrapper> taxWrapper, String objectName, String reasonVal ){
        Boolean shortNameFlag = false;
        Boolean longNameFlag = false;
        
        for( PREPTaxonomyWrapper w : taxWrapper ){
            if( w.taxonomy.SAP_Short_Text_Name__c.toUpperCase() != taxonomyMap.get( w.taxonomy.Id ).SAP_Short_Text_Name__c ){
                shortNameFlag = true;
            }
            
            if( w.taxonomy.Material_Service_Group_Name__c.toUpperCase() != taxonomyMap.get( w.taxonomy.Id ).Material_Service_Group_Name__c ){
                longNameFlag = true;
            }
        }
        
        List<PREP_Change_Header__c> headerList = new List<PREP_Change_Header__c>();
        PREP_Change_Header__c headerShort;
        PREP_Change_Header__c headerLong;
        
        if( shortNameFlag ){
            headerShort = new PREP_Change_Header__c( Reason__c = reasonVal, Change_Type__c = 'Change Short Name',
                                                    Request_Created_Date__c = Date.today(), Request_Last_Updated_Date__c = Date.today(),
                                                    Request_Title__c= requestTitle, Request_Status__c = 'New', Approval_Status__c = 'New', Change_Status__c = 'New',
                                                    Object_Name__c = objectName );
        
            headerList.add( headerShort );
        }
        
        if( longNameFlag ){
            headerLong = new PREP_Change_Header__c( Reason__c = reasonVal, Change_Type__c = 'Change Long Name',
                                                        Request_Created_Date__c = Date.today(), Request_Last_Updated_Date__c = Date.today(),
                                                        Request_Title__c= requestTitle, Request_Status__c = 'New', Approval_Status__c = 'New',
                                                        Change_Status__c = 'New', Object_Name__c = objectName );
            headerList.add( headerLong );
        }
        
        if( headerList.size() > 0 ){
            insert headerList;
        
            List<PREP_Change_Detail__c> detailList = new List<PREP_Change_Detail__c>();
            for( PREPTaxonomyWrapper w : taxWrapper ){
                if( w.taxonomy.SAP_Short_Text_Name__c != taxonomyMap.get( w.taxonomy.Id ).SAP_Short_Text_Name__c ){
                    PREP_Change_Detail__c detailRec = new PREP_Change_Detail__c( PREP_Change_Header__c = headerShort.id, Field_Name__c = 'Short Text',
                                                                                    From_Field_value__c = taxonomyMap.get( w.taxonomy.Id ).SAP_Short_Text_Name__c,
                                                                                    Record_Id__c = w.taxonomy.Id,
                                                                                    To_Field_value__c = w.taxonomy.SAP_Short_Text_Name__c.toUpperCase(),
                                                                                    To_Field_Id__c = w.taxonomy.SAP_Short_Text_Name__c,
                                                                                    From_Field_Id__c = taxonomyMap.get( w.taxonomy.Id ).SAP_Short_Text_Name__c,
                                                                                    Update_Record_Id__c = w.taxonomy.Id, Update_Record_Name__c = w.taxonomy.Name );
                    detailList.add( detailRec );
                }
                
                if( w.taxonomy.Material_Service_Group_Name__c != taxonomyMap.get( w.taxonomy.Id ).Material_Service_Group_Name__c ){
                    PREP_Change_Detail__c detailRec = new PREP_Change_Detail__c( PREP_Change_Header__c = headerLong.id, Field_Name__c = 'Long Text',
                                                                                    From_Field_value__c = taxonomyMap.get( w.taxonomy.Id ).Material_Service_Group_Name__c,
                                                                                    Record_Id__c = w.taxonomy.Id,
                                                                                    To_Field_value__c = w.taxonomy.Material_Service_Group_Name__c,
                                                                                    To_Field_Id__c = w.taxonomy.Material_Service_Group_Name__c,
                                                                                    From_Field_Id__c = taxonomyMap.get( w.taxonomy.Id ).Material_Service_Group_Name__c,
                                                                                    Update_Record_Id__c = w.taxonomy.Id, Update_Record_Name__c = w.taxonomy.Name );
                    detailList.add( detailRec );
                }
            }
            
            if( detailList.size() > 0 )    
                insert detailList;  
        }
        return headerList;
    }
    
    public class PREPTaxonomyWrapper {
        public Boolean selected { get; set; }
        public PREP_Material_Service_Group__c taxonomy { get; set; }
        
        public PREPTaxonomyWrapper( PREP_Material_Service_Group__c taxonomy ){
            this.taxonomy = taxonomy;
            this.selected = false;
        }
    }
}