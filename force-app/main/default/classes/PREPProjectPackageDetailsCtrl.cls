public with sharing class PREPProjectPackageDetailsCtrl {
	public String initID {get;set;}
	public String packageType {get;set;}
	public String projectName {get;set;}
	List<String> msgbaColumnHeaders;
	List<String> msgbaFieldsName;
	List<String> msguColumnHeaders ;
	List<String> msguFieldsName;
	List<String> unitHeaders;
	List<String> plannedFieldsName;
	List<String> forecastFieldsName;
	List<String> actualFieldsName;
	List<String> statusFieldsName;
	//key = PREP_Mat_Serv_Group_Baseline_Allocation__c.Id
	public List<PREP_Mat_Serv_Group_Update__c> packages {get;set;}
	
	public PREPProjectPackageDetailsCtrl()
	{
		initID = 'PREP-';
		packageType = 'Mat';
	//	runSearch();
	}
	public List<String> getMSGBAColumnHeaders()
	{
		if(msgbaColumnHeaders == null)
		{
			
			if (packageType == 'Mat')
				msgbaColumnHeaders = new List<String>{'M/S Group', 'Equipment/Service', 'Planned Spend<br/>Allocation ($)', 
											'Actual Spend<br/>Allocation ($)', 'AFE No.', 'Tag No.', 'Requisition No./<br/>CWP No.',
											'PO No.', 'Bid No./Ariba <br/>Document Number', 'Outline<br/>Agreement No.', 
											'MSA No./ESA No.', 'Husky Proc/<br/>Comm Rep', 'Husky Expeditor', 'External Rep',
											'External Expeditor', 'Market Approach', 'Compensation Mechanism', 'Supplier/Contractor', 'Acquisition<br/> Cycle Type'};
			else if (packageType == 'Serv')
				msgbaColumnHeaders = new List<String>{'M/S Group', 'Equipment/Service', 'Planned Spend<br/>Allocation ($)', 
											'Actual Spend<br/>Allocation ($)', 'AFE No.', 'Requisition No./<br/>CWP No.',
											'PO No.', 'Bid No./Ariba <br/>Document Number', 'Outline<br/>Agreement No.', 
											'MSA No./ESA No.', 'Husky Proc/<br/>Comm Rep', 'Husky Expeditor', 'External Rep',
											'External Expeditor', 'Market Approach', 'Compensation Mechanism', 'Supplier/Contractor', 'Acquisition<br/> Cycle Type'};
		}
		return msgbaColumnHeaders;
	}
	public List<String> getMSGBAFieldsName()
	{
		if (msgbaFieldsName == null)
		{
			if (packageType == 'Mat')
				msgbaFieldsName = new List<String>{'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Material_Service_Group__r.Name', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Equipment_Service__c', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Planned_Spend_Allocation_Dollar__c',
										'Actual_Spend_Allocation_Dollar__c',
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.AFE_No__c', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Tag_No__c', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.CWP_No__c', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Requisition_No__c', 
										'Bid_No__c/Ariba_Document_Number__c',
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Outline_Agreement_No__c', 
										'MSA', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Husky_Proc_Comm_Rep__r.Name', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Husky_Expeditor__c',
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.External_Rep__c', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.External_Expeditor__c', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Market_Approach__c', 
										'Compensation_Mechanism__c',
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Supplier_Contractor__c',
										'Mat_Serv_Group_Planned_Schedule_Id__r.Acquisition_Cycle_Type__c'};
			else if (packageType == 'Serv')
				msgbaFieldsName = new List<String>{'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Material_Service_Group__r.Name', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Equipment_Service__c',
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Planned_Spend_Allocation_Dollar__c',
										'Actual_Spend_Allocation_Dollar__c', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.AFE_No__c', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.CWP_No__c', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Requisition_No__c', 
										'Bid_No__c/Ariba_Document_Number__c',
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Outline_Agreement_No__c', 
										'MSA', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Husky_Proc_Comm_Rep__r.Name', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Husky_Expeditor__c',
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.External_Rep__c', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.External_Expeditor__c', 
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Market_Approach__c', 
										'Compensation_Mechanism__c',
										'Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Supplier_Contractor__c',
										'Mat_Serv_Group_Planned_Schedule_Id__r.Acquisition_Cycle_Type__c'};			
		}
		return msgbaFieldsName;
	}
	public List<String> getMSGUColumnHeaders()
	{
		if (msguColumnHeaders == null)
		{ 
			System.debug(packageType);
			if (packageType == 'Mat')
				msguColumnHeaders = new List<String>{'1. Segmentation<br/>& Team Selection', 'Business<br/>Requirements',
											'2. Business<br/>Requirements', 'Supplier<br/>& Market<br/>Analysis',
											'3. Supplier<br/>& Market<br/>Analysis', 'Sourcing<br/>Options',
											'4. Sourcing<br/>Options', 'Strategy<br/>Approval',
											'Strategy<br/>Approval', 'MRQ<br/>To Be<br/>Received',
											'MRQ<br/>Received', 'RFQ<br/>Prep',
											'Bid Request<br/>Issued (RFQ)', 'Bid<br/>Period',
											'Bids<br/>Received', 'Bid<br/>Evaluation<br/>1',
											'Commercial<br/>Evaluations<br/>Received', 'Bid<br/>Evaluation<br/>2',
											'Technical<br/>Evaluations<br/>Received', 'Bid<br/>Evaluation<br/>3', 
											'Bid Tab<br/>Finalized', 'Bid<br/>Evaluation<br/>4',
											'Decision<br/>Sum. Issued<br/>Approval', 'Husky<br/>Approval',
											'Decision<br/>Summary<br/>Approved', 'MRP<br/>Prep',
											'MRP<br/>Received', 'PO<br/>Prep/Issued',
											'P.O.<br/>Issued', 'Critical<br/>Vendor<br>Data<br/>Receipt1',
											'Vendor<br/>Initial<br/>Drawing<br/>Submittal', 'Critical<br/>Vendor<br/>Data<br/>Receipt2',
											'Drawings<br/>Approved', 'MFG.<br/>Lead<br/>Time',
											'Ship Date', 'Shipping<br/>Duration',
											'ETA', 'Float', 'R.A.S. Date', 'Delivery<br/>Variance<br/>Mat', 'Status<br/>Delivery<br/>Variance<br/>(Days) Mat',
											'Delays Type', 'Delay Reason', 'Delays Comments'};	
			else if (packageType == 'Serv')
				msguColumnHeaders = new List<String>{'1. Segmentation &<br/>Team Selection', 'Business<br/>Requirements',
											'2. Business<br/>Requirements', 'Supplier<br/>& Market<br/>Analysis',
											'3. Supplier<br/>& Market<br/>Analysis', 'Sourcing<br/>Options',
											'4. Sourcing<br/>Options', 'Strategy<br/>Approval',
											'Strategy<br/>Approval', 'CWP<br/>To Be<br/>Received',
											'CWP<br/>Received', 'RFQ<br/>Prep',
											'RFQ<br/>Issued', 'Bid<br/>Period',
											'Bids<br/>Received', 'Bid<br/>Evaluation<br/>1',
											'Commercial<br/>Evaluations<br/>Received', 'Bid<br/>Evaluation<br/>2',
											'Technical<br/>Evaluations<br/>Received', 'Bid<br/>Evaluation<br/>3', 
											'Bid<br/>Tab<br/>Finalized', 'Bid<br/>Evaluation<br/>4',
											'Decision<br/>Sum. Issued<br/>Approval', 'Husky<br/>Approval',
											'Decision<br/>Summary<br/>Approved', 'CWP<br/>Prep',
											'Final CWP<br/>Received', 'CT<br/>Prep/Issued',
											'Contract<br/>Executed/<br/>LOA Effective', 'Kickoff/<br/>Mobilization',
											'Mobilization &<br/>Construction<br/>Start', 'Duration<br/>of Work',
											'Construction<br/>Completion', 'Demob',
											'Demob', 'Float',
											'CWP<br/>Completion', 'Delivery<br/>Variance<br/>Serv', 'Status<br/>Delivery<br/>Variance<br/>(Days) Serv',
											'Delays Type', 'Delay Reason', 'Delays Comments'};	
											
		}
		return msguColumnHeaders;
	}
	public List<String> getMSGUFieldsName()
	{
		if(msguFieldsName == null)
		{
			if (packageType == 'Mat')
				msguFieldsName = new List<String>{'Mat_Serv_Group_Planned_Schedule_Id__r.Acquisition_Cycle__r.Float__c', 'R_A_S_Date__c',
											'Delivery_Variance_Days_Mat__c', 'Status_Delivery_Variance_Days_Mat__c',
											'Award_Delays_Type__c', 'Award_Delay_Reason__c', 'Award_Delays_Comments__c'};
			else if (packageType == 'Serv')	
				msguFieldsName = new List<String>{'Mat_Serv_Group_Planned_Schedule_Id__r.Acquisition_Cycle__r.Float__c', 'CWP_Completion__c',
											'Delivery_Variance_Days_Serv__c', 'Status_Delivery_Variance_Days_Serv__c',
											'Award_Delays_Type__c', 'Award_Delay_Reason__c', 'Award_Delays_Comments__c'};
		}
		return msguFieldsName;
	}
	public List<String> getUnitHeaders()
	{
		if (unitHeaders == null)
		{
			if (packageType == 'Mat')
				unitHeaders = new List<String>{'','D','','D','','D','','D','','D','','D','','D','','D','','D','','D',
											   '','D','','D','','D','','D','','W','','W','','W','','D','','D','','D', '', '','',''};
			else if (packageType == 'Serv')
				unitHeaders = new List<String>{'','D','','D','','D','','D','','D','','D','','D','','D','','D','','D',
											'','D','','D','','D','','D','','D','','W','','D','','D', '', 'D', '', '','', ''};							
			system.debug('packageType = ' + packageType + '  getUnitHeader = ' + unitHeaders);
		}
		return unitHeaders;
	}
	public List<String> getPlannedFieldsName()
	{
		if (plannedFieldsName == null)
		{
			if (packageType == 'Mat')
				plannedFieldsName = new List<String>{'Planned_1_Segmentation_Team_Selection__c', 'Planned_Business_Requirements__c',
											'Planned_2_Business_Requirements__c', 'Planned_Supplier_Market_Analysis__c', 
											'Planned_3_Supplier_Market_Analysis__c', 'Planned_Sourcing_Options__c',
											'Planned_4_Sourcing_Options__c', 'Planned_Strategy_Approval_Cycle__c',
											'Planned_Strategy_Approval__c', 'Planned_MRQ_To_Be_Received__c',
											'Planned_MRQ_Received__c', 'Planned_RFQ_Prep__c',
											'Planned_Bid_Request_Issued_RFQ__c', 'Planned_Bid_Period__c',
											'Planned_Bids_Received__c', 'Planned_Bid_Evaluation_1__c',
											'Planned_Commercial_Evaluations_Received__c', 'Planned_Bid_Evaluation_2__c',
											'Planned_Technical_Evaluations_Received__c', 'Planned_Bid_Evaluation_3__c',
											'Planned_Bid_Tab_Finalized__c', 'Planned_Bid_Evaluation_4__c',
											'Planned_Decision_Sum_Issued_Approval__c', 'Planned_Husky_Approval__c',
											'Planned_Decision_Summary_Approved__c', 'Planned_MRP_Prep__c',
											'Planned_MRP_Received__c', 'Planned_PO_Prep_Issued__c',
											'Planned_P_O_Issued__c', 'Planned_Critical_Vendor_Data_Receipt1_W__c',
											'Planned_Vendor_Init_Drawing_Submittal__c', 'Planned_Critical_Vendor_Data_Receipt2_W__c',
											'Planned_Drawings_Approved__c', 'Planned_MFG_Lead_Time_Weeks__c',
											'Planned_Ship_Date__c', 'Planned_Shipping_Duration__c', 'Planned_ETA__c'};
			else if (packageType == 'Serv')
			{
				plannedFieldsName = new List<String>{'Planned_1_Segmentation_Team_Selection__c', 'Planned_Business_Requirements__c',
										'Planned_2_Business_Requirements__c', 'Planned_Supplier_Market_Analysis__c', 
										'Planned_3_Supplier_Market_Analysis__c', 'Planned_Sourcing_Options__c',
										'Planned_4_Sourcing_Options__c', 'Planned_Strategy_Approval_Cycle__c',
										'Planned_Strategy_Approval__c', 'Planned_CWP_To_Be_Received__c',
										
										'Planned_CWP_Received__c', 'Planned_RFQ_Prep__c',
										'Planned_RFQ_Issued__c', 'Planned_Bid_Period__c',
										'Planned_Bids_Received__c', 'Planned_Bid_Evaluation_1__c',
										'Planned_Commercial_Evaluations_Received__c', 'Planned_Bid_Evaluation_2__c',
										'Planned_Technical_Evaluations_Received__c', 'Planned_Bid_Evaluation_3__c',
										'Planned_Bid_Tab_Finalized__c', 'Planned_Bid_Evaluation_4__c',
										'Planned_Decision_Sum_Issued_Approval__c', 'Planned_Husky_Approval__c',
										'Planned_Decision_Summary_Approved__c', 'Planned_CWP_Prep__c',
										'Planned_Final_CWP_Received__c', 'Planned_CT_Prep_Issued__c',
										'Planned_Contract_Executed_LOA_Effective__c', 'Planned_Kickoff_Mobilization__c',
										'Planned_Mobilization_Construction_Start__c', 'Planned_Duration_Of_Work_Weeks__c',
										'Planned_Construction_Completion__c', 'Planned_Demob_Cycle__c', 'Planned_Demob__c'};
				
			}								
		}
		return plannedFieldsName;
	} 
	public List<String> getForecastFieldsName()
	{
		if (forecastFieldsName == null)
		{
			if (packageType == 'Mat')
				forecastFieldsName = new List<String>{'Forecast_1_Segmentation_Team_Selection__c', 'Forecast_Business_Requirements__c',
											'Forecast_2_Business_Requirements__c', 'Forecast_Supplier_Market_Analysis__c', 
											'Forecast_3_Supplier_Market_Analysis__c', 'Forecast_Sourcing_Options__c',
											'Forecast_4_Sourcing_Options__c', 'Forecast_Strategy_Approval_Cycle__c',
											'Forecast_Strategy_Approval__c', 'Forecast_MRQ_To_Be_Received__c',
											'Forecast_MRQ_Received__c', 'Forecast_RFQ_Prep__c',
											'Forecast_Bid_Request_Issued_RFQ__c', 'Forecast_Bid_Period__c',
											'Forecast_Bids_Received_Mat__c', 'Forecast_Bid_Evaluation_1__c',
											'Forecast_Comm_Eval_Received_Mat__c', 'Forecast_Bid_Evaluation_2__c',
											'Forecast_Tech_Eval_Received_Mat__c', 'Forecast_Bid_Evaluation_3__c',
											'Forecast_Bid_Tab_Finalized_Mat__c', 'Forecast_Bid_Evaluation_4__c',
											'Forecast_Decision_Sum_Issued_Approval_M__c', 'Forecast_Husky_Approval__c',
											'Forecast_Decision_Summary_Approved_Mat__c', 'Forecast_MRP_Prep__c',
											'Forecast_MRQ_Received__c', 'Forecast_PO_Prep_Issued__c',
											'Forecast_P_O_Issued__c', 'Forecast_Critical_Vendor_Data_Receipt1_W__c',
											'Forecast_Vendor_Init_Drawing_Submittal__c', 'Forecast_Critical_Vendor_Data_Receipt2_W__c',
											'Forecast_Drawings_Approved__c', 'Forecast_MFG_Lead_Time_Weeks__c',
											'Forecast_Ship_Date__c', 'Forecast_Shipping_Duration__c', 'Forecast_ETA__c'};
				
			else if (packageType == 'Serv')
			{
				forecastFieldsName = new List<String>{'Forecast_1_Segmentation_Team_Selection__c', 'Forecast_Business_Requirements__c',
											'Forecast_2_Business_Requirements__c', 'Forecast_Supplier_Market_Analysis__c', 
											'Forecast_3_Supplier_Market_Analysis__c', 'Forecast_Sourcing_Options__c',
											'Forecast_4_Sourcing_Options__c', 'Forecast_Strategy_Approval_Cycle__c',
											'Forecast_Strategy_Approval__c', 'Forecast_CWP_To_Be_Received__c',
											'Forecast_CWP_Received__c', 'Forecast_RFQ_Prep__c',
											'Forecast_RFQ_Issued__c', 'Forecast_Bid_Period__c',
											'Forecast_Bids_Received_Serv__c', 'Forecast_Bid_Evaluation_1__c',
											'Forecast_Comm_Eval_Received_Serv__c', 'Forecast_Bid_Evaluation_2__c',
											'Forecast_Tech_Eval_Received_Serv__c', 'Forecast_Bid_Evaluation_3__c',
											'Forecast_Bid_Tab_Finalized_Serv__c', 'Forecast_Bid_Evaluation_4__c',
											'Forecast_Decision_Sum_Issued_Approval_S__c', 'Forecast_Husky_Approval__c',
											'Forecast_Decision_Summary_Approved_Serv__c', 'Forecast_CWP_Prep__c',
											'Forecast_Final_CWP_Received__c', 'Forecast_CT_Prep_Issued__c',
											'Forecast_Contract_Executed_LOA_Effective__c', 'Forecast_Kickoff_Mobilization__c',
											'Forecast_Mobilization_Construction_Start__c', 'Forecast_Duration_Of_Work_Weeks__c',
											'Forecast_Construction_Completion__c', 'Forecast_Demob_Cycle__c', 'Forecast_Demob__c'};
			}
		}
		return forecastFieldsName;
	}
	public List<String> getActualFieldsName()
	{
		if (actualFieldsName == null)
		{
			if (packageType == 'Mat')
				actualFieldsName = new List<String>{'Actual_1_Segmentation_Team_Selection__c', 'Actual_Business_Requirements__c',
											'Actual_2_Business_Requirements__c', 'Actual_Supplier_Market_Analysis__c', 
											'Actual_3_Supplier_Market_Analysis__c', 'Actual_Sourcing_Options__c',
											'Actual_4_Sourcing_Options__c', 'Actual_Strategy_Approval_Cycle__c',
											'Actual_Strategy_Approval__c', 'Actual_MRQ_To_Be_Received__c',
											'Actual_MRQ_Received__c', 'Actual_RFQ_Prep_Mat__c',
											'Actual_Bid_Request_Issued_RFQ__c', 'Actual_Bid_Period_Mat__c',
											'Actual_Bids_Received__c', 'Actual_Bid_Evaluation_1__c',
											'Actual_Comm_Eval_Received__c', 'Actual_Bid_Evaluation_2__c',
											'Actual_Tech_Eval_Received__c', 'Actual_Bid_Evaluation_3__c',
											'Actual_Bid_Tab_Finalized__c', 'Actual_Bid_Evaluation_4__c',
											'Actual_Decision_Sum_Issued_Approval__c', 'Actual_Husky_Approval__c',
											'Actual_Decision_Summary_Approved__c', 'Actual_MRP_Prep__c',
											'Actual_MRP_Received__c', 'Actual_PO_Prep_Issued__c',
											'Actual_P_O_Issued__c', 'Actual_Critical_Vendor_Data_Receipt1_W__c',
											'Actual_Vendor_Init_Drawing_Submittal__c', 'Actual_Critical_Vendor_Data_Receipt2_W__c',
											'Actual_Drawings_Approved__c', 'Actual_MFG_Lead_Time_Weeks__c',
											'Actual_Ship_Date__c', 'Actual_Shipping_Duration__c', 'Actual_ETA__c'};
				
			else if (packageType == 'Serv')
			{
				actualFieldsName = new List<String>{'Actual_1_Segmentation_Team_Selection__c', 'Actual_Business_Requirements__c',
											'Actual_2_Business_Requirements__c', 'Actual_Supplier_Market_Analysis__c', 
											'Actual_3_Supplier_Market_Analysis__c', 'Actual_Sourcing_Options__c',
											'Actual_4_Sourcing_Options__c', 'Actual_Strategy_Approval_Cycle__c',
											'Actual_Strategy_Approval__c', 'Actual_CWP_To_Be_Received__c',
											'Actual_CWP_Received__c', 'Actual_RFQ_Prep_Serv__c',
											'Actual_RFQ_Issued__c', 'Actual_Bid_Period_Serv__c',
											'Actual_Bids_Received__c', 'Actual_Bid_Evaluation_1__c',
											'Actual_Comm_Eval_Received__c', 'Actual_Bid_Evaluation_2__c',
											'Actual_Tech_Eval_Received__c', 'Actual_Bid_Evaluation_3__c',
											'Actual_Bid_Tab_Finalized__c', 'Actual_Bid_Evaluation_4__c',
											'Actual_Decision_Sum_Issued_Approval__c', 'Actual_Husky_Approval__c',
											'Actual_Decision_Summary_Approved__c', 'Actual_CWP_Prep__c',
											'Actual_Final_CWP_Received__c', 'Actual_CT_Prep_Issued__c', 
											'Actual_Contract_Executed_LOA_Effective__c', 'Actual_Kickoff_Mobilization__c',
											'Actual_Mobilization_Construction_Start__c', 'Actual_Duration_Of_Work_Weeks__c',
											'Actual_Construction_Completion__c', 'Actual_Demob_Cycle__c', 'Actual_Demob__c'};
			}
		}
		return actualFieldsName;
	}
	public List<String> getStatusFieldsName()
	{
		if (statusFieldsName == null)
		{
			if (packageType == 'Mat')
				statusFieldsName = new List<String>{'Status_1_Segmentation_Team_Selection__c', 'Status_Business_Requirements__c',
											'Status_2_Business_Requirements__c', 'Status_Supplier_Market_Analysis__c', 
											'Status_3_Supplier_Market_Analysis__c', 'Status_Sourcing_Options__c',
											'Status_4_Sourcing_Options__c', 'Status_Strategy_Approval_Cycle__c',
											'Status_Strategy_Approval__c', 'Status_MRQ_To_Be_Received__c',
											'Status_MRQ_Received__c', 'Status_RFQ_Prep_Mat__c',
											'Status_Bid_Request_Issued_RFQ__c', 'Status_Bid_Period_Mat__c',
											'Status_Bids_Received__c', 'Status_Bid_Evaluation_1__c',
											'Status_Commercial_Evaluations_Received__c', 'Status_Bid_Evaluation_2__c',
											'Status_Technical_Evaluations_Received__c', 'Status_Bid_Evaluation_3__c',
											'Status_Bid_Tab_Finalized__c', 'Status_Bid_Evaluation_4__c',
											'Status_Decision_Sum_Issued_Approval__c', 'Status_Husky_Approval__c',
											'Status_Decision_Summary_Approved__c', 'Status_MRP_Prep__c',
											'Status_MRP_Received__c', 'Status_PO_Prep_Issued__c',
											'Status_P_O_Issued__c', 'Status_Critical_Vendor_Data_Receipt1_W__c',
											'Status_Vendor_Init_Drawing_Submittal__c', 'Status_Critical_Vendor_Data_Receipt2_W__c',
											'Status_Drawings_Approved__c', 'Status_MFG_Lead_Time_Weeks__c',
											'Status_Ship_Date__c', 'Status_Shipping_Duration__c', 'Status_ETA__c'};
				
			else if (packageType == 'Serv')
			{
				statusFieldsName = new List<String>{'Status_1_Segmentation_Team_Selection__c', 'Status_Business_Requirements__c',
											'Status_2_Business_Requirements__c', 'Status_Supplier_Market_Analysis__c', 
											'Status_3_Supplier_Market_Analysis__c', 'Status_Sourcing_Options__c',
											'Status_4_Sourcing_Options__c', 'Status_Strategy_Approval_Cycle__c',
											'Status_Strategy_Approval__c', 'Status_CWP_To_Be_Received__c',
											'Status_CWP_Received__c', 'Status_RFQ_Prep_Serv__c',
											'Status_RFQ_Issued__c', 'Status_Bid_Period_Serv__c',
											'Status_Bids_Received__c', 'Status_Bid_Evaluation_1__c',
											'Status_Commercial_Evaluations_Received__c', 'Status_Bid_Evaluation_2__c',
											'Status_Technical_Evaluations_Received__c', 'Status_Bid_Evaluation_3__c',
											'Status_Bid_Tab_Finalized__c', 'Status_Bid_Evaluation_4__c',
											'Status_Decision_Sum_Issued_Approval__c', 'Status_Husky_Approval__c',
											'Status_Decision_Summary_Approved__c', 'Status_CWP_Prep__c',
											'Status_Final_CWP_Received__c', 'Status_CT_Prep_Issued__c',
											'Status_Contract_Executed_LOA_Effective__c', 'Status_Kickoff_Mobilization__c',
											'Status_Mobilization_Construction_Start__c', 'Status_Duration_Of_Work_Weeks__c',
											'Status_Construction_Completion__c', 'Status_Demob_Cycle__c', 'Status_Demob__c'};
			}
		}
		return statusFieldsName;
	}
	public PageReference runSearch()
	{
		msgbaColumnHeaders = null;
		msgbaFieldsName = null;
		msguColumnHeaders = null;
		msguFieldsName = null;
		unitHeaders = null;
		plannedFieldsName = null;
		forecastFieldsName = null;
		actualFieldsName = null;
		statusFieldsName = null;
	
	
		String sPackageType = '%' + packageType + '%';
		
		packages = new List<PREP_Mat_Serv_Group_Update__c>();
		Map<Id, PREP_Mat_Serv_Group_Update__c> dataMap = new Map<Id, PREP_Mat_Serv_Group_Update__c>();

		for(PREP_Mat_Serv_Group_Update__c msgu : [select Actual_1_Segmentation_Team_Selection__c, Actual_2_Business_Requirements__c, 
				Actual_3_Supplier_Market_Analysis__c, Actual_4_Sourcing_Options__c, Actual_Bid_Evaluation_1__c, 
				Actual_Bid_Evaluation_2__c, Actual_Bid_Evaluation_3__c, Actual_Bid_Evaluation_4__c, Actual_Bid_Period_Mat__c, 
				Actual_Bid_Period_Serv__c, Actual_Bid_Request_Issued_RFQ__c, Actual_Bid_Tab_Finalized__c, Actual_Bids_Received__c, 
				Actual_Business_Requirements__c, Actual_Comm_Eval_Received__c, Actual_Construction_Completion__c, 
				Actual_Contract_Executed_LOA_Effective__c, Actual_Contract_Execution__c, Actual_Critical_Vendor_Data_Receipt1_W__c, 
				Actual_Critical_Vendor_Data_Receipt2_W__c, Actual_CT_Prep_Issued__c, Actual_CWP_Prep__c, Actual_CWP_Received__c, 
				Actual_CWP_To_Be_Received__c, Actual_Decision_Sum_Issued_Approval__c, Actual_Decision_Summary_Approved__c, 
				Actual_Demob__c, Actual_Demob_Cycle__c, Actual_Drawings_Approved__c, Actual_Duration_Of_Work_Weeks__c, Actual_ETA__c, 
				Actual_Final_CWP_Received__c, Actual_Husky_Approval__c, Actual_Kickoff_Mobilization__c, Actual_MFG_Lead_Time_Weeks__c, 
				Actual_Mobilization_Construction_Start__c, Actual_MRP_Prep__c, Actual_MRP_Received__c, Actual_MRQ_Received__c, 
				Actual_MRQ_To_Be_Received__c, Actual_P_O_Issued__c, Actual_PO_Prep_Issued__c, Actual_RFQ_Issued__c, Actual_RFQ_Prep_Mat__c,
				Actual_RFQ_Prep_Serv__c, Actual_Savings_Allocation_Dollar__c, Actual_Ship_Date__c, Actual_Shipping_Duration__c, 
				Actual_Sourcing_Options__c, Actual_Spend_Allocation_Dollar__c, Actual_Strategy_Approval__c, Actual_Strategy_Approval_Cycle__c, 
				Actual_Supplier_Market_Analysis__c, Actual_Tech_Eval_Received__c, Actual_Vendor_Init_Drawing_Submittal__c, Award_Delay_Reason__c, 
				Award_Delays_Comments__c, Award_Delays_Type__c, Completed__c, Completed_By__c, CreatedById, CreatedDate, CWP_Completion__c, 
				Delivery_Variance_Days_Mat__c, Delivery_Variance_Days_Serv__c, Equipment_Service__c, Float__c, 
				Forecast_1_Segmentation_Team_Selection__c, Forecast_2_Business_Requirements__c, Forecast_3_Supplier_Market_Analysis__c, 
				Forecast_4_Sourcing_Options__c, Forecast_Bid_Evaluation_1__c, Forecast_Bid_Evaluation_2__c, Forecast_Bid_Evaluation_3__c, 
				Forecast_Bid_Evaluation_4__c, Forecast_Bid_Period__c, Forecast_Bid_Request_Issued_RFQ__c, Forecast_Bid_Tab_Finalized_Mat__c, 
				Forecast_Bid_Tab_Finalized_Serv__c, Forecast_Bids_Received_Mat__c, Forecast_Bids_Received_Serv__c, Forecast_Business_Requirements__c, 
				Forecast_Comm_Eval_Received_Mat__c, Forecast_Comm_Eval_Received_Serv__c, Forecast_Construction_Completion__c, 
				Forecast_Contract_Executed_LOA_Effective__c, Forecast_Critical_Vendor_Data_Receipt1_W__c, Forecast_Critical_Vendor_Data_Receipt2_W__c, 
				Forecast_CT_Prep_Issued__c, Forecast_CWP_Prep__c, Forecast_CWP_Received__c, Forecast_CWP_To_Be_Received__c, 
				Forecast_Decision_Sum_Issued_Approval_M__c, Forecast_Decision_Sum_Issued_Approval_S__c, Forecast_Decision_Summary_Approved_Mat__c, 
				Forecast_Decision_Summary_Approved_Serv__c, Forecast_Demob__c, Forecast_Demob_Cycle__c, Forecast_Drawings_Approved__c, 
				Forecast_Duration_Of_Work_Weeks__c, Forecast_ETA__c, Forecast_Final_CWP_Received__c, Forecast_Husky_Approval__c, 
				Forecast_Kickoff_Mobilization__c, Forecast_MFG_Lead_Time_Weeks__c, Forecast_Mobilization_Construction_Start__c, 
				Forecast_MRP_Prep__c, Forecast_MRP_Received__c, Forecast_MRQ_Received__c, Forecast_MRQ_To_Be_Received__c, Forecast_P_O_Issued__c, 
				Forecast_PO_Prep_Issued__c, Forecast_RFQ_Issued__c, Forecast_RFQ_Prep__c, Forecast_Ship_Date__c, 
				Forecast_Shipping_Duration__c, Forecast_Sourcing_Options__c, Forecast_Strategy_Approval__c, Forecast_Strategy_Approval_Cycle__c, 
				Forecast_Supplier_Market_Analysis__c, Forecast_Tech_Eval_Received_Mat__c, Forecast_Tech_Eval_Received_Serv__c, 
				Forecast_Vendor_Init_Drawing_Submittal__c, Id, Mat_Serv_Group_Planned_Schedule_Id__c, Name, 
				Planned_1_Segmentation_Team_Selection__c, Planned_2_Business_Requirements__c, Planned_3_Supplier_Market_Analysis__c, 
				Planned_4_Sourcing_Options__c, Planned_Bid_Evaluation_1__c, Planned_Bid_Evaluation_2__c, Planned_Bid_Evaluation_3__c, 
				Planned_Bid_Evaluation_4__c, Planned_Bid_Period__c, Planned_Bid_Request_Issued_RFQ__c, Planned_Bid_Tab_Finalized__c, 
				Planned_Bids_Received__c, Planned_Business_Requirements__c, Planned_Commercial_Evaluations_Received__c, 
				Planned_Construction_Completion__c, Planned_Contract_Executed_LOA_Effective__c, Planned_Contract_Execution__c, 
				Planned_Critical_Vendor_Data_Receipt1_W__c, Planned_Critical_Vendor_Data_Receipt2_W__c, Planned_CT_Prep_Issued__c, 
				Planned_CWP_Prep__c, Planned_CWP_Received__c, Planned_CWP_To_Be_Received__c, Planned_Decision_Sum_Issued_Approval__c, 
				Planned_Decision_Summary_Approved__c, Planned_Demob__c, Planned_Demob_Cycle__c, Planned_Drawings_Approved__c, 
				Planned_Duration_Of_Work_Weeks__c, Planned_ETA__c, Planned_Final_CWP_Received__c, Planned_Husky_Approval__c, 
				Planned_Kickoff_Mobilization__c, Planned_MFG_Lead_Time_Weeks__c, Planned_Mobilization_Construction_Start__c, 
				Planned_MRP_Prep__c, Planned_MRP_Received__c, Planned_MRQ_Received__c, Planned_MRQ_To_Be_Received__c, 
				Planned_P_O_Issued__c, Planned_PO_Prep_Issued__c, Planned_RFQ_Issued__c, Planned_RFQ_Prep__c, Planned_Ship_Date__c, 
				Planned_Shipping_Duration__c, Planned_Sourcing_Options__c, Planned_Strategy_Approval__c, Planned_Strategy_Approval_Cycle__c, 
				Planned_Supplier_Market_Analysis__c, Planned_Technical_Evaluations_Received__c, Planned_Vendor_Init_Drawing_Submittal__c, 
				R_A_S_Date__c, RecordTypeId, Status_1_Segmentation_Team_Selection__c, Status_2_Business_Requirements__c, 
				Status_3_Supplier_Market_Analysis__c, Status_4_Sourcing_Options__c, Status_Bid_Evaluation_1__c, 
				Status_Bid_Evaluation_2__c, Status_Bid_Evaluation_3__c, Status_Bid_Evaluation_4__c, Status_Bid_Period_Mat__c, 
				Status_Bid_Period_Serv__c, Status_Bid_Request_Issued_RFQ__c, Status_Bid_Tab_Finalized__c, Status_Bids_Received__c, 
				Status_Business_Requirements__c, Status_Commercial_Evaluations_Received__c, Status_Construction_Completion__c, 
				Status_Contract_Executed_LOA_Effective__c, Status_Contract_Execution__c, Status_Critical_Vendor_Data_Receipt1_W__c, 
				Status_Critical_Vendor_Data_Receipt2_W__c, Status_CT_Prep_Issued__c, Status_CWP_Prep__c, Status_CWP_Received__c, 
				Status_CWP_To_Be_Received__c, Status_Decision_Sum_Issued_Approval__c, Status_Decision_Summary_Approved__c, 
				Status_Delivery_Variance_Days_Mat__c, Status_Delivery_Variance_Days_Serv__c, Status_Demob__c, Status_Demob_Cycle__c, 
				Status_Drawings_Approved__c, Status_Duration_Of_Work_Weeks__c, Status_ETA__c, Status_Final_CWP_Received__c, 
				Status_Husky_Approval__c, Status_Kickoff_Mobilization__c, Status_MFG_Lead_Time_Weeks__c,  
				Status_Mobilization_Construction_Start__c, Status_MRP_Prep__c, Status_MRP_Received__c, Status_MRQ_Received__c, 
				Status_MRQ_To_Be_Received__c, Status_P_O_Issued__c, Status_PO_Prep_Issued__c, Status_RFQ_Issued__c, Status_RFQ_Prep_Mat__c, 
				Status_RFQ_Prep_Serv__c, Status_Ship_Date__c, Status_Shipping_Duration__c, Status_Sourcing_Options__c, 
				Status_Strategy_Approval__c, Status_Strategy_Approval_Cycle__c, Status_Supplier_Market_Analysis__c, 
				Status_Technical_Evaluations_Received__c, Status_Vendor_Init_Drawing_Submittal__c, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__c, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Acquisition_Cycle_Type__c, Mat_Serv_Group_Planned_Schedule_Id__r.Acquisition_Cycle__r.Float__c,
				
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Planned_Spend_Allocation_Dollar__c,
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.AFE_No__c, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Ariba_Document_Number__c, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Bid_No__c, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Compensation_Mechanism__c, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.CWP_No__c, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Equipment_Service__c, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.External_Expeditor__c, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.External_Rep__c, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Husky_Expeditor__c, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Husky_Proc_Comm_Rep__r.Name, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Market_Approach__c, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Material_Service_Group__r.Name, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Outline_Agreement_No__c, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Requisition_No__c, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Supplier_Contractor__c, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Tag_No__c,  
				
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Contract_Type__c, 
				Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Other_Mechanism__c
								
				from PREP_Mat_Serv_Group_Update__c 
				where Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Baseline_Id__r.Initiative_Id__r.Name =: initID
				and Completed__c = true and RecordType.Name like: sPackageType
				order by LastModifiedDate desc])
		{
		//	PREP_Mat_Serv_Group_Baseline_Allocation__c msgba = msgbaMap.get(msgu.Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__c);
			if (!dataMap.containsKey(msgu.Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__c))
				dataMap.put(msgu.Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__c, msgu); 
		}
		
		packages = dataMap.values();
	 
		return null;
	}
	public PageReference gotoPDF()
	{
		PageReference pdf = new PageReference('/apex/PREPProjectPackage_ExportExcel?initID=' + initID + '&type=' + packageType);
		pdf.setRedirect(true);
		return pdf;
	}
	public void ExportDetail()
	{
		try
		{
			initID = ApexPages.currentPage().getParameters().get('initID');
			packageType = ApexPages.currentPage().getParameters().get('type');
			if (initID.substringAfterLast('-') == '')
			{
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please select Initative ID.'));
				return;
			}
			projectName = [select Id, Name, Initiative_Name__c from PREP_Initiative__c where Name =: initID].Initiative_Name__c;
			runSearch();
		}
		catch(Exception e)
		{
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));

		}
		
	}

}