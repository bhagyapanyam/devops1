@isTest(SeeAllData=true)
private class PTrequestControllerTestClass{
    private static testmethod void CreateData(){
        PTTestData.createPTTestRecords();
    }
    private static testmethod void CreateCustomSettings(){
        PTTestData.ControllerCreateCustomSetting('Approval', 'Approval', 'Service Status');
        PTTestData.ControllerCreateCustomSetting('ControllerFilterTimeLoad', '700', 'Frecuency');
        PTTestData.ControllerCreateCustomSetting('ControllerFirsTimeLoad', '200', 'Frecuency');
        PTTestData.ControllerCreateCustomSetting('Quotation', 'Quotation', 'Service Status');
        PTTestData.ControllerCreateCustomSetting('Under Construction', 'Under Construction', 'Service Status');
        PTTestData.ControllerCreateCustomSetting('test', 'test', 'test');
    }    
    private static testmethod void allNullReport(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.siteName = null;
        ptrequest.lsd = null;
        ptrequest.amu = null;
        ptrequest.meternum = null;
        ptrequest.currentRequests = false;    
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);   
    }
    private static testmethod void allBlank(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();        
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.siteName = '';
        ptrequest.lsd = '';
        ptrequest.amu = '';
        ptrequest.meternum = '';
        ptrequest.currentRequests = false;    
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);   
    }  
    private static testmethod void currentRequestsTrue(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();   
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.siteName = '';
        ptrequest.lsd = '';
        ptrequest.amu = '';
        ptrequest.meternum = '';
        ptrequest.currentRequests = true;    
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);   
    }
    private static testmethod void lsdInfo(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.lsd = '11';
        ptrequest.siteName = '';    
        ptrequest.amu = '';
        ptrequest.meternum = '';
        ptrequest.currentRequests = false;  
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);       
    }
    private static testmethod void amuInfo(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();    
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.lsd = '';
        ptrequest.amu = 'Red';
        ptrequest.meternum = '';
        ptrequest.siteName = '';    
        ptrequest.currentRequests = false;    
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);          
    }
    private static testmethod void meterInfo(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();   
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.lsd = '';
        ptrequest.amu = '';
        ptrequest.meternum = '800';
        ptrequest.siteName = '';    
        ptrequest.currentRequests = false;   
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);                  
    } 
    private static testmethod void siteInfo(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.lsd = '';
        ptrequest.amu = '';
        ptrequest.meternum = '';
        ptrequest.siteName = 'Red';    
        ptrequest.currentRequests = false;   
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);                  
    }  
    private static testmethod void lsdsiteInfo(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();  
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.lsd = '11';
        ptrequest.siteName = 'Red';
        ptrequest.amu = '';
        ptrequest.meternum = '';
        ptrequest.currentRequests = false;
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);         
    }
    private static testmethod void lsdamuInfo(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.lsd = '11';
        ptrequest.siteName = '';
        ptrequest.amu = 'Red';
        ptrequest.meternum = '';
        ptrequest.currentRequests = false;
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);         
    }
    private static testmethod void lsdmeterInfo(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();   
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.lsd = '11';
        ptrequest.siteName = '';
        ptrequest.amu = '';
        ptrequest.meternum = '800';
        ptrequest.currentRequests = false;
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);         
    }
    private static testmethod void amutsiteInfo(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.lsd = '';
        ptrequest.siteName = 'Red';
        ptrequest.amu = 'Red';
        ptrequest.meternum = '';
        ptrequest.currentRequests = false;
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);         
    }
    private static testmethod void sitemeterInfo(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.lsd = '';
        ptrequest.siteName = 'Red';
        ptrequest.amu = '';
        ptrequest.meternum = '800';
        ptrequest.currentRequests = false;
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);         
    }
    private static testmethod void amumeterInfo(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();   
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.lsd = '';
        ptrequest.siteName = '';
        ptrequest.amu = 'Red';
        ptrequest.meternum = '800';
        ptrequest.currentRequests = false;
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);         
    }                          
    private static testmethod void lsdamusiteInfo(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.lsd = '11';
        ptrequest.siteName = 'Red';
        ptrequest.amu = 'Red';
        ptrequest.meternum= '';
        ptrequest.currentRequests = false;
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);         
    }
    private static testmethod void lsdsitemeterInfo(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();   
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.lsd = '11';
        ptrequest.siteName = 'Red';
        ptrequest.amu = '';
        ptrequest.meternum = '800';
        ptrequest.currentRequests = false;
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);         
    }
    private static testmethod void lsdamumeterInfo(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();   
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.lsd = '11';
        ptrequest.siteName = '';
        ptrequest.amu = 'Red';
        ptrequest.meternum = '800';
        ptrequest.currentRequests = false;
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);         
    }
    private static testmethod void amusitemeterInfo(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();   
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.lsd = '';
        ptrequest.siteName = 'Red';
        ptrequest.amu = 'Red';
        ptrequest.meternum = '800';
        ptrequest.currentRequests = false;
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);         
    }
    private static testmethod void allInfo(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();   
        PTTestData.createPTTestRecords();
        ptrequest.search1= true;
        ptrequest.lsd = '11';
        ptrequest.siteName = 'Red';
        ptrequest.amu = 'Red';
        ptrequest.meternum = '800';
        ptrequest.currentRequests = false;
    	ptrequest.GetRequestTable();
    	system.debug(ptrequest.wrapperList);         
    }
    
    private static testmethod void allNullSearch2(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();
        PTTestData.createPTTestRecords();
        ptrequest.search1 = false;
        ptrequest.search2 = true;
		ptrequest.site = null;
		ptrequest.utilityReferenceNum = null;
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);        
    }
    
    private static testmethod void allBlankSearch2(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();
        PTTestData.createPTTestRecords();
        ptrequest.search1 = false;
        ptrequest.search2 = true;
		ptrequest.site = '';
		ptrequest.utilityReferenceNum = '';
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);         
    }
    
    private static testmethod void siteIdRequest(){
        CreateCustomSettings();
        PTrequestController ptrequest = new PTrequestController();
        PTTestData.createPTTestRecords();
        ptrequest.search1 = false;
        ptrequest.search2 = true;
        ptrequest.site = '004';
        ptrequest.utilityReferenceNum = '';
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);
    }
    
    private static testmethod void siteIdRequestOver1000(){
        CreateCustomSettings();    
        PTrequestController ptrequest = new PTrequestController();
        PTTestData.createPTTestRecords();
        ptrequest.search1 = false;
        ptrequest.search2 = true;
        ptrequest.site = '0';
        ptrequest.utilityReferenceNum = '';
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);
    }
    
    private static testmethod void utilityRefRequest(){
        CreateCustomSettings();
        PTTestData.createPTTestRecords();
        PTrequestController ptrequest = new PTrequestController();
        ptrequest.search1 = false;
        ptrequest.search2 = true;
        ptrequest.site = '';
        ptrequest.utilityReferenceNum = 'TST';
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);        
    }
    
    private static testmethod void allInfo2(){
        CreateCustomSettings();
        PTTestData.createPTTestRecords();
        PTrequestController ptrequest = new PTrequestController();
        ptrequest.search1 = false;
        ptrequest.search2 = true;
        ptrequest.site = '004';
        ptrequest.utilityReferenceNum = 'TST';
        ptrequest.GetRequestTable();
        system.debug(ptrequest.wrapperList);        
    }
    
}