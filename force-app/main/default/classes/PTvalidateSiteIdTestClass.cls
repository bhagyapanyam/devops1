@isTest
public class PTvalidateSiteIdTestClass {

    public static testmethod void duplicateSiteId(){
        try{
        PTTestData.createPTTestRecords();
        PTTestData.TestDuplicateSiteId();
        PTTestData.TestModifyingServiceDuplicateSiteID();
        }catch (Exception e){
            System.assert(e.getMessage().contains('An existing Service contains the same site id. Please, assign a new site id to save this record.'),e.getMessage());
        }
            
    }
    public static testmethod void modifySiteIdService(){
        try{
        PTTestData.createPTTestRecords();
        PTTestData.TestModifyingServiceDuplicateSiteID();
        }catch (Exception e){
            System.assert(e.getMessage().contains('An existing Service contains the same site id. Please, assign a new site id to save this record.'),e.getMessage());
        }
    }
}