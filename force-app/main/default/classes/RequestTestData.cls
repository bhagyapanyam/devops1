@isTest
public class RequestTestData {
    public static Request__c createLabRequest(){
		
		User programManager = UserTestData.createTestUser();
		User ea = UserTestData.createTestUser();
        Business_Unit__c bu = TestUtilities.createBusinessUnit('Test Unit');
		
		insert programManager;
		insert ea; 
       	insert bu;
		
		programManager = [select id,Email from User where id=:programManager.id];
		ea = [select id,Email from User where id=:programManager.id];
        bu = [select id, Name from Business_Unit__c where id=:bu.id];
       		
		Request__c sampleRequest = new Request__c();		
		       
        sampleRequest.AFE__c = 'ABCDE';
        sampleRequest.Program_Category__c = 'Retail Transformation';
        sampleRequest.CPU__c = 30.0;
        sampleRequest.Memory__c = 120;
        sampleRequest.Storage__c = 6;
        sampleRequest.Duration_In_Years__c = 2;
        sampleRequest.Provisioning_Stage__c = 'Submit for Approval';
        
        sampleRequest.Program_Manager__c = programManager.id;                
        sampleRequest.EA__c = ea.Id;
        sampleRequest.Business_Unit__c = bu.Id;      
                               
        insert sampleRequest;       
        
        sampleRequest = [select id, Name, Program_Manager__c, EA__c, Business_Unit__c from Request__c where id=:sampleRequest.id];
        
        return sampleRequest;        
    }
    
    public static Request__c changeOwner(Id requestId, Id newOwnerId)
    {
        Request__c updateRequest = new Request__c();
        
        updateRequest = [select id, OwnerId from Request__c where id=:requestId];
        
        System.debug('%%Old OwnerID = ' + updateRequest.OwnerId);
        updateRequest.OwnerId = newOwnerId;
        System.debug('%%NEW OwnerID = ' + updateRequest.OwnerId);
        
        return updateRequest;
    }
    
    public static Request__c setApprovers(Id requestId, boolean PgMApprove, boolean EAApprove)
    {

		Request__c updateRequest = new Request__c();
        updateRequest = [select id, PgM_Approved__c, EA_Approved__c from Request__c where id=:requestId];
        
        updateRequest.PgM_Approved__c = PgMApprove;
        updateRequest.EA_Approved__c = EAApprove;
        
        return updateRequest;
    }
    
    public static Request__c setProvisionStage(Integer stageValue, Id requestId)
    {
		
		Request__c updateRequest = new Request__c();
        updateRequest = [select id, PgM_Approved__c, EA_Approved__c from Request__c where id=:requestId];
        
        if(stageValue == 0){
           	updateRequest.Provisioning_Stage__c = 'Open '; 
        }
        else if( stageValue ==1){
            updateRequest.Provisioning_Stage__c = 'Submit for Approval';
            System.debug('**Submit for Approval**');
        }
        else if (stageValue ==2){
           	updateRequest.Provisioning_Stage__c = 'Provisioning:  In Progress';
            System.debug('**Provisioning: In Progress**');
        }
        else if(stageValue ==3 ){
           		updateRequest.Provisioning_Stage__c = 'Provisioning: Complete';
        }
        
        return updateRequest;
    }
    
    public static Business_Unit__c createBusinessUnit(String name) {    	
		Business_Unit__c businessUnit = new Business_Unit__c(Name = name);
	
		return businessUnit;
	}
    
	
    public static User createApprover(string fakeText)
    {
        ID ProfileID = [ Select id from Profile where name = 'Standard IS User (Platform)'].id;

    	User user = new User();
        
        Double random = Math.Random();
        user.email = 'test-user' + random + '@fakeemail.com';
		user.EmailEncodingKey ='UTF-8';        
        user.ProfileId = ProfileID;
        user.UserName = fakeText + 'vCHS-test-user' + random + '@fakeemail.com';
        user.Alias = fakeText + 'vCHS';        
        user.CommunityNickName = fakeText + 'vCHStuser1' + random;        
        user.LocaleSidKey = 'en_US';                
        user.FirstName = fakeText + 'vCHS Test';
        user.LastName = 'User';
        user.TimeZoneSidKey ='America/Los_Angeles';
        user.LanguageLocaleKey='en_US';
        
        return user;
        
    }
        
    
    
}