public class RetailLocationTestData {

  public static Retail_Location__c createRetailLocation(Id AccountId, String name) {
  
    Retail_Location__c retailLocation = new Retail_Location__c();
    retailLocation.Name = name;
    retailLocation.Location_Number__c = name;
    retailLocation.Account__c = AccountId;
    
    return retailLocation;
  }
}