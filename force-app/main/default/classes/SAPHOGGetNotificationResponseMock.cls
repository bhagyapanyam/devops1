@isTest
global with sharing class SAPHOGGetNotificationResponseMock  implements WebServiceMock {

  //Floc Variables
  public Equipment__c equipment;
  public Location__c location;
  public Facility__c facility;
	
  //custom constructor
  global SAPHOGGetNotificationResponseMock (Equipment__c pEquipment, Location__c pLocation, Facility__c pFacility) {
    this.equipment = pEquipment;
    this.location = pLocation;
    this.facility = pFacility;
  }

	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
		
      System.debug('SAPHOGGetNotificationResponseMock->equipment: ' + this.equipment + ' location: ' + this.location + ' facility: ' + this.facility);
		  SAPHOGNotificationServices.GetNotificationResponse responseElement = new SAPHOGNotificationServices.GetNotificationResponse();
   		responseElement.Message = 'SAPHOGNotificationServices.GetNotificationResponse Test Message';
   		responseElement.type_x = true;
   		responseElement.Notification = new SAPHOGNotificationServices.Notification();
   		
   		responseElement.Notification.NotificationNumber = '7521256';
      responseElement.Notification.EquipmentNumber = (this.equipment != null) ? this.equipment.Equipment_Number__c : null;
      responseElement.Notification.FunctionalLocationObjectId = (this.location != null) ? this.location.SAP_Object_Id__c : 
        (this.facility != null) ? this.facility.SAP_Object_Id__c : null;

   		
   		response.put('response_x', responseElement);
	}
}