@isTest
global with sharing class SAPHOGWorkOrdersResponseMock implements WebServiceMock {
    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
        
		SAPHOGWorkOrderServices.CreateWorkOrdersResponse responseElement = new SAPHOGWorkOrderServices.CreateWorkOrdersResponse();
   		responseElement.Message = 'SAPHOGWorkOrdersResponseMock.CreateWorkOrdersResponse Test Message';
   		responseElement.type_x = True;

   		responseElement.WorkOrderResponseList = new SAPHOGWorkOrderServices.CreateWorkOrderResponse[]{};

		SAPHOGWorkOrderServices.CreateWorkOrderResponse item = new SAPHOGWorkOrderServices.CreateWorkOrderResponse();				
		item.WorkOrder = new SAPHOGWorkOrderServices.WorkOrder();
		item.WorkOrder.WorkOrderNumber = '1234567';

		responseElement.WorkOrderResponseList.add(item);

        response.put('response_x', responseElement);
    }
}