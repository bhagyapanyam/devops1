/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SM_RunSheetControllerXTest
{
    /////////////////
    //* CONSTANTS *//
    /////////////////

    private static final Integer SAND_STING_COUNT = 3;

    // ********************************** //
    // ** TEST CREATING A SAND REQUEST ** //
    // ********************************** //

    

    static testMethod void TestCreateSandRunSheet()
    {
        CreateTestData();

        /////////////////////////////////
        //* RETRIEVE TEST DATA VALUES *//
        /////////////////////////////////

        List<SM_Run_Sheet__c> lSandRunSheets = GetSandHeaderObjects();
        List<SM_Run_Sheet_Detail__c> lSandDetails = GetSandDetailObjects();
        List<Location__c> lLocations = GetLocationData();

        Location__c oLocation1 = lLocations[0];
        Location__c oLocation2 = lLocations[1];

        Contact oContact = [SELECT Id FROM Contact LIMIT 1];
        User oUser = GetTestUser();

        /////////////////////////////////////
        //* MISC VARIABLES FOR ASSIGNMENT *//
        /////////////////////////////////////

        SM_Run_Sheet__c oSandRunSheet = lSandRunSheets[0];
        List<SM_Run_Sheet_Detail__c> lTankData;
        List<SelectOption> lSelectTest;
        PageReference oPage;
        Date dToday = Date.today();

        //m.z. 21.6.2016
        //List<SM_RunSheetControllerX.SandDetailVendorsWrapper> vendorListWrapperList;
        //List<SM_Sand_Request_Vendor_Ticket__c> vendorTickets = new List<SM_Sand_Request_Vendor_Ticket__c>();

        //////////////////
        //* START TEST *//
        //////////////////

        Test.startTest();

        System.runAs(oUser)
        {
            // Create Controllers
            ApexPages.StandardController oController = new ApexPages.StandardController(oSandRunSheet);
            SM_RunSheetControllerX oExtension = new SM_RunSheetControllerX(oController);

            // Sand Objects SHOULD be null
            System.assert(oExtension.getNewSheet().Id == null);
            System.assert(oExtension.getViewSheet().Id == null);
            System.assert(oExtension.getViewSheetDetail().size() == 0);

            // Assign Page Data And Re-Initialize
            System.currentPageReference().getParameters().put('wellid', oLocation1.Id);
            System.currentPageReference().getParameters().put('route', oLocation1.Route__r.Name);
            oExtension = new SM_RunSheetControllerX(oController);

            // Sand Object SHOULD Be Initialized for New Sand Request
            System.assert(oExtension.getNewSheet().Id == oSandRunSheet.Id);

            // Get Tank-level data and fix validation issues
            lTankData = BuildSampleTankData(oExtension.getNewSheet(), oExtension.oSelectedLocation);
            lTankData[0].Sting_Type__c = 'Sting-to-air';
            lTankData[0].Tank_Level__c = 5;
            lTankData[0].Sand_Level__c = 6;
            lTankData[0].Water_Level__c = 7;
            lTankData[0].Priority__c = '2';
            lTankData[0].Blow_off_location__c = 'Another well';

            // Attempt Save /w Data --> Sheet SHOULD be Editable AND Save SHOULD Fail (Can't change Name field!)
            oExtension.setAddSandDetails(lTankData);
            //System.assert(oExtension.SaveSheet() == null);

            // Route List SHOULD have 1 Route (Even though 2 are Created, only Locations /w Routes are queried) AND Route Data should match specified location
            System.assert(oExtension.getFullRouteList().size() == 1);
            System.assert(oExtension.getRouteId() == oLocation1.Route__c);
            System.assert(oExtension.getRouteNumber() == oLocation1.Route__r.Name);
            System.assert(oExtension.getWellId() == oLocation1.Id);

            // New Sand Object SHOULD have today's date
            System.assert(oExtension.getNewSheet().Date__c == dToday);

            // Get Status List and Page Extensions
            lSelectTest = oExtension.StingStatusList;
            oPage = oExtension.UpdateWellOrder();

            // Refresh the Location with first location
            oExtension.sSelectedLocationID = oLocation1.Id;
            oExtension.RefreshSelectedWell();
            System.assert(oExtension.SaveSheet() == null);

            // Refresh the Location with second location
            oExtension.sSelectedLocationID = oLocation2.Id;
            oExtension.RefreshSelectedWell();

            // Sand Objects SHOULD be null (nothing created on this location yet)
            System.assert(oExtension.getNewSheet().Id == null);

            // Attempt Save --> SHOULD fail (No Tank Data Added)
            System.assert(oExtension.SaveSheet() == null);
            
            // Get Tank-level data
            lTankData = BuildSampleTankData(oExtension.getNewSheet(), oExtension.oSelectedLocation);

            // Attempt Save /w Data --> Sheet SHOULD be Editable AND Save SHOULD fail (Fields are missing)
            lTankData[0].Sting_Type__c = null;
            lTankData[0].Tank_Level__c = null;
            lTankData[0].Sand_Level__c = null;
            lTankData[0].Water_Level__c = null;
            lTankData[0].Priority__c = null;
            lTankData[0].Blow_off_location__c = null;
            System.assert(oExtension.getNewSandSheetIsEditable() == true);
            oExtension.setAddSandDetails(lTankData);
            System.assert(oExtension.SaveSheet() == null);

            // Attempt Save /w Data --> Sheet SHOULD be Editable AND Save SHOULD Succeed
            SM_Run_Sheet__c oSaveSheet = oExtension.getNewSheet();
            lTankData[0].Sting_Type__c = 'Sting-to-air';
            lTankData[0].Tank_Level__c = 5;
            lTankData[0].Sand_Level__c = 6;
            lTankData[0].Water_Level__c = 7;
            lTankData[0].Priority__c = '2';
            lTankData[0].Blow_off_location__c = 'Another well';

            oSaveSheet.Sand_Lead__c = oContact.Id;
            oExtension.setNewSheet(oSaveSheet);

            System.assert(oExtension.getNewSandSheetIsEditable() == true);
            System.assert(oExtension.getCanSubmitForm() == true);
            oExtension.setAddSandDetails(lTankData);
            oExtension.SaveSheet();
        }

        /////////////////
        //* STOP TEST *//
        /////////////////

        Test.stopTest();
    }

    // ********************************** //
    // ** TEST CREATING A SAND REQUEST ** //
    // ********************************** //

    static testMethod void TestEditSandRunSheet()
    {
        CreateTestData();

        /////////////////////////////////
        //* RETRIEVE TEST DATA VALUES *//
        /////////////////////////////////

        List<SM_Run_Sheet__c> lSandRunSheets = GetSandHeaderObjects();
        List<SM_Run_Sheet_Detail__c> lSandDetails = GetSandDetailObjects();
        List<Location__c> lLocations = GetLocationData();
        //MJ test
        //System.debug('AAA sand detail: ' + lSandDetails);
        //System.debug('AAA Locations: ' + llocations);
        //System.debug('AAA BUILD: ' + BuildSampleTankData(lSandRunSheets[0], lLocations[0]));
        List<SM_Run_Sheet_Detail__c> RunSheetDetail = BuildSampleTankData(lSandRunSheets[0], lLocations[0]);
        System.debug('AAA RunSheetDetail: ' + RunSheetDetail);
        System.debug('AAA lSandRunSheets: ' + lSandRunSheets);
        User oUser = GetTestUser();

        /////////////////////////////////////
        //* MISC VARIABLES FOR ASSIGNMENT *//
        /////////////////////////////////////

        SM_Run_Sheet__c oSandRunSheet = lSandRunSheets[0];
        List<SelectOption> lOptions;
        PageReference oPage;

        //////////////////
        //* START TEST *//
        //////////////////

        Test.startTest();

        System.runAs(oUser)
        {
            List<SM_Run_Sheet_Detail__c> lTankData;
            
            // Set Page Data
            PageReference oCurrentPage = Page.SM_EditRunSheet;
            Test.setCurrentPage(oCurrentPage);
            //MJ
            System.currentPageReference().getParameters().put('tank', oSandRunSheet.Id);
            System.currentPageReference().getParameters().put('id', oSandRunSheet.Id);

            // Create Controllers
            ApexPages.StandardController oController = new ApexPages.StandardController(oSandRunSheet);
            SM_RunSheetControllerX oExtension = new SM_RunSheetControllerX(oController);

            // Test Page References
            oPage = oExtension.RedirectCustomizeWellOrder();
            System.assert(oPage != null);
            oPage = oExtension.RedirectExitWellOrderForm();
            System.assert(oPage != null);
            oPage = oExtension.RedirectEditSheet();
            System.assert(oPage != null);
            oPage = oExtension.RedirectViewSheet();
            System.assert(oPage != null);
            oPage = oExtension.RedirectOperatorSheet();
            System.assert(oPage != null);

            // Test Get / Set Methods
            oExtension.setRouteId(oExtension.getRouteId());
            lOptions = oExtension.getFullLocationList();
            lSandDetails = oExtension.getAddSandDetails();

            // Check IF Sand Lead --> SHOULD be true
            System.assert(oExtension.getIsSandLead() == true);

            // Check IF Sour is marked --> SHOULD be N
            System.assert(oExtension.getCurrentSour() == 'N');

            // Sand Objects SHOULD NOT be null
            System.assert(oExtension.getViewSheet().Id == oSandRunSheet.Id);
            lSandDetails = oExtension.getViewSheetDetail();
                
            // Test Add Vendor Ticket Function
            oExtension.sandRequestId = lSandDetails[0].Id;
            oExtension.addVendorLine();
            //List<SM_Sand_Request_Vendor_Ticket__c> vendorTickets = oExtension.getViewVendorDetail();
            //System.assertEquals(vendorTickets.size(), 1);
            
            List<SM_Sand_Request_Vendor_Ticket__c> vendorTickets = oExtension.getViewVendorDetail();
            List<SM_RunSheetControllerX.SandDetailVendorsWrapper> vendorListWrapperList = oExtension.getViewVendorDetailWrapper();
            System.assertEquals(vendorListWrapperList.size(), 1);

            // Attempt Update
            oExtension.EditSheet();

            oExtension.rowToRemove = 0;
            oExtension.removeVendorLine();
            vendorListWrapperList = oExtension.getViewVendorDetailWrapper();
            System.assertEquals(vendorListWrapperList.size(), 0);
            //MJ 20160623 Asserted number increased (was 1 before) to cover number of both internal and external facilities (i.e. Facility + Picklist Manager Disposal value)
            //MZ 26.8.2016 Changed to 3 - added one more external disposal into test data
            System.assertEquals(3, oExtension.lDestinationLocations.size());

            //m.z.: 21.6.1016
            System.assertEquals(1, oExtension.pmVendorPLvalues.size());
            System.assertEquals(1, oExtension.pmUnitTypePLvalues.size());
        }

        /////////////////
        //* STOP TEST *//
        /////////////////

        Test.stopTest();
    }

    // ***************************************************************** //
    // ** TEST MANIPULATING THE STATUS ON SAND REQUEST DETAIL OBJECTS ** //
    // ***************************************************************** //

    static testMethod void TestUpdateSandDetailStatus()
    {
        CreateTestData();

        /////////////////////////////////
        //* RETRIEVE TEST DATA VALUES *//
        /////////////////////////////////

        List<SM_Run_Sheet_Detail__c> lSandDetails = GetSandDetailObjects();
        SM_Run_Sheet_Detail__c oOneDetail = lSandDetails[0];
        User oUser = GetTestUser();
        List<HOG_Picklist_Manager__c> pmVendorList = GetPMvendors();

        // Status SHOULD be New
        System.assert(oOneDetail.Status__c == SM_Utilities.SANDSTING_STATUS_NEW);

        //////////////////
        //* START TEST *//
        //////////////////

        Test.startTest();

        Facility__c facility1 = [select id, name from Facility__c where Sand_Destination_Ind__c = true limit 1][0];

        System.runAs(oUser)
        {
            //// Refresh Object -> Status SHOULD be changed to BOOKED
            //SM_Sand_Request_Vendor_Ticket__c testVendorTicket = new SM_Sand_Request_Vendor_Ticket__c();
            //testVendorTicket.Sand_Request__c = oOneDetail.Id;
            //testVendorTicket.Vendor__c = 'Clean Harbors';
            //testVendorTicket.Truck_Type__c = 'Body Vac';
            //testVendorTicket.Unit_Number__c = '0123456789';
            //testVendorTicket.Ticket_Date__c = Date.today().addDays(5);
            //testVendorTicket.Ticket_Number__c = '0123456789';
            //testVendorTicket.Ticket_Hours__c = 6.5;
            //testVendorTicket.Disposal_Facility__c = facility1.id; //'Husky Sand Cavern 7-30-49-27W3';
            //testVendorTicket.Ticket_Volume_m3__c = 20;
            //insert testVendorTicket;
            //lSandDetails = GetSandDetailObjects();
            //oOneDetail = lSandDetails[0];
            //System.assert(oOneDetail.Status__c == SM_Utilities.SANDSTING_STATUS_BOOKED);


            //SM status is still New
            lSandDetails = GetSandDetailObjects();
            oOneDetail = lSandDetails[0];           
            System.assert(oOneDetail.Status__c == SM_Utilities.SANDSTING_STATUS_NEW);

            //SM lets try to change status to booked
            try
            {
                oOneDetail.Status__c = SM_Utilities.SANDSTING_STATUS_BOOKED;
                update oOneDetail;
                //we should not be able to change status to Booked when Primary Vendor or Primary Disposal Location or Vendor Date give out are not set             
                System.assert(false, 'Changed status to BOOKED'); 
            }
            catch (DmlException e)
            {
                //passed
                System.assert( e.getMessage().contains('Status cannot be set to \'' + SM_Utilities.SANDSTING_STATUS_BOOKED + '\' if Primary Vendor, Primary Disposal and Vendor Date given out are not provided.'),
                    e.getMessage() );
            }


            lSandDetails = GetSandDetailObjects();
            oOneDetail = lSandDetails[0];   

            oOneDetail.Primary_Disposal_Facility__c = facility1.id;
            oOneDetail.Primary_Vendor__c = pmVendorList[0].Picklist_Value__c; //'Clean Harbors';
            oOneDetail.Primary_Vendor_PM__c = pmVendorList[0].Id;
            oOneDetail.Vendor_Date_given_out__c = Date.today();
            update oOneDetail;
            lSandDetails = GetSandDetailObjects();
            oOneDetail = lSandDetails[0];
            //after primary vendor, primary disposal and vendor date give out are filled, status will be changed to Booked
            System.assert(oOneDetail.Status__c == SM_Utilities.SANDSTING_STATUS_BOOKED);

            // Update Truck Type field AND Status Field to ON-HOLD --> needs comments
            oOneDetail.Status__c = SM_Utilities.SANDSTING_STATUS_ON_HOLD;
            oOneDetail.Comments__c = 'On Hold';
            update oOneDetail;

            // Refresh Object -> Status SHOULD be LOCKED to ON-HOLD (even though Truck Type is filled out, otherwise it would go to IN PROGRESS)
            lSandDetails = GetSandDetailObjects();
            oOneDetail = lSandDetails[0];
            System.assert(oOneDetail.Status__c == SM_Utilities.SANDSTING_STATUS_ON_HOLD);

            // Update Status Field to IN PROGRESS
            oOneDetail.Status__c = SM_Utilities.SANDSTING_STATUS_IN_PROGRESS;
            update oOneDetail;

            // Refresh Object -> Status SHOULD be changed to IN PROGRESS
            lSandDetails = GetSandDetailObjects();
            oOneDetail = lSandDetails[0];
            System.assert(oOneDetail.Status__c == SM_Utilities.SANDSTING_STATUS_IN_PROGRESS);

            // Update Vendor Unit field AND Chem&Rate field
            oOneDetail.Chem_and_rate__c = 'Potassium Benzoid';
            oOneDetail.Vendor_Date_given_out__c = Date.today();
            oOneDetail.Location_for_Water_Tank__c = 'Tank 1';
            oOneDetail.Status__c = SM_Utilities.SANDSTING_STATUS_COMPLETE;
            update oOneDetail;

            // Refresh Object -> Status SHOULD be changed to COMPLETE
            lSandDetails = GetSandDetailObjects();
            oOneDetail = lSandDetails[0];
            System.assert(oOneDetail.Status__c == SM_Utilities.SANDSTING_STATUS_COMPLETE);
        }

        /////////////////
        //* STOP TEST *//
        /////////////////

        Test.stopTest();
    }

    // ************************************** //
    // ** TEST EXPORTING SAND REQUEST DATA ** //
    // ************************************** //

    static testMethod void TestExportSandData()
    {
        CreateTestData();

        /////////////////////////////////
        //* RETRIEVE TEST DATA VALUES *//
        /////////////////////////////////

        List<SM_Run_Sheet_Detail__c> lSandRunSheets = GetSandDetailObjects();
        User oUser = GetTestUser();

        /////////////////////////////////////
        //* MISC VARIABLES FOR ASSIGNMENT *//
        /////////////////////////////////////

        List<SM_Run_Sheet_Detail__c> lExcelResults;
        PageReference oPage;
        String sHeaderData;

        //////////////////
        //* START TEST *//
        //////////////////

        Test.startTest();

        System.runAs(oUser)
        {
            // Create Controllers
            ApexPages.StandardSetController oController = new ApexPages.StandardSetController(lSandRunSheets);
            SM_RunSheetControllerExportX oExtension = new SM_RunSheetControllerExportX(oController);

            // Get Header Data -> Verify it is Set
            sHeaderData = oExtension.sExcelHeader;
            System.assert(sHeaderData != '' && sHeaderData != null);

            // Get Excel Results -> Verify the Export size matches the number of child records created
            lExcelResults = oExtension.getFullExport();
            System.assertEquals(lExcelResults.size(), SAND_STING_COUNT);


        }

        /////////////////
        //* STOP TEST *//
        /////////////////

        Test.stopTest();
    }


    //mz: W-000347
    // ***************************************************************** //
    // **      TEST MASS-UPDATE PRIMARY DISPOSAL ON SAND REQUEST **      //
    // ***************************************************************** //

    static testMethod void TestMassUpdatePrimaryDisposal(){


        CreateTestData();

        /////////////////////////////////
        //* RETRIEVE TEST DATA VALUES *//
        //////////////////////////////////

        List<SM_Run_Sheet__c> lSandRunSheets = GetSandHeaderObjects();
        List<SM_Run_Sheet_Detail__c> lSandDetails = GetTestSRdataForMassUpdate();

        List<Location__c> lLocations = GetLocationData();
        User oUser = GetTestUser();

        Contact oContact = [SELECT Id 
                            FROM Contact 
                            LIMIT 1];    

        HOG_Picklist_Manager__c extDisposal = [SELECT Id,Picklist_Value__c 
                                               FROM HOG_Picklist_Manager__c 
                                               WHERE Picklist_Value__c = 'Disposal Harbors' 
                                               LIMIT 1];

        Facility__c intDisposal = [SELECT Id, Name
                                   FROM Facility__c
                                   WHERE Name = 'Facility 1'];                                     


        //////////////////
        //* START TEST *//
        //////////////////
        

        Test.startTest();


        System.runAs(oUser){

            // Create Controllers
            ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(lSandDetails);
            setCon.setSelected(lSandDetails);
            SM_RunSheetControllerX controller = new SM_RunSheetControllerX(setCon);
            
            //System.Debug('debug_setCon.getRecords: ' + setCon.getRecords()); 
            
            System.assertEquals(2, controller.getSelectedSize());
              /*
              System.Debug('debug_getSelectedSize(): ' + controller.getSelectedSize());
              System.Debug('controller.setController.getSelected() ' + controller.setController.getSelected());
             */   

            //Test the condition for - None - facility selection
            controller.selectedDisposal = 'none';
            System.assertEquals(null, controller.getDisposalName());
            controller.MassUpdatePrimaryDisposal();


            //Test the condition for external facility selection
            controller.selectedDisposal = extDisposal.Id;
            System.assertEquals(controller.disposalIdNameMap.get(extDisposal.Id), controller.getDisposalName());
            controller.MassUpdatePrimaryDisposal();


            //Test the condition for internal facility selection
            controller.selectedDisposal = intDisposal.Id;
            System.assertEquals(controller.disposalIdNameMap.get(intDisposal.Id), controller.getDisposalName());
            controller.MassUpdatePrimaryDisposal();

            //Test of Exception throwing            
            lSandDetails[1].Vendor_Date_given_out__c = Date.today();   
            lSandDetails[1].Status__c = 'Complete';
            controller.selectedDisposal = 'none'; //this will throw an exception because of  field validation rule
            controller.MassUpdatePrimaryDisposal();

        }   

        /////////////////
        //* STOP TEST *//
        /////////////////
        

        Test.stopTest();
    }



    //////////////////////////////////////////////
    //* DATA RETRIEVAL FUNCTIONS --> TEST DATA *//
    //////////////////////////////////////////////

    private static List<SM_Run_Sheet__c> GetSandHeaderObjects()
    {
        return [SELECT Id, Date__c, Well_Location__r.Route__r.Name FROM SM_Run_Sheet__c];
    }

    private static List<SM_Run_Sheet_Detail__c> GetSandDetailObjects()
    {
        return [SELECT Id, Name, Blow_off_location__c, Chem_and_Rate__c, 
            Comments__c, Flood_Up__c, Flowline_push__c, Location_for_Water_Tank__c, 
            Location_for_Water_Well__c, Priority__c, Sand_Level__c, Sand_Run_Sheet__c, Status__c, 
            Sting_Type__c, Tank__c, Tank_Enabled__c, Tank_Level__c, Trigger_Bypass__c, 
            Vendor_Date_given_out__c, Water_Level__c, Work_Order__c,
            Primary_Vendor__c, 
            Primary_Disposal_Facility__c
            FROM SM_Run_Sheet_Detail__c];
    }

    private static List<Location__c> GetLocationData()
    {
        return [SELECT Id, Route__c, Route__r.Name, Location_Name_For_Fluid__c FROM Location__c];
    }

    //mz: 21.6.2016
    private static List<HOG_Picklist_Manager__c> GetPMvendors()
    {
        return [SELECT Id, Picklist_Name__c, Picklist_Value__c FROM HOG_Picklist_Manager__c WHERE Picklist_Name__c = 'Vendor'];
    }


    private static List<SM_Run_Sheet_Detail__c> GetTestSRdataForMassUpdate()
    {

        

        //Test  Sand Run Sheet Details for Mass-Update
        List<SM_Run_Sheet_Detail__c> muSandRunSheetDetailList = new List<SM_Run_Sheet_Detail__c>();


        HOG_Picklist_Manager__c d1 = [SELECT Id FROM HOG_Picklist_Manager__c WHERE Picklist_Value__c = 'Disposal Harbors' LIMIT 1];
        HOG_Picklist_Manager__c d2 = [SELECT Id FROM HOG_Picklist_Manager__c WHERE Picklist_Value__c = 'Test-Ext-Disposal' LIMIT 1];
        HOG_Picklist_Manager__c v2 = [SELECT Id FROM HOG_Picklist_Manager__c WHERE Picklist_Value__c = 'Clean Harbors' LIMIT 1];
        SM_Run_Sheet__c sr = [SELECT Id FROM SM_Run_Sheet__c LIMIT 1];


        SM_Run_Sheet_Detail__c sandRunSheetDetail1 = new SM_Run_Sheet_Detail__c();
        sandRunSheetDetail1.Primary_Disposal_Facility_Ext__c = d1.Id;
        sandRunSheetDetail1.Priority__c = '2';
        sandRunSheetDetail1.Sand_Level__c = 5;
        sandRunSheetDetail1.Sand_Run_Sheet__c = sr.Id;
        sandRunSheetDetail1.Status__c = 'New';
        sandRunSheetDetail1.Sting_Type__c = 'Production';
        sandRunSheetDetail1.Tank__c = 'Tank 1';
        sandRunSheetDetail1.Tank_Enabled__c = true;
        sandRunSheetDetail1.Tank_Level__c = 5;
        sandRunSheetDetail1.Water_Level__c = 7;
        muSandRunSheetDetailList.add(sandRunSheetDetail1);
        

        SM_Run_Sheet_Detail__c sandRunSheetDetail2 = new SM_Run_Sheet_Detail__c();
        sandRunSheetDetail2.Primary_Disposal_Facility_Ext__c = d2.Id;
        sandRunSheetDetail2.Primary_Vendor_PM__c = v2.Id;
        sandRunSheetDetail2.Priority__c = '2';
        sandRunSheetDetail2.Sand_Level__c = 5;
        sandRunSheetDetail2.Sand_Run_Sheet__c = sr.Id;
        sandRunSheetDetail2.Status__c = 'New';
        sandRunSheetDetail2.Sting_Type__c = 'Production';
        sandRunSheetDetail2.Tank__c = 'Tank 2';
        sandRunSheetDetail2.Tank_Enabled__c = true;
        sandRunSheetDetail2.Tank_Level__c = 5;
        sandRunSheetDetail2.Water_Level__c = 7;        
        muSandRunSheetDetailList.add(sandRunSheetDetail2);

        insert muSandRunSheetDetailList;

        return [SELECT Id, Name, Primary_Disposal_Facility_Ext__c, Disposal_Destination__c
                FROM SM_Run_Sheet_Detail__c
                WHERE Primary_Disposal_Facility_Ext__c <> null];
    }


    private static User GetTestUser()
    {
        return [SELECT Id FROM User WHERE Name = 'Guy Incognito' AND Alias = 'guyincog' AND Email = 'guy.incognito@testingsandtestclasses.com' LIMIT 1];
    }

    private static List<SM_Run_Sheet_Detail__c> BuildSampleTankData(SM_Run_Sheet__c oSandRequest, Location__c oLocation)
    {
        List<SM_Run_Sheet_Detail__c> lSampleTankData = new List<SM_Run_Sheet_Detail__c>();
        SM_Run_Sheet_Detail__c oTank1 = new SM_Run_Sheet_Detail__c();
        SM_Run_Sheet_Detail__c oTank2 = new SM_Run_Sheet_Detail__c();
        SM_Run_Sheet_Detail__c oTank3 = new SM_Run_Sheet_Detail__c();

        oTank1.Sand_Run_Sheet__c = oSandRequest.Id;
        oTank2.Sand_Run_Sheet__c = oSandRequest.Id;
        oTank3.Sand_Run_Sheet__c = oSandRequest.Id;

        oTank1.Tank_Enabled__c = true;
        oTank2.Tank_Enabled__c = true;
        oTank3.Tank_Enabled__c = true;

        oTank1.Tank__c = 'Tank 1';
        oTank2.Tank__c = 'Tank 2';
        oTank3.Tank__c = 'Tank 3';

        oTank1.Sting_Type__c = 'Sting-to-Air';
        oTank2.Sting_Type__c = 'Production';
        oTank3.Sting_Type__c = 'Door Pull';

        oTank1.Tank_Level__c = 501;
        oTank2.Tank_Level__c = 412;
        oTank3.Tank_Level__c = 306;

        oTank1.Sand_Level__c = 15;
        oTank2.Sand_Level__c = 9;
        oTank3.Sand_Level__c = 6;

        oTank1.Water_Level__c = 150;
        oTank2.Water_Level__c = 5;
        oTank3.Water_Level__c = 3;

        oTank1.Priority__c = '1';
        oTank2.Priority__c = '2';
        oTank3.Priority__c = '3';

        oTank1.Chem_and_rate__c = 'glaiven';
        oTank2.Chem_and_rate__c = 'floiven';
        oTank3.Chem_and_rate__c = 'maiiven';

        oTank1.Location_For_Water_Well__c = oLocation.Id;
        oTank2.Location_For_Water_Well__c = oLocation.Id;
        oTank3.Location_For_Water_Well__c = oLocation.Id;

        oTank1.Location_For_Water_Tank__c = 'Tank 1';
        oTank2.Location_For_Water_Tank__c = 'Tank 2';
        oTank3.Location_For_Water_Tank__c = 'Tank 3';

        oTank1.Comments__c = 'Sample Production Tank';
        oTank2.Comments__c = 'Sample Sting-to-air Tank';
        oTank3.Comments__c = 'Sample Door Pull Tank';

        oTank1.Blow_off_location__c = 'A Well';
        oTank2.Blow_off_location__c = 'Another Well';
        oTank3.Blow_off_location__c = 'Some Well';
        
        lSampleTankData.add(oTank1);
        lSampleTankData.add(oTank2);
        lSampleTankData.add(oTank3);
        
        return lSampleTankData;
    }

    // ******************************************* //
    // ** CREATE ALL TEST DATA FOR TEST METHODS ** //
    // ******************************************* //

    private static void CreateTestData()
    {
        List<Profile> lProfiles = [SELECT Id FROM Profile WHERE Name = 'Standard HOG - General User' LIMIT 1];

        // Test USER
        User oUser = new User();
        oUser.FirstName = 'Guy';
        oUser.LastName = 'Incognito';
        oUser.Email = oUser.FirstName.toLowerCase() + '.' + oUser.LastName.toLowerCase() + '@testingsandtestclasses.com';
        oUser.UserName = oUser.Email;
        oUser.Alias = 'guyincog';
        oUser.TimeZoneSidKey = 'America/New_York';
        oUser.LocaleSidKey = 'en_US';
        oUser.EmailEncodingKey = 'ISO-8859-1'; 
        oUser.LanguageLocaleKey='en_US';
        oUser.ProfileId = lProfiles[0].Id;
        oUser.IsActive = true;
        insert oUser;

        System.runAs(oUser)
        {
            // Permission Set Assignment - Sand Lead
            List<PermissionSet> lPermissions = [SELECT Id, Name FROM PermissionSet WHERE Name = 'HOG_Sand_Management_Lead' LIMIT 1];
            PermissionSet oPermission = (lPermissions.size() == 1) ? lPermissions[0] : null;

            if(oPermission != null)
            {
                PermissionSetAssignment oPermissionAssign = new PermissionSetAssignment(AssigneeId = oUser.Id, PermissionSetId = oPermission.id);
                insert oPermissionAssign;
            }
        }

        // Test Account
        Account oAccount = new Account();
        oAccount.Name = 'Husky Account';
        insert oAccount;

        // Test Contact
        Contact oContact = new Contact();
        oContact.Account = oAccount;
        oContact.FirstName = 'Senator';
        oContact.LastName = 'McTest';
        oContact.X2Z_Employee_ID__c = 'Octothorpe';
        insert oContact;

        // Test Business Department
        Business_Department__c oBusiness = new Business_Department__c();
        oBusiness.Name = 'Husky Business';
        insert oBusiness;

        // Test Operating District
        Operating_District__c oDistrict = new Operating_District__c();
        oDistrict.Name = 'District 1234';
        oDistrict.Business_Department__c = oBusiness.Id;
        insert oDistrict;

        // Test AMU Field
        Field__c oField = new Field__c();
        oField.Name = 'AMU Field';
        oField.Operating_District__c = oDistrict.Id;
        insert oField;


        Facility__c ofac1 = new Facility__c();
        ofac1.Name = 'Facility 1';
        ofac1.Sand_Destination_Ind__c = true;
        ofac1.Operating_Field_AMU__c = oField.id;
        insert ofac1;

        Facility__c ofac2 = new Facility__c();
        ofac2.Name = 'Facility 2';
        ofac2.Sand_Destination_Ind__c = false;
        ofac2.Operating_Field_AMU__c = oField.id;
        insert ofac2;

        // Test Route 1
        Route__c oRoute1 = new Route__c();
        oRoute1.Fluid_Management__c = true;
        oRoute1.Name = '777';
        oRoute1.Route_Number__c = '777';
        insert oRoute1;

        // Test Route 2
        Route__c oRoute2 = new Route__c();
        oRoute2.Fluid_Management__c = true;
        oRoute2.Name = '999';
        oRoute2.Route_Number__c = '999';
        insert oRoute2;

        // Test Location 1
        Location__c oLocation1 = new Location__c();
        oLocation1.Name = 'Husky Well';
        oLocation1.Fluid_Location_Ind__c = true;
        oLocation1.Location_Name_for_Fluid__c = 'Husky Well Fluid';
        oLocation1.Operating_Field_AMU__c = oField.Id;
        oLocation1.Hazardous_H2s__c = 'Y';
        oLocation1.Functional_Location_Category__c = 4;
        oLocation1.Well_Type__c = 'OIL';
        oLocation1.Route__c = oRoute2.Id;
        insert oLocation1;

        // Test Location 2
        Location__c oLocation2 = new Location__c();
        oLocation2.Name = 'Husky WIG Well';
        oLocation2.Fluid_Location_Ind__c = true;
        oLocation2.Location_Name_for_Fluid__c = 'Husky Well Fluid Stuff';
        oLocation2.Operating_Field_AMU__c = oField.Id;
        oLocation2.Hazardous_H2s__c = 'N';
        oLocation2.Functional_Location_Category__c = 4;
        oLocation2.Well_Type__c = 'OIL';
        oLocation2.Route__c = oRoute2.Id;
        insert oLocation2;

        // Well Event Type
        List<RecordType> lRecordTypes = [SELECT Id FROM RecordType WHERE SobjectType = 'Location__c' AND DeveloperName = 'Well_Event' LIMIT 1];
        RecordType oRecordType = lRecordTypes[0];

        // Test Well Event 1
        Location__c oEvent1 = new Location__c();
        oEvent1.Name = 'Husky Well Event';
        oEvent1.RecordTypeId = oRecordType.Id;
        oEvent1.Operating_Field_AMU__c = oField.Id;
        oEvent1.Route__c = oRoute2.Id;
        oEvent1.Well_ID__c = oLocation1.Id;
        oEvent1.PVR_AVGVOL_30D_OIL__c = 111;
        oEvent1.PVR_AVGVOL_30D_WATER__c = 222;
        insert oEvent1;

        // Test Well Event 2
        Location__c oEvent2 = new Location__c();
        oEvent2.Name = 'Another Husky Well Event';
        oEvent2.RecordTypeId = oRecordType.Id;
        oEvent2.Operating_Field_AMU__c = oField.Id;
        oEvent2.Route__c = oRoute2.Id;
        oEvent2.Well_ID__c = oLocation2.Id;
        oEvent2.PVR_AVGVOL_30D_OIL__c = 123;
        oEvent2.PVR_AVGVOL_30D_WATER__c = 321;
        insert oEvent2;

        // Test Favorite Object
        SM_CustomWellOrder__c oFavorite = new SM_CustomWellOrder__c();
        oFavorite.Route__c = oRoute2.Id;
        oFavorite.User__c = UserInfo.getUserId();
        oFavorite.WellIDList__c = oLocation1.Id;
        insert oFavorite;

        // Test Picklist Manager data
        HOG_Picklist_Manager__c oPicklistManager = new HOG_Picklist_Manager__c();
        oPicklistManager.Picklist_Name__c = 'Unit Type';
        oPicklistManager.Picklist_Value__c = 'tstUT01';
        insert oPicklistManager;

        HOG_Picklist_Manager__c oPicklistManager2 = new HOG_Picklist_Manager__c();
        oPicklistManager2.Picklist_Name__c = 'Vendor';
        oPicklistManager2.Picklist_Value__c = 'Clean Harbors';
        insert oPicklistManager2;
        
        HOG_Picklist_Manager__c oPicklistManager3 = new HOG_Picklist_Manager__c();
        oPicklistManager3.Picklist_Name__c = 'Disposal';
        oPicklistManager3.Picklist_Value__c = 'Disposal Harbors';
        insert oPicklistManager3;

       
        HOG_Picklist_Manager__c oPicklistManager4 = new HOG_Picklist_Manager__c();
        oPicklistManager4.Picklist_Name__c = 'Disposal';
        oPicklistManager4.Picklist_Value__c = 'Test-Ext-Disposal';
        insert oPicklistManager4;


        // Test Sand Run Sheet
        SM_Run_Sheet__c oSandRunSheet = new SM_Run_Sheet__c();
        oSandRunSheet.Date__c = Date.today();
        oSandRunSheet.Well_Location__c = oLocation1.Id;
        oSandRunSheet.Sand_Lead__c = oContact.Id;
        insert oSandRunSheet;

        // Test Sand Run Sheet Details
        SM_Run_Sheet_Detail__c oSandRunSheetDetail;

        for(Integer iCount = 0; iCount < SAND_STING_COUNT; iCount++)
        {
            oSandRunSheetDetail = new SM_Run_Sheet_Detail__c();
            oSandRunSheetDetail.Priority__c = '2';
            oSandRunSheetDetail.Sand_Level__c = 5;
            oSandRunSheetDetail.Sand_Run_Sheet__c = oSandRunSheet.Id;
            oSandRunSheetDetail.Status__c = 'New';
            oSandRunSheetDetail.Sting_Type__c = 'Production';
            oSandRunSheetDetail.Tank__c = 'Tank ' + (iCount + 1);
            oSandRunSheetDetail.Tank_Enabled__c = true;
            oSandRunSheetDetail.Tank_Level__c = 5;
            oSandRunSheetDetail.Water_Level__c = 7;
            insert oSandRunSheetDetail;
        }


        // Test FLUID Run Sheet
        FM_Run_Sheet__c oFluidRunSheet = new FM_Run_Sheet__c();
        oFluidRunSheet.Date__c = Date.today();
        oFluidRunSheet.Well__c = oLocation2.Id;
        oFluidRunSheet.Act_Flow_Rate__c = 5;
        oFluidRunSheet.Act_Tank_Level__c = 3;
        oFluidRunSheet.Axle__c = '6';
        oFluidRunSheet.Flowline_volume__c = 4;
        oFluidRunSheet.Sour__c = true;
        oFluidRunSheet.Tank__c = 'Tank 2';
        oFluidRunSheet.Tomorrow_Oil__c = 1;
        insert oFluidRunSheet;

        // Test OOC
        Operator_On_Call__c oOperatorOnCall1 = new Operator_On_Call__c();
        oOperatorOnCall1.Operator__c = oUser.Id;
        oOperatorOnCall1.Operator_Route__c = oRoute1.Id;
        insert oOperatorOnCall1;

        oRoute1.Operator_On_Call__c = oOperatorOnCall1.Id;
        update oRoute1;

        Operator_On_Call__c oOperatorOnCall2 = new Operator_On_Call__c();
        oOperatorOnCall2.Operator__c = oUser.Id;
        oOperatorOnCall2.Operator_Route__c = oRoute2.Id;
        insert oOperatorOnCall2;

        oRoute2.Operator_On_Call__c = oOperatorOnCall2.Id;
        update oRoute2;

    
    }
}