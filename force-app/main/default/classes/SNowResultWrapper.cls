public with sharing class SNowResultWrapper
{
    public List <Results> result;
    public SNowResultWrapper(){}
    
    public class Results
    {
        public string sys_id;
        public string u_service_delivery_specialist;
        public string name;
        
        public Results(){}
    }
    
}