/**
 * Created by mzelina@sk.ibm.com on 10/01/2019.
 */

public with sharing class SPCC_EWRDetailCtrlX {

    private SPCC_Engineering_Work_Request__c ewr;
    private Set<Id> bpoIdSet = new Set<Id>();
    private List<bpoWrapper> bpoWrapperList;
    private Map<Id, SPCC_Blanket_Purchase_Order__c> bpoMap;


    public SPCC_EWRDetailCtrlX(ApexPages.StandardController controller) {
        this.ewr = (SPCC_Engineering_Work_Request__c) controller.getRecord();
    }


    //fill the list of related blanket POs
    public List<bpoWrapper> getBPOs() {

        for (SPCC_Service_Entry_Sheet__c ses : [SELECT Blanket_Purchase_Order__c,
                                                          Cost_Element__r.AFE__r.Engineering_Work_Request__c
                                                   FROM SPCC_Service_Entry_Sheet__c
                                                   WHERE Cost_Element__r.AFE__r.Engineering_Work_Request__c = : ewr.Id]){

            bpoIdSet.add(ses.Blanket_Purchase_Order__c);
        }

        bpoMap = new Map<Id, SPCC_Blanket_Purchase_Order__c>([SELECT Vendor_Account__r.Name, Name, Description__c,
                                                                     Estimate_to_Complete__c, CreatedDate
                                                              FROM SPCC_Blanket_Purchase_Order__c
                                                              WHERE Id IN : bpoIdSet]);

        bpoWrapperList = new List<bpoWrapper>();

        for (AggregateResult result : [
                SELECT Blanket_Purchase_Order__c,
                        SUM(Amount__c) totalAmount
                FROM SPCC_Service_Entry_Sheet__c
                WHERE Cost_Element__r.AFE__r.Engineering_Work_Request__c = :ewr.Id
                GROUP BY Blanket_Purchase_Order__c
        ]) {

            SPCC_Blanket_Purchase_Order__c bpo = bpoMap.get((String) result.get('Blanket_Purchase_Order__c'));
            Decimal totalAmount = (Decimal) result.get('totalAmount');
            bpoWrapperList.add(new bpoWrapper(bpo, totalAmount));
        }
        return bpoWrapperList;
    }

    public class bpoWrapper{
        public SPCC_Blanket_Purchase_Order__c bpo {get; set;}
        public Decimal totalAmount {get; set;}

        public bpoWrapper(SPCC_Blanket_Purchase_Order__c bpo, Decimal totalAmount){
        this.bpo = bpo;
        this.totalAmount = totalAmount;
        }
    }
}