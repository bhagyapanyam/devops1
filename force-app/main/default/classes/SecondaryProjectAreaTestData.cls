public with sharing class SecondaryProjectAreaTestData {
		
	public static Secondary_Project_Area__c createSecondaryProjectArea(Id changeRequestId, Id projectAreaId) {
		
		Secondary_Project_Area__c secondaryProjectArea = new Secondary_Project_Area__c();
		secondaryProjectArea.Change_Request__c = changeRequestId;
		secondaryProjectArea.Project_Area__c = projectAreaId;
		
		return secondaryProjectArea;
	}
}