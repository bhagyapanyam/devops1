public  class Shift_Handover_Report_Ctrl {

    //search criteria
    public String dateRange {get;set;}
    public String dFrom { get; set; }
    public String dTo { get; set; }
    public String selectedAreas{get;set;}
    public String selectedUnits {get;set;}
    public String selectedOperators {get;set;}
    public String selectedTeams {get;set;}
    public String selectedRoles {get;set;}
    public List<String> selectedPriority {get;set;}
    public Operational_Event__c opEventVar {get;set;}
    //this is used to keep number of record of each Area
    public transient Map<String, integer> areaMap {get;set;}
    public integer resultNo {get;set;}
    //list of report result
    public transient Map<String, List<Operational_Event__c>> resultMap {get;set;}
    //this is store Team Role of Operational Event: Map<OperationalEventId, Role>
    public transient Map<Id, String> teamRoleMap {get;set;}
    public String selectedDepartment {get;set;}
    public List<SelectOption> departmentList {get;set;}
    
    public String equipmentTagDesc{get;set;}
    
    public Shift_Handover_Report_Ctrl()
    {
        if (ApexPages.currentPage().getParameters().get('startDate') != null 
                && ApexPages.currentPage().getParameters().get('endDate') != null)   
        {
            dFrom = ApexPages.currentPage().getParameters().get('startDate');
            dTo = ApexPages.currentPage().getParameters().get('endDate');  
            if (ApexPages.currentPage().getParameters().get('priority') != null)
            {
                 String priority = ApexPages.currentPage().getParameters().get('priority');
                 System.debug('priority =' + priority);
                 selectedPriority = priority.split(',');
            }
            if (ApexPages.currentPage().getParameters().get('area') != null)
                selectedAreas = ApexPages.currentPage().getParameters().get('area');
            if (ApexPages.currentPage().getParameters().get('unit') != null)   
                selectedUnits = ApexPages.currentPage().getParameters().get('unit'); 
            
            if (ApexPages.currentPage().getParameters().get('operator') != null)   
                selectedOperators = ApexPages.currentPage().getParameters().get('operator'); 
            if (ApexPages.currentPage().getParameters().get('team') != null)   
                selectedTeams = ApexPages.currentPage().getParameters().get('team');    
            
            if (ApexPages.currentPage().getParameters().get('role') != null)   
                selectedRoles = ApexPages.currentPage().getParameters().get('role');             
        }
        else
        {
            //set default search criteria
            dateRange = 'LAST_N_DAYS:7';
            dFrom = datetime.now().addDays(-7).format(DateUtilities.DDMMYYYY_DATE_FORMAT);
            dTo = datetime.now().format(DateUtilities.DDMMYYYY_DATE_FORMAT);
            
        }
        
        selectedPriority = new List<String>{'Routine','Important','Critical'};        
        
        getAllDepartments();
        System.debug('From =' + dFrom + ' to=' + dTo);
        RunReport();
    }
    private void getAllDepartments()
    {
        departmentList = new List<SelectOption>();
        departmentList.add(new SelectOption('','All'));
        departmentList.add(new SelectOption('Operations', 'Operations'));
        departmentList.add(new SelectOption('Maintenance', 'Maintenance'));
        departmentList.add(new SelectOption('Operations Technical Training', 'Operations Technical Training'));
        
        UserRole role = [select DeveloperName from UserRole where Id =: UserInfo.getUserRoleId()];
        if (role.DeveloperName.contains('Operations'))
            selectedDepartment = 'Operations';
        else if (role.DeveloperName.contains('Maintenance'))   
            selectedDepartment = 'Maintenance';
        else if (role.DeveloperName == 'Sunrise' || role.DeveloperName == 'Oil_Sands')
            selectedDepartment = '';
               
        
    }
    private void resetResult() 
    {
         resultMap = new Map<String, List<Operational_Event__c>>();
         areaMap = new Map<String, integer>();
         teamRoleMap = new Map<Id, String>();
         resultNo = 0;
    } 
    public List<SelectOption> getPriorityList()  
    {
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Operational_Event__c.Priority__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
        for( Schema.PicklistEntry f : ple)
        {
          options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
    
    //run report and show the result
    public void RunReport()
    {
        resetResult();
        System.debug('selectedPriority =' + selectedPriority);
        System.debug('From =' + dFrom + '  To =' + dTo);
        Date selectedDFrom = DateUtilities.toDMYYYDate(dFrom, '/', null);
        Date selectedDTo = DateUtilities.toDMYYYDate(dTo, '/', null);
        System.debug( 'selectedDFrom = ' + selectedDFrom + '  ' + selectedDTo);
        if (selectedDFrom == null)
        {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'When does the search start ?'));
           return ;
        }
        if (dTo == null)
        {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'When does the search end ?'));   
           return ;
        }
         
        try
        {
            String soql = 'SELECT Area__c, Area__r.Name, Team_Allocation__c, Team_Allocation__r.Date__c,' 
                           + 'Time__c, Team_Allocation__r.Shift__c, Team_Allocation__r.Shift__r.Name,'
                           + 'Priority__c, Description__c, Operator_Contact__c, Operator_Contact__r.Name,'
                           + 'Unit__r.Name, Equipment_Tag_Description__c, Shift_Resource_Role__c, Shift_Resource_Team__c,'
                           + 'Team_Allocation__r.Team__c, Team_Allocation__r.Team__r.Name, RecordType.Name, Work_Order_Number__c'
                           + ' FROM Operational_Event__c'
                           + ' WHERE Team_Allocation__r.Date__c >= :selectedDFrom  and Team_Allocation__r.Date__c <= :selectedDTo';
            if (selectedPriority.size() > 0)
                soql += ' and Priority__c in :selectedPriority' ;    
            
            System.debug(selectedAreas)    ;
            if (selectedAreas != null && selectedAreas != '')
            {
                String[] areaList = selectedAreas.split(',');
                System.debug(areaList);
                soql += ' and Area__r.Name in :areaList';
            }          
            System.debug(selectedUnits);
            if (selectedUnits != null && selectedUnits != '')
            {
                String[] unitList = selectedUnits.split(',');
                soql += ' and Unit__r.Name in :unitList';
            }
            System.debug('selectedOperators =' + selectedOperators);
            System.debug('selectedRoles =' + selectedRoles);
            if (selectedOperators != null && selectedOperators != '')
            {
                String[] operatorList = selectedOperators.split(',');
                if (selectedRoles == null || selectedRoles == '') 
                    soql += ' and Operator_Contact__r.Name in :operatorList';
            }
      
            if (selectedRoles != null && selectedRoles != '')
            {
                String[] roleList = selectedRoles.split(',');
                soql += ' and Shift_Resource_Role__c in :roleList';
            }
            
           
            if (selectedTeams != null && selectedTeams != '')
            {
                String[] teamList = selectedTeams.split(',');
                soql += ' and Team_Allocation__r.Team__r.Name in :teamList';
            }
            System.debug('selectedDepartment =' + selectedDepartment);
            if (selectedDepartment != '' && selectedDepartment != null)
                soql += ' and RecordType.Name =: selectedDepartment';
            
            if(equipmentTagDesc != null && equipmentTagDesc !='')
            {
                //equipmentTagDesc ='%' + equipmentTagDesc +'%';
                String s ='\'%' + String.escapeSingleQuotes(equipmentTagDesc.trim()) + '%\'';

                soql += ' and Equipment_Tag_Description__c  LIKE  ' + s ;
            }
            
                
            soql += ' ORDER BY Area__r.Name, Team_Allocation__r.Date__c, Time__c';
                           
            System.debug(soql);    
            List<Operational_Event__c> oEvents = Database.query(soql);
            
            if (oEvents != null && oEvents.size() > 0)
            {
                resultNo = oEvents.size();    
                // Load and group all the events by area 
                System.debug('resultNo =' + resultNo);
                for(Operational_Event__c oEvent : oEvents) 
                {
                    System.debug(oEvent.Description__c);
                    
                    oEvent.Description__c = oEvent.Description__c.replaceAll('\n', '<br/>');
                    // Area group
                    String curAreaName = oEvent.Area__r.Name;
                    List<Operational_Event__c> curEventList = new List<Operational_Event__c>();
                    if (resultMap.containsKey(curAreaName)) 
                    {
                        curEventList = resultMap.get(curAreaName);
                        curEventList.add(oEvent);
                    }
                    else
                    {
                       curEventList.add(oEvent);
                       resultMap.put(curAreaName, curEventList);
                    }
                }
                
                for(String areaName : resultMap.keySet())
                {
                   areaMap.put(areaName, resultMap.get(areaName).size());   
                }
            } 
        }
        catch(QueryException ex)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage()));   
        }
        catch(Exception ex)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage()));   
        }
        
    }
    //navigate the page to the PDF view
    public PageReference ExportDetail()
    {
        string url = '/apex/Shift_Handover_Report_Export_Detail?startDate=' + dFrom + '&endDate=' + dTo;
        if (selectedPriority.size() > 0)
            url += '&priority=' + selectedPriority;
        if (selectedAreas != null && selectedAreas != '')
            url += '&area=' + selectedAreas;
        if(selectedUnits != null && selectedUnits != '')
            url += '&unit=' + selectedUnits;    
        if(selectedOperators != null && selectedOperators != '')
            url += '&operator=' + selectedOperators;
        if(selectedTeams != null && selectedTeams != '')
            url += '&team=' + selectedTeams;            
        if (selectedRoles != null && selectedRoles != '')
            url += '&role=' + selectedRoles;    
        PageReference detailPage = new PageReference(url);
        
        return detailPage;
    }
    
}