@isTest
private class StandingOrderTriggerTest {

    @testSetup
    static void setUp() {
        Standing_Order__c stdOrder1 = new Standing_Order__c();
        stdOrder1.Details__c = 'Standing Order 1 details here';
        stdOrder1.Plant__c = 'Oil Processing';
        stdOrder1.Start_Date__c = System.Today() - 365;
        stdOrder1.Completion_Date__c = System.Today() + 365;

        Standing_Order__c stdOrder2 = new Standing_Order__c();
        stdOrder2.Details__c = 'Standing Order 2 details here';
        stdOrder2.Plant__c = 'Field Facilities';
        stdOrder2.Start_Date__c = System.Today() - 365;
        stdOrder2.Completion_Date__c = System.Today();        
        
        Standing_Order__c stdOrder3 = new Standing_Order__c();
        stdOrder3.Details__c = 'Standing Order 3 details here';
        stdOrder3.Plant__c = 'Water De-Oiling';
        stdOrder3.Start_Date__c = System.Today() - 365;
        stdOrder3.Completion_Date__c = null;        
        
        Standing_Order__c stdOrder4 = new Standing_Order__c();
        stdOrder4.Details__c = 'Standing Order 4 details here';
        stdOrder4.Plant__c = 'Steam Generation';
        stdOrder4.Start_Date__c = System.Today() - 365;
        stdOrder4.Completion_Date__c = System.Today() - 20;        

        insert stdOrder1;
        insert stdOrder2;
        insert stdOrder3;
        insert stdOrder4;

        Standing_Order_Acknowledgement__c ack = new Standing_Order_Acknowledgement__c();
        ack.Standing_Order__c = stdOrder1.Id;
        ack.Acknowledged_User__c = UserInfo.getUserId();
        ack.Read_Acknowledged__c = true;
        ack.Read_Acknowledged_Date__c = System.Now();

        insert ack;
    }

    @isTest
    static void testResetReadAcknowledgement_Edit_StartDate() {
        User u = TestUtilities.createOperationsUser();

        System.runAs(u) {
            List<Standing_Order__c> standingOrders = [SELECT Id, Details__c, Plant__c, Start_Date__c, Completion_Date__c FROM Standing_Order__c];

            List<Standing_Order_Acknowledgement__c> acknowledgements = [SELECT Id, Standing_Order__c, Acknowledged_User__c, Read_Acknowledged__c, Read_Acknowledged_Date__c 
                                                                            FROM Standing_Order_Acknowledgement__c];

            System.assertEquals(4, standingOrders.size());
            System.assertEquals(1, acknowledgements.size());
            System.assertEquals(true, acknowledgements[0].Read_Acknowledged__c);
            System.assertNotEquals(null, acknowledgements[0].Read_Acknowledged_Date__c);

            Standing_Order__c stdOrder = new Standing_Order__c();
            stdOrder.Id = acknowledgements[0].Standing_Order__c;
            stdOrder.Start_Date__c = System.Today();

            update stdOrder;

            acknowledgements = [SELECT Id, Standing_Order__c, Acknowledged_User__c, Read_Acknowledged__c, Read_Acknowledged_Date__c 
                                FROM Standing_Order_Acknowledgement__c]; 

            System.assertEquals(1, acknowledgements.size());
            System.assertEquals(false, acknowledgements[0].Read_Acknowledged__c);
            System.assertEquals(null, acknowledgements[0].Read_Acknowledged_Date__c);   
            
        }
    }

    @isTest
    static void testResetReadAcknowledgement_Edit_WithNoChanges() {
        User u = TestUtilities.createOperationsUser();

        System.runAs(u) {
            List<Standing_Order__c> standingOrders = [SELECT Id, Details__c, Plant__c, Start_Date__c, Completion_Date__c FROM Standing_Order__c];

            List<Standing_Order_Acknowledgement__c> acknowledgements = [SELECT Id, Standing_Order__c, Acknowledged_User__c, Read_Acknowledged__c, Read_Acknowledged_Date__c 
                                                                            FROM Standing_Order_Acknowledgement__c];

            System.assertEquals(4, standingOrders.size());
            System.assertEquals(1, acknowledgements.size());
            System.assertEquals(true, acknowledgements[0].Read_Acknowledged__c);
            System.assertNotEquals(null, acknowledgements[0].Read_Acknowledged_Date__c);

            //Just do a dummy update on the record. This should not trigger anything in read acknowledgements
            Standing_Order__c stdOrder = new Standing_Order__c();
            stdOrder.Id = acknowledgements[0].Standing_Order__c;

            update stdOrder;

            acknowledgements = [SELECT Id, Standing_Order__c, Acknowledged_User__c, Read_Acknowledged__c, Read_Acknowledged_Date__c 
                                FROM Standing_Order_Acknowledgement__c]; 

            System.assertEquals(1, acknowledgements.size());
            System.assertEquals(true, acknowledgements[0].Read_Acknowledged__c);
            System.assertNotEquals(null, acknowledgements[0].Read_Acknowledged_Date__c);
            
        }
    }    

}