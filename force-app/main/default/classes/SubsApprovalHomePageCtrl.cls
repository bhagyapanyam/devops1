public class SubsApprovalHomePageCtrl 
{
	@AuraEnabled
    public static List<TableWrapper> getUserPendingProcess(String sortField, Boolean isAsc)
    {
        Id userId = userInfo.getUserId();
        //System.debug('Current User Id - ' + userId);
        system.debug('sortField -- ' + sortField);
        List<ProcessInstance> prcInstList = new List<ProcessInstance>();
        Set<Id> targetObjIdSet = new Set<Id>();
        Map<Id,ProcessInstance> targetIdProcessMap = new Map<Id,ProcessInstance>();
        Map<Id,jbrvrm__Subscription_Order__c>  subsOrdMap = new Map<Id,jbrvrm__Subscription_Order__c>();
        Map<Id,String> sObjectIdNameMap = new Map<Id,String>();
        List<jbrvrm__Subscription_Order__c> subsOrderList = new List<jbrvrm__Subscription_Order__c>();
        String sSoql = '';
        Set<Id> targetIdSet = new Set<Id>();
        prcInstList = [SELECT Id, TargetObjectId, TargetObject.Name, Createddate, (SELECT Id, CreatedDate, OriginalActor.Name, Actor.Name FROM Workitems where ActorId =: userId)FROM ProcessInstance where status = 'Pending'];
    	//system.debug('prcInstList - ' + prcInstList);
        List<TableWrapper> twList = new List<TableWrapper>();
        if(prcInstList.size()>0)
        {
            for(ProcessInstance prc : prcInstList)
            {
                targetIdProcessMap.put(prc.TargetObjectId,prc);
                String sobjectType = prc.TargetObjectId.getSObjectType().getDescribe().getName();
                sObjectIdNameMap.put(prc.TargetObjectId,sobjectType);
                targetIdSet.add(prc.TargetObjectId);
            }
            if(sortField == '')
            	subsOrdMap = new Map<Id,jbrvrm__Subscription_Order__c>([Select Id, Name, jbrvrm__Annual_Amount__c, jbrvrm__Start_Date__c, jbrvrm__End_Date__c, jbrvrm__Currency__r.Name from jbrvrm__Subscription_Order__c where id in: targetIdSet]);
            else
            {
                if(sortField=='RelatedTo' || sortField=='Name')
        			sortField= 'Name';
                else if(sortField=='Amount')
                    sortField = 'jbrvrm__Annual_Amount__c';
                else if(sortField == 'Currency')
                    sortField = 'jbrvrm__Currency__r.Name';
                else if(sortField == 'StartDate')
                    sortField = 'jbrvrm__Start_Date__c';
                else if(sortField == 'EndDate')
                    sortField = 'jbrvrm__End_Date__c'; 
            
                sSoql = 'SELECT Id, Name, jbrvrm__Annual_Amount__c, jbrvrm__Start_Date__c, jbrvrm__End_Date__c, jbrvrm__Currency__r.Name from jbrvrm__Subscription_Order__c where id in: targetIdSet';
                
                sSoql += ' order by ' + sortField;
                
                if (isAsc) 
                {sSoql += ' asc';} 
                else 
                {sSoql += ' desc';}
                
                
                try 
                {
                    system.debug('The query is' + sSoql);
                    subsOrderList = Database.query(sSoql);
                    System.debug('subsOrderList --- ' + subsOrderList);
                    for(jbrvrm__Subscription_Order__c so: subsOrderList)
                    {
                        subsOrdMap.put(so.Id,so);
                    }
                }
                catch (Exception ex) 
                {
                 // for handle Exception
                 System.debug('Exception --- ' + ex.getMessage());
                }
            }
            
            System.debug('subsOrdMap --- ' + subsOrdMap);
        	System.debug('targetIdProcessMap --- ' + targetIdProcessMap);
            //System.debug('Related To ----------- Type --------- Subcription Order ------- Amount ------- Currency -------- Start Date -------- End Date -------- Approver --------- Submitted Date');
            String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
            if(targetIdProcessMap.size() > 0)
            {
                /*for(Id trgId : targetIdProcessMap.keySet())
                {
                    if(targetIdProcessMap.get(trgId).Workitems.size()>0 && subsOrdMap.containskey(trgId))
                    {
                        ID relatedToId = targetIdProcessMap.get(trgId).Workitems[0].Id;
                        String relatedTo = baseUrl+'/lightning/r/ProcessInstanceWorkitem/'+relatedToId+'/view';
                        
                        TableWrapper twObj = new TableWrapper();
                        twObj.prcWrap = targetIdProcessMap.get(trgId);
                        twObj.relatedToWrap = relatedTo;
                        twObj.actorIdWrap = baseUrl+'/lightning/r/'+targetIdProcessMap.get(trgId).Workitems[0].ActorId+'/view';
                        twObj.subsIdWrap = baseUrl+'/lightning/r/'+sObjectIdNameMap.get(trgId)+'/'+subsOrdMap.get(trgId).Id+'/view';
                        twObj.subOrdWrap = subsOrdMap.get(trgId);
                        
                        twList.add(twObj);
                        
                    }
                } */
                
                for(Id subOrdId : subsOrdMap.keySet())
                {
                    if(targetIdProcessMap.get(subOrdId).Workitems.size()>0 && targetIdProcessMap.containskey(subOrdId))
                    {
                        System.debug('subOrdId --- ' + subOrdId);
                        ID relatedToId = targetIdProcessMap.get(subOrdId).Workitems[0].Id;
                        String relatedTo = baseUrl+'/lightning/r/ProcessInstanceWorkitem/'+relatedToId+'/view';
                        
                        TableWrapper twObj = new TableWrapper();
                        twObj.prcWrap = targetIdProcessMap.get(subOrdId);
                        twObj.relatedToWrap = relatedTo;
                        twObj.actorIdWrap = baseUrl+'/lightning/r/'+targetIdProcessMap.get(subOrdId).Workitems[0].ActorId+'/view';
                        twObj.subsIdWrap = baseUrl+'/lightning/r/'+sObjectIdNameMap.get(subOrdId)+'/'+subsOrdMap.get(subOrdId).Id+'/view';
                        twObj.subOrdWrap = subsOrdMap.get(subOrdId);
                        
                        twList.add(twObj);
                    }
                }
            }
        }
        return twList;
    }
    
    public class TableWrapper
    {
        @AuraEnabled public ProcessInstance prcWrap;
        @AuraEnabled public String relatedToWrap;
        @AuraEnabled public String subsIdWrap;
        @AuraEnabled public String actorIdWrap;
        @AuraEnabled public jbrvrm__Subscription_Order__c subOrdWrap;
        
        public TableWrapper()
        {
            prcWrap = new ProcessInstance();
            relatedToWrap = '';
            subsIdWrap = '';
            actorIdWrap = '';
            subOrdWrap = new jbrvrm__Subscription_Order__c();
        }
    }
}