/******************************************************
 * Manages the life cycle of team allocations 
 * 1. Temporarily expires the active one if elapsed.
 * 2. Permanently expires the temporarily expired one 
 *    if grace period has elapsed.
 * 3. Creates a new active one for the current shift.
 ******************************************************/
public class TeamAllocationLifecycleManager {
    
    @future
    public static void process() {
        // Lookup current time
        Datetime rightNow = Datetime.now(); 
        
        // In-activate the active-expired allocation if obsolete.
        List<Team_Allocation__c> activeExpiredTAs = TeamAllocationUtilities.getActiveExpiredTeamAllocations();
        if (activeExpiredTAs != null) 
        {
        	for(Team_Allocation__c ta : activeExpiredTAs)
        	{
	            Shift_Configuration__c activeExpiredTAShiftConfig = ShiftConfigurationUtilities.lookupShift(ta.Shift__c);
	            Datetime activeExpiredTAEndTime = TeamAllocationUtilities.getEndDatetime(ta, activeExpiredTAShiftConfig);
	            Long elapsedTime = (rightNow.getTime() - activeExpiredTAEndTime.getTime());
	            
	            System.debug('\n******************************\n' 
	                 + 'rightNow = ' + rightNow
	                 + '\n'
	                 + 'activeExpiredTAEndTime = ' + activeExpiredTAEndTime
	                 + '\n'
	                 + '(rightNow.getTime() - activeExpiredTAEndTime.getTime()) = ' + elapsedTime
	                 + '\n'
	                 + 'ACTIVE_EXPIRED_DURATION_IN_MILLIS = ' + TeamAllocationUtilities.ACTIVE_EXPIRED_DURATION_IN_MILLIS
	                 + '\n******************************\n');
	            
	            if (elapsedTime >= TeamAllocationUtilities.ACTIVE_EXPIRED_DURATION_IN_MILLIS) {
	                // Flip TA's status to inactive.
	                ta.Status__c = TeamAllocationUtilities.INACTIVE_STATUS_NAME;
	                
	            }  
        	} 
        	update activeExpiredTAs;         
        }
        
        // In-activate or partially expire the active TA if obsolete.
    //    Boolean activeTAExpired = False;
        List<Team_Allocation__c> activeTAs = TeamAllocationUtilities.getActiveTeamAllocations();
        if (activeTAs.size() > 0) 
        {
        	System.debug('activeTAs size =' + activeTAs);
        	for(Team_Allocation__c ta : activeTAs)
        	{
	            Shift_Configuration__c activeTAShiftConfig = ShiftConfigurationUtilities.lookupShift(ta.Shift__c);
	            Datetime activeTAEndTime = TeamAllocationUtilities.getEndDatetime(ta, activeTAShiftConfig);
	
	            if (rightNow.getTime() >= activeTAEndTime.getTime()) 
	            {
	                Long elapsedTime = (rightNow.getTime() - activeTAEndTime.getTime());
	
	                System.debug('\n******************************\n' 
	                    + 'rightNow = ' + rightNow
	                    + '\n'
	                    + 'activeTAEndTime = ' + activeTAEndTime
	                    + '\n'
	                    + '(rightNow.getTime() - activeTAEndTime.getTime()) = ' + elapsedTime
	                    + '\n'
	                    + 'ACTIVE_EXPIRED_DURATION_IN_MILLIS = ' + TeamAllocationUtilities.ACTIVE_EXPIRED_DURATION_IN_MILLIS
	                    + '\n******************************\n');
	                
	                // Shift has elapsed.
	                if ((rightNow.getTime() - activeTAEndTime.getTime()) <= TeamAllocationUtilities.ACTIVE_EXPIRED_DURATION_IN_MILLIS) {
	                    // Flip TA's status to active-expired.
	                    ta.Status__c = TeamAllocationUtilities.ACTIVE_EXPIRED_STATUS_NAME;
	                  //  update activeTA;
	                } else {
	                    // Flip TA's status to inactive.
	                    ta.Status__c = TeamAllocationUtilities.INACTIVE_STATUS_NAME;
	                  //  update activeTA;
	                }
	                
	               // activeTAExpired = True;
	            } 
        	} 
        	update activeTAs;
        }
        Map<String, Schema.Recordtypeinfo> shiftRTMap = Shift_Configuration__c.SObjectType.getDescribe().getRecordTypeInfosByName();
        for(String rtName : shiftRTMap.keySet())
        {
        	List<Team_Allocation__c> activeTAsByRT = TeamAllocationUtilities.getActiveTeamAllocationsByType(rtName);
        	createActiveTAs(activeTAsByRT, rtName);
        }
        
    }
    private static void createActiveTAs(List<Team_Allocation__c> activeTAs, String rtName)
    {
    	Datetime rightNow = Datetime.now(); 
    	// Create active TA
        if ((activeTAs == null || activeTAs.size() == 0) ) 
        {
            System.debug('create active TAs');
            // Lookup active shift right now.
            List<Shift_Configuration__c> activeShiftConfig = ShiftConfigurationUtilities.findActiveShift(rightNow, rtName);
            System.debug('active Shift Config =' + activeShiftConfig);
            List<Team_Allocation__c> newTAList = new List<Team_Allocation__c>();
            Map<String, Schema.Recordtypeinfo> recTypesByName = Team_Allocation__c.SObjectType.getDescribe().getRecordTypeInfosByName();
            if (activeShiftConfig != null) { 
                for(Shift_Configuration__c shift : activeShiftConfig)
                {
                    // Create new active TA.
                    Team_Allocation__c newTA = new Team_Allocation__c();
                    newTA.Status__c = TeamAllocationUtilities.ACTIVE_STATUS_NAME;
                    newTA.Date__c = Date.newInstance(rightNow.year(), rightNow.month(), rightNow.day());
                    newTA.Shift__c = shift.ID;
                    newTa.Operating_District__c = shift.Operating_District__c;
                    
                    newTA.RecordTypeId = recTypesByName.get(shift.RecordType.Name).getRecordTypeId();
                    newTAList.add(newTA);
                }
                insert newTAList;
            }
        }   
    }
}