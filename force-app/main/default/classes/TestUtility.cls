// ====================================================================
// Helper class for test methods
@isTest
public with sharing class TestUtility {
	
	
	// ------------------------------------------------------------------
	// Generate a fake ID for given object type
	public static String getFakeId (Schema.Sobjecttype objectType, 
	                                Integer idNum) {
		String result = String.valueOf(idNum);
		return objectType.getDescribe().getKeyPrefix() + 
		       '0'.repeat(12 - result.length()) + result;
	}

}