@isTest
private class TicketEmailResponseHandler_Test
{
    static Testmethod void testEDSEmail()
    { 
        Milestone1_Project__c project= new Milestone1_Project__c(Name='Test Project');
        insert project;
        Ticket__c ticketRecord = new Ticket__c(Project__c = project.Id);
        ticketRecord.Ticket_Stage__c = 'New';
        ticketRecord.Requester_Email__c='test@test.com';
        insert ticketRecord;
        String subject = [Select Name from Ticket__c where Id=:ticketRecord.Id][0].Name;
        System.debug(subject);
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        // add an Binary attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
        
        
        
        // add an Text atatchment
        
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
        email.FromName='Test';
        email.FromAddress='test@test.com';
        email.Subject='RE: Ticket:'+subject;
        List<String> ccAddr = new List<String>{'abc@abc.com'};
        List<String> toAddr = new List<String>{'xyz@abc.com'};
        email.CcAddresses=ccAddr;
        email.ToAddresses=toaddr;
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        
        TicketEmailResponseHandler handler= new TicketEmailResponseHandler();
        handler.handleInboundEmail(email, envelope);
        Milestone1_Project__c projRecord = new Milestone1_Project__c(Name='Unassigned Project', RecordTypeId = SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Project - Ticketing App').getRecordTypeId());
        insert projRecord;
        handler.handleInboundEmail(email, envelope);
        String lengthyLastName = 'This is Length and invalid last name given to a contact to check if the exception is raised.';
        email.FromName=lengthyLastName;  
        email.FromAddress='test@test2.com';
        handler.handleInboundEmail(email, envelope);
    }
}