/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Service class that handles file manipulation for Vent Gas Alerts
Test Class:     VG_AlertFileServiceTest
History:        jschn 23/01/2020 - Created.
*************************************************************************************************/
public inherited sharing class VG_AlertFileService {

    private static final String CLASS_NAME = String.valueOf(VG_AlertFileService.class);

    /**
     * Parse JSON serialized list of VG_File records, builds a ContentVersion from them and inserts them.
     * If fails, Exception is thrown.
     *
     * @param files
     * @param alertId
     *
     * @return Boolean
     */
    public Boolean saveFiles(String files, Id alertId) {
        List<ContentVersion> cvs = new List<ContentVersion>();

        try {
            List<VG_File> parsedFiles = (List<VG_File>) JSON.deserialize(files, List<VG_File>.class);

            for (VG_File file : parsedFiles) {
                cvs.add(buildFile(file, alertId));
            }

            insert cvs;

        } catch(Exception ex) {
            System.debug(CLASS_NAME + ' -> saveFiles. Exception: ' +ex.getMessage());
            throw new HOG_Exception(Label.VG_File_Upload_Error);
        }

        return true;
    }

    /**
     * Builds File (ContentVersion) record based on VG_File param and ID of parent
     *
     * @param file
     * @param alertId
     *
     * @return ContentVersion
     */
    private ContentVersion buildFile(VG_File file, Id alertId) {
        ContentVersion cv = new ContentVersion();

        cv.Title = file.name;
        cv.PathOnClient = '/' + file.name;
        cv.FirstPublishLocationId = alertId;
        cv.VersionData = EncodingUtil.base64Decode(EncodingUtil.urlDecode(file.fileContents, 'UTF-8'));
        cv.IsMajorVersion = true;

        return cv;
    }

}