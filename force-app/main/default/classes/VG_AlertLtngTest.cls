/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    HOG Service class for lightning containing endpoints for Custom Lookup component.
History:        jschn 07.20.2018 - Created.
**************************************************************************************************/
@IsTest private class VG_AlertLtngTest {

    public static final String CLASS_NAME = 'VG_AlertLtngTest';
	
	@IsTest static void testGetAlert_v1_withCorrectIdStringWithPermission_success() {
		HOG_Vent_Gas_Alert__c alert = createAlert();
		HOG_SelectResponse response;
		User usr = [SELECT Id FROM User WHERE Alias = 'IronMan'];

		Test.startTest();
		System.runAs(usr) {
			response = (HOG_SelectResponseImpl) VG_AlertLtng.getAlert_v1(alert.Id);
		}
		Test.stopTest();

		System.assert(response != null);
		System.assert(response.success);
		System.assert(response.errors == null);
		System.assert(response.resultObjects != null);
		System.assertEquals(1, response.resultObjects.size());
		HOG_Vent_Gas_Alert__c resultAlert = (HOG_Vent_Gas_Alert__c) response.resultObjects.get(0);
		System.assertEquals(alert.Id, resultAlert.Id);
		System.assertEquals(alert.Name, resultAlert.Name);
	}

	@IsTest static void testGetAlert_v1_withCorrectIdString_noRecordFail() {

		Test.startTest();
		HOG_SelectResponse response = (HOG_SelectResponseImpl) VG_AlertLtng.getAlert_v1('1234566789abcdef');
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObjects == null);
		System.assertEquals(1, response.errors.size());
	}

	@IsTest static void testGetAlert_v1_withWrongIdString() {

		Test.startTest();
		HOG_SelectResponse response = (HOG_SelectResponseImpl) VG_AlertLtng.getAlert_v1('abc');
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObjects == null);
		System.assertEquals(1, response.errors.size());
	}

	@IsTest static void testGetAlert_v1_withoutParam() {

		Test.startTest();
		HOG_SelectResponse response = (HOG_SelectResponseImpl) VG_AlertLtng.getAlert_v1(null);
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObjects == null);
		System.assertEquals(1, response.errors.size());
	}

	@IsTest static void testUpsertAlert_v1_withoutParam() {

		Test.startTest();
		HOG_DMLResponse response = (HOG_DMLResponse) VG_AlertLtng.upsertAlert_v1(null);
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObject == null);
		System.assertEquals(1, response.errors.size());
	}

	@IsTest static void testUpsertAlert_v1_withWrongParam() {

		Test.startTest();
		HOG_DMLResponse response = (HOG_DMLResponse) VG_AlertLtng.upsertAlert_v1('{wrongJSON}');
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObject == null);
		System.assertEquals(1, response.errors.size());
	}

	@IsTest static void testUpsertAlert_v1_withoutId() {
		HOG_Vent_Gas_Alert__c alert = createAlert();
		alert.Id = null;

		Test.startTest();
		HOG_DMLResponse response = (HOG_DMLResponse) VG_AlertLtng.upsertAlert_v1(JSON.serialize(alert));
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObject == null);
		System.assertEquals(1, response.errors.size());
	}

	@IsTest static void testUpsertAlert_v1_withWrongUser() {
		HOG_Vent_Gas_Alert__c alert = createAlert();
		alert.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS;
		User runningUser = [SELECT Id FROM User WHERE Alias = 'dude'];
		HOG_DMLResponse response;

		Test.startTest();
		System.runAs(runningUser) {
			response = (HOG_DMLResponse) VG_AlertLtng.upsertAlert_v1(JSON.serialize(alert));
		}
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObject == null);
		System.assertEquals(1, response.errors.size());
		System.assertNotEquals(alert.Status__c, [SELECT Status__c FROM HOG_Vent_Gas_Alert__c WHERE Id =: alert.Id].Status__c);
	}

	@IsTest static void testUpsertAlert_v1_withCorrectUser() {
		HOG_Vent_Gas_Alert__c alert = createAlert();
		alert.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS;

		User runningUser = [SELECT Id FROM User WHERE Alias = 'IronMan'];
		HOG_DMLResponse response;

		Test.startTest();
		System.runAs(runningUser) {
			response = (HOG_DMLResponse) VG_AlertLtng.upsertAlert_v1(JSON.serialize(alert));
		}
		Test.stopTest();

		System.assert(response != null);
		System.assert(response.success);
		System.assert(response.errors == null);
		System.assert(response.resultObject != null);
		System.assertEquals(alert, response.resultObject);
		System.assertEquals(alert.Status__c, [SELECT Status__c FROM HOG_Vent_Gas_Alert__c WHERE Id =: alert.Id].Status__c);
	}

	@IsTest static void testGetUserPermissionsForAlert_withoutParam() {

		Test.startTest();
		HOG_CustomResponse response = (HOG_CustomResponseImpl) VG_AlertLtng.getUserPermissionsForAlert(null);
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObjects == null);
		System.assertEquals(1, response.errors.size());
	}

	@IsTest static void testGetUserPermissionsForAlert_withWrongParam() {

		Test.startTest();
		HOG_CustomResponse response = (HOG_CustomResponseImpl) VG_AlertLtng.getUserPermissionsForAlert('abc');
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObjects == null);
		System.assertEquals(1, response.errors.size());
	}

	@IsTest static void testGetUserPermissionsForAlert_wrongUser() {
		HOG_Vent_Gas_Alert__c alert = createAlert();
		User runningUser = [SELECT Id FROM User WHERE Alias = 'dude'];
		HOG_CustomResponse response;

		Test.startTest();
		System.runAs(runningUser) {
			response = (HOG_CustomResponseImpl) VG_AlertLtng.getUserPermissionsForAlert(alert.Id);
		}
		Test.stopTest();

		System.assert(response != null);
		System.assert(response.success);
		System.assert(response.errors == null);
		System.assert(response.resultObjects != null);
		System.assertEquals(1, response.resultObjects.size());
		VG_AlertLtng.UserPermissionsWrapper result = (VG_AlertLtng.UserPermissionsWrapper) response.resultObjects.get(0);
		System.assert(!result.canEditAlert);
	}

	@IsTest static void testGetUserPermissionsForAlert_assignedUser() {
		HOG_Vent_Gas_Alert__c alert = createAlert();
		User runningUser = [SELECT Id FROM User WHERE Alias = 'IronMan'];
		HOG_CustomResponse response;

		Test.startTest();
		System.runAs(runningUser) {
			response = (HOG_CustomResponseImpl) VG_AlertLtng.getUserPermissionsForAlert(alert.Id);
		}
		Test.stopTest();

		System.assert(response != null);
		System.assert(response.success);
		System.assert(response.errors == null);
		System.assert(response.resultObjects != null);
		System.assertEquals(1, response.resultObjects.size());
		VG_AlertLtng.UserPermissionsWrapper result = (VG_AlertLtng.UserPermissionsWrapper) response.resultObjects.get(0);
		System.assert(result.canEditAlert);
	}

	@IsTest static void testGetUserPermissionsForAlert_adminUser() {
		HOG_Vent_Gas_Alert__c alert = createAlert();
		User runningUser = [SELECT Id FROM User WHERE Alias = 'eyepatch'];
		HOG_CustomResponse response;

		Test.startTest();
		System.runAs(runningUser) {
			response = (HOG_CustomResponseImpl) VG_AlertLtng.getUserPermissionsForAlert(alert.Id);
		}
		Test.stopTest();

		System.assert(response != null);
		System.assert(response.success);
		System.assert(response.errors == null);
		System.assert(response.resultObjects != null);
		System.assertEquals(1, response.resultObjects.size());
		VG_AlertLtng.UserPermissionsWrapper result = (VG_AlertLtng.UserPermissionsWrapper) response.resultObjects.get(0);
		System.assert(result.canEditAlert);
	}

	@TestSetup
	private static void createUser() {
		User usr = HOG_VentGas_TestData.createUser();
		usr = HOG_VentGas_TestData.assignPermissionSet(usr, 'HOG_Production_Engineer');
		HOG_VentGas_TestData.createUser('testUser', 'forVG', 'dude');
		HOG_VentGas_TestData.createAdmin();
	}

	private static HOG_Vent_Gas_Alert__c createAlert() {
		User usr = [SELECT Id FROM User WHERE Alias = 'IronMan'];
		Location__c loc = HOG_VentGas_TestData.createLocation();
		HOG_Vent_Gas_Alert__c alert = HOG_VentGas_TestData.createAlert(
				HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST,'TestDescription',
				loc.Id,
				usr.Id,
				HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM,
				HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED,
				HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST
		);
		return alert;
	}
	
}