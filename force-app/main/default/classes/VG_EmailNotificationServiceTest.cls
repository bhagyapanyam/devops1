/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for VG_EmailNotificationService
History:        jschn 24/01/2020 - Created.
*************************************************************************************************/
@IsTest
private class VG_EmailNotificationServiceTest {

    @IsTest
    static void constructor_withoutSettings() {
        new VG_EmailNotificationService();
    }

    @IsTest
    static void constructor_withWrongSettings() {
        VG_EmailNotificationService.settings.Default_Notification_Sender_Name__c = 'somethingBad';
        new VG_EmailNotificationService();
    }

    @IsTest
    static void constructor_withGoodSettings() {
        List<OrgWideEmailAddress> addresses = [SELECT DisplayName, Address FROM OrgWideEmailAddress LIMIT 1];
        VG_EmailNotificationService.settings.Default_Notification_Sender_Name__c = addresses.get(0).DisplayName;
        new VG_EmailNotificationService();
    }

    @IsTest
    static void buildNotifications_nullParam() {
        VG_EmailNotificationService service = new VG_EmailNotificationService().buildNotifications(null);

        System.assertNotEquals(null, service.emailList);
    }

    @IsTest
    static void buildNotifications_emptyParam() {
        List<HOG_Vent_Gas_Alert__c> alerts = new List<HOG_Vent_Gas_Alert__c>();
        VG_EmailNotificationService service = new VG_EmailNotificationService().buildNotifications(alerts);

        System.assertNotEquals(null, service.emailList);
    }

    @IsTest
    static void buildNotifications_mockedAlert() {
        List<HOG_Vent_Gas_Alert__c> alerts = new List<HOG_Vent_Gas_Alert__c>{
                new HOG_Vent_Gas_Alert__c(
                        Name = 'Test',
                        Type__c = HOG_VentGas_Utilities.ALERT_TYPE_ENG_NOTIF,
                        Status__c = HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED,
                        Comments__c = 'TestComments',
                        Well_Location__c = null,
                        Well_Location__r = new Location__c(Name = 'TestLocation'),
                        Production_Engineer__c = UserInfo.getUserId(),
                        Production_Engineer__r = new User(),
                        Priority__c = HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM
                )
        };
        VG_EmailNotificationService service = new VG_EmailNotificationService().buildNotifications(alerts);

        System.assertNotEquals(null, service.emailList);
    }

    @IsTest
    static void sendNotifications_withRecords() {
        List<HOG_Vent_Gas_Alert__c> alerts = new List<HOG_Vent_Gas_Alert__c>{
                new HOG_Vent_Gas_Alert__c(
                        Name = 'Test',
                        Type__c = HOG_VentGas_Utilities.ALERT_TYPE_ENG_NOTIF,
                        Status__c = HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED,
                        Comments__c = 'TestComments',
                        Well_Location__c = null,
                        Well_Location__r = new Location__c(Name = 'TestLocation'),
                        Production_Engineer__c = UserInfo.getUserId(),
                        Production_Engineer__r = new User(),
                        Priority__c = HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM
                )
        };
        new VG_EmailNotificationService().buildNotifications(alerts).sendNotifications();
    }

    @IsTest
    static void sendNotifications_withoutRecords() {
        List<HOG_Vent_Gas_Alert__c> alerts = new List<HOG_Vent_Gas_Alert__c>();
        new VG_EmailNotificationService().buildNotifications(alerts).sendNotifications();
    }

}