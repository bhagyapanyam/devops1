/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Database access interface for VTT Calendar

History:        mbrimus 2019-09-24 - Created.
*************************************************************************************************/
public interface VTT_LTNG_CalendarDAO {

    /**
     * Returns List of entries for tradesman on selected date
     * @return  List of Work_Order_Activity_Log_Entry__c
     */
    List<Work_Order_Activity_Log_Entry__c> getLogEntriesForTradesmanOnDate(String tradesmanId, Date currDate);

    /**
     * Return list of account for selection on Calendar
     *
     * @return List of Accounts
     */
    List<Account> getVendorAccounts();

    /**
     * Return list of contacts (tradesman) for selected vendor (account
     *
     * @return List of Contact
     */
    List<Contact> getTradesmanForAccount(String accountId);

    /**
     * Return list of activity logs for tradesman
     *
     * @return List of Work_Order_Activity_Log__c
     */
    List<Work_Order_Activity_Log__c> getActivityLogsForTradesman(String tradesmanID, Date startDate, Date endDate);

    /**
     * Return list of AutoCompleted LogEntries in Set of ids
     * @return List of Work_Order_Activity_Log__c
     */
    List<Work_Order_Activity_Log__c> getAutoCompletedActivities(Set<Id> workLogActivitySet);

    /**
     * Return list of Scheduled Activities for tradesman except ones in Set of Ids
     *
     * @param workedActivities
     * @param tradesmanId
     *
     * @return
     */
    List<Work_Order_Activity__c> getScheduledActivities(Set<Id> workedActivities, String tradesmanId, Date startDate, Date endDate);

    /**
     * Loads onHold activities for tradesman
     *
     * @param tradesmanID
     *
     * @return
     */
    List<Work_Order_Activity__c> getOnHoldActivities(String tradesmanID);

    /**
     * Returns contact tied to currently logged in user
     * @return  List of Work_Order_Activity_Log_Entry__c
     */
    Contact getTradesmanInfo();

    /**
     * Return Map of Permissionsets used by admin, internal/external supervisor
     *
     * @return
     */
    Map<Id, PermissionSet> getSupervisorPermissionSets();

    /**
     * Returns list of psa assignements for current user where PS is in set of ps IDs
     *
     * @param psIds
     *
     * @return
     */
    List<PermissionSetAssignment> getPermissionSetAssignmentsOfCurrentUser(Set<Id> psIds);

    /**
     * Returns flag if current User is System Admin
     *
     * @return
     */
    Boolean isUserSystemAdmin();
}