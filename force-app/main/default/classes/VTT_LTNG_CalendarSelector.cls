/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Database queries for VTT Lightning Calendar
Test Class:     VTT_LTNG_CalendarSelectorTest
History:        mbrim 2019-09-03 - Created.
*************************************************************************************************/
public inherited sharing class VTT_LTNG_CalendarSelector implements VTT_LTNG_CalendarDAO {

    public List<Work_Order_Activity_Log_Entry__c> getLogEntriesForTradesmanOnDate(String tradesmanId, Date currDate) {
        return [
                SELECT Id, Status__c, Work_Order_Activity__r.Name, TimeStamp__c, Duration_Hours__c,
                        Include_in_Calculation__c, OnEquipment__c, OffEquipment__c, Duration_Text__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Number__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Location__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Well_Event__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Functional_Equipment_Level__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Yard__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Sub_System__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.System__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Facility__r.Name,
                        Work_Order_Activity__r.Status__c,Work_Order_Activity__r.User_Status__c,
                        Work_Order_Activity__r.Operating_Field_AMU__c, Work_Order_Activity__r.Operating_Field_AMU__r.Is_Thermal__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c
                FROM Work_Order_Activity_Log_Entry__c
                WHERE Work_Order_Activity_Log_Lookup__r.Tradesman__c = :tradesmanId
                AND Date__c = :currDate
                //mz 3-Jan-18
                AND (((Work_Order_Activity__r.Operating_Field_AMU__c != NULL
                AND ((Work_Order_Activity__r.Operating_Field_AMU__r.Is_Thermal__c = TRUE
                AND (Work_Order_Activity__r.User_Status__c LIKE '2SCH %FIX%'
                OR Work_Order_Activity__r.User_Status__c LIKE '3BEX%'
                OR (Work_Order_Activity__r.User_Status__c LIKE '4COM%'
                AND Work_Order_Activity__r.Status__c = 'Completed')))
                OR Work_Order_Activity__r.Operating_Field_AMU__r.Is_Thermal__c = FALSE)))
                OR ((Work_Order_Activity__r.Operating_Field_AMU__c = NULL
                AND Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c != NULL
                AND ((Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c = TRUE
                AND (Work_Order_Activity__r.User_Status__c LIKE '2SCH %FIX%'
                OR Work_Order_Activity__r.User_Status__c LIKE '3BEX%'
                OR (Work_Order_Activity__r.User_Status__c LIKE '4COM%'
                AND Work_Order_Activity__r.Status__c = 'Completed')))
                OR Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c = FALSE)))
                OR (Work_Order_Activity__r.Operating_Field_AMU__c = NULL
                AND Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c = NULL))
                ORDER BY TimeStamp__c DESC
                LIMIT 1500
        ];
    }

    public List<Account> getVendorAccounts() {
        return VTT_Utilities.GetVendorAccountList();
    }

    public List<Contact> getTradesmanForAccount(String accountId) {
        return [
                SELECT Id, Name
                FROM Contact
                WHERE AccountId = :accountId
                AND User__c <> NULL
                ORDER BY Name
                LIMIT 5000
        ];
    }

    public List<Work_Order_Activity_Log__c> getActivityLogsForTradesman(String tradesmanID, Date startDate, Date endDate) {
        return [
                SELECT Id, Work_Order_Activity__c,
                        Work_Order_Activity__r.Name, Started_New__c, Finished_New__c, Date__c,
                        Work_Order_Activity__r.Scheduled_Start_Date__c,
                        Work_Order_Activity__r.Status__c,
                        Work_Order_Activity__r.User_Status__c,
                        Work_Order_Activity__r.Finished__c,
                        Work_Order_Activity__r.Equipment__c,
                        Work_Order_Activity__r.Equipment__r.Tag_Number__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Equipment__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Equipment__r.Tag_Number__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Priority_Number__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Number__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Well_Event__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Functional_Equipment_Level__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Functional_Equipment_Level__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Yard__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Name,
                        Work_Order_Activity__r.Operating_Field_AMU__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Yard__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Sub_System__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Sub_System__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.System__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.System__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Facility__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Facility__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Location__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Location__r.Name,
                        Work_Order_Activity__r.Operating_Field_AMU__c,
                        Work_Order_Activity__r.Operating_Field_AMU__r.Is_Thermal__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c, (
                        SELECT Id, Name, Status__c,
                                Work_Order_Activity_Log_Entry__c.TimeStamp__c
                        FROM Work_Order_Activity_Log_Entries2__r
                        ORDER BY TimeStamp__c DESC
                        LIMIT 1
                )
                FROM Work_Order_Activity_Log__c
                WHERE Tradesman__c = :tradesmanID
                AND Date__c >= :startDate AND Date__c <= :endDate
                AND Started_New__c <> NULL
                //mz 3-Jan-18
                AND (((Work_Order_Activity__r.Operating_Field_AMU__c != NULL
                AND ((Work_Order_Activity__r.Operating_Field_AMU__r.Is_Thermal__c = TRUE
                AND (Work_Order_Activity__r.User_Status__c LIKE '2SCH %FIX%'
                OR Work_Order_Activity__r.User_Status__c LIKE '3BEX%'
                OR (Work_Order_Activity__r.User_Status__c LIKE '4COM%'
                AND Work_Order_Activity__r.Status__c = 'Completed')))
                OR Work_Order_Activity__r.Operating_Field_AMU__r.Is_Thermal__c = FALSE)))
                OR ((Work_Order_Activity__r.Operating_Field_AMU__c = NULL
                AND Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c != NULL
                AND ((Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c = TRUE
                AND (Work_Order_Activity__r.User_Status__c LIKE '2SCH %FIX%'
                OR Work_Order_Activity__r.User_Status__c LIKE '3BEX%'
                OR (Work_Order_Activity__r.User_Status__c LIKE '4COM%'
                AND Work_Order_Activity__r.Status__c = 'Completed')))
                OR Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c = FALSE)))
                OR (Work_Order_Activity__r.Operating_Field_AMU__c = NULL
                AND Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c = NULL))
                ORDER BY Date__c DESC
                LIMIT 1500
        ];
    }

    public List<Work_Order_Activity_Log__c> getAutoCompletedActivities(Set<Id> workLogActivitySet) {
        return [
                SELECT Id, Work_Order_Activity__c,
                        Work_Order_Activity__r.Name,
                        Started_New__c,
                        Finished_New__c,
                        Work_Order_Activity__r.Status__c,
                        Work_Order_Activity__r.Equipment__c,
                        Work_Order_Activity__r.Equipment__r.Tag_Number__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Equipment__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Equipment__r.Tag_Number__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Priority_Number__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Number__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Well_Event__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Functional_Equipment_Level__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Functional_Equipment_Level__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Yard__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Yard__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Sub_System__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Sub_System__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.System__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.System__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Facility__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Facility__r.Name,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Location__c,
                        Work_Order_Activity__r.Maintenance_Work_Order__r.Location__r.Name, (
                        SELECT Id, Name, Status__c
                        FROM Work_Order_Activity_Log_Entries2__r
                        WHERE Status__c = :VTT_Utilities.LOGENTRY_AUTOCOMPLETE
                )
                FROM Work_Order_Activity_Log__c
                WHERE Id IN :workLogActivitySet
        ];
    }

    public List<Work_Order_Activity__c> getScheduledActivities(Set<Id> workedActivitySet, String tradesmanId, Date startDate, Date endDate) {
        return [
                SELECT Id, Name, Scheduled_Start_Date__c, Scheduled_Finish_Date__c,
                        Maintenance_Work_Order__c, User_Status__c, Status__c,
                        Maintenance_Work_Order__r.Work_Order_Priority_Number__c,
                        Maintenance_Work_Order__r.Work_Order_Number__c,
                        Maintenance_Work_Order__r.Well_Event__c,
                        Maintenance_Work_Order__r.Well_Event__r.Name,
                        Maintenance_Work_Order__r.Functional_Equipment_Level__c,
                        Maintenance_Work_Order__r.Functional_Equipment_Level__r.Name,
                        Maintenance_Work_Order__r.Yard__c,
                        Maintenance_Work_Order__r.Yard__r.Name,
                        Maintenance_Work_Order__r.Sub_System__c,
                        Maintenance_Work_Order__r.Sub_System__r.Name,
                        Maintenance_Work_Order__r.System__c,
                        Maintenance_Work_Order__r.System__r.Name,
                        Maintenance_Work_Order__r.Facility__c,
                        Maintenance_Work_Order__r.Facility__r.Name,
                        Maintenance_Work_Order__r.Location__c,
                        Maintenance_Work_Order__r.Location__r.Name,
                        Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Name,
                        Equipment__c,
                        Equipment__r.Tag_Number__c,
                        Maintenance_Work_Order__r.Equipment__c,
                        Maintenance_Work_Order__r.Equipment__r.Tag_Number__c,
                        Operating_Field_AMU__c, Operating_Field_AMU__r.Is_Thermal__c,
                        Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c,
                        Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c
                FROM Work_Order_Activity__c
                WHERE
                Id IN (
                        SELECT Work_Order_Activity__c
                        FROM Work_Order_Activity_Assignment__c
                        WHERE Rejected__c <> TRUE AND Tradesman__c = :tradesmanId
                )
                AND Scheduled_Start_Date__c >= :startDate AND Scheduled_Start_Date__c <= :endDate
                AND Id NOT IN :workedActivitySet
                AND SAP_Deleted__c = FALSE
                AND Status__c <> :VTT_Utilities.ACTIVITY_STATUS_COMPLETED
                //mz 3-Jan-18
                AND (((Operating_Field_AMU__c != NULL
                AND ((Operating_Field_AMU__r.Is_Thermal__c = TRUE
                AND (User_Status__c LIKE '2SCH %FIX%'
                OR User_Status__c LIKE '3BEX%'))
                OR Operating_Field_AMU__r.Is_Thermal__c = FALSE))
                OR (Operating_Field_AMU__c = NULL
                AND Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c != NULL
                AND ((Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c = TRUE
                AND (User_Status__c LIKE '2SCH %FIX%'
                OR User_Status__c LIKE '3BEX%'))
                OR Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c = FALSE))
                OR (Operating_Field_AMU__c = NULL
                AND Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c = NULL)))
                ORDER BY Scheduled_Start_Date__c DESC
                LIMIT 1500
        ];
    }

    public List<Work_Order_Activity__c> getOnHoldActivities(String tradesmanID) {
        return [
                SELECT Id, Name, Scheduled_Start_Date__c, Status__c,
                        Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Name, (
                        SELECT Date__c, Work_Order_Activity__r.Status__c
                        FROM Work_Order_Activity_Logs__r
                        ORDER BY Date__c DESC
                        LIMIT 1
                )
                FROM Work_Order_Activity__c
                WHERE Id IN (
                        SELECT Work_Order_Activity__c
                        FROM Work_Order_Activity_Assignment__c
                        WHERE Rejected__c <> TRUE
                        AND Tradesman__c = :tradesmanID
                )
                AND SAP_Deleted__c = FALSE
                AND Status__c = :VTT_Utilities.ACTIVITY_STATUS_ONHOLD
                ORDER BY Scheduled_Start_Date__c DESC
                LIMIT 1500
        ];
    }

    public Contact getTradesmanInfo() {
        List<Contact> result = VTT_Utility_EndpointHandler.contactDAO.getTradesmanInfo();
        Contact tradesman;

        if(result.size() == 0) {
            tradesman = new Contact();
        } else {
            tradesman = result[0];
        }

        return tradesman;
    }

    public Map<Id, PermissionSet> getSupervisorPermissionSets() {
        Map<Id, PermissionSet> ps = new Map<Id, PermissionSet>([
                SELECT Id,
                        Name
                FROM PermissionSet
                WHERE Name = :VTT_Utilities.VENDORSUPERVISOR_PERMISSIONSET_NAME
                OR Name = :VendorPortalUtility.VENDOR_PORTAL_SUPERVISOR
                OR Name = :VendorPortalUtility.VENDOR_PORTAL_SUPERVISOR_LIGHTNING
                LIMIT 3
        ]);
        return ps;
    }

    public List<PermissionSetAssignment> getPermissionSetAssignmentsOfCurrentUser(Set<Id> psIds) {
        List<PermissionSetAssignment> psalist = [
                SELECT
                        Id
                FROM PermissionSetAssignment
                WHERE AssigneeId = :UserInfo.getUserId()
                AND PermissionSetId IN :psIds
                LIMIT 1
        ];

        return psalist;
    }


    public Boolean isUserSystemAdmin() {
        Boolean isAdminUser = false;
        List<Profile> userProfile = [
                SELECT
                        Id,
                        Name
                FROM Profile
                WHERE Id = :UserInfo.getProfileId()
                AND Name = 'System Administrator'
                LIMIT 1
        ];

        if(userProfile.size() > 0){
            isAdminUser = true;
        }

        return isAdminUser;
    }
}