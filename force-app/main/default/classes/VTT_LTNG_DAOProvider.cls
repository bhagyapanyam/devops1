/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Database access interface for VTT Lightning Application

History:        mbrimus 2019-09-24 - Created.
*************************************************************************************************/
public inherited sharing class VTT_LTNG_DAOProvider {
    public static VTT_LTNG_CalendarDAO calendarDao = new VTT_LTNG_CalendarSelector();
    public static  VTT_LTNG_WorkFlowEngineDAO engineDAO = new VTT_LTNG_WorkFlowEngineSelector();
    public static  VTT_LTNG_PortalManagementDAO portalManagementDAO = new VTT_LTNG_PortalManagementSelector();
}