/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    VTT_LTNG_PortalManagementService for Lightning Vendor Community
Test Class:     VTT_LTNG_PortalManagementEndpointTest
History:        mbrimus 28/11/2019. - Created.
*************************************************************************************************/
public inherited sharing class VTT_LTNG_PortalManagementService {

    public HOG_CustomResponseImpl loadUserData() {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        Boolean isCurrentUserPortalUser = isUserVendorPortalSupervisor();
        List<String> messages = new List<String>();

        VTT_LTNG_PortalManagementResponse model = new VTT_LTNG_PortalManagementResponse();
        model.isCommunityUser = isCurrentUserPortalUser;

        // Response just for HOG Admins
        if (!isCurrentUserPortalUser) {
            RecordType recordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'HOG_Vendor_Contact' LIMIT 1];

            List<Account> vendorAccounts =
                    VTT_LTNG_DAOProvider.portalManagementDAO.getAccounts();
            model.vendorAccounts = vendorAccounts;
            model.hogContactRecordType = recordType.Id;
            if (vendorAccounts.size() > 0) {
                // Populate contacts for first default account that will be preselected
                model.vendorAccount = vendorAccounts.get(0);
                model.enabledContacts = VTT_LTNG_DAOProvider.portalManagementDAO.getContactsForAccount(vendorAccounts.get(0).Id, true);
                model.disabledContacts = VTT_LTNG_DAOProvider.portalManagementDAO.getContactsForAccount(vendorAccounts.get(0).Id, false);
            }
            // Response for Vendor Supervisors
        } else {
            List<Contact> userContact =
                    VTT_LTNG_DAOProvider.portalManagementDAO.getUsersContact(UserInfo.getUserId());

            if (userContact.isEmpty()) {
                messages.add('User has no contact assigned or this contact is not under portal enabled Account...');
            } else if (userContact.size() > 1) {
                messages.add('Your user is referenced in multiple contacts that are under portal enabled account, please remove reference from one of them: ');
                for (Contact c : userContact) {
                    messages.add(c.Id + ' Referenced in ' + c.Account.Name);
                }
            } else {
                model.vendorAccount = userContact.get(0).Account;
                model.enabledContacts = VTT_LTNG_DAOProvider.portalManagementDAO.getContactsForAccount(userContact.get(0).AccountId, true);
                model.disabledContacts = VTT_LTNG_DAOProvider.portalManagementDAO.getContactsForAccount(userContact.get(0).AccountId, false);
            }
        }
        response.addResult(model);
        return response;
    }

    /**
     * This endpoint is only available to HOG Admins or internal users with access to Vendor portal management tab
     * And is accessed when user selects account from list
     *
     * @param accountId
     *
     * @return
     */
    public HOG_CustomResponseImpl getContacts(String accountId) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        VTT_LTNG_PortalManagementResponse model = new VTT_LTNG_PortalManagementResponse();
        model.isCommunityUser = false;
        model.vendorAccount = VTT_LTNG_DAOProvider.portalManagementDAO.getAccountById(accountId);
        model.enabledContacts = VTT_LTNG_DAOProvider.portalManagementDAO.getContactsForAccount(accountId, true);
        model.disabledContacts = VTT_LTNG_DAOProvider.portalManagementDAO.getContactsForAccount(accountId, false);
        response.addResult(model);
        return response;
    }

    /**
     * Based on Vendor_Portal_User_Type__c of User tied to Contact
     * either migrates or reverts from / to Lightning and classic community
     * @param accountId
     *
     * @return
     */
    public HOG_CustomResponseImpl migrate(Contact contact) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

        if (contact.User__c != null) {
            List<User> userRecords = VTT_LTNG_DAOProvider.portalManagementDAO.getUserById(contact.User__c);

            if (userRecords.size() > 0) {
                User u = userRecords.get(0);
                if (VendorPortalUtility.PERMISSION_SET_MIGRATION_MAP_TO_LIGHTNING.containsKey(u.Vendor_Portal_User_Type__c)) {
                    Profile p = [
                            SELECT
                                    Id,
                                    Name
                            FROM Profile
                            WHERE Name = :VendorPortalUtility.VENDOR_PORTAL_PROFILE_LIGHTNING
                    ];
                    u.Vendor_Portal_User_Type__c = VendorPortalUtility.PERMISSION_SET_MIGRATION_MAP_TO_LIGHTNING.get(u.Vendor_Portal_User_Type__c);
                    String nickName = u.FirstName + '.' + u.LastName;
                    nickName = nickName.length() < 37 ? nickName + VendorPortalUtility.randomRandomInteger(100) : nickName.substring(0, 37) + VendorPortalUtility.randomRandomInteger(100);
                    u.CommunityNickname = nickName;
                    u.ProfileId = p.Id;

                    System.debug('Updating this user to lightning ' + u);
                    update u;
                    return response;
                } else if(VendorPortalUtility.PERMISSION_SET_MIGRATION_MAP_FROM_LIGHTNING.containsKey(u.Vendor_Portal_User_Type__c)) {
                    Profile p = [
                            SELECT
                                    Id,
                                    Name
                            FROM Profile
                            WHERE Name = :VendorPortalUtility.VENDOR_PORTAL_PROFILE
                    ];
                    u.Vendor_Portal_User_Type__c = VendorPortalUtility.PERMISSION_SET_MIGRATION_MAP_FROM_LIGHTNING.get(u.Vendor_Portal_User_Type__c);
                    u.ProfileId = p.Id;

                    System.debug('Reverting this user to classic ' + u);
                    update u;
                    return response;
                } else {
                    response.addError('Vendor_Portal_User_Type__c value is unknown');
                    return response;
                }

            } else {
                response.addError('User not found for this contact or Vendor_Portal_User_Type__c was empty');
                return response;
            }
        } else {
            response.addError('User field is empty for this contact');
            return response;
        }
    }

    /**
     * Enables contact as Portal User for Lightning Vendor Community
     *
     * @param accountId
     *
     * @return
     */
    public HOG_CustomResponseImpl enabledForLightningCommunity(Contact contact, String userType) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        Account ac = VTT_LTNG_DAOProvider.portalManagementDAO.getAccountById(contact.AccountId);

        // Validations
        if (contact.FirstName == null || contact.LastName == null || contact.Email == null) {
            response.addError('Error: First Name, Last Name and Email cannot be empty on contact...');
        } else if (ac.Remaining_Licenses__c < 1) {
            response.addError('You cannot create any more users because your Account doesnt have any more remaining licenses');
        } else if (!(userType.contains(VendorPortalUtility.VENDOR_PORTAL_SUPERVISOR_LIGHTNING) ||
                userType.contains(VendorPortalUtility.VENDOR_PORTAL_TRADESMAN_LIGHTNING) ||
                // For now support also these old PS.. but these values should not come from frontend
                userType.contains(VendorPortalUtility.VENDOR_PORTAL_SUPERVISOR) ||
                userType.contains(VendorPortalUtility.VENDOR_PORTAL_TRADESMAN))) {
            response.addError('User Type not supported');
        } else {
            Profile vendorProfile = [SELECT Id,Name FROM Profile WHERE Name = :VendorPortalUtility.VENDOR_PORTAL_PROFILE_LIGHTNING LIMIT 1];
            VendorPortalUtility.createOrReactivatePortalUser(
                    contact.Id,
                    contact.Email,
                    contact.FirstName,
                    contact.LastName,
                    vendorProfile.Id,
                    userType);
        }
        return response;
    }

    /**
     * Disable user - logged in as Internal User
     *
     * @param accountId
     *
     * @return
     */
    public HOG_CustomResponseImpl disableAsInternalUser(Contact contact) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        List<Profile> vendorPortalProfile = [
                SELECT
                        Id,
                        Name
                FROM Profile
                WHERE Name = :VendorPortalUtility.VENDOR_PORTAL_PROFILE OR Name = :VendorPortalUtility.VENDOR_PORTAL_PROFILE_LIGHTNING
        ];
        Set<Id> profileIds = HOG_GeneralUtilities.getSetOfIds(vendorPortalProfile);
        User user = [
                SELECT Id, Name, IsPortalEnabled, ProfileId, ContactId, IsActive, Username, Email
                FROM User
                WHERE IsActive = TRUE
                AND IsPortalEnabled = TRUE
                AND ProfileId IN :profileIds
                AND ContactId = :contact.Id
        ];

        user.IsActive = false;
        user.Email = user.Email + '.disabled';
        System.debug('Disabling this user: ' + user);

        try {
            update user;
            return response;
        } catch (Exception ex) {
            response.addError('Error when disabling user - ' + ex.getMessage());
            System.debug('disableAsInternalUser EXCEPTION: ' + ex);
            return response;
        }
    }

    /**
     * Disable user - logged in as Community User
     *
     * @param accountId
     *
     * @return
     */
    public HOG_CustomResponseImpl disableAsCommunityUser(Contact contact) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

        //If its null then it means that this user is already disabled or doesnt exist
        if (contact != null && contact.Name != null && contact.Account.Name != null) {

            String adminPageUrl = Url.getSalesforceBaseUrl().toExternalForm() + '/lightning/n/Lightning_Community_Management';

            //Prepare message for admin
            String adminEmailText = UserInfo.getName() + ' has requested to disable this contact: '
                    + contact.Name + ' for vendor: '
                    + contact.Account.Name
                    + '</br>'
                    + '<a href="' + adminPageUrl + '">Click here to go to Vendor Portal Management Page</a>';
            String adminEmailSubjectLine = 'Disable user for ' + contact.Account.Name;

            //HOG Admins emails
            HOG_Settings__c oSettings = HOG_Settings__c.getInstance();
            List<String> toAddresses = new List<String>{
                    oSettings.Administrator_Email__c
            };

            Messaging.SingleEmailMessage emailToAdmin = new Messaging.SingleEmailMessage();
            emailToAdmin.setSubject(adminEmailSubjectLine);
            emailToAdmin.setToAddresses(toAddresses);
            emailToAdmin.setHtmlBody(adminEmailText);

            // Prepare message for requester
            List<String> requesterEmail = new List<String>{
                    UserInfo.getUserEmail()
            };
            String requesterEmailText = 'You have requested disabling of this user: ' + contact.Name
                    + ' if this was done by mistake please contact HOGLloydSF@huskyenergy.com as soon as possible';
            String requesterEmailSubjectLine = 'Requested disabling of this user: ' + contact.Name;

            Messaging.SingleEmailMessage emailToRequester = new Messaging.SingleEmailMessage();
            emailToRequester.setSubject(requesterEmailSubjectLine);
            emailToRequester.setToAddresses(requesterEmail);
            emailToRequester.setHtmlBody(requesterEmailText);

            System.debug('Sending email to disable ' + contact.Name + ' to these users: ' + toAddresses);

            try {
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
                        emailToAdmin, emailToRequester
                });
                return response;
            } catch (Exception e) {
                response.addError('Failed to Send Email. Please try again. Please contact your Administrator if the error persists');
                System.debug('Error sending email: ' + e.getMessage());
                return response;
            }
        } else {
            response.addError('This user has already been disabled');
            return response;
        }
    }

    /**
     * Check if current user has vendor portal profile
     *
     * @param profileId
     *
     * @return
     */
    private Boolean isUserVendorPortalSupervisor() {
        List<Profile> vendorPortalProfile = [
                SELECT
                        Id,
                        Name
                FROM Profile
                WHERE Name = :VendorPortalUtility.VENDOR_PORTAL_PROFILE OR Name = :VendorPortalUtility.VENDOR_PORTAL_PROFILE_LIGHTNING
        ];
        Set<Id> profileIds = HOG_GeneralUtilities.getSetOfIds(vendorPortalProfile);
        return profileIds.contains(UserInfo.getProfileId());
    }
}