/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Database access interface for VTT Workflow Engine

History:        mbrimus 2019-09-24 - Created.
*************************************************************************************************/
public interface VTT_LTNG_WorkFlowEngineDAO {

    /**
     * Returns Work_Order_Activity__c based on Id and also pulls assignements for current user
     * @param woaId
     * @return
     */
    List<Work_Order_Activity__c> getActivitiesWithAssignment(String woaId);

    /**
     * Returns list of contact where currently logged in user is referenced
     * @return
     */
    List<Contact> getTradesmanContacts();

    /**
     * Returns last entry for tradesmen and activity
     * @param woaId
     * @param tradesmanId
     * @return
     */
    List<Work_Order_Activity_Log_Entry__c> getLastLogEntryForTradesmanAndActivity(String woaId, String tradesmanId);

    /**
     * Returns latest version of activity data from database
     * @param woaId
     * @return
     */
    Work_Order_Activity__c getLatestVersionOfActivity(String woaId);

    /**
     * Return list of Close Out Checklists for activity and company for today
     * @param activity
     * @param tradesman
     * @return
     */
    List<HOG_Work_Execution_Close_Out_Checklist__c> getCloseOutChecklistForToday(Work_Order_Activity__c activity, Contact tradesman);

    /**
     * Return set of contact id from work order assignment - assigned tradesmans
     * @param activity
     * @return
     */
    List<Contact> getAssignedTradesman(Id workOrderActivityId, Id userId);

    /**
     * Returns non rejected assignment for tradesman and activity
     *
     * @param activity
     * @param tradesman
     * @return
     */
    List<Work_Order_Activity_Assignment__c> getNonRejectedAssignment(Id activityId, Id tradesmanId);

    /**
     * Returns list of tradesman who are still working for given activity
     *
     * @param workOrderActivity
     * @param assignedTradesmenList
     * @param tradesman
     *
     * @return
     */
    List<Contact> getOtherWorkingTradesman(
            Work_Order_Activity__c workOrderActivity,
            List<Contact> assignedTradesmenList,
            Contact tradesman);

    /**
     * Return list of activities that can be completed with job complete action (for thermal)
     *
     * @param workOrderActivity
     * @return
     */
    List<VTT_LTNG_AvailableActivity> getAvailableActivitiesToComplete(Contact tradesman, Work_Order_Activity__c workOrderActivity);

    /**
     * This us used to populate runningTally time - time that work order activity took to complete up until this point
     *
     * Queries latest Work Order Activity Logs and also gets latest Start at Equipment entry that has not been finished
     * so we can precisly calculate time by adding this entry logs duration to Work time of activity log
     *
     * @param activity
     * @param activityLogs
     * @return
     */
    Double getRunningTallyTime(Work_Order_Activity__c activity, List<Work_Order_Activity_Log__c> activityLogs);

    /**
     * Returns list of latest log entries
     *
     * @param activity
     *
     * @return
     */
    List<Work_Order_Activity_Log__c> getActivityLogs(Id activityId, Id userId);

    /**
     * Returns Parts by supplied Catalogue Code
     *
     * @param catalogueCode
     *
     * @return
     */
    List<HOG_Part__c> getPartsByCatalogueCode(String catalogueCode);

    /**
     * Returns Damage by supplied PartCode
     *
     * @param partCode
     *
     * @return
     */
    List<HOG_Damage__c> getDamageByPartCode(String partCode);

    /**
     * Returns all Causes
     *
     * @return
     */
    List<HOG_Cause__c> getCause();
}