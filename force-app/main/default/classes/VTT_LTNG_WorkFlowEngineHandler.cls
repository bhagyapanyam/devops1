/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Handler class that handles basic logic for VTT_LTNG_WorkFlowEngine_Endpoint POST requests.
Test Class:     VTT_LTNG_WorkFlowEngineHandlerTest TODO
History:        mbrimus 02/10/2019 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class VTT_LTNG_WorkFlowEngineHandler {
    private static final String CLASS_NAME = String.valueOf(VTT_LTNG_WorkFlowEngineHandler.class);

    public void handleSaveAction(String request, Work_Order_Activity__c activity, Contact tradesman) {
        VTT_LTNG_Request data = (VTT_LTNG_Request) JSON.deserialize(request.trim(), VTT_LTNG_Request.class);

        System.debug(CLASS_NAME + ' -> saveAction. request: ' + request);
        System.debug(CLASS_NAME + ' -> saveAction. activity: ' + activity);
        System.debug(CLASS_NAME + ' -> saveAction. tradesman: ' + tradesman);
        System.debug(CLASS_NAME + ' -> saveAction. serialized data: ' + data);

        // Nullify Estimated_Rescheduled_Date__c
        if (data.actionName != System.Label.VTT_LTNG_Job_On_Hold_Action) {
            activity.Estimated_Rescheduled_Date__c = null;
        }

        // START JOB
        if (data.actionName == System.Label.VTT_LTNG_Start_Job_Action) {

            VTT_LTNG_EngineAction startWorkAction =
                    VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_STARTJOB);
            new VTT_LTNG_WorkFlowEngineService().handleGenericActivityEnd(data, startWorkAction, activity, tradesman);

            // START JOB AT EQUIPMENT
        } else if (data.actionName == System.Label.VTT_LTNG_Start_At_Equipment_Action) {
            VTT_LTNG_EngineAction startAtEquipment =
                    VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_STARTATSITE);
            new VTT_LTNG_WorkFlowEngineService().handleGenericActivityEnd(data, startAtEquipment, activity, tradesman);

            // START JOB AT EQUIPMENT (Thermal)
        } else if (data.actionName == System.Label.VTT_LTNG_Start_Job_At_Equipment_Action) {
            VTT_LTNG_EngineAction startWorkAction =
                    VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_STARTJOB);
            new VTT_LTNG_WorkFlowEngineService().handleStartJobAtEquipmentAction(data, startWorkAction, activity, tradesman);

            // FINISH JOB AT EQUIPMENT
        } else if (data.actionName == System.Label.VTT_LTNG_Finished_Job_At_Equipment_Action) {
            VTT_LTNG_EngineAction startWorkAction =
                    VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_FINISHEDATSITE);
            new VTT_LTNG_WorkFlowEngineService().handleGenericActivityEnd(data, startWorkAction, activity, tradesman);

            // FINISHED FOR THE DAY
        } else if (data.actionName == System.Label.VTT_LTNG_Finished_for_the_Day_Action) {
            VTT_LTNG_EngineAction startWorkAction =
                    VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY);
            new VTT_LTNG_WorkFlowEngineService().handleGenericActivityEnd(data, startWorkAction, activity, tradesman);

            // JOB ON HOLD
        } else if (data.actionName == System.Label.VTT_LTNG_Job_On_Hold_Action) {
            VTT_LTNG_EngineAction putLogOnHoldAction =
                    VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_JOBONHOLD);
            new VTT_LTNG_WorkFlowEngineService().handleJobOnHoldEnd(data, putLogOnHoldAction, activity, tradesman);

            // REJECT ACTIVITY
        } else if (data.actionName == System.Label.VTT_LTNG_Reject_Activity_Action) {
            new VTT_LTNG_WorkFlowEngineService().handleRejectActivity(data, activity, tradesman);

            // JOB COMPLETE ACTIVITY
        } else if (data.actionName == System.Label.VTT_LTNG_Job_Complete_Action) {
            VTT_LTNG_EngineAction jobComplete =
                    VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_JOBCOMPLETE);
            new VTT_LTNG_WorkFlowEngineService().handleJobComplete(data, jobComplete, activity, tradesman);
        }
    }

    public HOG_CustomResponseImpl handleSaveActionWithCheckList(
            String request,
            Work_Order_Activity__c activity,
            Contact tradesman,
            HOG_Work_Execution_Close_Out_Checklist__c checklist) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        VTT_LTNG_Request data = (VTT_LTNG_Request) JSON.deserialize(request.trim(), VTT_LTNG_Request.class);

        System.debug(CLASS_NAME + ' -> saveActionWithCheckList. request: ' + request);
        System.debug(CLASS_NAME + ' -> saveActionWithCheckList. activity: ' + activity);
        System.debug(CLASS_NAME + ' -> saveActionWithCheckList. tradesman: ' + tradesman);
        System.debug(CLASS_NAME + ' -> saveActionWithCheckList. serialized data: ' + data);

        // START JOB AT EQUIPMENT
        if (data.actionName == System.Label.VTT_LTNG_Start_At_Equipment_Action) {

            validateAction(data, response);

            if (response.success) {
                VTT_LTNG_EngineAction startAtEquipment =
                        VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_STARTATSITE);
                new VTT_LTNG_WorkFlowEngineService().handleGenericActivityEndWithCheckList(
                        data,
                        startAtEquipment,
                        activity,
                        tradesman,
                        checklist
                );
            }

            // START JOB AT EQUIPMENT (Thermal)
        } else if (data.actionName == System.Label.VTT_LTNG_Start_Job_At_Equipment_Action) {

            validateAction(data, response);

            if (response.success) {
                VTT_LTNG_EngineAction startWorkAction =
                        VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_STARTJOB);
                new VTT_LTNG_WorkFlowEngineService().handleStartJobAtEquipmentActionWithCheckList(
                        data,
                        startWorkAction,
                        activity,
                        tradesman,
                        checklist
                );
            }

            // Finish Job at Equipment
        } else if (data.actionName == System.Label.VTT_LTNG_Finished_Job_At_Equipment_Action) {

            validateAction(data, response);

            if (response.success) {
                VTT_LTNG_EngineAction startWorkAction =
                        VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_FINISHEDATSITE);
                new VTT_LTNG_WorkFlowEngineService().handleGenericActivityEndWithCheckList(
                        data,
                        startWorkAction,
                        activity,
                        tradesman,
                        checklist
                );
            }

            // JOB ON HOLD
        } else if (data.actionName == System.Label.VTT_LTNG_Job_On_Hold_Action) {

            validateAction(data, response);

            if (response.success) {
                VTT_LTNG_EngineAction putLogOnHoldAction =
                        VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_JOBONHOLD);
                new VTT_LTNG_WorkFlowEngineService().handleJobOnHoldEndWithCheckList(
                        data,
                        putLogOnHoldAction,
                        activity,
                        tradesman,
                        checklist
                );
            }
        }

        return response;
    }

    private void validateAction(VTT_LTNG_Request data, HOG_CustomResponseImpl response) {
        if (String.isEmpty(data.actionComment) || data.actionComment == null) {
            response.success = false;
            response.addError('Comment cannot be empty');
        }
    }
}