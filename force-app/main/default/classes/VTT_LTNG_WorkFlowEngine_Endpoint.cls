/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    WorkFlow engine endpoints for VTT
Test Class:     VTT_LTNG_WorkFlowEngine_EndpointTest
History:        mbrimus 2019-09-20 - Created.
*************************************************************************************************/
public without sharing class VTT_LTNG_WorkFlowEngine_Endpoint {
    private static final String CLASS_NAME = String.valueOf(VTT_LTNG_WorkFlowEngine_Endpoint.class);

    @AuraEnabled
    public static HOG_CustomResponseImpl loadEngineState(String workOrderActivityId) {

        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

        try {
            response = new VTT_LTNG_WorkFlowEngineService().getEngineState(workOrderActivityId);
            System.debug(CLASS_NAME + ' loadEngineState DATA ' + response);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' loadEngineState exception ' + ex.getMessage());
            response.addError(ex);
        }

        return response;
    }

    @AuraEnabled
    public static HOG_CustomResponseImpl suppressEPD(Boolean flag, String workOrderActivityId) {
        return new VTT_LTNG_WorkFlowEngineService().suppressEPD(flag, workOrderActivityId);
    }

    @AuraEnabled
    public static HOG_CustomResponseImpl getPermitHolderData(
            Work_Order_Activity__c activity,
            Contact tradesman) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

        try {
            response = new VTT_LTNG_WorkFlowEngineService().getPermitHolderData(activity, tradesman);
            System.debug(CLASS_NAME + ' getPermitHolderData DATA ' + response);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' getPermitHolderData exception ' + ex.getMessage());
            response.addError(ex);
        }

        return response;
    }

    @AuraEnabled
    public static HOG_CustomResponseImpl getDataForJobComplete(
            Work_Order_Activity__c activity,
            Contact tradesman
    ) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

        try {
            response = new VTT_LTNG_WorkFlowEngineService().getDataForJobComplete(activity, tradesman);
            System.debug(CLASS_NAME + ' getJobCompleteData DATA ' + response);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' getJobCompleteData exception ' + ex.getMessage());
            response.addError(ex);
        }

        return response;
    }

    @AuraEnabled
    public static HOG_CustomResponseImpl getDamage(
            String partCode) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

        try {
            response = new VTT_LTNG_WorkFlowEngineService().getDamage(partCode);
            System.debug(CLASS_NAME + ' getDamage DATA ' + response + ' PartCode ' + partCode);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' getDamage exception ' + ex.getMessage());
            response.addError(ex);
        }

        return response;
    }

    @AuraEnabled
    public static HOG_CustomResponseImpl canActivityBePutOnHold(Work_Order_Activity__c activity, Contact tradesman) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

        try {
            response = new VTT_LTNG_WorkFlowEngineService().canActivityBePutOnHold(activity, tradesman);
            System.debug(CLASS_NAME + ' canActivityBePutOnHold DATA ' + response);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' canActivityBePutOnHold exception ' + ex.getMessage());
            response.addError(ex);
        }

        return response;
    }

    @AuraEnabled
    public static HOG_CustomResponseImpl hasActivityChanged(Work_Order_Activity__c activity) {

        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

        try {
            response = new VTT_LTNG_WorkFlowEngineService().hasActivityChanged(activity);
            System.debug(CLASS_NAME + ' hasActivityChanged DATA ' + response);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' hasActivityChanged exception ' + ex.getMessage());
            response.addError(ex);
        }

        return response;
    }

    @AuraEnabled
    public static HOG_CustomResponseImpl saveAction(
            String request,
            Work_Order_Activity__c activity,
            Contact tradesman,
            HOG_Work_Execution_Close_Out_Checklist__c checklist) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        Savepoint sp = Database.setSavepoint();

        try {
            // Generic check if activity changed before user clicked submit
            Boolean isActivityLatest = VTT_LTNG_WorkFlowEngineService.optimisticLockCheck(activity);

            if (!isActivityLatest) {
                response.success = false;
                response.addResult(isActivityLatest);
                return response;
            } else {
                if (checklist != null) {
                    System.debug(CLASS_NAME + ' checklist exception ');
                    // This also adds errors into response
                    response = new VTT_LTNG_WorkFlowEngineHandler().handleSaveActionWithCheckList(
                            request,
                            activity,
                            tradesman,
                            checklist);
                } else {
                    new VTT_LTNG_WorkFlowEngineHandler().handleSaveAction(
                            request,
                            activity,
                            tradesman);
                }
            }

        } catch (Exception ex) {
            Database.rollback(sp);
            System.debug(CLASS_NAME + ' saveAction exception line: ' + ex.getLineNumber());
            System.debug(CLASS_NAME + ' saveAction exception trace: ' + ex.getStackTraceString());
            System.debug(CLASS_NAME + ' saveAction exception message: ' + ex.getMessage());
            System.debug(CLASS_NAME + ' saveAction exception cause: ' + ex.getCause());
            response.addError(ex);
        }
        return response;
    }
}