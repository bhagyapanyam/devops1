/*-------------------------------------------------------------------------------------------------
Author:         Shreyas Dhond
Company:        Husky Energy
Description:    A batch job that runs at every hour for resending errored confirmations.

Test Class:     VTT_RetryConfirmationBatch
History:        ssd 10.3.2018 - Created.
---------------------------------------------------------------------------------------------------*/
global class VTT_RetryConfirmationBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {
    public static final Integer DEFAULT_CREATED_DAYS = 30;
    public static final String JOBTYPE_RETRY_CONFIRMATION_INTEGRATION_ERRORS = 'CONFIRMATION_INTEGRATION_ERRORS';
    public static final String ERROR_CONFIRMATION_QUERY = 'Select  Id, Name, ' +
            'Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Number__c, '+
            'Work_Order_Activity__r.Operation_Number__c, Work_Order_Activity__r.Description__c, '+
            'Work_Order_Activity__r.System_Status__c, Work_Order_Activity__r.Work_Center__c, ' +
            'Actual_Work__c, Final_Confirmation__c, Work_Start__c, Work_Finish__c, ' +
            'Confirmation_Text__c, Confirmation_Long_Text__c, ' +
            'Work_Order_Activity__r.Operating_Field_AMU__r.Maintenance_Plant__c, ' +
            'Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Maintenance_Plant__c, '+
            'Confirmation_Log__c, Status__c, Work_Order_Activity__c, Tradesman__c, Status_Icon__c ' +
            'From Work_Order_Activity_Confirmation__c ' +
            'Where Status__c = \'' + VTT_ConfirmationsUtilities.CONFIRMATION_STATUS_ERROR + '\'' +
            'And CreatedDate = LAST_N_DAYS:'
            + Integer.valueOf((HOG_Settings__c.getInstance().VTT_Retry_Confirmations_Expiry_Days__c <> null) ?
            HOG_Settings__c.getInstance().VTT_Retry_Confirmations_Expiry_Days__c : DEFAULT_CREATED_DAYS);

    public String jobType {get; private set;}
    public String query {get; private set;}

    global VTT_RetryConfirmationBatch() {
        this(JOBTYPE_RETRY_CONFIRMATION_INTEGRATION_ERRORS);
    }

    global VTT_RetryConfirmationBatch(String jobType) {
        this.jobType = jobType;

        query = '';
        if(jobType == JOBTYPE_RETRY_CONFIRMATION_INTEGRATION_ERRORS) {
            query = ERROR_CONFIRMATION_QUERY;
        }
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('start: ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        //Get set of ids for confirmations
        Set<Id> errorConfirmationIdSet = new Set<Id>();
        for(Work_Order_Activity_Confirmation__c confirmation : (List<Work_Order_Activity_Confirmation__c>) scope) {
            errorConfirmationIdSet.add(confirmation.Id);
        }

        VTT_ConfirmationsUtilities.SAP_CreateConfirmationsCallout(errorConfirmationIdSet);
    }

    global void finish(Database.BatchableContext BC) {
        //Nothing here yet
    }
}