/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Handler class for VTT_Utility_Endpoint which contains base logic for all requests
                that can VTT_Utility_Endpoint handle.
                !WARNING! runs with inherited sharing
Test Class:     VTT_Utility_EndpointHandlerTest
History:        jschn 04/09/2019 - Created.
*************************************************************************************************/
public inherited sharing class VTT_Utility_EndpointHandler {

    public static VTT_Utility_WOA_LogEntryDAO logEntryDAO = new VTT_WOA_LogEntryDAOImpl();
    public static VTT_ContactUtilityDAO contactDAO = new HOG_ContactDAOImpl();

    /**
     * It gets Flags (isAdmin, isVendorSupervisor), Contact record and Id of WOA that current user is working on.
     *
     * @return VTT_TradesmanFlags
     */
    public VTT_TradesmanFlags getTradesmanInfo() {
        Boolean isAdmin = VTT_Utilities.IsAdminUser();
        Boolean isVendorSupervisor =  VTT_Utilities.IsVendorSupervisor();
        Contact tradesman = loadTradesmanInfo();
        return new VTT_TradesmanFlags()
                .setFlags(isAdmin, isVendorSupervisor)
                .setTradesman(tradesman)
                .setCurrentWOAId(getCurrentWOAId(tradesman));
    }

    /**
     * Retrieves Contact data related to Users session. If no contact is found, empty contact instance is created.
     *
     * @return Contact
     */
    public Contact loadTradesmanInfo() {
        List<Contact> result = contactDAO.getTradesmanInfo();
        Contact tradesman;

        if(result.size() == 0) {
            tradesman = new Contact();
        } else {
            tradesman = result[0];
        }

        return tradesman;
    }

    /**
     * Tries to get WOA Id that tradesman was/is working on.
     * First it tries to check if tradesman is currently working on activity and use that Id as result.
     * If tradesman is not working on activity, it will try to load his last log entry and check if
     * that log entry has any other status then ones contained in VTT_Constants.NOT_FINISHED_LOG_ENTRY_STATUSES
     * and returns it.
     *
     * @param tradesman
     *
     * @return Id
     */
    private Id getCurrentWOAId(Contact tradesman) {
        Id currentWorkOrderActivityId = tradesman.Current_Work_Order_Activity__c;

        if(currentWorkOrderActivityId == null) {
            //Get last log entry for this tradesman and check if Job Complete or Finished for the day
            List<Work_Order_Activity_Log_Entry__c> logEntriesForTradesman = logEntryDAO.getLastLogEntryForTradesman(tradesman.Id);

            if(logEntriesForTradesman <> null && !logEntriesForTradesman.isEmpty()) {
                Work_Order_Activity_Log_Entry__c lastLogEntry = logEntriesForTradesman[0];
                if(!VTT_Constants.FINISHED_LOG_ENTRY_STATUSES.contains(lastLogEntry.Status__c)) {
                    currentWorkOrderActivityId = lastLogEntry.Work_Order_Activity__c;
                }
            }
        }

        return currentWorkOrderActivityId;
    }

}