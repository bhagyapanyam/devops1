/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Endpoint class for Lightning Manage assignments actions
Test Class:     VTT_WOA_Assignment_EndpointTest
History:        jschn 20/09/2019 - Created.
*************************************************************************************************/
public with sharing class VTT_WOA_Assignment_Endpoint {

    @AuraEnabled
    public static List<HOG_PicklistItem_Simple> getVendors() {
        return new VTT_WOA_Assignment_EndpointHandler().getVendorsPicklistValues();
    }

    @AuraEnabled
    public static List<HOG_PicklistItem_Simple> getTradesmen(Id accountId) {
        return new VTT_WOA_Assignment_EndpointHandler().getTradesmenByAccountId(accountId);
    }

    @AuraEnabled
    public static String assign(List<Work_Order_Activity__c> activities, List<Id> tradesmenIds, Id vendorAccountId) {
        return String.valueOf(new VTT_WOA_Assignment_EndpointHandler().assign(activities, tradesmenIds, vendorAccountId));
    }

    @AuraEnabled
    public static String unassign(List<Work_Order_Activity__c> activities, List<Id> tradesmenIds, Id vendorAccountId) {
        return String.valueOf(new VTT_WOA_Assignment_EndpointHandler().unassign(activities, tradesmenIds, vendorAccountId));
    }

    @AuraEnabled
    public static List<String> getAssignedTradesmenForActivity(Id activityId) {
        return new VTT_WOA_Assignment_EndpointHandler().getAssignedTradesmenForActivity(activityId);
    }

}