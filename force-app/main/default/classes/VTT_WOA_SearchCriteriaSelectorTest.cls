/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for VTT_WOA_SearchCriteriaSelector class
History:        jschn 31/10/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_WOA_SearchCriteriaSelectorTest {

    @IsTest
    static void getUserActivitySearchFilters_nullParam() {
        VTT_WOA_SearchCriteriaDAO selector = new VTT_WOA_SearchCriteriaSelector();
        List<Work_Order_Activity_Search_Criteria__c> result;
        Integer expectedCount = 0;

        System.runAs(VTT_TestData.createVTTUser()) {
            Test.startTest();
            result = selector.getUserActivitySearchFilters(null);
            Test.stopTest();
        }

        System.assertNotEquals(null, result, 'Result of query should not be null.');
        System.assertEquals(expectedCount, result.size(), 'As null was provided as parameter, empty list should be result.');
    }

    @IsTest
    static void getUserActivitySearchFilters_noRecords() {
        VTT_WOA_SearchCriteriaDAO selector = new VTT_WOA_SearchCriteriaSelector();
        List<Work_Order_Activity_Search_Criteria__c> result;
        Integer expectedCount = 0;

        System.runAs(VTT_TestData.createVTTUser()) {
            Test.startTest();
            result = selector.getUserActivitySearchFilters(UserInfo.getUserId());
            Test.stopTest();
        }

        System.assertNotEquals(null, result, 'Result of query should not be null.');
        System.assertEquals(expectedCount, result.size(), 'As user does not have any Search Criteria records, result should be an empty list.');
    }

    @IsTest
    static void getUserActivitySearchFilters_oneRecord() {
        VTT_WOA_SearchCriteriaDAO selector = new VTT_WOA_SearchCriteriaSelector();
        List<Work_Order_Activity_Search_Criteria__c> result;
        Integer expectedCount = 1;

        System.runAs(VTT_TestData.createVTTUser()) {
            insert new Work_Order_Activity_Search_Criteria__c(
                    Assigned_Name__c = 'Test1',
                    Last_Search_Criteria_Selected__c = false,
                    Filter_String__c = 'Test1',
                    User__c = UserInfo.getUserId()
            );

            Test.startTest();
            result = selector.getUserActivitySearchFilters(UserInfo.getUserId());
            Test.stopTest();
        }

        System.assertNotEquals(null, result, 'Result of query should not be null.');
        System.assertEquals(expectedCount, result.size(), 'As user has one record, result should return this record in list.');
    }

}