/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Selector class containing all queries for Work_Order_Activity__c SObject
Test Class:     VTT_WorkOrderActivitySelectorTest
History:        jschn 2019-05-26 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class VTT_WorkOrderActivitySelector implements VTT_WorkOrderActivityDAO, VTT_WOA_MA_ActivityDAO {

    /**
     * Method that queries list of Work Order Activity record based on record ID.
     *
     * @param workOrderActivityId
     *
     * @return List<Work_Order_Activity__c>
     */
    public List<Work_Order_Activity__c> getRecord(Id workOrderActivityId) {
        return [
                SELECT Name, Functional_Location__c, Equipment__c,
                        Maintenance_Work_Order__c, Maintenance_Work_Order__r.Planner_Group__c,
                        Maintenance_Work_Order__r.Work_Order_Number__c,
                        Maintenance_Work_Order__r.Equipment__c,
                        Maintenance_Work_Order__r.Functional_Location__c,
                        Maintenance_Work_Order__r.Surface_Location__c,
                        Assigned_Vendor__c, Assigned_Vendor__r.Name,
                        Maintenance_Work_Order__r.Location__c, Maintenance_Work_Order__r.Location__r.Product_Strategy__c,
                        Maintenance_Work_Order__r.Facility__c, Maintenance_Work_Order__r.Facility__r.Product_Strategy__c,
                        Maintenance_Work_Order__r.Facility__r.Name
                FROM Work_Order_Activity__c
                WHERE Id =: workOrderActivityId
        ];
    }

    /**
     * Method that queries list of Work Order Activity record based on record Ids.
     *
     * @param woaIds
     *
     * @return List<Work_Order_Activity__c>
     */
    public List<Work_Order_Activity__c> getRecordsForAssignments(List<Id> woaIds) {
        return [
                SELECT Name, Assigned_Vendor__c, Assigned_Vendor__r.Name, Description__c,
                        Work_Center__c, Assigned_Text__c, Maintenance_Work_Order__r.Location__r.Name,
                        Maintenance_Work_Order__r.Plant_Section__c, Scheduled_Start_Date__c, Status__c,
                        Status_Reason__c, Maintenance_Work_Order__r.Order_Type__c,
                        Maintenance_Work_Order__r.Work_Order_Priority_Number__c,
                        Maintenance_Work_Order__r.User_Status_Code__c, Maintenance_Work_Order__c,
                        Maintenance_Work_Order__r.Name, Functional_Location_Description_SAP__c,
                        Location_Name__c, Maintenance_Work_Order__r.Location__r.Hazardous_H2S__c,
                        Location__r.Hazardous_H2S__c, Priority__c
                FROM Work_Order_Activity__c
                WHERE Id IN :woaIds
        ];
    }

}