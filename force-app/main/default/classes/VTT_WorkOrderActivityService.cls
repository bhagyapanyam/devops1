/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Service class containing all methods and business logic regarding Work Order Activity.
                Or at least it will contain after all controller will be rebuild.
Test Class:     VTT_WorkOrderActivityServiceTest
History:        jschn 2019-05-27 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class VTT_WorkOrderActivityService {

    /**
     * Loads Work Order Activity record from DB (with selector) and if inappropriate number of records are retrieved
     * it throws an exception. Expected number of records is 1.
     * If conditions are met, desired record is returned.
     *
     * @param workOrderActivityId
     *
     * @return Work_Order_Activity__c
     */
    public Work_Order_Activity__c getRecord(Id workOrderActivityId) {
        Work_Order_Activity__c activity;

        List<Work_Order_Activity__c> activities = EPD_DAOProvider.workOrderActivityDAO.getRecord(workOrderActivityId);

        if(activities != null && activities.size() == 1) {
            activity = activities.get(0);
        } else {
            HOG_ExceptionUtils.throwError(
                    Label.VTT_Unexpected_WOA_Records,
                    new List<String> {
                            workOrderActivityId
                    }
            );
        }

        return activity;
    }

}