/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Endpoint that serves for querying Work Order Activities for Lightning components
Test Class:     VTT_WorkOrderActivity_EndpointTest
History:        jschn 20/11/2019 - Created.
*************************************************************************************************/
public with sharing class VTT_WorkOrderActivity_Endpoint {

    public static VTT_WOA_MA_ActivityDAO selector = new VTT_WorkOrderActivitySelector();

    /**
     * Using selector, it retrieves Work Order Activities based on provided WOA Ids
     *
     * @param woaIds
     *
     * @return List<Work_Order_Activity__c>
     */
    @AuraEnabled
    public static List<Work_Order_Activity__c> getRecords(List<Id> woaIds) {
        return selector.getRecordsForAssignments(woaIds);
    }

}