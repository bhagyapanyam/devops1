/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for VTT_WorkOrderFLOCReparentBatch class
History:        jschn 2018-12-14 - Created. (US - 001323)
*************************************************************************************************/
@IsTest
public class VTT_WorkOrderFLOCReparentBatchTest {

    @IsTest
    static void withoutProperParams() {
        Boolean errorOccurred = false;
        try {
            Test.startTest();
            VTT_WorkOrderFLOCReparentBatch job = new VTT_WorkOrderFLOCReparentBatch(null, null);
            Database.executeBatch(job);
            Test.stopTest();
        } catch(Exception ex) {
            errorOccurred = true;
        }

        System.assert(errorOccurred, 'start method of bach job should fail bcs of missing query string' +
                ' generated based on first parameter.');
    }

    @IsTest
    static void workOrder_withoutRerunChild() {
        setData(DataType.QUERYABLE);

        Test.startTest();
        VTT_WorkOrderFLOCReparentBatch job = new VTT_WorkOrderFLOCReparentBatch(
                VTT_WorkOrderFLOCReparentBatch.BatchType.WORK_ORDER, false);
        Database.executeBatch(job);
        Test.stopTest();

        List<HOG_Maintenance_Servicing_Form__c> workOrders = [
                SELECT Business_Unit_Lookup__c, Operating_Field_AMU_Lookup__c
                FROM HOG_Maintenance_Servicing_Form__c
        ];
        System.assertEquals(1, workOrders.size(), 'Only one WO should be stored in DB');
        HOG_Maintenance_Servicing_Form__c workOrder = workOrders.get(0);
        System.assert(String.isNotBlank(workOrder.Operating_Field_AMU_Lookup__c), 'AMU should be populated by now.');
    }

    @IsTest
    static void workOrder_withRerunChild() {
        setData(DataType.QUERYABLE);

        Test.startTest();
        VTT_WorkOrderFLOCReparentBatch job = new VTT_WorkOrderFLOCReparentBatch(
                VTT_WorkOrderFLOCReparentBatch.BatchType.WORK_ORDER, true);
        Database.executeBatch(job);
        Test.stopTest();

        List<HOG_Maintenance_Servicing_Form__c> workOrders = [
                SELECT Business_Unit_Lookup__c, Operating_Field_AMU_Lookup__c
                FROM HOG_Maintenance_Servicing_Form__c
        ];
        System.assertEquals(1, workOrders.size(), 'Only one WO should be stored in DB');
        HOG_Maintenance_Servicing_Form__c workOrder = workOrders.get(0);
        System.assert(String.isNotBlank(workOrder.Operating_Field_AMU_Lookup__c), 'AMU should be populated by now for WO.');

        List<Work_Order_Activity__c> activities = [
                SELECT Operating_Field_AMU__c
                FROM Work_Order_Activity__c
        ];
        System.assertEquals(1, activities.size(), 'Only one WOA should be stored in DB');
        Work_Order_Activity__c activity = activities.get(0);
        System.assert(String.isNotBlank(activity.Operating_Field_AMU__c), 'AMU should be populated by now for WOA.');
    }

    @IsTest
    static void workOrderActivities() {
        setData(DataType.QUERYABLE);

        Test.startTest();
        VTT_WorkOrderFLOCReparentBatch job = new VTT_WorkOrderFLOCReparentBatch(
                VTT_WorkOrderFLOCReparentBatch.BatchType.WORK_ORDER_ACTIVITY, false);
        Database.executeBatch(job);
        Test.stopTest();

        List<HOG_Maintenance_Servicing_Form__c> workOrders = [
                SELECT Business_Unit_Lookup__c, Operating_Field_AMU_Lookup__c
                FROM HOG_Maintenance_Servicing_Form__c
        ];
        System.assertEquals(1, workOrders.size(), 'Only one WO should be stored in DB');
        HOG_Maintenance_Servicing_Form__c workOrder = workOrders.get(0);
        System.assert(String.isBlank(workOrder.Operating_Field_AMU_Lookup__c), 'AMU should not be populated by now for WO.');

        List<Work_Order_Activity__c> activities = [
                SELECT Operating_Field_AMU__c
                FROM Work_Order_Activity__c
        ];
        System.assertEquals(1, activities.size(), 'Only one WOA should be stored in DB');
        Work_Order_Activity__c activity = activities.get(0);
        System.assert(String.isNotBlank(activity.Operating_Field_AMU__c), 'AMU should be populated by now for WOA.');
    }


    @IsTest
    static void withoutQueryableRecord() {
        setData(DataType.NON_QUERYABLE);

        Test.startTest();
        VTT_WorkOrderFLOCReparentBatch job = new VTT_WorkOrderFLOCReparentBatch(
                VTT_WorkOrderFLOCReparentBatch.BatchType.WORK_ORDER_ACTIVITY, false);
        Database.executeBatch(job);
        Test.stopTest();

        List<HOG_Maintenance_Servicing_Form__c> workOrders = [
                SELECT Business_Unit_Lookup__c, Operating_Field_AMU_Lookup__c
                FROM HOG_Maintenance_Servicing_Form__c
        ];
        System.assertEquals(1, workOrders.size(), 'Only one WO should be stored in DB');
        HOG_Maintenance_Servicing_Form__c workOrder = workOrders.get(0);
        System.assert(String.isBlank(workOrder.Operating_Field_AMU_Lookup__c), 'AMU should not be populated by now for WO.');

        List<Work_Order_Activity__c> activities = [
                SELECT Operating_Field_AMU__c
                FROM Work_Order_Activity__c
        ];
        System.assertEquals(1, activities.size(), 'Only one WOA should be stored in DB');
        Work_Order_Activity__c activity = activities.get(0);
        System.assert(String.isBlank(activity.Operating_Field_AMU__c), 'AMU should not be populated by now for WOA.');

    }

    private static void setData(DataType type) {
        String funcLoc = '123-456-789';

        HOG_Maintenance_Servicing_Form__c wo = VTT_TestDataFactory.createWorkOrder(false);
        wo.Functional_Location__c = funcLoc;
        wo.Functional_Location_Category_SAP__c = 9;
        if(type == DataType.NON_QUERYABLE) {
            wo.DLFL__c = true;
            wo.ALT_Confirmed__c = false;
        } else if(type == DataType.QUERYABLE) {
            wo.ALT_Confirmed__c = false;
        }
        insert wo;

        Work_Order_Activity__c woa = VTT_TestDataFactory.createWorkOrderActivity('New', 'Test woa', wo.Id , null, false);
        woa.Functional_Location__c = funcLoc;
        insert woa;

        Field__c amu = HOG_TestDataFactory.createAMU('test amu', false);
        amu.Functional_Location__c =  funcLoc;
        insert amu;

        Location__c location = (Location__c) HOG_SObjectFactory.createSObject(new Location__c(), false);
        location.Operating_Field_AMU__c = amu.Id;
        insert location;

        Well_Event__c event = (Well_Event__c) HOG_SObjectFactory.createSObject(new Well_Event__c(), false);
        event.Well_ID__c = location.Id;
        event.Functional_Location__c = funcLoc;
        event.Functional_Location_Description_SAP__c = funcLoc;
        insert event;

    }

    private enum DataType {
        QUERYABLE, NON_QUERYABLE
    }

}