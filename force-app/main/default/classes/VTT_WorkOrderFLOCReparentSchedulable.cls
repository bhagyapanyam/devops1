/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Executes batch which will reparent all required Work Orders and Work Order Activities
Test Class:     VTT_WorkOrderFLOCReparentSchedulableTest
History:        jschn 2018-12-14 - Created. (US - 001323)
*************************************************************************************************/
public without sharing class VTT_WorkOrderFLOCReparentSchedulable implements System.Schedulable{

    public void execute(SchedulableContext context) {
        Database.executeBatch(
                new VTT_WorkOrderFLOCReparentBatch(VTT_WorkOrderFLOCReparentBatch.BatchType.WORK_ORDER, true)
        );
    }

}