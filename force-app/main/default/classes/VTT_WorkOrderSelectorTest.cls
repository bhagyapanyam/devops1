/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for VTT_WorkOrderSelector class
History:        jschn 04/11/2019 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class VTT_WorkOrderSelectorTest {

    @IsTest
    static void getRecordsByFilter_withoutFilterParam() {
        VTT_WOA_SearchCriteria filter;
        String sorting = 'Name ASC';
        Integer offset = 0;
        Integer queryLimit = 1;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;
        List<HOG_Maintenance_Servicing_Form__c> result;
        VTT_WOA_ListViewDAO selector = new VTT_WorkOrderSelector();

        Test.startTest();
        try {
            result = selector.getRecordsByFilter(filter, sorting, offset, queryLimit);
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            System.debug(ex.getStackTraceString());
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void getRecordsByFilter_withoutSortingParam() {
        VTT_WOA_SearchCriteria filter = new VTT_WOA_SearchCriteria();
        String sorting;
        Integer offset = 0;
        Integer queryLimit = 1;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;
        List<HOG_Maintenance_Servicing_Form__c> result;
        VTT_WOA_ListViewDAO selector = new VTT_WorkOrderSelector();

        Test.startTest();
        try {
            result = selector.getRecordsByFilter(filter, sorting, offset, queryLimit);
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            System.debug(ex.getStackTraceString());
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void getRecordsByFilter_withoutOffsetParam() {
        VTT_WOA_SearchCriteria filter = new VTT_WOA_SearchCriteria();
        String sorting = 'Name ASC';
        Integer offset;
        Integer queryLimit = 1;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;
        List<HOG_Maintenance_Servicing_Form__c> result;
        VTT_WOA_ListViewDAO selector = new VTT_WorkOrderSelector();

        Test.startTest();
        try {
            result = selector.getRecordsByFilter(filter, sorting, offset, queryLimit);
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            System.debug(ex.getStackTraceString());
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void getRecordsByFilter_withoutQueryLimitParam() {
        VTT_WOA_SearchCriteria filter = new VTT_WOA_SearchCriteria();
        String sorting = 'Name ASC';
        Integer offset = 0;
        Integer queryLimit;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;
        List<HOG_Maintenance_Servicing_Form__c> result;
        VTT_WOA_ListViewDAO selector = new VTT_WorkOrderSelector();

        Test.startTest();
        try {
            result = selector.getRecordsByFilter(filter, sorting, offset, queryLimit);
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            System.debug(ex.getStackTraceString());
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void getRecordsByFilter_withAllParams() {
        VTT_WOA_SearchCriteria filter = new VTT_WOA_SearchCriteria();
        String sorting = 'Name ASC';
        Integer offset = 0;
        Integer queryLimit = 1;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        List<HOG_Maintenance_Servicing_Form__c> result;
        VTT_WOA_ListViewDAO selector = new VTT_WorkOrderSelector();
        Integer expectedCount = 0;

        Test.startTest();
        try {
            result = selector.getRecordsByFilter(filter, sorting, offset, queryLimit);
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            System.debug(ex.getStackTraceString());
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

}