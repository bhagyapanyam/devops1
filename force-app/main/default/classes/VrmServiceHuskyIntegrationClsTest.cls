@isTest

public class VrmServiceHuskyIntegrationClsTest
{
    @isTest
    public static void unitTest()
    {
        Profile p = [Select id from Profile where Name = 'System Administrator' Limit 1];
        
        User u = new User(FirstName = 'Test', LastName= 'User', Alias = 'UserT', ProfileId = p.Id, Email = 'abc@abc.abc',
                            Username = 'abc@abc.abc'+System.currentTimeMillis(),CompanyName = 'TEST',Title = 'title',TimeZoneSidKey = 'America/Los_Angeles',EmailEncodingKey = 'UTF-8',
                            LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US');
        insert u;
        
        User u1 = new User(FirstName = 'Test', LastName= 'User1', Alias = 'UserT1', ProfileId = p.Id, Email = 'abc1@abc1.abc1',
                            Username = 'abc1@abc1.abc1'+System.currentTimeMillis(),CompanyName = 'TEST',Title = 'title',TimeZoneSidKey = 'America/Los_Angeles',EmailEncodingKey = 'UTF-8',
                            LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US');
        insert u1;
        
        jbrvrm__Vendor_Subscription_Title__c vst = new jbrvrm__Vendor_Subscription_Title__c(Name = 'Test Vendor Subscription Title', jbrvrm__ServiceNow_SYS_ID__c = '0021295fdd2916446b6dbf97591750cc',
                                            jbrvrm__Contract_End_Date__c = System.today(), jbrvrm__Vendor_Analyst__c = u.Id, jbrvrm__Status__c = 'Active');
        insert vst;
        
        jbrvrm__Vendor_Subscription_Title_Component__c vstc = new jbrvrm__Vendor_Subscription_Title_Component__c(Name = 'Test Vendor Subscription Title Comp', jbrvrm__ServiceNow_SYS_ID__c = '0021295fdd2916446b6dbf97591750cc', jbrvrm__Vendor_Subscription_Title__c = vst.Id);
        insert vstc;
        
        jbrvrm__Vendor_Subscription_Title__c vst1 = new jbrvrm__Vendor_Subscription_Title__c(Name = 'Test Vendor Subscription Title', jbrvrm__ServiceNow_SYS_ID__c = '0021295fdd2916446b6dbf97591750dd',
                                            jbrvrm__Contract_End_Date__c = System.today(), jbrvrm__Vendor_Analyst__c = u1.Id, jbrvrm__Status__c = 'Active');
        insert vst1;
        
        jbrvrm__Vendor_Subscription_Title_Component__c vstc1 = new jbrvrm__Vendor_Subscription_Title_Component__c(Name = 'Test Vendor Subscription Title 2 Comp', jbrvrm__ServiceNow_SYS_ID__c = '0021295fdd2916446b6dbf97591750dd', jbrvrm__Vendor_Subscription_Title__c = vst1.Id);
        insert vstc1;
        
        List<VrmServiceHuskyIntegrationCls.VSTInfo> vTestList = new List<VrmServiceHuskyIntegrationCls.VSTInfo>();
        
        VrmServiceHuskyIntegrationCls.VSTInfo vTest = new VrmServiceHuskyIntegrationCls.VSTInfo();
        vTest.name = 'Test SNow';
        vTest.u_service_delivery_specialist = 'Test User1 (UserT1)';
        vTest.sys_id = '0021295fdd2916446b6dbf97591750cc';
        
        vTestList.add(vTest);
        
        VrmServiceHuskyIntegrationCls.VSTInfo vTest1 = new VrmServiceHuskyIntegrationCls.VSTInfo();
        vTest1.name = 'Test SNow 1';
        vTest1.u_service_delivery_specialist = 'Test User (UserT)';
        vTest1.sys_id = '0021295fdd2916446b6dbf97591750dd';
        vTestList.add(vTest1);
        
        
        
        List<VrmServiceHuskyIntegrationCls.VSTInfo> vTestList2 = new List<VrmServiceHuskyIntegrationCls.VSTInfo>();
        
        VrmServiceHuskyIntegrationCls.VSTInfo vTest2 = new VrmServiceHuskyIntegrationCls.VSTInfo();
        vTest2.name = 'Test SNow';
        vTest2.u_service_delivery_specialist = 'Test User1 (UserT1)';
        vTest2.sys_id = '0021295fdd2916446b6dbf97591750cc';
        
        vTestList2.add(vTest2);
        
        VrmServiceHuskyIntegrationCls.VSTInfo vTest3 = new VrmServiceHuskyIntegrationCls.VSTInfo();
        vTest3.name = 'Test SNow 1';
        vTest3.u_service_delivery_specialist = 'Test User (UserT)';
        vTest3.sys_id = '0021295fdd2916446b6dbf97591750dd';
        vTestList2.add(vTest3);
        
        /* -------------------------------------------------------------------------------------- */
        
        List<VrmSubsCompServiceHuskyIntCls.VSTInfo> vTestListA = new List<VrmSubsCompServiceHuskyIntCls.VSTInfo>();
        
        VrmSubsCompServiceHuskyIntCls.VSTInfo vTestA = new VrmSubsCompServiceHuskyIntCls.VSTInfo();
        vTestA.name = 'Test SNow';
        vTestA.u_service_delivery_specialist = 'Test User1 (UserT1)';
        vTestA.sys_id = '0021295fdd2916446b6dbf97591750cc';
        
        vTestListA.add(vTestA);
        
        VrmSubsCompServiceHuskyIntCls.VSTInfo vTestB = new VrmSubsCompServiceHuskyIntCls.VSTInfo();
        vTestB.name = 'Test SNow 1';
        vTestB.u_service_delivery_specialist = 'Test User (UserT)';
        vTestB.sys_id = '0021295fdd2916446b6dbf97591750dd';
        vTestListA.add(vTestB);
        
        
        
        List<VrmSubsCompServiceHuskyIntCls.VSTInfo> vTestListB = new List<VrmSubsCompServiceHuskyIntCls.VSTInfo>();
        
        VrmSubsCompServiceHuskyIntCls.VSTInfo vTestC = new VrmSubsCompServiceHuskyIntCls.VSTInfo();
        vTestC.name = 'Test SNow';
        vTestC.u_service_delivery_specialist = 'Test User1 (UserT1)';
        vTestC.sys_id = '0021295fdd2916446b6dbf97591750cc';
        
        vTestListB.add(vTestC);
        
        VrmSubsCompServiceHuskyIntCls.VSTInfo vTestD = new VrmSubsCompServiceHuskyIntCls.VSTInfo();
        vTestD.name = 'Test SNow 1';
        vTestD.u_service_delivery_specialist = 'Test User (UserT)';
        vTestD.sys_id = '0021295fdd2916446b6dbf97591750dd';
        vTestListB.add(vTestD);
        
        
        
        Test.startTest();
        
        /*VrmServiceHuskyIntegrationCls.SNowToVrm(vTestList);
        VrmServiceHuskyIntegrationCls.VrmToSNow();
        */
        
        VrmServiceHuskyIntegrationCls.SNowToVrm(vTestList2);
        VrmServiceHuskyIntegrationCls.VrmToSNow();
        
        VrmSubsCompServiceHuskyIntCls.SNowToVrm(vTestListB);
        VrmSubsCompServiceHuskyIntCls.VrmToSNow();
        
        Test.stopTest();
        
        
    }
}