/*************************************************************************************************\
Author:         Martin Bilicka
Company:        Husky Energy
Related Class:  WS_FieldDefaults
				HOG_SObjectFactory
Description:    Well Servicies SObjects factory class using field defaults for testing purposes.
History:        mbilicka 25.3.2018 - Created.
****************************************************************************************************
WARNING.. WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..
****************************************************************************************************
DO NOT CHANGE THIS CLASS, you can extend but don't change any values these. If for any reason
you have to then rerun following tests:
WS_WellOptimizationControllerXTest
**************************************************************************************************/
@isTest
public with sharing class WS_TestDataFactory {


    private static Well_Optimization__c createWellOptimization(){
        return (Well_Optimization__c) HOG_SObjectFactory.createSObject(new Well_Optimization__c(),WS_FieldDefaults.CLASS_NAME);
    }

    /**
     * Well Optimization creation class
	 * @param  wellLocationId       Location__c
	 * @param  controlRoomCenterId  Control_Room_Centre__c
	 * @param  doInsert             Indicates if returned record is stored in DB
	 * @return                      Well_Optimization__c
	 */
    public static Well_Optimization__c createWellOptimization(Id wellLocationId, Id controlRoomCenterId, boolean doInsert){
        Well_Optimization__c wellOpt = createWellOptimization();
        wellOpt.Well_Location__c = wellLocationId;
        wellOpt.Control_Room_Centre__c = controlRoomCenterId;

        if (doInsert) {
            insert wellOpt;
        }
        return wellOpt;
    }

    /**
     * Well Optimization creation class
     * @param  wellLocationId       Location__c
     * @param  controlRoomCenterId  Control_Room_Centre__c
     * @param  finalSpeed           Final_Speed__c
     * @param  pumpRate             Pump_Rate_m_RPM_Day__c
     * @param  doInsert             Indicates if returned record is stored in DB
     * @return                      Well_Optimization__c
     */
    public static Well_Optimization__c createWellOptimization(Id wellLocationId, Id controlRoomCenterId, Integer finalSpeed, Decimal pumpRate, boolean doInsert){
        Well_Optimization__c wellOpt = createWellOptimization();
        wellOpt.Well_Location__c = wellLocationId;
        wellOpt.Control_Room_Centre__c = controlRoomCenterId;
        wellOpt.Final_Speed__c = finalSpeed;
        wellOpt.Pump_Rate_m_RPM_Day__c = pumpRate;

        if (doInsert) {
            insert wellOpt;
        }
        return wellOpt;
    }

    /**
     * Well Optimization creation class
     * @param  doInsert             Indicates if returned record is stored in DB
     * @return                      Well_Optimization__c
     */
    public static Well_Optimization__c createWellOptimization(boolean doInsert){
        Location__c wellLocation = HOG_TestDataFactory.createLocation(true);
        Field__c opField = HOG_TestDataFactory.createAMU(true);

        Well_Optimization__c wellOpt = createWellOptimization();
        wellOpt.Well_Location__c = wellLocation.Id;
        wellOpt.Control_Room_Centre__c = opField.Id;

        if (doInsert) {
            insert wellOpt;
        }
        return wellOpt;
    }

    public static User createWSUser(){
        User testUser = HOG_TestDataFactory.createUser('Test','User', 'testuser');
        testUser = HOG_TestDataFactory.assignPermissionSet(testUser,'HOG_Control_Room_Operator_New');
        return testUser;
    }

}