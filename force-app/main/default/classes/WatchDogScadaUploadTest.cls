@isTest
private class WatchDogScadaUploadTest {
    
    static testmethod void runTest() {
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.subject = 'Volume Report';
        email.fromName = 'test@gmail.com';
        email.plainTextBody = 'Attached Files';
        
        String str = 'WellName,UWI,Run Name,Area Sup,Foreman,Drop Below LCL,Min,Max,Average,Stan. Dev.,LCL,UCL,'
                        + '2017-08-10,2017-08-09,2017-08-08,2017-08-07,2017-08-06,2017-08-05,2017-08-04,2017-08-03,'
                        + '2017-08-02,2017-08-01,2017-07-31,2017-07-30,2017-07-29,2017-07-28,2017-07-27,2017-07-26,'
                        + '2017-07-25,2017-07-24,2017-07-23,2017-07-22,2017-07-21,2017-07-20,2017-07-19,2017-07-18,'
                        + '2017-07-17,2017-07-16,2017-07-15,2017-07-14,2017-07-13,2017-07-12\n'
                        + 'ANSL100-08-26-049-25GasCarRun1,100/08-26-049-25W5/02,AnsellFacility,Harold Norberg,Steve Melnyk,'
                        + '0,0,0,0,0,0,0,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'
                        + '0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00';
        Blob b = Blob.valueOf( str );
        
        Messaging.InboundEmail.BinaryAttachment[] binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[1];  
        Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
        binaryAttachment.Filename = 'MassData.txt';
        binaryAttachment.Body = b;
        binaryAttachments[0] =  binaryAttachment ;
        email.binaryAttachments = binaryAttachments ;
        envelope.fromAddress = 'test@huskyenergy.com';
        
        test.startTest();
            // setup controller object
            WatchDogScadaUpload controller = new WatchDogScadaUpload();
            Messaging.InboundEmailResult result = controller.handleInboundEmail( email, envelope );
            
            System.assertEquals( result.success  ,true);
        test.stopTest();
    }
}