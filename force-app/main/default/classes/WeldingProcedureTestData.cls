public class WeldingProcedureTestData {

    public Static List<Welding_Procedure__c> createProcedures(Integer recCount, Account acct) {
        List<Welding_Procedure__c> weldingProcedureList = new List<Welding_Procedure__c>();
        
        for(Integer i = 0; i < recCount; i++) {
        	Welding_Procedure__c procedure = new Welding_Procedure__c();
            procedure.Name = 'Welding Procedure ' + i;
            procedure.Vendor__c = acct.Id;
            procedure.WPS_Number__c = 'WPS Number ' + i;
            procedure.Revision_Date__c = System.today();
            procedure.Min_Thickness__c = '1 mm';
            procedure.Min_Diameter__c = '1 cm';
            procedure.Application__c = 'ASME';
            procedure.CVN__c = 'No';
            procedure.Process__c = 'FCAW';
            procedure.Service__c = 'Sweet';
            procedure.Registered_Locations__c = 'AB';
            procedure.PWHT__c = 'Yes';
            procedure.Material_1__c = 'P. No. 1';
            procedure.PQR_Number__c = 'FCAW';
            procedure.Electrode__c = 'E71T - 1';
            procedure.Revision_Number__c = '1';
            procedure.Max_Thickness__c = '19.5 mm';
            procedure.Max_Diameter__c = 'Unlimited';
            procedure.Hardness__c = 'Brinell';
            procedure.Position__c = '1G';
            procedure.Material_2__c = '1G';
            procedure.Status__c = 'Accepted';
            procedure.NormTemp__c = true;
            
            weldingProcedureList.add(procedure);
        }
        
        insert weldingProcedureList;
        
        return weldingProcedureList;
    }
}