public class WellPartnersController
{
    public Id wellId {get;set;}
    
    public GRD_Borehole__c well {get;set;}
    
    public List<Well_Partner__c> partners {get;set;}
    
    public String sortField         {get;set;}
    public String previousSortField {get;set;}
    public String sortOrder         {get;set;}
    
    public WellPartnersController()
    {
        // Get well id
        wellId = ApexPages.CurrentPage().getParameters().get('id');
        
        well = [SELECT Name FROM GRD_Borehole__c WHERE Id =: wellId];
        
        // Get all its partners
        partners = [SELECT Id, Name, Effective__c, Expiry__c, Percentage__c FROM Well_Partner__c WHERE GRD_Borehole__c =: wellId ORDER BY Name];
        sortField = 'Name';
        previousSortField = null;
    }
    
    public PageReference sortPartners()
    {
        Boolean hasWHERE = false;
        
        String order = 'asc NULLS FIRST';
        
        sortOrder = 'ASC';
        
        // If we`re sorting the same field as last time, then we want to switch the order from ASC to DESC
        if(previousSortField == sortField)
        {
            order = 'desc NULLS LAST';
            previousSortField = null;
            sortOrder = 'DESC';
        }
        else
        {
            previousSortField = sortField;
        }
        
        String query = 'SELECT Id, Name, Effective__c, Expiry__c, Percentage__c FROM Well_Partner__c WHERE GRD_Borehole__c =: wellId ';
        
        if(sortField == 'Effective')
        {
            query += 'ORDER BY Effective__c ' + order;
        }
        else if(sortField == 'Expiry')
        {
            query += 'ORDER BY Expiry__c ' + order;
        }
        else if(sortField == 'Percentage')
        {
            query += 'ORDER BY Percentage__c ' + order;
        }
        else // sortField == 'Name'
        {
            query += 'ORDER BY Name ' + order;
        }
        
        partners = Database.query(query);
        return null;
    }
}