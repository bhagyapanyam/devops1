/*---------------------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: Test class for WellServicingGlobal
----------------------------------------------------------------------------------------------------------*/  
@isTest
private class WellServicingGlobalTest 
{
    static List<HOG_Maintenance_Servicing_Form__c> riglessServicingForm;
	static List<HOG_Service_Request_Notification_Form__c> serviceRequest;
	static List<Well_Tracker__c> wellTracker;
    static List<HOG_Notification_Type__c> notificationType;
    static List<HOG_Notification_Type_Priority__c> notificationTypePriority;
    static List<HOG_Work_Order_Type__c> workOrderType;
    static HOG_Service_Code_MAT__c serviceCodeMAT;
    static HOG_Service_Category__c serviceCategory;
    static HOG_Service_Code_Group__c serviceCodeGroup;
    static HOG_Service_Required__c serviceRequiredList;
	static HOG_User_Status__c userStatus;
	static HOG_Service_Priority__c servicePriorityList;
	static Business_Unit__c businessUnit;
	static Business_Department__c businessDepartment;
	static Operating_District__c operatingDistrict;
	static Field__c field;
	static Route__c route;
	static Location__c location;	
	static Facility__c facility;		
	static Account account;			
	static HOG_Settings__c settings;
	static Id recordTypeId;
	static List<String> WORKORDERRECORDTYPE = new List<String>{'Flushby - Production Flush', 'Coil Tubing Unit - Rod Side Entry'};
	
    @isTest    
    static void WellServicingGlobal_Test()
    {
		SetupTestData();
                        
        Test.startTest();
   
        String returnMessage;

        //test error condition after API success
        riglessServicingForm[4].Notification_Number__c = '9988776F';
        riglessServicingForm[3].Service_Status__c = 'Complete';
        riglessServicingForm[4].Service_Completed__c = System.now();
        update riglessServicingForm[4];
        WellServicingGlobal.testErrorCondition = true;
        returnMessage = WellServicingGlobal.updateTECO(riglessServicingForm[4].Id, true);
        WellServicingGlobal.testErrorCondition = false;
        System.assertNotEquals(null, returnMessage);

		// TECOed
        returnMessage = WellServicingGlobal.updateTECO(riglessServicingForm[0].Id, true);    
        System.assertNotEquals(null, returnMessage);

		// No Work Order Nummber
        returnMessage = WellServicingGlobal.updateTECO(riglessServicingForm[1].Id, true);    
        System.assertNotEquals(null, returnMessage);

		// Service Status is not Complete and not Cancelled
        returnMessage = WellServicingGlobal.updateTECO(riglessServicingForm[2].Id, true);    
        System.assertNotEquals(null, returnMessage);

        // Test API        
        returnMessage = WellServicingGlobal.updateTECO(riglessServicingForm[3].Id, true);    
        System.assertNotEquals(null, returnMessage);
        
        // invalid Id
        returnMessage = WellServicingGlobal.updateTECO(null, true);    
        System.assertNotEquals(null, returnMessage);

		// notification = null
        returnMessage = WellServicingGlobal.updateTECO(riglessServicingForm[4].Id, true);    
        System.assertNotEquals(null, returnMessage);

        Test.stopTest();                
    }  
    
    private static void SetupTestData()
	{        
        //-- Begin setup of data needed to test the Service Request Notification controller --//
                
        //-- Setup Service Category
        serviceCategory = ServiceCategoryTestData.createServiceCategory('Test Category');
        insert serviceCategory;
        //--

        //-- Setup Service Code Group
        serviceCodeGroup = ServiceCodeGroupTestData.createServiceCodeGroup('Test Group', true);
        insert serviceCodeGroup;
        //--

        //-- Setup Notification Type
	    notificationType = new List<HOG_Notification_Type__c>();        
 		notificationType.add((HOG_Notification_Type__c)NotificationTypeTestData.createNotificationType(serviceCategory.Id, false, true, true, true));
 		notificationType.add((HOG_Notification_Type__c)NotificationTypeTestData.createNotificationType(serviceCategory.Id, true, false, true, true));
        insert notificationType;
        //--
 
        //-- Setup Service Code MAT        
 	    serviceCodeMAT = ServiceCodeMATTestData.createServiceCodeMAT('Test MAT1', 'TS1', WORKORDERRECORDTYPE[0]);
        insert serviceCodeMAT;
        //--

        //-- Setup Service Required
        serviceRequiredList = ServiceRequiredTestData.createServiceRequired('Test Service Required', true, true, true);
        insert serviceRequiredList;
        //--

        //-- Setup Service User Status
		userStatus = UserStatusTestData.createUserStatus('Test User Status', 'Test Description');
        insert userStatus;
        //--

        //-- Setup Service - Priority
		servicePriorityList = ServicePriorityTestData.createServicePriority('Service Priority 1', 1, '1');
        insert servicePriorityList;
        //--

        //-- Setup Notification Type - Priority Detail
     	notificationTypePriority = new List<HOG_Notification_Type_Priority__c>();
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[0].Id, servicePriorityList.Id, true));                
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[0].Id, servicePriorityList.Id, false));                
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[1].Id, servicePriorityList.Id, true));                
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[1].Id, servicePriorityList.Id, false));                
        insert notificationTypePriority;        
        //--
                
        //-- Setup Work Order Type
 	    workOrderType = new List<HOG_Work_Order_Type__c>();
 	    workOrderType.add(WorkOrderTypeTestData.createWorkOrderType(notificationType[0].Id, serviceCodeMAT.Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[0], true));
 	    workOrderType.add(WorkOrderTypeTestData.createWorkOrderType(notificationType[1].Id, serviceCodeMAT.Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[1], true));
        insert workOrderType;
        //--
                                        
        //-- Setup objects for Well Tracker                                
        businessUnit = new Business_Unit__c(Name = 'Test Business Unit');
        insert businessUnit;

		businessDepartment = BusinessDepartmentTestData.createBusinessDepartment('Test Business Department');    
        insert businessDepartment;

		operatingDistrict = OperatingDistrictTestData.createOperatingDistrict('Test Field', businessDepartment.Id, businessUnit.Id);
        insert operatingDistrict;

        recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;

		field = FieldTestData.createField('Test Field Control Centre', operatingDistrict.Id, recordTypeId);
        insert field;

		route = RouteTestData.createRoute('999');
        insert route;                  

		location = LocationTestData.createLocation('Test Location', route.Id, field.Id);		
        insert location;

		facility = FacilityTestData.createFacility('Test Facility', route.Id, field.Id);
        insert facility;        

		account = AccountTestData.createAccount('Test Account', null);			
        insert account;

		wellTracker = new List<Well_Tracker__c>();
		wellTracker.add((Well_Tracker__c)WellTrackerTestData.createWellTracker(location.Id, null, field.Id, 1, Datetime.valueOf('2013-06-20 12:00:00'), 'Producing', 'Engineering'));
		wellTracker.add((Well_Tracker__c)WellTrackerTestData.createWellTracker(null, facility.Id, field.Id, 1, Datetime.valueOf('2013-06-20 12:00:00'), 'Producing', 'Engineering'));
        insert wellTracker;
        //--

		serviceRequest = new List<HOG_Service_Request_Notification_Form__c>();

		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[0].Id,
				workOrderType[0].Id,
				null,
				null,
				null,
				null,
				null,
				null,
				notificationTypePriority[1].Id,
				notificationTypePriority[0].Id,
				account.Id,
				'9988776A',
				'9999ABCD',
				'Test Detail',
                'Test Title',
				Date.today(),
				Date.today() + 10
			));
			
		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[1].Id,
				workOrderType[1].Id,
				null,
				null,
				null,
				null,
				null,
				null,
				notificationTypePriority[3].Id,
				notificationTypePriority[2].Id,
				account.Id,
				'9988775B',
				'9999ABCE',
				'Test Detail',
                'Test Title',
				Date.today(),
				Date.today() + 10
			));

		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[0].Id,
				workOrderType[0].Id,
				null,
				null,
				null,
				null,
				null,
				null,
				notificationTypePriority[1].Id,
				notificationTypePriority[0].Id,
				account.Id,
				'9988776C',
				'9999ABCX',
				'Test Detail',
                'Test Title',
				Date.today(),
				Date.today() + 10
			));

		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[0].Id,
				workOrderType[0].Id,
				null,
				null,
				null,
				null,
				null,
				null,
				notificationTypePriority[1].Id,
				notificationTypePriority[0].Id,
				account.Id,
				'9988776D',
				'9999ABCY',
				'Test Detail',
                'Test Title',
				Date.today(),
				Date.today() + 10
			));

		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[0].Id,
				workOrderType[0].Id,
				null,
				null,
				null,
				null,
				null,
				null,
				notificationTypePriority[1].Id,
				notificationTypePriority[0].Id,
				account.Id,
				'9988776F',
				'9999ABCZ',
				'Test Detail',
                'Test Title',
				Date.today(),
				Date.today() + 10
			));
        //-- End setup of data needed to test the Service Request Notification controller --//

        //-- Start setup data for Work Order        
	    ServiceRequestNotificationUtilities.executeTriggerCode = false;
    	WellServicingUtilities.executeTriggerCode = false;
    	MaintenanceServicingUtilities.executeTriggerCode = false;
        
        insert serviceRequest;

        riglessServicingForm = new List<HOG_Maintenance_Servicing_Form__c>();
        
		ServiceRequestNotificationUtilities.ServiceRequest request = new ServiceRequestNotificationUtilities.ServiceRequest(); 

    	request.workOrderName = 'Test Location';		
    	request.workOrderCount = 1;
		request.notificationType = notificationType[0].Id;
        request.serviceTimeIsRequired = false;
		request.vendorInvoicingPersonnelIsRequired = false;
        					
		request.record = serviceRequest[0];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[1];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[2];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[3];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[4];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
        
        riglessServicingForm[0].TECO__c = true;
        riglessServicingForm[0].Unit_Number__c = 'Test';        
        riglessServicingForm[1].Work_Order_Number__c = null;        
        riglessServicingForm[1].Unit_Number__c = 'Test';        
        riglessServicingForm[2].Service_Status__c = null;
        riglessServicingForm[2].Unit_Number__c = 'Test';        
        riglessServicingForm[3].Service_Status__c = 'Complete';
        riglessServicingForm[3].Service_Completed__c = System.now();
        riglessServicingForm[3].Unit_Number__c = 'Test';        
        riglessServicingForm[4].Service_Status__c = 'Complete';
        riglessServicingForm[4].Notification_Number__c = null;
        riglessServicingForm[4].Unit_Number__c = 'Test';        
        
        insert riglessServicingForm;
        //-- End setup data for Work Order        
	}
}