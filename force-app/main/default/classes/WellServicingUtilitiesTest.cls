/*---------------------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: Test class for WellServicingUtilities
----------------------------------------------------------------------------------------------------------*/  
@isTest
private class WellServicingUtilitiesTest
{
    static List<HOG_Maintenance_Servicing_Form__c> riglessServicingForm;
	static List<HOG_Service_Request_Notification_Form__c> serviceRequest;
	static List<Well_Tracker__c> wellTracker;
    static List<HOG_Notification_Type__c> notificationType;
    static List<HOG_Notification_Type_Priority__c> notificationTypePriority;
    static List<HOG_Work_Order_Type__c> workOrderType;
	static List<HOG_Service_Time__c> serviceTime;
	static List<HOG_Vendor_Personnel__c> vendorPersonnel;
    static HOG_Service_Code_MAT__c serviceCodeMAT;
    static HOG_Service_Category__c serviceCategory;
    static HOG_Service_Code_Group__c serviceCodeGroup;
    static HOG_Service_Required__c serviceRequiredList;
	static HOG_User_Status__c userStatus;
	static HOG_Service_Priority__c servicePriorityList;
	static Business_Unit__c businessUnit;
	static Business_Department__c businessDepartment;
	static Operating_District__c operatingDistrict;
	static Field__c field;
	static Route__c route;
	static Location__c location;	
	static Facility__c facility;		
    static Equipment__c equipment;
	static Account account;			
	static HOG_Settings__c settings;
	static Id recordTypeId;
	static List<String> WORKORDERRECORDTYPE = new List<String>{'Flushby - Production Flush', 'Coil Tubing Unit - Rod Side Entry'};
	
	@isTest    
    static void TriggerBeforeUpdateWellServicingForm_Test()
    {
		SetupTestData();
		
        List<HOG_Maintenance_Servicing_Form__c> updatedRecord = 
            [Select 
                Id,
                Name,
                Work_Details__c,
                Work_Order_Number__c,
                Well_Down_On__c,
                TECO__c,
                Service_Status__c,
                Pressure_Test__c,
                Start_Time__c,
                Stop_Time__c,
                Well_On_Production__c,
                Supervised__c,
                Vendor_Company__c,
                Service_Time_Is_Required__c,
                Vendor_Invoicing_Personnel_Is_Required__c,
                Unit_Number__c,
                SAP_Generated_Work_Order_Number__c,
                Service_Status_Reason__c,
                Action_Steps__c,
                Date_Completed__c,
                Equipment__c,
                Location__c,
                Facility__c
             From HOG_Maintenance_Servicing_Form__c
             Order By Id];

        System.assertEquals(9, updatedRecord.size());

        updatedRecord[0].Work_Details__c = 'Test1'; 
        updatedRecord[0].Service_Status__c = 'Complete'; 
        updatedRecord[0].Well_On_Production__c  = 'Yes'; 

        updatedRecord[1].Work_Details__c = 'Test2'; 
        updatedRecord[1].Service_Status__c = 'New Request'; 
        
        updatedRecord[2].Work_Details__c = 'Test3'; 
        updatedRecord[2].Pressure_Test__c = 'test pressure'; 
        updatedRecord[2].Start_Time__c = Datetime.valueOf('2013-06-19 11:40:00'); 
        updatedRecord[2].Stop_Time__c = Datetime.valueOf('2013-06-19 12:30:00'); 
        updatedRecord[2].Service_Status__c = 'Complete'; 
        updatedRecord[2].Well_On_Production__c  = 'No'; 
        updatedRecord[2].Supervised__c = false; 
        updatedRecord[2].Vendor_Company__c = account.Id;  

        updatedRecord[3].Work_Details__c = 'Test4';
        updatedRecord[3].Pressure_Test__c = 'test pressure'; 
        updatedRecord[3].Start_Time__c = Datetime.valueOf('2013-06-19 11:40:00'); 
        updatedRecord[3].Stop_Time__c = Datetime.valueOf('2013-06-19 12:30:00'); 
        updatedRecord[3].Service_Status__c = 'Complete'; 
        updatedRecord[3].Well_On_Production__c  = 'Yes'; 
        updatedRecord[3].Supervised__c = true; 
        updatedRecord[3].Vendor_Company__c = account.Id;  
         
        updatedRecord[4].Work_Details__c = 'Test5';
        updatedRecord[4].Service_Status__c = 'Requires Reassignment'; 

        updatedRecord[5].Work_Details__c = 'Test6';
        
        updatedRecord[6].Work_Details__c = 'Test7';
        updatedRecord[6].Service_Status__c = 'Cancelled';                 
        
        updatedRecord[7].Work_Details__c = 'Test8';
        updatedRecord[7].Service_Status__c = 'Complete'; 
        updatedRecord[7].Well_On_Production__c  = 'No'; 

        updatedRecord[8].Work_Details__c = 'Test8';
        updatedRecord[8].Service_Status__c = 'Complete'; 
        updatedRecord[8].Well_On_Production__c = 'Yes'; 
        updatedRecord[8].Supervised__c = true; 
        updatedRecord[8].Work_Order_Number__c = 'WRK00000';                

        System.debug('\n*****************************************\n'
            + 'METHOD: HOG_Rigless_Servicing_Form_Trigger()'
            + '\nriglessServicingForm: ' + riglessServicingForm
            + '\nupdatedRecord: ' + updatedRecord
            + '\n************************************************\n');                  

        //Map<Id, HOG_Maintenance_Servicing_Form__c> updatedRecordMap = new Map<Id, HOG_Maintenance_Servicing_Form__c>(updatedRecord);               
        //WellServicingUtilities.TriggerBeforeUpdateWellServicingForm(riglessServicingForm, updatedRecordMap);
        Map<Id, HOG_Maintenance_Servicing_Form__c> updatedRecordMap = new Map<Id, HOG_Maintenance_Servicing_Form__c>(riglessServicingForm);
        WellServicingUtilities.TriggerBeforeUpdateWellServicingForm(updatedRecord, updatedRecordMap);
        WellServicingUtilities.CancelWorkOrderInSAP(updatedRecord[6].Id);    	
    }
	    
    private static void SetupTestData()
	{        
        //-- Begin setup of data needed to test the Service Request Notification controller --//
                
        //-- Setup Service Category
        serviceCategory = ServiceCategoryTestData.createServiceCategory('Test Category');
        insert serviceCategory;
        //--

        //-- Setup Service Code Group
        serviceCodeGroup = ServiceCodeGroupTestData.createServiceCodeGroup('Test Group', true);
        insert serviceCodeGroup;
        //--

        //-- Setup Notification Type
	    notificationType = new List<HOG_Notification_Type__c>();        
 		notificationType.add((HOG_Notification_Type__c)NotificationTypeTestData.createNotificationType(serviceCategory.Id, false, true, true, true));
 		notificationType.add((HOG_Notification_Type__c)NotificationTypeTestData.createNotificationType(serviceCategory.Id, true, false, true, true));
        insert notificationType;
        //--
 
        //-- Setup Service Code MAT        
 	    serviceCodeMAT = ServiceCodeMATTestData.createServiceCodeMAT('Test MAT1', 'TS1', WORKORDERRECORDTYPE[0]);
        insert serviceCodeMAT;
        //--

        //-- Setup Service Required
        serviceRequiredList = ServiceRequiredTestData.createServiceRequired('Test Service Required', true, true, true);
        insert serviceRequiredList;
        //--

        //-- Setup Service User Status
		userStatus = UserStatusTestData.createUserStatus('Test User Status', 'Test Description');
        insert userStatus;
        //--

        //-- Setup Service - Priority
		servicePriorityList = ServicePriorityTestData.createServicePriority('Service Priority 1', 1, '1');
        insert servicePriorityList;
        //--

        //-- Setup Notification Type - Priority Detail
     	notificationTypePriority = new List<HOG_Notification_Type_Priority__c>();
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[0].Id, servicePriorityList.Id, true));                
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[0].Id, servicePriorityList.Id, false));                
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[1].Id, servicePriorityList.Id, true));                
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[1].Id, servicePriorityList.Id, false));                
        insert notificationTypePriority;        
        //--
                
        //-- Setup Work Order Type
 	    workOrderType = new List<HOG_Work_Order_Type__c>();
 	    workOrderType.add(WorkOrderTypeTestData.createWorkOrderType(notificationType[0].Id, serviceCodeMAT.Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[0], true));
 	    workOrderType.add(WorkOrderTypeTestData.createWorkOrderType(notificationType[1].Id, serviceCodeMAT.Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[1], true));
        insert workOrderType;
        //--
                                        
        //-- Setup objects for Well Tracker                                
        businessUnit = new Business_Unit__c(Name = 'Test Business Unit');
        insert businessUnit;

		businessDepartment = BusinessDepartmentTestData.createBusinessDepartment('Test Business Department');    
        insert businessDepartment;

		operatingDistrict = OperatingDistrictTestData.createOperatingDistrict('Test Field', businessDepartment.Id, businessUnit.Id);
        insert operatingDistrict;

        recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;

		field = FieldTestData.createField('Test Field Control Centre', operatingDistrict.Id, recordTypeId);
        insert field;

		route = RouteTestData.createRoute('999');
        insert route;                  

		location = LocationTestData.createLocation('Test Location', route.Id, field.Id);		
        insert location;

		facility = FacilityTestData.createFacility('Test Facility', route.Id, field.Id);
        insert facility;        

        equipment = new Equipment__c
            (
                Name = 'Equipment Test',
                Location__c = location.Id,          
                Catalogue_Code__c = 'CATCODE'
            );                
        insert equipment;

		account = AccountTestData.createAccount('Test Account', null);			
        insert account;

		wellTracker = new List<Well_Tracker__c>();
		wellTracker.add((Well_Tracker__c)WellTrackerTestData.createWellTracker(location.Id, null, field.Id, 1, Datetime.valueOf('2013-06-20 12:00:00'), 'Producing', 'Engineering'));
		wellTracker.add((Well_Tracker__c)WellTrackerTestData.createWellTracker(null, facility.Id, field.Id, 1, Datetime.valueOf('2013-06-20 12:00:00'), 'Producing', 'Engineering'));
		wellTracker.add((Well_Tracker__c)WellTrackerTestData.createWellTracker(location.Id, null, field.Id, 1, Datetime.valueOf('2013-06-20 12:00:00'), 'Well Down', 'Engineering'));
		wellTracker.add((Well_Tracker__c)WellTrackerTestData.createWellTracker(null, facility.Id, field.Id, 1, Datetime.valueOf('2013-06-20 12:00:00'), 'Well Down', 'Engineering'));
        insert wellTracker;
        //--

		serviceRequest = new List<HOG_Service_Request_Notification_Form__c>();

		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[0].Id,
				workOrderType[0].Id,
				businessUnit.Id,
				operatingDistrict.Id,
				field.Id,
				facility.Id,
				location.Id,
				null,
				notificationTypePriority[1].Id,
				notificationTypePriority[0].Id,
				account.Id,
				'999888AA',
				'9999ABCF',
				'Test Detail',
				'Test Description',
				Date.today(),
				Date.today() + 10
			));
			
		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[1].Id,
				workOrderType[1].Id,
				businessUnit.Id,
				operatingDistrict.Id,
				field.Id,
				facility.Id,
				null,
				null,
				notificationTypePriority[3].Id,
				notificationTypePriority[2].Id,
				account.Id,
				'999888BB',
				'9999ABCG',
				'Test Detail',
				'Test Description',
				Date.today(),
				Date.today() + 10
			));

		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[2].Id,
				workOrderType[0].Id,
				businessUnit.Id,
				operatingDistrict.Id,
				field.Id,
				facility.Id,
				location.Id,
				null,
				notificationTypePriority[1].Id,
				notificationTypePriority[0].Id,
				account.Id,
				'999888CC',
				'9999ABCH',
				'Test Detail',
				'Test Description',
				null,
				null
			));
			
		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[3].Id,
				workOrderType[1].Id,
				businessUnit.Id,
				operatingDistrict.Id,
				field.Id,
				facility.Id,
				location.Id,
				null,
				notificationTypePriority[3].Id,
				notificationTypePriority[2].Id,
				account.Id,
				null,
				null,
				'Test Detail',
				'Test Description',
				null,
				null
			));

		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[0].Id,
				workOrderType[0].Id,
				businessUnit.Id,
				operatingDistrict.Id,
				field.Id,
				facility.Id,
				location.Id,
				null,
				notificationTypePriority[1].Id,
				notificationTypePriority[0].Id,
				account.Id,
				'999888EE',
				'9999ABCJ',
				'Test Detail',
				'Test Description',
				Date.today(),
				Date.today() + 10
			));

		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[0].Id,
				workOrderType[0].Id,
				businessUnit.Id,
				operatingDistrict.Id,
				field.Id,
				facility.Id,
				location.Id,
				null,
				notificationTypePriority[1].Id,
				notificationTypePriority[0].Id,
				account.Id,
				'999888FF',
				'9999ABCK',
				'Test Detail',
				'Test Description',
				Date.today(),
				Date.today() + 10
			));

		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[0].Id,
				workOrderType[0].Id,
				businessUnit.Id,
				operatingDistrict.Id,
				field.Id,
				facility.Id,
				location.Id,
				null,
				notificationTypePriority[1].Id,
				notificationTypePriority[0].Id,
				account.Id,
				'999888GG',
				'9999ABCL',
				'Test Detail',
				'Test Description',
				Date.today(),
				Date.today() + 10
			));

		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[0].Id,
				workOrderType[0].Id,
				businessUnit.Id,
				operatingDistrict.Id,
				field.Id,
				facility.Id,
				null,
				null,
				notificationTypePriority[1].Id,
				notificationTypePriority[0].Id,
				account.Id,
				'999888HH',
				'9999ABCM',
				'Test Detail',
				'Test Description',
				Date.today(),
				Date.today() + 10
			));

		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[0].Id,
				workOrderType[0].Id,
				businessUnit.Id,
				operatingDistrict.Id,
				field.Id,
				facility.Id,
				location.Id,
				equipment.Id,
				notificationTypePriority[1].Id,
				notificationTypePriority[0].Id,
				account.Id,
				'999888II',
				'9999ABCN',
				'Test Detail',
				'Test Description',
				Date.today(),
				Date.today() + 10
			));
        //-- End setup of data needed to test the Service Request Notification controller --//

	    ServiceRequestNotificationUtilities.executeTriggerCode = false;
    	WellServicingUtilities.executeTriggerCode = false;
    	MaintenanceServicingUtilities.executeTriggerCode = false;        

        insert serviceRequest;

        //-- Start setup data for Work Order        
        riglessServicingForm = new List<HOG_Maintenance_Servicing_Form__c>();
        
		ServiceRequestNotificationUtilities.ServiceRequest request = new ServiceRequestNotificationUtilities.ServiceRequest(); 

    	request.workOrderName = 'Test Location';		
    	request.workOrderCount = 1;
		request.notificationType = notificationType[0].Id;
        request.serviceTimeIsRequired = false;
		request.vendorInvoicingPersonnelIsRequired = false;
        					
		request.record = serviceRequest[0];

        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[1];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[4];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[5];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[6];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[7];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[8];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[2];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[3];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));

        riglessServicingForm[0].TECO__c = true;
        riglessServicingForm[0].SAP_Generated_Work_Order_Number__c = true;
        riglessServicingForm[0].Unit_Number__c = 'Test';
        riglessServicingForm[0].Service_Time_Is_Required__c = true;
		riglessServicingForm[0].Vendor_Invoicing_Personnel_Is_Required__c = true;        
        riglessServicingForm[1].Work_Order_Number__c = null;        
        riglessServicingForm[1].SAP_Generated_Work_Order_Number__c = true;
        riglessServicingForm[1].Unit_Number__c = 'Test';
        riglessServicingForm[2].Service_Status__c = null;
        riglessServicingForm[2].SAP_Generated_Work_Order_Number__c = true;
        riglessServicingForm[2].Unit_Number__c = 'Test';
        riglessServicingForm[3].Service_Status__c = 'Complete';
        riglessServicingForm[3].Service_Completed__c = System.now();
        riglessServicingForm[3].SAP_Generated_Work_Order_Number__c = true;
        riglessServicingForm[3].Unit_Number__c = 'Test';
        riglessServicingForm[3].Service_Time_Is_Required__c = true;
		riglessServicingForm[3].Vendor_Invoicing_Personnel_Is_Required__c = true;        
        riglessServicingForm[4].SAP_Generated_Work_Order_Number__c = true;
        riglessServicingForm[4].Unit_Number__c = 'Test';
        riglessServicingForm[5].TECO__c = true;
        riglessServicingForm[5].Unit_Number__c = 'Test';
        riglessServicingForm[6].Service_Status__c = 'Cancelled';
        riglessServicingForm[6].Unit_Number__c = 'Test';
        riglessServicingForm[7].Unit_Number__c = 'Test';
        riglessServicingForm[8].Unit_Number__c = 'Test';
        riglessServicingForm[8].Service_Time_Is_Required__c = true;
		riglessServicingForm[8].Vendor_Invoicing_Personnel_Is_Required__c = true;        
		
        insert riglessServicingForm;

		serviceTime = new List<HOG_Service_Time__c>();
		serviceTime.add((HOG_Service_Time__c)ServiceTimeTestData.createServiceTime(riglessServicingForm[0].Id, 'Day 1', Datetime.valueOf('2013-06-19 12:00:00'), Datetime.valueOf('2013-06-20 12:00:00')));
		serviceTime.add((HOG_Service_Time__c)ServiceTimeTestData.createServiceTime(riglessServicingForm[3].Id, 'Day 1', Datetime.valueOf('2013-06-19 12:00:00'), Datetime.valueOf('2013-06-20 12:00:00')));
        insert serviceTime;

		vendorPersonnel = new List<HOG_Vendor_Personnel__c>();
		vendorPersonnel.add((HOG_Vendor_Personnel__c)VendorPersonnelTestData.createVendorPersonnel(riglessServicingForm[0].Id, account.Id, 'Communication', Datetime.valueOf('2013-06-19 12:00:00'), Datetime.valueOf('2013-06-20 12:00:00')));
		vendorPersonnel.add((HOG_Vendor_Personnel__c)VendorPersonnelTestData.createVendorPersonnel(riglessServicingForm[3].Id, account.Id, 'Communication', Datetime.valueOf('2013-06-19 12:00:00'), Datetime.valueOf('2013-06-20 12:00:00')));
        insert vendorPersonnel;        
        //-- End setup data for Work Order        

		settings = HOGSettingsTestData.createHOGSettings();
		insert settings;
	}    
}