@isTest
private class WellWorkpackageHoverControllerTest {

    static testMethod void dotest() {
        
        Milestone1_Project__c wp = new Milestone1_Project__c(Network_Activity_Code__c='123-456;789-012');
        
        insert wp;
        
        Class_C_Budget_Capital__c cp = new Class_C_Budget_Capital__c(MostLikelyCostEst__c = 1,
                                                                     OptimisticCostEst__c = 1,
                                                                     PessimisticCostEst__c = 1,
                                                                     PERTCostEst__c = 1,
                                                                     Project__c = wp.Id,
                                                                     Capital_Cost_Wells_GUID__c = '1234567890');
        
        insert cp;
        
        wp.Current_Budget__c = cp.Id;
        
        update wp;
        
        Test.setCurrentPageReference(new PageReference('Page.WellWorkpackageHover'));
        System.currentPageReference().getParameters().put('id', wp.Id);
        System.currentPageReference().getParameters().put('hoverPosition', ' topLeft');
        
        WellWorkpackageHoverController wwphc = new WellWorkpackageHoverController();
        
        
        
        WCPBudgetController wbc = new WCPBudgetController();
        
//        WCPBudgetHover
    }
}