/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the HOG_Work_Order_Type__c
-------------------------------------------------------------------------------------------------*/
@isTest                
public class WorkOrderTypeTestData
{
    public static HOG_Work_Order_Type__c createWorkOrderType
    (
    	Id notificationTypeId,
    	Id serviceCodeMATId, 
    	Id serviceRequiredId,
    	Id userStatusId,
    	String recordTypeName,
    	Boolean active    	
   	)
    {                
        HOG_Work_Order_Type__c results = new HOG_Work_Order_Type__c
            (
                HOG_Notification_Type__c = notificationTypeId,
                HOG_Service_Code_MAT__c = serviceCodeMATId,
                HOG_Service_Required__c = serviceRequiredId,
                HOG_User_Status__c = userStatusId,
                Record_Type_Name__c = recordTypeName,
                Active__c = active                
            );

        return results;
    }
}